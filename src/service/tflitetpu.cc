/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflitetpu.hh"

#include <tensorflow/lite/kernels/register.h>

#include "edgetpu.h"

using namespace std;

namespace larod {
namespace backendunit {

TFLiteTPU::TFLiteTPU() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Creating TPU context...");

    tpuContext = edgetpu::EdgeTpuManager::GetSingleton()->NewEdgeTpuContext();
    if (!tpuContext) {
        throw runtime_error("Could not initialize TPU");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

TFLiteTPU::~TFLiteTPU() {
    // We own tpuContext and we must make sure that no job is running (and using
    // it) while we destroy it.
    stopProcessingQueues();

    // We must make sure no one uses the models that depend on tpuContext.
    unique_lock<mutex> lock(mtxInterpreters);
    interpreters.clear();
    lock.unlock();
}

unique_ptr<tflite::Interpreter>
    TFLiteTPU::buildInterpreter(const tflite::FlatBufferModel& model) {
    // Check that the model is compiled for EdgeTPU.
    if (model.GetModel()->operator_codes()) {
        bool hasEdgeTPUCustomOp = false;
        for (const auto& opcode : *model.GetModel()->operator_codes()) {
            if (opcode->builtin_code() == tflite::BuiltinOperator_CUSTOM &&
                opcode->custom_code()->str() == "edgetpu-custom-op") {
                hasEdgeTPUCustomOp = true;
                break;
            }
        }
        if (!hasEdgeTPUCustomOp) {
            throw invalid_argument("Model is not compiled for EdgeTPU");
        }
    }

    // Register TPU as a custom op handler.
    tflite::ops::builtin::BuiltinOpResolver resolver;
    resolver.AddCustom(edgetpu::kCustomOp, edgetpu::RegisterCustomOp());

    tflite::InterpreterBuilder builder(model, resolver);
    unique_ptr<tflite::Interpreter> interpreter;
    builder(&interpreter);

    // Bind TPU context.
    if (interpreter) {
        interpreter->SetExternalContext(kTfLiteEdgeTpuContext,
                                        tpuContext.get());
    }

    return interpreter;
}

} // namespace backendunit
} // namespace larod
