/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <vector>

namespace larod {

/**
 * @brief Minimal implementation of std::span (C++20).
 */
template<typename T> class span {
private:
    /**
     * @brief Minimal bidirectional iterator implementation for span.
     */
    class iterator : public std::iterator<std::bidirectional_iterator_tag, T> {
    public:
        iterator(T* p) : ptr(p) {}
        iterator(const iterator& it) : ptr(it.ptr) {}

        iterator& operator++() { // prefix ++
            ++ptr;
            return *this;
        }
        iterator operator++(int) { // postfix ++
            iterator tmp(*this);
            operator++();
            return tmp;
        }
        iterator& operator--() { // prefix --
            --ptr;
            return *this;
        }
        iterator operator--(int) { // postfix --
            iterator tmp(*this);
            operator--();
            return tmp;
        }
        bool operator==(const iterator& rhs) const { return ptr == rhs.ptr; }
        bool operator!=(const iterator& rhs) const { return ptr != rhs.ptr; }
        T& operator*() { return *ptr; }

    private:
        T* ptr;
    };

public:
    constexpr span() noexcept : dataPtr(nullptr), dataSz(0) {}

    constexpr span(T* beg, size_t size) noexcept : dataPtr(beg), dataSz(size) {}
    constexpr span(T* beg, T* end) noexcept : span(beg, end - beg) {}
    constexpr span(std::vector<T>& vec) noexcept
        : span(vec.data(), vec.size()) {}

    constexpr T* data() const noexcept { return dataPtr; }
    constexpr iterator begin() const noexcept { return iterator(dataPtr); }
    constexpr iterator end() const noexcept {
        return iterator(dataPtr + dataSz);
    }
    constexpr size_t size() const noexcept { return dataSz; }

    constexpr T& operator[](size_t i) const noexcept { return dataPtr[i]; }

private:
    T* dataPtr;
    size_t dataSz;
};

} // namespace larod
