/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "jobrequest.hh"

#include <cassert>
#include <cerrno>
#include <unistd.h>

#include "log.hh"
#include "session.hh"

using namespace std;

namespace larod {

JobRequest::JobRequest(weak_ptr<Session> session, const uint64_t modelId,
                       const vector<Tensor>& inputTensors,
                       vector<vector<uint8_t>>&& inputTensorsData,
                       const vector<Tensor>& outTensors,
                       vector<vector<uint8_t>>&& outTensorsData,
                       const vector<FdInfo>& outputFdInfos,
                       const uint8_t priority, const uint64_t metaData,
                       const ParamsMap&& params)
    : AsyncMsg(session), MODEL_ID(modelId), INPUT_TENSORS(inputTensors),
      outputTensors(outTensors), OUTPUT_FD_INFOS(outputFdInfos),
      PRIORITY(priority), META_DATA(metaData), PARAMS(std::move(params)),
      INPUT_TENSORS_DATA(std::move(inputTensorsData)),
      outputTensorsData(std::move(outTensorsData)) {
    // Check number of tensors and corresponding data.
    if (INPUT_TENSORS.size() != INPUT_TENSORS_DATA.size()) {
        throw invalid_argument(
            "Number of input tensors and input tensors data are not equal");
    }

    if (outputTensors.size() != outputTensorsData.size()) {
        throw invalid_argument(
            "Number of output tensors and output tensors data are not equal");
    }

    if (outputTensorsData.size() != OUTPUT_FD_INFOS.size()) {
        throw invalid_argument("Number of output tensors data and output file "
                               "descriptors are not equal");
    }

    // Check output file descriptor infos.
    assert(all_of(OUTPUT_FD_INFOS.begin(), OUTPUT_FD_INFOS.end(),
                  [](const auto& fdInfo) {
                      return get<0>(fdInfo) >= 0 && get<2>(fdInfo) >= 0;
                  }));

    // Check that allocated data is correctly referenced in Tensors.
    for (size_t i = 0; i < inputTensors.size(); ++i) {
        assert(INPUT_TENSORS[i].getData().data() ==
               INPUT_TENSORS_DATA[i].data());
    }

    for (size_t i = 0; i < outputTensors.size(); ++i) {
        assert(outputTensors[i].getData().data() ==
               outputTensorsData[i].data());
    }
}

JobRequest::~JobRequest() {
    for (const auto& fdInfo : OUTPUT_FD_INFOS) {
        if (close(get<0>(fdInfo))) {
            LOG.warning("Could not close output fd (" + to_string(errno) + ")");
        }
    }
}

void JobRequest::signal() {
    if (auto sessionPtr = session.lock()) {
        sessionPtr->sendJobResult(this);
    } else {
        LOG.error("Could not send job result: Session has expired");
    }
}

} // namespace larod
