/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "session.hh"

#include <cassert>
#include <poll.h>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <unistd.h>
#include <utility>

#include "joborder.hh"
#include "log.hh"
#include "message.h"
#include "model.hh"
#include "tensor.h"

using namespace std;

namespace larod {

Session::Session(sdbusplus::bus::bus& bus, const string path,
                 const vector<unique_ptr<BackEndUnit>>& units)
    : stopBusProc(false), hasHandshaked(false), ID(nextId),
      ID_STR(to_string(ID)), UNITS(units) {
    // Create sd-bus interface server.
    sdbusServer = make_unique<SessionSdbusServer>(
        bus, path, bind(&Session::getSessionId, this),
        bind(&Session::getModels, this));

    assert(!UNITS.empty());

    // Create job request priority queues for each unit.
    for (size_t i = 0; i < UNITS.size(); ++i) {
        auto [ignore, ret] = jobReqs.emplace(
            piecewise_construct, forward_as_tuple(i), forward_as_tuple());
        if (!ret) {
            throw runtime_error("Could not create job request priority queues");
        }
    }

    // Initialize list of private models.
    privateModels.resize(UNITS.size());

    // Create asynchronous buses
    int serverReadClientWriteFds[2]; // 0 is our read and 1 is client's write.
    int serverWriteClientReadFds[2]; // 0 is our write and 1 is client's read.
    if (socketpair(AF_UNIX, SOCK_STREAM, 0, serverReadClientWriteFds) ||
        socketpair(AF_UNIX, SOCK_STREAM, 0, serverWriteClientReadFds)) {
        throw runtime_error(string("Could not create socket pairs: ") +
                            strerror(errno));
    }

    clientAsyncWriteFd = serverReadClientWriteFds[1];
    clientAsyncReadFd = serverWriteClientReadFds[1];

    asyncReadBus.reset(createAsyncBus(serverReadClientWriteFds[0]));
    asyncWriteBus.reset(createAsyncBus(serverWriteClientReadFds[0]));

    // Create eventfd for signaling asynchronous read bus processing thread
    busProcEventFd = eventfd(0, 0);
    if (busProcEventFd < 0) {
        throw runtime_error(string("Could not create eventfd: ") +
                            strerror(errno));
    }

    // Start asynchronous read bus processing
    busProcThread = thread(&Session::processAsyncReadBus, this);

    ++nextId;

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed " + ID_STR);
}

Session::~Session() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructing...");

    // Check that bus processing has been stopped.
    assert(busProcEventFd < 0);
    if (busProcEventFd >= 0) {
        LOG.error("Session is destructing without stopping the bus processing");
    }

    // Delete all private models.
    for (size_t i = 0; i < privateModels.size(); ++i) {
        for (auto it = privateModels[i].begin(); it != privateModels[i].end();
             ++it) {
            try {
                UNITS[i]->deleteModel(ID, *it);
                LOG.debug(THIS_NAMESPACE + __func__ +
                          "(): Deleted private model " + to_string(*it) +
                          " on chip " + to_string(i));
            } catch (exception& e) {
                LOG.error(e.what());
            }
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed " + ID_STR);
}

void Session::stopBusProcessing() noexcept {
    if (busProcEventFd < 0) {
        // This is the only place where we close busProcEventFd after the
        // constructor and set it to -1 (from closeFd() below). We can thus use
        // it as a flag to check if we have already called this function before
        // and stopped the bus processing already.
        return;
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopping bus processing...");

    // Delete the sd-bus interface server.
    sdbusServer.reset(nullptr);

    // Stop bus processing thread.
    stopBusProc = true;

    // Trigger the eventfd to release the thread blocking on ppoll().
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Writing to eventfd...");
    uint64_t value = 1;
    ssize_t writtenBytes = write(busProcEventFd, &value, sizeof(value));
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Wrote " +
              to_string(writtenBytes) + " bytes");

    if (static_cast<size_t>(writtenBytes) == sizeof(value)) {
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Joining busProcThread...");
        busProcThread.join();
        LOG.debug(THIS_NAMESPACE + __func__ + "(): busProcThread joined");
    } else {
        // Forcibly destruct thread using pthreads. If thread is joinable
        // std::terminate() will be called upon thread destruction.
        // Therefore, we detach it first.
        busProcThread.detach();
        pthread_cancel(busProcThread.native_handle());
    }

    // Close file descriptors.
    closeFd({busProcEventFd, clientAsyncReadFd, clientAsyncWriteFd});
}

sd_bus* Session::createAsyncBus(int fd) {
    int ret;
    sd_bus* bus;
    if ((ret = sd_bus_new(&bus)) < 0) {
        throw runtime_error(string("Could not create a new bus: ") +
                            strerror(-ret));
    }

    if ((ret = sd_bus_set_fd(bus, fd, fd)) < 0) {
        throw runtime_error(string("Could not set file descriptor for bus: ") +
                            strerror(-ret));
    }

    sd_id128_t id;
    sd_id128_randomize(&id);
    if ((ret = sd_bus_set_server(bus, true, id)) < 0) {
        throw runtime_error(string("Could not set server setting for bus: ") +
                            strerror(-ret));
    }

    if ((ret = sd_bus_set_anonymous(bus, false)) < 0) {
        throw runtime_error(
            string("Could not set anonymous setting for bus: ") +
            strerror(-ret));
    }

    if ((ret = sd_bus_negotiate_fds(bus, true)) < 0) {
        throw runtime_error(
            string("Could not set negotiation setting for bus: ") +
            strerror(-ret));
    }

    if ((ret = sd_bus_start(bus)) < 0) {
        throw runtime_error(string("Could not start bus: ") + strerror(-ret));
    }

    return bus;
}

void Session::queueLoadModel(const int fd, const uint64_t sz,
                             const size_t chipId, const int32_t access,
                             const string name, const uint64_t metaData,
                             const BackEndUnit::ParamsMap&& params) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": chipId = " + to_string(chipId));
    Model::Access modelAccess(static_cast<Model::Access>(access));
    if (modelAccess != Model::Access::PRIVATE &&
        modelAccess != Model::Access::PUBLIC) {
        throw invalid_argument("Invalid model access specifier");
    }

    if (chipId >= UNITS.size()) {
        throw invalid_argument("Invalid chip ID");
    }

    if (sz > SIZE_MAX) {
        string errMsg =
            to_string(sz) + " bytes is larger than " + to_string(SIZE_MAX);
        throw TypeSizeOverflow(errMsg);
    }
    size_t size = static_cast<size_t>(sz);

    if (!name.empty()) {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model \"" + name +
                  "\"...");
    } else {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model...");
    }

    vector<uint8_t> buf;
    try {
        buf.resize(size);
    } catch (bad_alloc& e) {
        throw runtime_error("Could not resize model vector to " +
                            to_string(size) + " bytes: " + e.what());
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Reading model file...");
    readFromFd(fd, 0, buf);
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Model file read");

    auto loadModelReq = make_shared<LoadModelRequest>(
        weak_from_this(), UNITS[chipId].get(), ID, chipId, std::move(buf),
        modelAccess, name, metaData, std::move(params));
    UNITS[chipId]->queueLoadModel(loadModelReq);

    if (!name.empty()) {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model \"" + name +
                  "\" done");
    } else {
        LOG.debug("Session " + ID_STR + ": Queuing loading of model done");
    }
}

void Session::deleteModel(uint64_t modelId, const size_t chipId) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Deleting model...");

    UNITS[chipId]->deleteModel(ID, modelId);

    // Might need to clear from the Session's collection of private models as
    // well. This call works since all model IDs are unique and does nothing if
    // it was not found in the collection.
    {
        lock_guard<mutex> lockGuard(mtxPrivateModels);
        privateModels[chipId].erase(modelId);
    }

    LOG.debug("Session " + ID_STR + ": Model " + to_string(modelId) +
              " deleted");
}

void Session::queueJob(const uint64_t modelId, const size_t chipId,
                       vector<Tensor>&& inputTensors,
                       vector<vector<uint8_t>>&& inputTensorsData,
                       vector<Tensor>&& outputTensors,
                       vector<JobRequest::FdInfo>& outputFdInfos,
                       const uint8_t priority, const uint64_t metaData,
                       const BackEndUnit::ParamsMap&& params) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Queuing job on model " + to_string(modelId) + "...");

    assert(outputTensors.size() == outputFdInfos.size());

    // Allocate data for output tensors.
    vector<size_t> outputSizes = UNITS[chipId]->getOutputByteSizes(modelId);
    assert(outputFdInfos.size() == outputSizes.size());
    vector<vector<uint8_t>> outputTensorsData(outputSizes.size());
    for (size_t i = 0; i < outputSizes.size(); ++i) {
        try {
            outputTensorsData[i].resize(outputSizes[i]);
        } catch (bad_alloc& e) {
            throw runtime_error("Could not resize output vector to " +
                                to_string(outputSizes[i]) +
                                " bytes: " + e.what());
        }

        outputTensors[i].setData(outputTensorsData[i]);
    }

    auto jobReq = make_shared<JobRequest>(
        weak_from_this(), modelId, inputTensors, std::move(inputTensorsData),
        outputTensors, std::move(outputTensorsData), outputFdInfos, priority,
        metaData, std::move(params));

    // The job request now has ownership of outputFdInfos, thus the caller
    // should not do any fd cleanup. We set the reference to caller's fds to -1
    // to mark that it has lost ownership so any clean-up-on-error code (e.g. in
    // Session) checking for fd > 0 will not try to release it.
    for (auto& fdInfo : outputFdInfos) {
        get<0>(fdInfo) = -1;
    }

    try {
        // Add job request to relevant priority job queue.
        lock_guard<mutex> lockGuard(mtxJobReqs);
        jobReqs.at(chipId).push(jobReq);
    } catch (out_of_range& e) {
        // Should never happen...
        throw runtime_error("Could not find chip with index " +
                            to_string(chipId));
    }

    auto jobOrder = make_shared<JobOrder>(weak_from_this(), chipId);
    UNITS[chipId]->queueJob(jobOrder);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Queued job request on model " + to_string(modelId));
}

Session::TensorMetaData Session::createTensorMsg(const Tensor& tensor) {
    vector<size_t> dims(tensor.getDims());
    vector<size_t> pitches(tensor.getPitches());

    // FIXME: Assuming SIZE_MAX <= UINT64_MAX always holds.
    return {tensor.getDataType(),
            tensor.getLayout(),
            static_cast<uint64_t>(tensor.getByteSize()),
            vector<uint64_t>(dims.begin(), dims.end()),
            vector<uint64_t>(pitches.begin(), pitches.end()),
            tensor.getName()};
}

vector<Session::TensorMetaData>
    Session::createTensorMsg(const vector<Tensor>& tensors) {
    vector<TensorMetaData> msgs;
    for (const Tensor& tensor : tensors) {
        msgs.push_back(createTensorMsg(tensor));
    }

    return msgs;
}

tuple<vector<Tensor>, vector<vector<uint8_t>>, vector<Tensor>,
      vector<JobRequest::FdInfo>>
    Session::readTensorMsg(RunJobTensorsMsg inputsMsg,
                           RunJobTensorsMsg outputsMsg) {
    if (inputsMsg.empty()) {
        throw invalid_argument("No input tensors provided");
    }

    if (outputsMsg.empty()) {
        throw invalid_argument("No output tensors provided");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Reading input tensors...");

    // Read input tensors.
    vector<Tensor> inputTensors;
    vector<vector<uint8_t>> inputTensorsData(inputsMsg.size());
    for (size_t i = 0; i < inputsMsg.size(); ++i) {
        const tuple<int32_t /* data type */, int32_t /* layout */,
                    vector<uint64_t> /* dims */, vector<uint64_t> /* pitches */,
                    sdbusplus::message::unix_fd, uint64_t /* size */,
                    int64_t /* offset */>& inputTensorMsg = inputsMsg[i];

        if (get<5>(inputTensorMsg) > numeric_limits<size_t>::max()) {
            throw TypeSizeOverflow("Input tensors byte size is too large (" +
                                   to_string(get<5>(inputTensorMsg)) + ")");
        }
        const size_t dataSz = static_cast<size_t>(get<5>(inputTensorMsg));

        try {
            inputTensorsData[i].resize(dataSz);
        } catch (bad_alloc& e) {
            throw runtime_error("Could not resize input vector to " +
                                to_string(dataSz) + " bytes: " + e.what());
        }

        LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
                  ": Reading input file for tensor " + to_string(i) + "...");
        readFromFd(get<4>(inputTensorMsg), get<6>(inputTensorMsg),
                   inputTensorsData[i]);
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
                  ": Input file for tensor " + to_string(i) + " read");

        try {
            Tensor tensor(get<0>(inputTensorMsg), get<1>(inputTensorMsg),
                          get<2>(inputTensorMsg), get<3>(inputTensorMsg),
                          dataSz, span<uint8_t>(inputTensorsData[i]));
            inputTensors.push_back(std::move(tensor));
        } catch (invalid_argument& e) {
            throw invalid_argument(string("Could not parse input tensors: ") +
                                   e.what());
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Input tensors read");
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Reading output tensors...");

    // Read output tensors.
    vector<Tensor> outputTensors;
    vector<JobRequest::FdInfo> outputFdInfos;
    for (const auto& outputTensorMsg : outputsMsg) {
        try {
            Tensor tensor(get<0>(outputTensorMsg), get<1>(outputTensorMsg),
                          get<2>(outputTensorMsg), get<3>(outputTensorMsg));
            outputTensors.push_back(std::move(tensor));
        } catch (invalid_argument& e) {
            throw invalid_argument(string("Could not parse output tensors: ") +
                                   e.what());
        }

        // Copy file descriptors since sd-bus closes them when the message is
        // freed. These ones will be closed in sendJobResult(), after
        // output has been written.
        int outFd = dup(get<4>(outputTensorMsg));
        if (outFd < 0) {
            throw FileDescriptorError(
                string("Could not duplicate file descriptor: ") +
                strerror(errno));
        }
        if (get<5>(outputTensorMsg) > numeric_limits<size_t>::max()) {
            throw TypeSizeOverflow("Output tensors byte size is too large (" +
                                   to_string(get<5>(outputTensorMsg)) + ")");
        }
        const size_t dataSz = static_cast<size_t>(get<5>(outputTensorMsg));

        outputFdInfos.emplace_back(outFd, dataSz, get<6>(outputTensorMsg));
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Session " + ID_STR +
              ": Output tensors read");

    return {
        std::move(inputTensors),
        std::move(inputTensorsData),
        std::move(outputTensors),
        std::move(outputFdInfos),
    };
}

void Session::readFromFd(const int fd, const int64_t offset,
                         const span<uint8_t>& buf) {
    if (lseek(fd, offset, SEEK_SET) == -1) {
        throw FileDescriptorError(
            string("Could not go to file descriptor offset ") +
            to_string(offset) + ": " + strerror(errno));
    }

    size_t totalBytes = 0;
    ssize_t readBytes = 0;
    while (totalBytes != buf.size()) {
        readBytes = read(fd, buf.data() + totalBytes, buf.size() - totalBytes);
        if (readBytes == 0) {
            // EOF
            break;
        } else if (readBytes < 0) {
            // TODO: Handle EINTR, EAGAIN and EWOULDBLOCK?
            throw FileDescriptorError(
                string("Could not read from file descriptor: ") +
                strerror(errno));
        }

        totalBytes += static_cast<size_t>(readBytes);

        LOG.debug(THIS_NAMESPACE + __func__ + "(): Read " +
                  to_string(readBytes) + " bytes");
    }

    if (totalBytes != buf.size()) {
        throw FileDescriptorError(
            string("Could only read ") + to_string(totalBytes) + " out of " +
            to_string(buf.size()) + " bytes from file descriptor");
    }
}

void Session::writeToFd(const int fd, const size_t maxFdSize,
                        const int64_t offset, const span<uint8_t>& buf) {
    // Verify that the size of the output we want to write to the fd is not
    // larger that than what the client has allowed us to.
    if (maxFdSize > 0 && buf.size() > maxFdSize) {
        throw FileDescriptorError("Output buffer too small (" +
                                  to_string(maxFdSize) + " < " +
                                  to_string(buf.size()) + ")");
    }

    if (lseek(fd, offset, SEEK_SET) == -1) {
        throw FileDescriptorError(
            string("Could not go to file descriptor offset ") +
            to_string(offset) + ": " + strerror(errno));
    }

    size_t totalBytes = 0;
    ssize_t writtenBytes = 0;
    while (totalBytes != buf.size()) {
        writtenBytes =
            write(fd, buf.data() + totalBytes, buf.size() - totalBytes);
        if (writtenBytes == 0) {
            // EOF
            break;
        } else if (writtenBytes < 0) {
            // TODO: Handle EINTR, EAGAIN and EWOULDBLOCK?
            throw FileDescriptorError(
                string("Could not write to file descriptor: ") +
                strerror(errno));
        }

        totalBytes += static_cast<size_t>(writtenBytes);

        LOG.debug(THIS_NAMESPACE + __func__ + "(): Wrote " +
                  to_string(writtenBytes) + " bytes");
    }

    if (totalBytes != buf.size()) {
        throw FileDescriptorError(
            string("Could only write ") + to_string(totalBytes) + " out of " +
            to_string(buf.size()) + " bytes to file descriptor");
    }
}

template<typename T> void Session::closeFd(T& fd) {
    static_assert(is_integral<T>::value || is_convertible<T, int>::value,
                  "Fd must be an integral type or convertible to int");

    if (fd < 0) {
        return;
    }

    if (close(fd) < 0) {
        LOG.warning(string("Could not close file descriptor: ") +
                    strerror(errno));
    }

    fd = -1;
}

template<typename T> void Session::closeFd(vector<reference_wrapper<T>>& fds) {
    for (T& fd : fds) {
        closeFd(fd);
    }
}

void Session::closeFd(vector<reference_wrapper<int>>&& fds) {
    for (int& fd : fds) {
        closeFd(fd);
    }
}

void Session::closeFdInfo(vector<JobRequest::FdInfo>& fdInfos) {
    for (auto& fdInfo : fdInfos) {
        closeFd(get<0>(fdInfo));
    }
}

uint64_t Session::getNextId() {
    return nextId;
}

pair<int, int> Session::getClientAsyncFds() const {
    return {clientAsyncReadFd, clientAsyncWriteFd};
}

uint64_t Session::getSessionId() const {
    return ID;
}

Session::ModelsRetType Session::getModels() const {
    Session::ModelsRetType ret;
    for (size_t chipId = 0; chipId < UNITS.size(); ++chipId) {
        unordered_map<uint64_t, Model> models = UNITS[chipId]->getModels();
        for (const auto& model : models) {
            auto [inputTensors, outputTensors] = model.second.getTensors();
            ret.emplace_back(model.second.getSize(), model.second.getId(),
                             static_cast<uint64_t>(chipId),
                             static_cast<int32_t>(UNITS[chipId]->getChip()),
                             static_cast<int32_t>(model.second.getAccessType()),
                             model.second.getName(),
                             createTensorMsg(inputTensors),
                             createTensorMsg(outputTensors));

            LOG.debug(
                THIS_NAMESPACE + __func__ + "(): Session " + ID_STR + ": " +
                model.second.getName() + ", " +
                to_string(model.second.getId()) + ", " + to_string(chipId) +
                ", " + to_string(model.second.getSize()) + ", " +
                to_string(static_cast<int>(model.second.getAccessType())));
        }
    }

    return ret;
}

void Session::procHandshakeMsg(sd_bus_message* msg) {
    string clientName;
    try {
        sdbusplus::message::message(msg).read(clientName);
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    // We need to respond to the handshake in order for the bus to complete the
    // authentication phase. If we respond later (after BUS_AUTH_TIMEOUT) we
    // would get a ETIMEDOUT.
    sendMsg(LAROD_MSG_HANDSHAKE, ID);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Handshaked with peer " +
              clientName);

    // Close our copies of the client's fds.
    closeFd({clientAsyncReadFd, clientAsyncWriteFd});

    hasHandshaked = true;
}

void Session::procQueueLoadModelMsg(sd_bus_message* sdbusMsg) {
    sdbusplus::message::unix_fd msgFd = -1;
    uint64_t sz = 0;
    uint64_t chipId = 0;
    int32_t access = 0;
    std::string name;
    uint64_t metaData = 0;
    BackEndUnit::ParamsMap params;
    sdbusplus::message::message msg(sdbusMsg);
    try {
        msg.read(msgFd, sz, chipId, access, name, metaData);
        if (containsDict(msg)) {
            msg.read(params);
        }
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    if (chipId > UNITS.size()) {
        string errMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR + ": Could not load model: " + errMsg);
        sendLoadModelResultError(LAROD_ERROR_INVALID_CHIP_ID, errMsg, metaData);

        return;
    }

    // Copy file descriptor since sd-bus closes them when the message is freed.
    int fd = dup(msgFd);
    if (fd < 0) {
        string errMsg = "Could not duplicate file descriptor";
        string errnoMsg = strerror(errno);
        LOG.error("Session " + ID_STR + ": " + errMsg +
                  " for incoming message: " + errnoMsg);
        sendLoadModelResultError(LAROD_ERROR_FD, errMsg + ": " + errnoMsg,
                                 metaData);
        return;
    }

    try {
        queueLoadModel(fd, sz, static_cast<size_t>(chipId), access, name,
                       metaData, std::move(params));
    } catch (FileDescriptorError& e) {
        LOG.error("Session " + ID_STR + ": Could not load model: " + e.what());
        sendLoadModelResultError(LAROD_ERROR_FD, e.what(), metaData);
    } catch (TypeSizeOverflow& e) {
        LOG.error("Session " + ID_STR + ": Could not load model: " + e.what());
        sendLoadModelResultError(static_cast<larodErrorCode>(EOVERFLOW),
                                 e.what(), metaData);
    } catch (exception& e) {
        LOG.error("Session " + ID_STR + ": Could not load model: " + e.what());
        sendLoadModelResultError(LAROD_ERROR_LOAD_MODEL, e.what(), metaData);
    }

    closeFd(fd);
}

void Session::procDeleteModelMsg(sd_bus_message* msg) {
    uint64_t modelID = 0;
    uint64_t chipID = 0;
    uint64_t callbackPtr = 0;
    try {
        sdbusplus::message::message(msg).read(modelID, chipID, callbackPtr);
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    int32_t errorCode = LAROD_ERROR_NONE;
    string errorMsg;
    if (chipID > UNITS.size()) {
        errorMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR +
                  ": Could not delete model: " + errorMsg);

        errorCode = LAROD_ERROR_DELETE_MODEL;
        try {
            lock_guard<recursive_mutex> lockGuard(mtxSend);
            sendMsg(LAROD_MSG_DELETE_MODEL, callbackPtr, errorCode, errorMsg);
        } catch (ConnectionError& e) {
            // Client unexpectedly disconnected. We just ignore it (it will be
            // handled in read bus process thread).
        }

        return;
    }

    try {
        deleteModel(modelID, static_cast<size_t>(chipID));
    } catch (BackEndUnit::ModelNotFound& e) {
        errorCode = LAROD_ERROR_MODEL_NOT_FOUND;
        errorMsg = string("Could not delete model: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errorMsg);
    } catch (BackEndUnit::PermissionDenied& e) {
        errorCode = LAROD_ERROR_PERMISSION;
        errorMsg = string("Could not delete model: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errorMsg);
    } catch (exception& e) {
        errorCode = LAROD_ERROR_DELETE_MODEL;
        errorMsg = string("Could not delete model: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errorMsg);
    }

    try {
        lock_guard<recursive_mutex> lockGuard(mtxSend);
        sendMsg(LAROD_MSG_DELETE_MODEL, callbackPtr, errorCode, errorMsg);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::procQueueJobMsg(sd_bus_message* sdbusMsg) {
    uint64_t modelId;
    uint64_t chipId;
    uint8_t priority;
    uint64_t metaData;
    RunJobTensorsMsg inputsMsg;
    RunJobTensorsMsg outputsMsg;
    BackEndUnit::ParamsMap params;
    sdbusplus::message::message msg(sdbusMsg);
    try {
        msg.read(modelId, chipId, priority, metaData, inputsMsg, outputsMsg);
        if (containsDict(msg)) {
            msg.read(params);
        }
    } catch (sdbusplus::exception::SdBusError& e) {
        LOG.error("Session " + ID_STR +
                  ": Could not read message: " + e.what());

        return;
    }

    if (chipId > UNITS.size()) {
        string errMsg = "Chip ID is larger than number of units";
        LOG.error("Session " + ID_STR + ": Could not run job: " + errMsg);
        sendLoadModelResultError(LAROD_ERROR_INVALID_CHIP_ID, errMsg, metaData);

        return;
    }

    vector<JobRequest::FdInfo> outputFdInfos;
    try {
        vector<Tensor> inputTensors;
        vector<vector<uint8_t>> inputTensorsData;
        vector<Tensor> outputTensors;
        tie(inputTensors, inputTensorsData, outputTensors, outputFdInfos) =
            readTensorMsg(inputsMsg, outputsMsg);
        queueJob(modelId, static_cast<size_t>(chipId), std::move(inputTensors),
                 std::move(inputTensorsData), std::move(outputTensors),
                 outputFdInfos, priority, metaData, std::move(params));
    } catch (FileDescriptorError& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + e.what());
        closeFdInfo(outputFdInfos);
        sendJobResultError(LAROD_ERROR_FD, errMsg, metaData);
    } catch (TypeSizeOverflow& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + e.what());
        closeFdInfo(outputFdInfos);
        sendJobResultError(static_cast<larodErrorCode>(EOVERFLOW), errMsg,
                           metaData);
    } catch (BackEndUnit::ModelNotFound& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_MODEL_NOT_FOUND, errMsg, metaData);
    } catch (exception& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + e.what());
        closeFdInfo(outputFdInfos);
        sendJobResultError(LAROD_ERROR_JOB, errMsg, metaData);
    }
}

void Session::processAsyncReadBus() noexcept {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    // Initialize inputs to ppoll().
    static constexpr int NUM_FD = 2;
    struct pollfd pfd[NUM_FD] = {};
    pfd[0].fd = sd_bus_get_fd(asyncReadBus.get());
    pfd[1].fd = busProcEventFd;
    pfd[0].events = pfd[1].events = POLLIN | POLLPRI | POLLRDHUP | POLLERR;

    sd_bus_message* msg = nullptr;
    int ret;
    while (!stopBusProc) {
        ret = sd_bus_process(asyncReadBus.get(), &msg);
        if (ret < 0) {
            LOG.warning("Session " + ID_STR +
                        ": Could not process incoming asynchronous requests: " +
                        strerror(-ret));

            msg = nullptr;
            stopBusProc = true;
        }

        if (msg) {
            string msgMember = sd_bus_message_get_member(msg);
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Got message: " + msgMember);

            if (msgMember == LAROD_MSG_HANDSHAKE && !hasHandshaked) {
                procHandshakeMsg(msg);
            } else if (msgMember == LAROD_MSG_QUEUE_LOAD_MODEL &&
                       hasHandshaked) {
                procQueueLoadModelMsg(msg);
            } else if (msgMember == LAROD_MSG_QUEUE_JOB && hasHandshaked) {
                procQueueJobMsg(msg);
            } else if (msgMember == LAROD_MSG_DELETE_MODEL && hasHandshaked) {
                procDeleteModelMsg(msg);
            } else if (msgMember == LAROD_MSG_DISCONNECTED) {
                // This message is sent (by sd-bus itself) when connection is
                // reset by peer.
                stopBusProc = true;
            } else {
                LOG.error("Session " + ID_STR + ": Invalid message " +
                          msgMember + " from client");
            }
        }

        msg = sd_bus_message_unref(msg);

        if (ret == 0) {
            // No new messages, wait for some on the bus.
            pfd[0].revents = pfd[1].revents = 0;
            int pollRet = ppoll(pfd, NUM_FD, NULL, NULL);

            if (pollRet < 0) {
                LOG.error(
                    "Session " + ID_STR +
                    ": Could not process incoming asynchronous requests: " +
                    strerror(errno));
                stopBusProc = true;
            } else if (pfd[1].revents != 0) {
                // Something triggered the eventfd. Stop the processing.
                stopBusProc = true;
            }
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

shared_ptr<JobRequest> Session::popJobRequest(const size_t chipId) {
    lock_guard<mutex> lockGuard(mtxJobReqs);

    auto& queue = jobReqs.at(chipId);
    auto jobReq = queue.top();
    queue.pop();

    return jobReq;
}

template<typename... Args>
void Session::sendMsg(const string& msgType, Args&&... data) {
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Sending message: msgType: " + msgType);

    sd_bus_message* tmp;
    int ret = sd_bus_message_new_method_call(
        asyncWriteBus.get(), &tmp, "com.axis.Larod1", "/", "com.axis.Larod1",
        msgType.c_str());
    if (ret < 0) {
        handleSendMsgError(ret, "Could not allocate method call");
    }

    unique_ptr<sd_bus_message, decltype(msgDeleter)*> msg{tmp,
                                                          Session::msgDeleter};
    sdbusplus::message::message(msg.get()).append(std::forward<Args>(data)...);

    ret = sd_bus_send(asyncWriteBus.get(), msg.get(), NULL);
    if (ret < 0) {
        handleSendMsgError(ret, "Could not send message");
    }

    ret = sd_bus_flush(asyncWriteBus.get());
    if (ret < 0) {
        handleSendMsgError(ret, "Could not flush bus");
    }
}

template<>
void Session::sendMsg(const string& msgType, const LoadModelRequest& req) {
    assert(msgType == LAROD_MSG_QUEUE_LOAD_MODEL);

    Session::LoadModelRetType msg;
    auto [inputTensors, outputTensors] = req.getTensors();

    // Fill model metadata.
    assert(req.CHIP_ID < UNITS.size());
    get<0>(msg) = {req.DATA.size(),
                   req.getModelId(),
                   req.CHIP_ID,
                   static_cast<int32_t>(
                       UNITS[static_cast<size_t>(req.CHIP_ID)]->getChip()),
                   static_cast<int32_t>(req.ACCESS),
                   req.NAME,
                   createTensorMsg(inputTensors),
                   createTensorMsg(outputTensors)};

    // Fill return information.
    get<1>(msg) = req.META_DATA;
    get<2>(msg) = LAROD_ERROR_NONE;
    get<3>(msg) = "";

    sendMsg(msgType, std::move(msg));
}

void Session::sendLoadModelResult(const LoadModelRequest* req) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    // Check for error.
    exception_ptr eptr = req->getException();
    try {
        if (eptr) {
            rethrow_exception(eptr);
        }
    } catch (exception& e) {
        LOG.error("Session " + ID_STR + ": " + e.what());
        sendLoadModelResultError(LAROD_ERROR_LOAD_MODEL, e.what(),
                                 req->META_DATA);
        return;
    }

    // Update private models.
    if (req->ACCESS == Model::Access::PRIVATE) {
        lock_guard<mutex> lockGuard(mtxPrivateModels);
        size_t unitId = static_cast<size_t>(req->CHIP_ID);
        bool ret = privateModels[unitId].insert(req->getModelId()).second;
        if (!ret) {
            // Should never happen...
            throw runtime_error("Session " + ID_STR +
                                ": Could not insert private model " +
                                to_string(req->getModelId()) + " into set");
        }
    }

    if (!req->NAME.empty()) {
        LOG.debug("Session " + ID_STR + ": Model \"" + req->NAME + "\" loaded");
    } else {
        LOG.debug("Session " + ID_STR + ": Model loaded");
    }

    try {
        sendMsg(LAROD_MSG_QUEUE_LOAD_MODEL, *req);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::sendLoadModelResultError(const larodErrorCode error,
                                       const string& errorMsg,
                                       const uint64_t metaData) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    // Fill return information.
    LoadModelRetType msg;
    get<1>(msg) = metaData;
    get<2>(msg) = error;
    get<3>(msg) = errorMsg;

    try {
        sendMsg(LAROD_MSG_QUEUE_LOAD_MODEL, std::move(msg));
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in sd_bus_process() in read bus process thread).
    }
}

void Session::sendJobResult(JobRequest* req) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    // Check for error.
    exception_ptr eptr = req->getException();
    try {
        if (eptr) {
            rethrow_exception(eptr);
        }
    } catch (BackEndUnit::ModelNotFound& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_MODEL_NOT_FOUND, errMsg, req->META_DATA);
        return;
    } catch (BackEndUnit::PermissionDenied& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_PERMISSION, errMsg, req->META_DATA);
        return;
    } catch (exception& e) {
        string errMsg = string("Could not run job: ") + e.what();
        LOG.error("Session " + ID_STR + ": " + errMsg);
        sendJobResultError(LAROD_ERROR_JOB, errMsg, req->META_DATA);
        return;
    }

    // Write job result to output file descriptors.
    assert(req->OUTPUT_FD_INFOS.size() == req->outputTensors.size());
    for (size_t i = 0; i < req->OUTPUT_FD_INFOS.size(); ++i) {
        try {
            apply(&Session::writeToFd,
                  tuple_cat(req->OUTPUT_FD_INFOS[i],
                            forward_as_tuple(req->outputTensors[i].getData())));
        } catch (exception& e) {
            string errMsg = string("Could not run job: ") + e.what();
            LOG.error("Session " + ID_STR + ": " + errMsg);
            sendJobResultError(LAROD_ERROR_JOB, errMsg, req->META_DATA);
            return;
        }
    }

    try {
        sendMsg(LAROD_MSG_QUEUE_JOB, req->META_DATA,
                static_cast<int32_t>(LAROD_ERROR_NONE), "");
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::sendJobResultError(const larodErrorCode error,
                                 const string& errorMsg,
                                 const uint64_t metaData) {
    lock_guard<recursive_mutex> lockGuard(mtxSend);

    try {
        sendMsg(LAROD_MSG_QUEUE_JOB, metaData, static_cast<int32_t>(error),
                errorMsg);
    } catch (ConnectionError& e) {
        // Client unexpectedly disconnected. We just ignore it (it will be
        // handled in read bus process thread).
    }
}

void Session::handleSendMsgError(const int retCode, const string& msg) {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): " + msg + ": " +
              strerror(-retCode));
    if (-retCode == ECONNRESET || -retCode == ENOTCONN) {
        throw ConnectionError(strerror(-retCode));
    }

    throw runtime_error("Could not send message");
}

} // namespace larod
