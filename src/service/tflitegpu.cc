/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflitegpu.hh"

using namespace std;

namespace {

string eglErrorMessage(EGLint err) {
    switch (err) {
    case EGL_SUCCESS:
        return "No error";
    case EGL_NOT_INITIALIZED:
        return "EGL_NOT_INITIALIZED";
    case EGL_BAD_ACCESS:
        return "EGL_BAD_ACCESS";
    case EGL_BAD_ALLOC:
        return "EGL_BAD_ALLOC";
    case EGL_BAD_ATTRIBUTE:
        return "EGL_BAD_ATTRIBUTE";
    case EGL_BAD_CONTEXT:
        return "EGL_BAD_CONTEXT";
    case EGL_BAD_CONFIG:
        return "EGL_BAD_CONFIG";
    case EGL_BAD_CURRENT_SURFACE:
        return "EGL_BAD_CURRENT_SURFACE";
    case EGL_BAD_DISPLAY:
        return "EGL_BAD_DISPLAY";
    case EGL_BAD_SURFACE:
        return "EGL_BAD_SURFACE";
    case EGL_BAD_MATCH:
        return "EGL_BAD_MATCH";
    case EGL_BAD_PARAMETER:
        return "EGL_BAD_PARAMETER";
    case EGL_BAD_NATIVE_PIXMAP:
        return "EGL_BAD_NATIVE_PIXMAP";
    case EGL_BAD_NATIVE_WINDOW:
        return "EGL_BAD_NATIVE_WINDOW";
    case EGL_CONTEXT_LOST:
        return "EGL_CONTEXT_LOST";
    default:
        return "Unknown error";
    }
}

} // namespace

namespace larod {
namespace backendunit {

TFLiteGPU::EGLDisplayWrapper::EGLDisplayWrapper() {
    display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (display == EGL_NO_DISPLAY) {
        EGLint error = eglGetError();
        throw runtime_error("Failed to get default display: " +
                            eglErrorMessage(error));
    }
}

TFLiteGPU::EGLDisplayWrapper::~EGLDisplayWrapper() {
    eglTerminate(display);
}

TFLiteGPU::EGLContextWrapper::EGLContextWrapper() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Creating GPU context...");

    EGLint major = 0;
    EGLint minor = 0;
    EGLBoolean ret = eglInitialize(displayWrapper.display, &major, &minor);
    if (ret != EGL_TRUE) {
        throw runtime_error("Failed to initialize display: " +
                            eglErrorMessage(eglGetError()));
    }
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Initialized OpenGL ES v" +
              to_string(major) + "." + to_string(minor));

    EGLConfig config = nullptr;
    EGLint numConfig = 0;
    EGLint configSize = 1;
    ret = eglChooseConfig(displayWrapper.display, ATTR_LIST, &config,
                          configSize, &numConfig);
    if (ret != EGL_TRUE) {
        throw runtime_error("Failed to choose config: " +
                            eglErrorMessage(eglGetError()));
    }

    context = eglCreateContext(displayWrapper.display, config, EGL_NO_CONTEXT,
                               CONTEXT_ATTRS);
    if (context == EGL_NO_CONTEXT) {
        throw runtime_error("Failed creating context: " +
                            eglErrorMessage(eglGetError()));
    }
}

TFLiteGPU::EGLContextWrapper::~EGLContextWrapper() {
    eglDestroyContext(displayWrapper.display, context);
}

TFLiteGPU::TFLiteGPU() {
    bindEGLContext();

    TfLiteGpuDelegateOptions options = TfLiteGpuDelegateOptionsDefault();
    options.metadata = nullptr;
    options.compile_options.precision_loss_allowed = ALLOW_FP16;
    options.compile_options.preferred_gl_object_type =
        TFLITE_GL_OBJECT_TYPE_FASTEST;
    options.compile_options.dynamic_batch_enabled = 0;
    options.compile_options.inline_parameters = 0;

    auto* delegatePtr = TfLiteGpuDelegateCreate(&options);
    delegate.reset(delegatePtr);

    unbindEGLContext();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

TFLiteGPU::~TFLiteGPU() {
    // We own EGL context and we must make sure that no job is running (and
    // using it) while we destroy it.
    stopProcessingQueues();

    // We must make sure no one uses the models that depend on the delegate.
    unique_lock<mutex> lock(mtxInterpreters);
    interpreters.clear();
    lock.unlock();
}

unique_ptr<tflite::Interpreter>
    TFLiteGPU::buildInterpreter(const tflite::FlatBufferModel& model) {
    bindEGLContext();

    tflite::ops::builtin::BuiltinOpResolver resolver;
    unique_ptr<tflite::Interpreter> interpreter;
    tflite::InterpreterBuilder(model, resolver)(&interpreter);

    if (interpreter->ModifyGraphWithDelegate(delegate.get()) != kTfLiteOk) {
        throw runtime_error("Failed to modify graph with GPU Delegate");
    }

    unbindEGLContext();

    return interpreter;
}

// Overload our runJobVirtual, because we need to set the eglContext
// for the gpu delegate to work...
void TFLiteGPU::runJobVirtual(const uint64_t modelId,
                              const vector<Tensor>& inputTensors,
                              vector<Tensor>& outputTensors,
                              const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    bindEGLContext();
    TFLite::runJobVirtual(modelId, inputTensors, outputTensors, params);
    unbindEGLContext();
}

void TFLiteGPU::bindEGLContext() const {
    EGLBoolean ret =
        eglMakeCurrent(display(), EGL_NO_SURFACE, EGL_NO_SURFACE, context());
    if (ret != EGL_TRUE) {
        throw runtime_error("Failed to bind OpenGL ES context: " +
                            eglErrorMessage(eglGetError()));
    }
}

void TFLiteGPU::unbindEGLContext() const {
    EGLBoolean ret = eglMakeCurrent(display(), EGL_NO_SURFACE, EGL_NO_SURFACE,
                                    EGL_NO_CONTEXT);
    if (ret != EGL_TRUE) {
        throw runtime_error("Failed to unbind OpenGL ES context: " +
                            eglErrorMessage(eglGetError()));
    }
}

} // namespace backendunit
} // namespace larod
