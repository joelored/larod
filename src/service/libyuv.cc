/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "libyuv.hh"

#include <libyuv.h>
#include <thread>

using namespace std;

namespace larod {
namespace backendunit {

const string imgFormatToString(imageparams::Format imgFormat) {
    switch (imgFormat) {
    case imageparams::Format::NV12:
        return "nv12";
    case imageparams::Format::RGB_INTERLEAVED:
        return "rgb-interleaved";
    case imageparams::Format::RGB_PLANAR:
        return "rgb-planar";
    default:
        return "unknown format";
    }
}

LibYuv::Context::Context(const ParamsMap& params) {
    tie(inputMetaData, outputMetaData) =
        imageparams::getInputOutputInfo(params, numeric_limits<int>::max());

    if (inputMetaData.height % 2 != 0) {
        throw invalid_argument("Input height is not a multiple of 2");
    }
    if (inputMetaData.width % 2 != 0) {
        throw invalid_argument("Input width is not a multiple of 2");
    }
    if (inputMetaData.rowPitch % 2 != 0) {
        throw invalid_argument("Input row pitch is not a multiple of 2");
    }

    if (outputMetaData.height % 2 != 0) {
        throw invalid_argument("Output height is not a multiple of 2");
    }
    if (outputMetaData.width % 2 != 0) {
        throw invalid_argument("Output width is not a multiple of 2");
    }
    if (outputMetaData.rowPitch % 2 != 0) {
        throw invalid_argument("Output row pitch is not a multiple of 2");
    }

    if (inputMetaData.format == imageparams::Format::RGB_PLANAR ||
        outputMetaData.format == imageparams::Format::RGB_PLANAR) {
        throw invalid_argument("rgb-planar is not supported by libyuv backend");
    }
}

LibYuv::LibYuv() : keepProcessingQueues(true) {
    // Start queue processing threads.
    procQueueThreads.push_back(thread(&LibYuv::processJobQueue, this));
    procQueueThreads.push_back(thread(&LibYuv::processLoadModelQueue, this));

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

LibYuv::~LibYuv() {
    // Stop processing queues.
    keepProcessingQueues = false;

    // Eventually notify them if they are waiting.
    stopJobQueueWait();
    stopLoadModelQueueWait();

    for (thread& th : procQueueThreads) {
        try {
            th.join();
        } catch (system_error& e) {
            LOG.error(THIS_NAMESPACE + __func__ +
                      "(): Could not join thread: " + e.what());
        }
    }
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Stopped queue processing threads");

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

vector<Tensor>
    LibYuv::createTensorsFromMetaData(const imageparams::MetaData& metaData) {
    size_t byteSize = imageparams::getByteSize(metaData);

    switch (metaData.format) {
    case imageparams::Format::NV12: {
        return {Tensor(
            LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_420SP,
            {3, metaData.height, metaData.width},
            {byteSize, metaData.height * metaData.rowPitch, metaData.rowPitch},
            byteSize, "YUV420SP")};
    }
    case imageparams::Format::RGB_PLANAR: {
        return {Tensor(LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_NCHW,
                       {1, 3, metaData.height, metaData.width},
                       {byteSize, byteSize, metaData.height * metaData.rowPitch,
                        metaData.rowPitch},
                       byteSize, "RGB_PLANAR")};
    }
    case imageparams::Format::RGB_INTERLEAVED: {
        return {Tensor(LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_NHWC,
                       {1, metaData.height, metaData.width, 3},
                       {byteSize, metaData.height * metaData.rowPitch,
                        metaData.rowPitch, 3},
                       byteSize, "RGB_INTERLEAVED")};
    }
    default:
        throw invalid_argument(imgFormatToString(metaData.format) +
                               " is not supported by libyuv backend");
    }
}

pair<vector<Tensor>, vector<Tensor>>
    LibYuv::loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                             const ParamsMap& params) {
    if (data.size() != 0) {
        throw invalid_argument("libyuv does not use model data");
    }

    lock_guard<mutex> lockGuard(mtxContexts);

    shared_ptr<Context> ctx = make_shared<Context>(params);

    if (!contexts
             .emplace(piecewise_construct, forward_as_tuple(modelId),
                      forward_as_tuple(ctx))
             .second) {
        throw runtime_error("Could not save context");
    }

    auto inputs = createTensorsFromMetaData(ctx->inputMetaData);
    auto outputs = createTensorsFromMetaData(ctx->outputMetaData);

    return {inputs, outputs};
}

void LibYuv::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxContexts);
    auto it = contexts.find(modelId);
    if (it == contexts.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }
    contexts.erase(it);
}

void LibYuv::runJobVirtual(const uint64_t modelId,
                           const std::vector<Tensor>& inputTensors,
                           vector<Tensor>& outputTensors,
                           const ParamsMap& params) {
    shared_ptr<Context> ctx;
    {
        lock_guard<mutex> contextsLockGuard(mtxContexts);
        auto it = contexts.find(modelId);
        if (it == contexts.end()) {
            // Should never happen...
            throw ModelNotFound("Model " + to_string(modelId) + " not found");
        }
        ctx = it->second;
    }

    // Sanity check sizes (these should have already been checked previously in
    // the call chain).
    assert(inputTensors.size() == 1);
    assert(outputTensors.size() == 1);
    assert(inputTensors[0].getByteSize() ==
           imageparams::getByteSize(ctx->inputMetaData));

    // Pointer to input data.
    uint8_t* inputData = inputTensors[0].getData().data();
    uint8_t* outputData = outputTensors[0].getData().data();

    // Check what operations should be done.
    bool doCropScale = false;
    bool doConvertFormat = false;
    auto [doCrop, cropInfo] = imageparams::getCropInfo(
        params, ctx->inputMetaData.width, ctx->inputMetaData.height,
        numeric_limits<int>::max());

    if (ctx->inputMetaData.width != ctx->outputMetaData.width ||
        ctx->inputMetaData.height != ctx->outputMetaData.height || doCrop) {
        doCropScale = true;
    }
    if (ctx->inputMetaData.format != ctx->outputMetaData.format) {
        doConvertFormat = true;
    }

    // Perform either crop-scale-convert, crop-scale or convert-format.
    if (doCropScale && doConvertFormat) {
        imageparams::MetaData tmpMetaData;
        tmpMetaData.format = ctx->inputMetaData.format;
        tmpMetaData.width = ctx->outputMetaData.width;
        tmpMetaData.height = ctx->outputMetaData.height;
        switch (tmpMetaData.format) {
        case imageparams::Format::NV12:
        case imageparams::Format::RGB_PLANAR:
            tmpMetaData.rowPitch = tmpMetaData.width;
            break;
        case imageparams::Format::RGB_INTERLEAVED:
            tmpMetaData.rowPitch = 3 * tmpMetaData.width;
            break;
        default:
            throw invalid_argument("Unknown image format");
        }

        vector<uint8_t> tmpBuffer(imageparams::getByteSize(tmpMetaData));

        cropScale(inputData, tmpBuffer.data(), ctx->inputMetaData, tmpMetaData,
                  cropInfo);

        convertFormat(tmpBuffer.data(), outputData, tmpMetaData,
                      ctx->outputMetaData);
    } else if (doCropScale) {
        cropScale(inputData, outputData, ctx->inputMetaData,
                  ctx->outputMetaData, cropInfo);
    } else if (doConvertFormat) {
        convertFormat(inputData, outputData, ctx->inputMetaData,
                      ctx->outputMetaData);
    } else {
        throw invalid_argument("No operation to be done");
    }
}

void LibYuv::convertFormat(uint8_t* inputData, uint8_t* outputData,
                           const imageparams::MetaData& inputMetaData,
                           const imageparams::MetaData& outputMetaData) const {
    switch (inputMetaData.format) {
    case imageparams::Format::RGB_PLANAR:
        // TODO: Support RGB_PLANAR conversions.
        throw invalid_argument("rgb-planar not supported in libyuv backend");
    case imageparams::Format::RGB_INTERLEAVED:
        convertFormatFromRgbInterleaved(inputData, outputData, inputMetaData,
                                        outputMetaData);
        return;
    case imageparams::Format::NV12:
        convertFormatFromNv12(inputData, outputData, inputMetaData,
                              outputMetaData);
        return;
    default:
        throw invalid_argument("Unknown color format in libyuv backend");
    }
}

void LibYuv::convertFormatFromRgbInterleaved(
    uint8_t* inputData, uint8_t* outputData,
    const imageparams::MetaData& inputMetaData,
    const imageparams::MetaData& outputMetaData) const {
    int ret = 0;
    switch (outputMetaData.format) {
    case imageparams::Format::RGB_PLANAR: {
        // TODO: Convert to planar!
        break;
    }
    case imageparams::Format::NV12: {
        // There is no function in libyuv to do full conversion in one step, so
        // instead we convert RGB->ARGB->NV12.
        int tempRowPitch = static_cast<int>(inputMetaData.width) * 4;
        vector<uint8_t> temp(inputMetaData.height * inputMetaData.width * 4);
        ret = libyuv::RAWToARGB(
            inputData, static_cast<int>(inputMetaData.rowPitch), temp.data(),
            tempRowPitch, static_cast<int>(inputMetaData.width),
            static_cast<int>(inputMetaData.height));
        if (ret) {
            throw runtime_error("Convert-format from rgb-interleaved failed in "
                                "libyuv backend (" +
                                to_string(ret) + ")");
        }
        ret = libyuv::ARGBToNV12(temp.data(), tempRowPitch, outputData,
                                 static_cast<int>(outputMetaData.rowPitch),
                                 outputData + outputMetaData.rowPitch *
                                                  outputMetaData.height,
                                 static_cast<int>(outputMetaData.rowPitch),
                                 static_cast<int>(outputMetaData.width),
                                 static_cast<int>(outputMetaData.height));
        if (ret) {
            throw runtime_error("Convert-format from rgb-interleaved failed in "
                                "libyuv backend (" +
                                to_string(ret) + ")");
        }
        break;
    }
    default:
        throw invalid_argument("Unknown color format in libyuv backend");
    }
}

void LibYuv::convertFormatFromNv12(
    uint8_t* inputData, uint8_t* outputData,
    const imageparams::MetaData& inputMetaData,
    const imageparams::MetaData& outputMetaData) const {
    int ret = 0;
    switch (outputMetaData.format) {
    case imageparams::Format::RGB_INTERLEAVED: {
        ret = libyuv::NV12ToRAWMatrix(
            inputData, static_cast<int>(inputMetaData.rowPitch),
            inputData + inputMetaData.height * inputMetaData.rowPitch,
            static_cast<int>(inputMetaData.rowPitch), outputData,
            static_cast<int>(outputMetaData.rowPitch),
            &libyuv::kYuvJPEGConstants, static_cast<int>(outputMetaData.width),
            static_cast<int>(outputMetaData.height));
        if (ret) {
            throw runtime_error(
                "Convert-format from nv12 failed in libyuv backend (" +
                to_string(ret) + ")");
        }
        break;
    }
    default:
        throw invalid_argument("Unknown color format in libyuv backend");
    }
}

void LibYuv::cropScale(uint8_t* input, uint8_t* output,
                       const imageparams::MetaData& inputMetaData,
                       imageparams::MetaData& outputMetaData,
                       const imageparams::CropInfo& crop) const {
    libyuv::FourCC fourcc;
    switch (inputMetaData.format) {
    case imageparams::Format::NV12: {
        fourcc = libyuv::FOURCC_NV12;
        break;
    }
    case imageparams::Format::RGB_INTERLEAVED: {
        fourcc = libyuv::FOURCC_RAW;
        break;
    }
    default:
        throw invalid_argument("Crop-scale for " +
                               imgFormatToString(inputMetaData.format) +
                               " is not supported by libyuv backend");
    }

    // Create temporary buffer.
    // There is no direct function to only perform crop-scale, so we instead do
    // 1.) ConvertToI420(), which crops the image.
    // 2.) I420Scale().
    // 3.) ConvertFromI420(), back to original data format.
    int srcWidth = static_cast<int>(inputMetaData.width);
    int srcHeight = static_cast<int>(inputMetaData.height);
    int dstWidth = static_cast<int>(outputMetaData.width);
    int dstHeight = static_cast<int>(outputMetaData.height);
    int dstRowPitch = static_cast<int>(outputMetaData.rowPitch);

    int cropX = static_cast<int>(crop.x);
    int cropY = static_cast<int>(crop.y);
    int cropWidth = static_cast<int>(crop.w);
    int cropHeight = static_cast<int>(crop.h);

    // Create temp buffer for cropped image.
    size_t ySizeCropped = static_cast<size_t>(cropWidth * cropHeight);
    size_t uSizeCropped =
        static_cast<size_t>(((cropWidth + 1) / 2) * ((cropHeight + 1) / 2));
    vector<uint8_t> tempCropped(ySizeCropped + 2 * uSizeCropped);

    int ret = libyuv::ConvertToI420(
        input, imageparams::getByteSize(inputMetaData), tempCropped.data(),
        cropWidth, tempCropped.data() + ySizeCropped, (cropWidth + 1) / 2,
        tempCropped.data() + ySizeCropped + uSizeCropped, (cropWidth + 1) / 2,
        cropX, cropY, srcWidth, srcHeight, cropWidth, cropHeight,
        libyuv::kRotate0, fourcc);
    if (ret) {
        throw runtime_error(
            "Crop part of crop-scale failed in libyuv backend (" +
            to_string(ret) + ")");
    }

    // Create temp buffer for scaled image.
    size_t ySize = outputMetaData.width * outputMetaData.height;
    size_t uSize =
        ((outputMetaData.width + 1) / 2) * ((outputMetaData.height + 1) / 2);
    vector<uint8_t> temp(ySize + 2 * uSize);

    ret = libyuv::I420Scale(
        tempCropped.data(), cropWidth, tempCropped.data() + ySizeCropped,
        (cropWidth + 1) / 2, tempCropped.data() + ySizeCropped + uSizeCropped,
        (cropWidth + 1) / 2, cropWidth, cropHeight, temp.data(), dstWidth,
        temp.data() + ySize, (dstWidth + 1) / 2, temp.data() + ySize + uSize,
        (dstWidth + 1) / 2, dstWidth, dstHeight, libyuv::kFilterBilinear);
    if (ret) {
        throw runtime_error(
            "Scale part of crop-scale failed in libyuv backend (" +
            to_string(ret) + ")");
    }

    ret = libyuv::ConvertFromI420(
        temp.data(), dstWidth, temp.data() + ySize, (dstWidth + 1) / 2,
        temp.data() + ySize + uSize, (dstWidth + 1) / 2, output, dstRowPitch,
        dstWidth, dstHeight, fourcc);
    if (ret) {
        throw runtime_error(
            "Convert format part of crop-scale failed in libyuv backend (" +
            to_string(ret) + ")");
    }
}

void LibYuv::processJobQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<JobRequest> jobReq;
        try {
            jobReq = popJobQueueWait();
        } catch (InvalidJobRequest& e) {
            // We just ignore this particular request.
            LOG.warning(string("Skipping job request: ") + e.what());
            continue;
        }

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!jobReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Job request is null");
        } else if (jobReq->getException()) { // Check if the request is valid.
            jobReq->signal();
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Skipping job request; it had exceptions");
            continue;
        }

        try {
            runJobVirtual(jobReq->MODEL_ID, jobReq->INPUT_TENSORS,
                          jobReq->outputTensors, jobReq->PARAMS);
        } catch (...) {
            jobReq->setException(current_exception());
        }

        // Signal job has finished.
        jobReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

void LibYuv::processLoadModelQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<LoadModelRequest> loadModelReq = popLoadModelQueueWait();

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!loadModelReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Load model request is null");
        }

        try {
            loadModelReq->setTensors(loadModelVirtual(
                const_cast<vector<uint8_t>&>(loadModelReq->DATA),
                loadModelReq->getModelId(), loadModelReq->PARAMS));
        } catch (...) {
            loadModelReq->setException(current_exception());
        }

        // Signal model has been loaded.
        loadModelReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

} // namespace backendunit
} // namespace larod
