/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "debugchip.hh"

#include <numeric>

#include "log.hh"
#include "model.hh"

using namespace std;

namespace larod {
namespace backendunit {

DebugChip::DebugChip() : keepProcessingQueues(true) {
    for (size_t i = 0; i < HW_CONCURRENCY; ++i) {
        procJobQueueThreads.emplace_back(&DebugChip::processJobQueue, this);
        procLoadModelQueueThreads.emplace_back(
            &DebugChip::processLoadModelQueue, this);
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

DebugChip::~DebugChip() {
    // Stop processing queues
    keepProcessingQueues = false;

    // Eventually notify them if they are waiting
    stopJobQueueWait();
    stopLoadModelQueueWait();

    for (size_t i = 0; i < HW_CONCURRENCY; ++i) {
        try {
            procJobQueueThreads[i].join();
            procLoadModelQueueThreads[i].join();
        } catch (system_error& e) {
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Could not join thread: " + e.what());
        }
    }
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Stopped queue processing threads");

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destroyed");
}

pair<vector<Tensor>, vector<Tensor>>
    DebugChip::loadModelVirtual(const span<uint8_t>&, const uint64_t modelId,
                                const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    unique_lock<mutex> lock(mtxModels);

    auto ret = models.insert(modelId);
    if (!ret.second) {
        // Should never happen...
        string msg = "Could not load model: ID " + to_string(modelId) +
                     " already exists";
        LOG.error(msg);
        throw runtime_error(msg);
    }

    lock.unlock();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Model " + to_string(modelId) +
              " loaded");

    return {INPUT_TENSORS, OUTPUT_TENSORS};
}

void DebugChip::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    models.erase(it);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Deleted model " +
              to_string(modelId));
}

void DebugChip::runJobVirtual(const uint64_t modelId, const vector<Tensor>&,
                              vector<Tensor>& outputTensors,
                              const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    unique_lock<mutex> lock(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    lock.unlock();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Running job...");

    // Write sequential data to the output tensors.
    size_t totalOutputSize = 0;
    for (const auto& outputTensor : outputTensors) {
        span<uint8_t> data = outputTensor.getData();
        iota(data.begin(), data.end(), 0);

        totalOutputSize += data.size();
    }

    // Simulate work by sleeping for a while. The total sleep time is
    // totalOutputSize * 1250 ns. For example, using a total output size of 4 KB
    // yields 4 * 1000 * 1250 ns = 5 ms sleep time. There is no simulated
    // loading time if totalOutputSize <= 1.
    if (totalOutputSize > 1) {
        this_thread::sleep_for(chrono::nanoseconds(totalOutputSize * 1250));
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Job done");
}

void DebugChip::processJobQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<JobRequest> jobReq;
        try {
            jobReq = popJobQueueWait();
        } catch (InvalidJobRequest& e) {
            // We just ignore this particular request.
            LOG.warning(string("Skipping job request: ") + e.what());
            continue;
        }

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!jobReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Job request is null");
        } else if (jobReq->getException()) { // Check if the request is valid.
            jobReq->signal();
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Skipping job request; it had exceptions");
            continue;
        }

        try {
            runJobVirtual(jobReq->MODEL_ID, jobReq->INPUT_TENSORS,
                          jobReq->outputTensors, jobReq->PARAMS);
        } catch (...) {
            jobReq->setException(current_exception());
        }

        // Signal job has finished.
        jobReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

void DebugChip::processLoadModelQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<LoadModelRequest> loadModelReq = popLoadModelQueueWait();

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!loadModelReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Load model request is null");
        }

        try {
            loadModelReq->setTensors(loadModelVirtual(
                const_cast<vector<uint8_t>&>(loadModelReq->DATA),
                loadModelReq->getModelId(), loadModelReq->PARAMS));
        } catch (...) {
            loadModelReq->setException(current_exception());
        }

        // Signal model has been loaded.
        loadModelReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

} // namespace backendunit
} // namespace larod
