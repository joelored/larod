/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <functional>
#include <string>

#include "larod.h"
#include "span.hh"

namespace larod {

/**
 * @brief Class to represent a tensor.
 */
class Tensor {
public:
    /**
     * @brief Create a tensor.
     *
     * This will throw if any of the attributes data type, layout, dimensions,
     * pitches and byte size are invalid or inconsistent with one another.
     */
    Tensor(const larodTensorDataType dataType, const larodTensorLayout layout,
           const std::vector<size_t> dims, const std::vector<size_t> pitches,
           const size_t byteSize, const std::string name = "",
           const span<uint8_t> data = {});

    Tensor(const int32_t dataType, const int32_t layout,
           const std::vector<uint64_t> dims,
           const std::vector<uint64_t> pitches, const size_t byteSize = 0,
           const span<uint8_t> data = {});

    void setData(const span<uint8_t> data) { this->data = data; }
    span<uint8_t> getData() const { return data; }
    larodTensorDataType getDataType() const { return DATA_TYPE; }
    larodTensorLayout getLayout() const { return LAYOUT; }
    std::vector<size_t> getDims() const { return DIMS; }
    std::vector<size_t> getPitches() const { return PITCHES; }
    std::string getName() const { return NAME; }
    size_t getByteSize() const { return BYTE_SIZE; }

    /**
     * @brief Check if tensors have equal attributes.
     *
     * Throws @c invalid_argument if @p lhs @c Tensor has an attribute value not
     * equal to @p rhs @c Tensor. Specifically, data type, layout, dimensions
     * and byte size will be compared. If any of these attributes are
     * unspecified (zero for byte size), that attribute will not be checked.
     *
     * @param lhs @c Tensor to check against @p rhs.
     * @param rhs @c Tensor that @lhs will be matched against.
     */
    static void assertEqual(const Tensor& lhs, const Tensor& rhs);

private:
    const larodTensorDataType DATA_TYPE;
    const larodTensorLayout LAYOUT;
    const std::vector<size_t> DIMS;
    const std::vector<size_t> PITCHES;
    const size_t BYTE_SIZE;
    const std::string NAME;
    span<uint8_t> data;

    /**
     * @brief Cast vector<S> into vector<D>.
     */
    template<typename S, typename D>
    static std::vector<D> castVector(const std::vector<S>& s);

    static std::string vectorToString(const std::vector<size_t>& vec);
};

} // namespace larod
