/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>

#include "backendunit.hh"

namespace larod {
namespace backendunit {

class CvFlow : public BackEndUnit {
public:
    CvFlow();
    virtual ~CvFlow();

    CvFlow(const CvFlow&) = delete;
    CvFlow& operator=(const CvFlow&) = delete;

    int getCavalryFd() { return fdCavalry; }

protected:
    /**
     * @brief Allocate memory with cavalry.
     *
     * This class yields a RAII interface for memory handling with cavalry.
     */
    class CavalryMemory {
    public:
        /**
         * @brief Allocate memory.
         *
         * @param size Size to allocate
         * @param cached If the memory should be cached or not.
         */
        CavalryMemory(const size_t size, const bool cached = true);

        /**
         * @brief Free memory.
         *
         * Frees the memory allocated in the constructor.
         */
        ~CavalryMemory();

        CavalryMemory(const CavalryMemory& other) = delete;
        CavalryMemory(CavalryMemory&& other) = delete;
        CavalryMemory& operator=(const CavalryMemory& other) = delete;
        CavalryMemory& operator=(CavalryMemory&& other) = delete;

        unsigned long getSize() { return size; }
        unsigned long getPhysAddr() { return physAddr; } // Be careful.
        void* getVirtAddr() { return virtAddr; }         // Be careful.

        /**
         * @brief Flushes the cache for the allocated memory.
         *
         * Should be called whenever CPU has written to the allocated memory and
         * something else wants to read.
         */
        void flushCache();

        /**
         * @brief Invalidates the cache for the allocated memory.
         *
         * Should be called whenever something has written to the allocated
         * memory and CPU wants to read.
         */
        void invalidateCache();

    private:
        unsigned long size;
        unsigned long physAddr;
        void* virtAddr;
        bool cached;
    };

private:
    static const std::string THIS_NAMESPACE;
    static int fdCavalry;
    /// For cavalry_mem_init() and cavalry_mem_exit(). Since they can only be
    /// called once.
    static uint64_t numInstances;

    std::mutex numInstancesMtx;
};

} // namespace backendunit
} // namespace larod
