/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <cstdint> // vproc.h requires this...
#include <thread>
#include <vproc.h>

#include "cvflow.hh"
#include "imageparams.hh"

namespace larod {
namespace backendunit {

class CvFlowVProc : public CvFlow {
public:
    CvFlowVProc();
    ~CvFlowVProc();

    larodChip getChip() override { return LAROD_CHIP_CVFLOW_PROC; };
    int getMinModelFormatVersion() const override { return 0; }

    enum class OpType { CONVERT_FORMAT, CROP_SCALE, DEFORMATION };

protected:
    std::pair<std::vector<Tensor>, std::vector<Tensor>>
        loadModelVirtual(const span<uint8_t>&, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       const std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;

private:
    struct FirmwareBinary {
        FirmwareBinary(uint32_t size) : mem(size, false) {}
        ~FirmwareBinary();

        CavalryMemory mem;
    };

    struct Vec {
        Vec(const size_t height, const size_t width, const size_t pitch);
        Vec(const size_t height, const size_t width, const size_t pitch,
            const std::pair<bool, struct imageparams::CropInfo>& crop);

        vect_desc_t vec;
    };

    struct YuvImg {
        YuvImg(const size_t height, const size_t width, const size_t pitch);

        Vec y;
        Vec uv;
        size_t byteSize;
    };

    struct RgbImg {
        RgbImg(const size_t height, const size_t width, const size_t pitch);

        Vec rgb;
        size_t byteSize;
    };

    /**
     * @brief Model context.
     *
     * Created in loadModelVirtual() from meta data extracted from the @c
     * ParamsMap.
     */
    struct Context {
        Context(const std::pair<imageparams::MetaData, imageparams::MetaData>&
                    metaData);

        std::pair<imageparams::MetaData, imageparams::MetaData> imgMetaData;
    };

private:
    static inline const std::string THIS_NAMESPACE = "CvFlowVProc::";
    /// Firmware binary name.
    static inline const std::string BIN_NAME = "/lib/firmware/vproc.bin";
    static constexpr size_t NV12_HW_ALIGNMENT = 2;
    static constexpr size_t PITCH_ALIGNMENT = 32;
    static constexpr double DEFORM_MAX_SCALE_FACTOR = 4.0;
    static constexpr double CROP_SCALE_MAX_FACTOR = 256.0;

    FirmwareBinary bin;
    yuv2rgb_mat_t yuv2RgbMat;
    std::vector<std::thread> procQueueThreads;
    std::atomic<bool> keepProcessingQueues;
    std::unordered_map<uint64_t, std::shared_ptr<Context>> contexts;
    std::mutex mtxContexts;

    static uint32_t initVProc();

    static OpType checkOpParams(
        const std::pair<imageparams::MetaData, imageparams::MetaData>&
            imgMetaData,
        const std::pair<bool, struct imageparams::CropInfo>& crop);
    static void checkScaleFactor(const double widthQuotient,
                                 const double heightQuotient,
                                 const double maxScaleFactor);
    static std::vector<Tensor>
        createTensorsFromMetaData(const imageparams::MetaData& metaData);
    static void checkMetaData(imageparams::MetaData& metaData,
                              const std::string& desc);
    static void
        cropScale(std::unique_ptr<CavalryMemory>& input,
                  std::unique_ptr<CavalryMemory>& output,
                  const imageparams::MetaData& inputMetaData,
                  imageparams::MetaData& outputMetaData,
                  const std::pair<bool, struct imageparams::CropInfo>& crop);
    static void cropScaleConvert(
        std::unique_ptr<CavalryMemory>& input,
        std::unique_ptr<CavalryMemory>& output,
        const imageparams::MetaData& inputMetaData,
        imageparams::MetaData& outputMetaData,
        const std::pair<bool, struct imageparams::CropInfo>& crop);

    void convertFormat(std::unique_ptr<CavalryMemory>& input,
                       std::unique_ptr<CavalryMemory>& output,
                       const imageparams::MetaData& inputMetaData,
                       imageparams::MetaData& outputMetaData);
    void processJobQueue();
    void processLoadModelQueue();
};

} // namespace backendunit
} // namespace larod
