/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdio.h>
#include <string.h>
#include <tensorflow/lite/error_reporter.h>
#include <tensorflow/lite/interpreter.h>
#include <tensorflow/lite/kernels/register.h>
#include <tensorflow/lite/model.h>
#include <thread>

#include "backendunit.hh"
#include "log.hh"

// Number of threads to set for tflite::Interpreter.
#ifndef LAROD_NBR_OF_THREADS
#define LAROD_NBR_OF_THREADS 1
#endif

namespace larod {
namespace backendunit {

class TFLite : public BackEndUnit {
public:
    TFLite();
    virtual ~TFLite();

    TFLite(const TFLite&) = delete;
    TFLite& operator=(const TFLite&) = delete;

protected:
    /**
     * @brief Subclass @c tflite::ErrorReporter to print errors using
     * @c larod::Log.
     *
     * We only override @c Report(), where we use @c LOG.error() instead.
     */
    class ErrorReporterLog : public tflite::ErrorReporter {
    public:
        int Report(const char* format, va_list args) override {
            char* errMsg;

            if (vasprintf(&errMsg, format, args) == -1) {
                throw std::invalid_argument("Encoding error in format");
            }

            LOG.error(errMsg);

            free(errMsg);

            return 0;
        };
    };

protected:
    static inline ErrorReporterLog errRepLog;

    std::unordered_map<uint64_t,
                       std::tuple<std::shared_ptr<tflite::Interpreter>,
                                  std::shared_ptr<tflite::FlatBufferModel>,
                                  std::shared_ptr<std::vector<uint8_t>>>>
        interpreters;
    std::mutex mtxInterpreters; ///< Mutex for interpreters.

    std::pair<std::vector<Tensor>, std::vector<Tensor>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       const std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;

    /**
     * @brief Builds a tflite::Interpreter.
     *
     * This function is called during loadModelVirtual() to allow sub-classes to
     * build relevant tflite::Interpreter (e.g. for TPU).
     *
     * @return unique_ptr to the built tflite::Interpreter. Can be nullptr in
     * case of any errors.
     */
    virtual std::unique_ptr<tflite::Interpreter>
        buildInterpreter(const tflite::FlatBufferModel& model) = 0;

    int getMinModelFormatVersion() const override {
        return MIN_MODEL_FORMAT_VERSION;
    }

    /**
     * @brief Stop processing of request queues.
     */
    void stopProcessingQueues() noexcept;

private:
    static inline const std::string THIS_NAMESPACE = "TFLite::";
    /// Version of the larod model format this backend requires.
    static const int32_t MIN_MODEL_FORMAT_VERSION = 1;
    static inline const std::unordered_map<TfLiteType, larodTensorDataType>
        DATA_TYPES = {
            {kTfLiteFloat32, LAROD_TENSOR_DATA_TYPE_FLOAT32},
            {kTfLiteInt32, LAROD_TENSOR_DATA_TYPE_INT32},
            {kTfLiteUInt8, LAROD_TENSOR_DATA_TYPE_UINT8},
            {kTfLiteInt64, LAROD_TENSOR_DATA_TYPE_INT64},
            {kTfLiteBool, LAROD_TENSOR_DATA_TYPE_BOOL},
            {kTfLiteInt16, LAROD_TENSOR_DATA_TYPE_INT16},
            {kTfLiteInt8, LAROD_TENSOR_DATA_TYPE_INT8},
            {kTfLiteFloat16, LAROD_TENSOR_DATA_TYPE_FLOAT16},
        };

    std::vector<std::thread> procQueueThreads;
    std::atomic<bool> keepProcessingQueues;

    Tensor parseTensor(const TfLiteTensor* tensor);
    void processJobQueue();
    void processLoadModelQueue();
};

} // namespace backendunit
} // namespace larod
