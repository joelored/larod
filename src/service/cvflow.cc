/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "cvflow.hh"

#include <cavalry/cavalry_ioctl.h>
#include <cavalry_mem.h>
#include <chrono>
#include <fcntl.h>
#include <thread>
#include <unistd.h>

#include "log.hh"

using namespace std;

namespace larod {
namespace backendunit {

const string CvFlow::THIS_NAMESPACE = "CvFlow::";
uint64_t CvFlow::numInstances = 0;
int CvFlow::fdCavalry = -1;

CvFlow::CvFlow() {
    lock_guard<mutex> lockGuard(numInstancesMtx);
    if (!numInstances) {
        // First time initializing.

        // Initialize cavalary.
        fdCavalry = open(CAVALRY_DEV_NODE, O_RDWR, 0);
        if (fdCavalry < 0) {
            throw runtime_error("Cavalry dev open failed: " + to_string(errno));
        }

        // cavalry_mem_init() can only be called once per process.
        if (cavalry_mem_init(fdCavalry, 0) < 0) {
            throw runtime_error("Cavalry mem init failed");
        }
    }

    ++numInstances;

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

CvFlow::~CvFlow() {
    lock_guard<mutex> lockGuard(numInstancesMtx);
    if (numInstances-- == 1) {
        // Last instance to destruct.
        cavalry_mem_exit();

        if (fdCavalry >= 0) {
            close(fdCavalry);
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

CvFlow::CavalryMemory::CavalryMemory(const size_t sz, const bool c)
    : size(sz), physAddr(0), virtAddr(nullptr), cached(c) {
    if (size > numeric_limits<uint32_t>::max()) {
        throw invalid_argument("Size is too big (" + to_string(size) + " > " +
                               to_string(numeric_limits<uint32_t>::max()) +
                               ")");
    }

    uint8_t nrTries = 8;
    int sleepMS = 1;
    int ret = -1;
    while (ret && nrTries--) {
        ret = cavalry_mem_alloc(&size, &physAddr, &virtAddr, cached);
        if (ret < 0 && errno == EBUSY) {
            // The kernel allocation may fail and in that case the cavalry
            // driver will return EBUSY (by setting errno). Let's try again...
            this_thread::sleep_for(chrono::milliseconds(sleepMS));
            sleepMS *= 2;

            LOG.warning("Could not allocate cavalry memory. Trying again...");
        } else if (ret < 0) {
            break;
        }
    }

    if (ret < 0) {
        throw runtime_error("Could not allocate " + to_string(size) +
                            " bytes cavalry memory (" + to_string(ret) +
                            "): " + strerror(errno));
    }
}

CvFlow::CavalryMemory::~CavalryMemory() {
    if (cavalry_mem_free(size, physAddr, virtAddr)) {
        LOG.error("Could not free cavalry memory");
    }
}

void CvFlow::CavalryMemory::flushCache() {
    if (!cached) {
        throw logic_error("Memory is not cached");
    }

    int ret = cavalry_mem_sync_cache(size, physAddr, true, false);
    if (ret < 0) {
        throw runtime_error("Could not flush cavalry memory cache (" +
                            to_string(ret) + ")");
    }
}

void CvFlow::CavalryMemory::invalidateCache() {
    if (!cached) {
        throw logic_error("Memory is not cached");
    }

    int ret = cavalry_mem_sync_cache(size, physAddr, false, true);
    if (ret < 0) {
        throw runtime_error("Could not invalidate cavalry memory cache (" +
                            to_string(ret) + ")");
    }
}

} // namespace backendunit
} // namespace larod
