/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <utility>
#include <variant>
#include <vector>

#include "asyncmsg.hh"
#include "model.hh"
#include "tensor.hh"

namespace larod {

class BackEndUnit; // Forward declaration.

class LoadModelRequest : public AsyncMsg {
    friend class BackEndUnit;

public:
    using ParamsMap = std::unordered_map<
        std::string, std::variant<int64_t, std::string, std::vector<int64_t>>>;

    LoadModelRequest(
        std::weak_ptr<Session> session, BackEndUnit* backEndUnit,
        const uint64_t creatorId, const uint64_t chipId,
        const std::vector<uint8_t>&& data, const Model::Access access,
        const std::string name, const uint64_t metaData,
        const ParamsMap&& params);

    void signal() override;
    uint64_t getModelId() const { return modelId; }
    void setTensors(
        std::pair<std::vector<Tensor>, std::vector<Tensor>>&& tensors);
    void setTensors(
        const std::pair<std::vector<Tensor>, std::vector<Tensor>>& tensors);
    std::pair<std::vector<Tensor>, std::vector<Tensor>> getTensors() const {
        return {inputTensors, outputTensors};
    }

    const uint64_t CREATOR_ID;
    const uint64_t CHIP_ID;
    const std::vector<uint8_t> DATA;
    const Model::Access ACCESS;
    const std::string NAME;
    const uint64_t META_DATA;
    const ParamsMap PARAMS;

private:
    uint64_t modelId;
    BackEndUnit* backEndUnit; // Used only as a std::experimental::observer_ptr.
    std::vector<Tensor> inputTensors;
    std::vector<Tensor> outputTensors;
};

} // namespace larod
