/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "backendunit.hh"

#include <thread>

#include "log.hh"
#include "model.hh"
#include "tensor.hh"

using namespace std;

namespace larod {

BackEndUnit::BackEndUnit()
    : waitForJobQueue(true), waitForLoadModelQueue(true) {
}

Model BackEndUnit::loadModel(const uint64_t callerId, const span<uint8_t>& data,
                             const Model::Access access, const string& name,
                             const ParamsMap& params) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto [inputTensors, outputTensors] =
        loadModelVirtual(data, modelId, params);

    // Check returned tensors.
    if (inputTensors.empty()) {
        throw runtime_error("Input tensors reported by model are empty");
    }

    if (outputTensors.empty()) {
        throw runtime_error("Output tensors reported by model are empty");
    }

    const Model& model = addModel(callerId, modelId, name, data.size(), access,
                                  inputTensors, outputTensors);
    modelId++;

    return model;
}

const modelformat::Model*
    BackEndUnit::verifyFbModel(const span<uint8_t>& model) {
    flatbuffers::Verifier verifier(model.data(), model.size());
    if (!modelformat::VerifyModelBuffer(verifier)) {
        throw invalid_argument("Input model has an invalid format");
    }

    const modelformat::Model* fbModel = modelformat::GetModel(model.data());
    int version = getMinModelFormatVersion();
    if (fbModel->version() < version) {
        throw invalid_argument("Model has format version " +
                               to_string(fbModel->version()) +
                               " but chip requires >= " + to_string(version));
    }

    return fbModel;
}

Model& BackEndUnit::addModel(const uint64_t& callerId, const uint64_t& modelId,
                             const string& name, const size_t& size,
                             const Model::Access& access,
                             const vector<Tensor> inputTensors,
                             const vector<Tensor> outputTensors) {
    auto [it, ret] =
        models.emplace(piecewise_construct, forward_as_tuple(modelId),
                       forward_as_tuple(modelId, name, size, access,
                                        inputTensors, outputTensors));
    if (!ret) {
        // Should never happen...
        string msg = "Could not map ID to model: ID " + to_string(modelId) +
                     " already exists";
        LOG.error(msg);
        throw runtime_error(msg);
    }

    Model& model = it->second;
    ret = modelCreators.insert({model, callerId}).second;
    if (!ret) {
        // Should never happen...
        string msg = "Could not map model to creator: Model ID " +
                     to_string(modelId) + " already exists";
        LOG.error(msg);
        throw runtime_error(msg);
    }

    return model;
}

void BackEndUnit::addModel(LoadModelRequest* req) {
    // Check for errors.
    if (req->getException()) {
        return;
    }

    // Model has been loaded successfully.
    try {
        lock_guard<mutex> lockGuard(mtxModels);
        addModel(req->CREATOR_ID, req->getModelId(), req->NAME,
                 req->DATA.size(), req->ACCESS, req->inputTensors,
                 req->outputTensors);
    } catch (...) {
        req->setException(current_exception());
    }
}

void BackEndUnit::queueLoadModel(shared_ptr<LoadModelRequest> loadModelReq) {
    // Reserve a future model ID.
    {
        lock_guard<mutex> lockGuard(mtxModels);
        loadModelReq->modelId = modelId++;
    }

    {
        lock_guard<mutex> lockGuard(mtxLoadModelQueue);
        loadModelQueue.push(loadModelReq);
        LOG.debug(THIS_NAMESPACE + __func__ + "(): Pushed (size = " +
                  to_string(loadModelQueue.size()) + ")");
    }

    condVarLoadModelQueue.notify_all();
}

void BackEndUnit::deleteModel(const uint64_t callerId, const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    Model& model = it->second;
    if (hasPermission(callerId, model)) {
        deleteModelVirtual(modelId);
    } else {
        throw PermissionDenied("Caller " + to_string(callerId) +
                               " does not have permissions for model " +
                               string(model));
    }

    // Delete from modelCreators
    bool removed = modelCreators.erase(model);
    if (!removed) {
        // Should never happen...
        LOG.error("Could not find creator for model " + to_string(modelId));
    }

    // Delete from models
    models.erase(it);
}

void BackEndUnit::runJob(const uint64_t callerId, const uint64_t modelId,
                         const vector<Tensor>& inputTensors,
                         vector<Tensor>& outputTensors,
                         const ParamsMap& params) {
    unique_lock<mutex> lock(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    // Check permissions.
    Model& model = it->second;
    if (!hasPermission(callerId, model)) {
        throw PermissionDenied("Caller " + to_string(callerId) +
                               " does not have permissions for model " +
                               string(model));
    }

    // Check input and output tensors against what model expects.
    checkTensors({inputTensors, outputTensors}, model.getTensors());

    runJobVirtual(modelId, inputTensors, outputTensors, params);
}

vector<size_t> BackEndUnit::getOutputByteSizes(const uint64_t modelId) {
    unique_lock<mutex> lock(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    auto outputTensors = get<1>(it->second.getTensors());

    vector<size_t> outputByteSizes;
    for (const auto& outputTensor : outputTensors) {
        outputByteSizes.emplace_back(outputTensor.getByteSize());
    }

    return outputByteSizes;
}

void BackEndUnit::queueJob(shared_ptr<JobOrder> jobOrder) {
    lock_guard<mutex> lockGuard(mtxJobQueue);

    jobQueue.push(jobOrder);
    condVarJobQueue.notify_all();

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Pushed (size = " + to_string(jobQueue.size()) + ")");
}

bool BackEndUnit::hasPermission(const uint64_t callerId, Model& model) {
    if (model.getAccessType() == Model::Access::PRIVATE) {
        try {
            uint64_t creator = modelCreators.at(model);
            if (creator != callerId) {
                return false;
            }
        } catch (out_of_range& e) {
            // Should never happen...
            LOG.error("Creator for model " + string(model) + " not found");
            throw;
        }
    }

    return true;
}

unordered_map<uint64_t, Model> BackEndUnit::getModels() const {
    unique_lock<mutex> lock(mtxModels);

    return models;
}

void BackEndUnit::checkTensors(
    const pair<vector<Tensor>, vector<Tensor>>& lhs,
    const pair<vector<Tensor>, vector<Tensor>>& rhs) {
    const auto& [lhsInputTensors, lhsOutputTensors] = lhs;
    const auto& [rhsInputTensors, rhsOutputTensors] = rhs;

    // Check number of inputs.
    if (lhsInputTensors.size() != rhsInputTensors.size()) {
        throw invalid_argument("Number of input tensors mismatch (got " +
                               to_string(lhsInputTensors.size()) +
                               ", expected " +
                               to_string(rhsInputTensors.size()) + ")");
    }

    // Check number of outputs.
    if (lhsOutputTensors.size() != rhsOutputTensors.size()) {
        throw TensorMismatch("Number of output tensors mismatch (got " +
                             to_string(lhsOutputTensors.size()) +
                             ", expected " +
                             to_string(rhsOutputTensors.size()) + ")");
    }

    // Check input attributes.
    for (size_t i = 0; i < lhsInputTensors.size(); ++i) {
        try {
            Tensor::assertEqual(lhsInputTensors[i], rhsInputTensors[i]);
        } catch (invalid_argument& e) {
            throw TensorMismatch("Input tensor " + to_string(i) +
                                 " mismatch: " + e.what());
        }
    }

    // Check output attributes.
    for (size_t i = 0; i < lhsOutputTensors.size(); ++i) {
        try {
            Tensor::assertEqual(lhsOutputTensors[i], rhsOutputTensors[i]);
        } catch (invalid_argument& e) {
            throw TensorMismatch("Output tensor " + to_string(i) +
                                 " mismatch: " + e.what());
        }
    }
}

void BackEndUnit::checkJobRequest(JobRequest& jobReq) {
    uint64_t sessionId;
    try {
        sessionId = jobReq.getSessionId();
    } catch (Msg::SessionExpired& e) {
        jobReq.setException(
            InvalidJobRequest(string("Could not get session ID: ") + e.what()));

        return;
    }

    // Note the position of locking mtxModels: We obtain the mtxModels lock only
    // AFTER we have performed jobReq.getSessionId() above. The call to
    // jobReq.getSessionId() can in rare cases lead to session destructor being
    // called, attempting to clean up private models for that session. The
    // clean-up operation leads to attempt to lock mtxModels possibly leading to
    // a deadlock if the non-recursive mtxModels is already locked in this
    // method.
    lock_guard<mutex> lockGuard(mtxModels);

    // Check if model is valid.
    auto it = models.find(jobReq.MODEL_ID);
    if (it == models.end()) {
        jobReq.setException(ModelNotFound(
            "Model " + to_string(jobReq.MODEL_ID) + " not found"));

        return;
    }
    // Check permissions on model.
    Model& model = it->second;
    if (!hasPermission(sessionId, model)) {
        jobReq.setException(PermissionDenied(
            "Caller " + to_string(sessionId) +
            " does not have permissions for model " + string(model)));

        return;
    }

    // Check input and output tensors against what model expects.
    try {
        checkTensors({jobReq.INPUT_TENSORS, jobReq.outputTensors},
                     model.getTensors());
    } catch (...) {
        jobReq.setException(current_exception());

        return;
    }
}

shared_ptr<JobRequest> BackEndUnit::popJobQueueWait() {
    unique_lock<mutex> lockInfQueue(mtxJobQueue);

    waitForQueue(jobQueue, condVarJobQueue, lockInfQueue, waitForJobQueue);

    if (!waitForJobQueue) {
        // We got a stop waiting signal.
        return nullptr;
    }

    shared_ptr<JobRequest> jobReq = popQueue(jobQueue);
    lockInfQueue.unlock();

    checkJobRequest(*jobReq);

    return jobReq;
}

shared_ptr<LoadModelRequest> BackEndUnit::popLoadModelQueueWait() {
    unique_lock<mutex> lock(mtxLoadModelQueue);

    waitForQueue(loadModelQueue, condVarLoadModelQueue, lock,
                 waitForLoadModelQueue);

    if (!waitForLoadModelQueue) {
        // We got a stop waiting signal.
        return nullptr;
    }

    return popQueue(loadModelQueue);
}

template<typename T> T BackEndUnit::popQueue(queue<T>& queue) {
    if (queue.empty()) {
        throw runtime_error("Can not pop empty queue");
    }

    T element(queue.front());
    queue.pop();

    return element;
}

shared_ptr<JobRequest>
    BackEndUnit::popQueue(queue<shared_ptr<JobOrder>>& queue) {
    auto jobOrder = popQueue<shared_ptr<JobOrder>>(queue);
    shared_ptr<JobRequest> jobReq;
    try {
        jobReq = jobOrder->getJobRequest();
    } catch (Msg::SessionExpired& e) {
        throw InvalidJobRequest(string("Could not get job request: ") +
                                e.what());
    }

    return jobReq;
}

template<typename T, typename C, typename L>
void BackEndUnit::waitForQueue(queue<T>& queue, C& condVar,
                               unique_lock<L>& lock, atomic<bool>& pred) {
    if (!queue.empty()) {
        return;
    }

    // Queue is empty. Wait for signal.
    condVar.wait(lock, [&] { return !queue.empty() || !pred; });
}

void BackEndUnit::stopJobQueueWait() {
    lock_guard<mutex> lockGuard(mtxJobQueue);
    waitForJobQueue = false;
    condVarJobQueue.notify_all();
}

void BackEndUnit::stopLoadModelQueueWait() {
    lock_guard<mutex> lockGuard(mtxLoadModelQueue);
    waitForLoadModelQueue = false;
    condVarLoadModelQueue.notify_all();
}

size_t BackEndUnit::getJobQueueSize() const {
    lock_guard<mutex> lockGuard(mtxJobQueue);
    return jobQueue.size();
}

size_t BackEndUnit::getLoadModelQueueSize() const {
    lock_guard<mutex> lockGuard(mtxLoadModelQueue);
    return loadModelQueue.size();
}

} // namespace larod
