/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "msg.hh"

#include "session.hh"

using namespace std;

namespace larod {

Msg::Msg(weak_ptr<Session> s)
    : session(s), CREATION_TIME(chrono::steady_clock::now()) {
    if (session.expired()) {
        throw SessionExpired();
    }
}

uint64_t Msg::getSessionId() {
    if (auto sessionPtr = session.lock()) {
        return sessionPtr->getSessionId();
    } else {
        throw SessionExpired();
    }
}

} // namespace larod
