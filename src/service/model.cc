/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "model.hh"

using namespace std;

namespace larod {

Model::Model(const uint64_t id, const string name, const size_t size,
             const Access access, const vector<Tensor> inputTensors,
             const vector<Tensor> outputTensors)
    : ID(id), NAME(name), SIZE(size), ACCESS(access),
      INPUT_TENSORS(inputTensors), OUTPUT_TENSORS(outputTensors) {
}

uint64_t Model::getId() const {
    return ID;
}

string Model::getName() const {
    return NAME;
}

size_t Model::getSize() const {
    return SIZE;
}

Model::Access Model::getAccessType() const {
    return ACCESS;
}

bool Model::operator==(const Model& other) const {
    return this->ID == other.ID;
}

Model::operator string() const {
    string access = ACCESS == Access::PUBLIC ? "public" : "private";
    return to_string(ID) + " " + NAME + " " + to_string(SIZE) + " " + access;
}

} // namespace larod
