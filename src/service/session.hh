/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <atomic>
#include <systemd/sd-bus.h>
#include <thread>
#include <unordered_set>

#include "backendunit.hh"
#include "larod.h"
#include "sessionsdbusserver.hh"

namespace larod {

// Define a comparison function for std::shared_ptr<JobRequest>.
struct sharedPtrJobReqIsLessThan {
    bool operator()(const std::shared_ptr<JobRequest>& lhs,
                    const std::shared_ptr<JobRequest>& rhs) const {
        return *lhs < *rhs;
    }
};

class Session : public std::enable_shared_from_this<Session> {
public:
    explicit Session(sdbusplus::bus::bus& bus, const std::string objPath,
                     const std::vector<std::unique_ptr<BackEndUnit>>& units);

    /**
     * @brief Destructor.
     *
     * Make sure to call stopBusProcessing() before destructing. Since during
     * destruction, new requests might get submitted on the sd-bus interface,
     * and if we already have other outgoing requests at the same time,
     * potential deadlocks may occur. Even if one knows that there are no
     * outgoing requests (or incoming), stopBusProcessing() should still be
     * called before actually destructing the Session.
     */
    ~Session();

    // Sessions can not be copied
    Session(const Session&) = delete;
    Session& operator=(const Session&) = delete;

    static uint64_t getNextId();

    std::pair<int, int> getClientAsyncFds() const;
    void queueJob(const uint64_t modelId, const size_t chipId,
                  std::vector<Tensor>&& inputTensors,
                  std::vector<std::vector<uint8_t>>&& inputTensorsData,
                  std::vector<Tensor>&& outputTensors,
                  std::vector<JobRequest::FdInfo>& outputFds,
                  const uint8_t priority, const uint64_t metaData,
                  const BackEndUnit::ParamsMap&& params = {});
    void queueLoadModel(const int fd, const uint64_t sz, const size_t chipId,
                        const int32_t access, const std::string name,
                        const uint64_t metaData,
                        const BackEndUnit::ParamsMap&& params = {});
    void sendJobResult(JobRequest* req);
    void sendLoadModelResult(const LoadModelRequest* req);
    void deleteModel(uint64_t modelId, const size_t chipId);

    /**
     * @brief Stop processing messages buses.
     *
     * This will delete the sd-bus interface server and also close the
     * asynchronous bus processing file descriptors. Note that it is important
     * to call this before destructing Session (c.f. ~Session()).
     */
    void stopBusProcessing() noexcept;

    /**
     * @brief Get the first job request in line for a specific chip.
     *
     * The job requests are queued according to their priorities. This
     * method will return the first in a queue for a chip, i.e. the most
     * prioritized.
     *
     * @param chipId Chip id for the job request to get.
     * @return The first job request in line for @p chipId.
     */
    std::shared_ptr<JobRequest> popJobRequest(const size_t chipId);

    // Return type of createTensorMsg().
    using TensorMetaData =
        std::tuple<int32_t, int32_t, uint64_t, std::vector<uint64_t>,
                   std::vector<uint64_t>, std::string>;

    // Return type for QueueLoadModel message.
    using LoadModelRetType = std::tuple<
        std::tuple<uint64_t, uint64_t, uint64_t, int32_t, int32_t, std::string,
                   std::vector<TensorMetaData>, std::vector<TensorMetaData>>,
        uint64_t, int32_t, std::string>;

    // Argument type for tensors in QueueJob message. The tuple contains,
    // in order: data type, layout, dims vector, pitches vector, fd, size,
    // offset.
    using RunJobTensorsMsg =
        std::vector<std::tuple<int32_t, int32_t, std::vector<uint64_t>,
                               std::vector<uint64_t>,
                               sdbusplus::message::unix_fd, uint64_t, int64_t>>;

    // D-Bus interface members.

    // Return type for models().
    using ModelsRetType = std::vector<
        std::tuple<uint64_t, uint64_t, uint64_t, int32_t, int32_t, std::string,
                   std::vector<TensorMetaData>, std::vector<TensorMetaData>>>;

    uint64_t getSessionId() const;
    ModelsRetType getModels() const;

private:
    // For debug output.
    inline static const std::string THIS_NAMESPACE = "Session::";
    inline static std::atomic<uint64_t> nextId = 0;

    static TensorMetaData createTensorMsg(const Tensor& tensor);
    static std::vector<TensorMetaData>
        createTensorMsg(const std::vector<Tensor>& tensors);
    static void busDeleter(sd_bus* bus) { sd_bus_flush_close_unref(bus); }
    static void msgDeleter(sd_bus_message* msg) { sd_bus_message_unref(msg); }
    static sd_bus* createAsyncBus(int fd);
    static void readFromFd(const int fd, const int64_t offset,
                           const span<uint8_t>& buf);
    static void writeToFd(const int fd, const size_t maxFdSize,
                          const int64_t offset, const span<uint8_t>& buf);

    /**
     * @brief Check if sd-bus message contains a dictionary type.
     *
     * It will check for a{sv} in the message signature.
     *
     * @param msg Message to check.
     * @return True if @p msg contains a dictionary type, otherwise false.
     */
    // FIXME: Make msg const when sdbusplus::message::message.get_signature()
    // gets declared as const upstream.
    static bool containsDict(sdbusplus::message::message& msg) {
        static constexpr std::string_view DICT_SIGNATURE = "a{sv}";
        return std::string(msg.get_signature()).find(DICT_SIGNATURE) !=
               std::string::npos;
    }

    /// Sd-bus interface server.
    std::unique_ptr<SessionSdbusServer> sdbusServer;
    /// Bus for reading messages from client.
    std::unique_ptr<sd_bus, decltype(busDeleter)*> asyncReadBus{nullptr,
                                                                busDeleter};
    std::thread busProcThread; ///< Processing thread for asyncReadBus.
    /// Flag to signal busProcThread to exit.
    std::atomic<bool> stopBusProc;
    int busProcEventFd;
    /// Peer handshake flag, indicating if a handshake has been made.
    bool hasHandshaked;

    /// Bus for writing messages to client.
    std::unique_ptr<sd_bus, decltype(busDeleter)*> asyncWriteBus{nullptr,
                                                                 busDeleter};
    int clientAsyncReadFd;
    int clientAsyncWriteFd;
    ///< For sd-bus functions regarding sending messages.
    std::recursive_mutex mtxSend;
    std::mutex mtxPrivateModels; ///< Mutex for set of private models.

    const uint64_t ID;
    const std::string ID_STR;
    const std::vector<std::unique_ptr<BackEndUnit>>& UNITS;
    /// Set of private models for each chip (list index corresponds to the same
    /// chip index in UNITS).
    std::vector<std::unordered_set<uint64_t>> privateModels;
    /// Priority queues with job requests for each chip index.
    std::unordered_map<
        size_t, std::priority_queue<std::shared_ptr<JobRequest>,
                                    std::vector<std::shared_ptr<JobRequest>>,
                                    sharedPtrJobReqIsLessThan>>
        jobReqs;
    /// Mutex for the queued job requests @c jobReqs.
    std::mutex mtxJobReqs;

    // TODO: static?
    /**
     * @brief Parse serialized tensor messages.
     *
     * This parses the input argument messages for @c Session::runJob()
     * (RunJobTensorsMsg).
     *
     * @param inputsMsg Input tensors message.
     * @param outputsMsg Output tensors message.
     * @return A tuple containing the parsed input tensors, read input tensors
     * data, parsed output tensors and JobRequest::fdInfo for output
     * tensors.
     */
    std::tuple<std::vector<Tensor>, std::vector<std::vector<uint8_t>>,
               std::vector<Tensor>, std::vector<JobRequest::FdInfo>>
        readTensorMsg(RunJobTensorsMsg inputsMsg, RunJobTensorsMsg outputsMsg);

    template<typename T> void closeFd(T& fd);
    template<typename T>
    void closeFd(std::vector<std::reference_wrapper<T>>& fds);
    void closeFd(std::vector<std::reference_wrapper<int>>&& fds);
    void closeFdInfo(std::vector<JobRequest::FdInfo>& fdInfos);

    void processAsyncReadBus() noexcept;
    void sendJobResultError(const larodErrorCode error,
                            const std::string& errorMsg,
                            const uint64_t metaData);
    void sendLoadModelResultError(const larodErrorCode error,
                                  const std::string& errorMsg,
                                  const uint64_t metaData);

    void procHandshakeMsg(sd_bus_message* msg);
    void procQueueLoadModelMsg(sd_bus_message* msg);
    void procQueueJobMsg(sd_bus_message* msg);
    void procDeleteModelMsg(sd_bus_message* msg);

    /**
     * @brief Send an asynchronous message response.
     *
     * This allocates a sd_bus_message with appropriate appending types defined
     * by @p args and sends it over the @c asyncWriteBus. It can handle all
     * fundamental types and as well as strings, vectors, tuples etc. as defined
     * by sdbusplus.
     *
     * @param msgType The type of message.
     * @param args Function paramater pack of the arguments to append in the
     * message (corresponding to the types defined by @p msgType).
     */
    template<typename... Args>
    void sendMsg(const std::string& msgType, Args&&... args);

    void handleSendMsgError(const int retCode, const std::string& msg);

public:
    struct FileDescriptorError : std::runtime_error {
        FileDescriptorError(const std::string& msg) : runtime_error(msg) {}
    };

    struct ConnectionError : std::runtime_error {
        ConnectionError(const std::string& msg) : runtime_error(msg) {}
    };

    struct TypeSizeOverflow : std::runtime_error {
        TypeSizeOverflow(const std::string& msg) : runtime_error(msg) {}
    };
};

} // namespace larod
