/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tflite.hh"

#include <algorithm>
#include <tensorflow/lite/kernels/register.h>

#include "model.hh"
#include "modelformat_generated.h"

using namespace std;

namespace larod {
namespace backendunit {

TFLite::TFLite() : keepProcessingQueues(true) {
    // Start queue processing threads.
    procQueueThreads.emplace_back(&TFLite::processJobQueue, this);
    procQueueThreads.emplace_back(&TFLite::processLoadModelQueue, this);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

TFLite::~TFLite() {
    stopProcessingQueues();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

void TFLite::stopProcessingQueues() noexcept {
    if (!procQueueThreads.size()) {
        // This is the only place where we join the queue processing threads and
        // erase them. If there aren't any threads, that means we have already
        // called this function before and there is therefore nothing to stop.
        return;
    }

    keepProcessingQueues = false;

    // Eventually notify them if they are waiting.
    stopJobQueueWait();
    stopLoadModelQueueWait();

    for (thread& th : procQueueThreads) {
        try {
            th.join();
        } catch (system_error& e) {
            LOG.error("Could not join thread: " + string(e.what()));
        }
    }

    procQueueThreads.clear();

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Stopped queue processing threads");
}

pair<vector<Tensor>, vector<Tensor>>
    TFLite::loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                             const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    const modelformat::Model* fbModel = verifyFbModel(data);

    const modelformat::TFLite* fbTFLite = fbModel->backEndData_as_TFLite();
    if (!fbTFLite) {
        throw invalid_argument(
            "Failed model format type check, got type: " +
            string(
                modelformat::EnumNamesBackEnd()[fbModel->backEndData_type()]) +
            " and expected " +
            string(
                modelformat::EnumNamesBackEnd()[modelformat::BackEnd_TFLite]));
    }

    const flatbuffers::Vector<uint8_t>* nativeData = fbTFLite->nativeData();

    // tflite::FlatBufferModel requires the model buffer to be kept alive. Thus,
    // we store a copy of the tflite model. We let Flatbuffers get pointers into
    // this copy.
    shared_ptr<vector<uint8_t>> nativeModelBuf =
        make_shared<vector<uint8_t>>(nativeData->begin(), nativeData->end());

    // Since flatbuffers only supports reading "byte" buffers as signed or
    // unsigned char, and VerifyAndBuildFromBuffer() requires char we need to do
    // a reinterpret cast here.
    // NOTE: This may overflow on some architectures.
    const char* tfLiteData =
        reinterpret_cast<const char*>(nativeModelBuf.get()->data());

    lock_guard<mutex> lockGuard(mtxInterpreters);

    // Load model
    shared_ptr<tflite::FlatBufferModel> model =
        tflite::FlatBufferModel::VerifyAndBuildFromBuffer(
            tfLiteData, nativeData->size(), nullptr, &errRepLog);
    if (!model) {
        throw runtime_error("Could not verify and build model from buffer");
    }

    // Build the interpreter.
    shared_ptr<tflite::Interpreter> interpreter = buildInterpreter(*model);
    if (!interpreter) {
        throw runtime_error("Could not build an interpreter of the model");
    }

    interpreter->SetNumThreads(LAROD_NBR_OF_THREADS);
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Set " +
              to_string(LAROD_NBR_OF_THREADS) + " thread(s)");

    // Allocate tensor buffers
    if (interpreter->AllocateTensors() != kTfLiteOk) {
        throw runtime_error("Could not allocate tensors");
    }

    // Extract input and output tensor information
    vector<Tensor> inputTensors;
    for (size_t i = 0; i < interpreter->inputs().size(); ++i) {
        const TfLiteTensor* inputTensor =
            interpreter->tensor(interpreter->inputs()[i]);
        if (!inputTensor) {
            throw runtime_error("Invalid input tensor");
        }

        inputTensors.emplace_back(parseTensor(inputTensor));
    }

    vector<Tensor> outputTensors;
    for (size_t i = 0; i < interpreter->outputs().size(); ++i) {
        const TfLiteTensor* outputTensor =
            interpreter->tensor(interpreter->outputs()[i]);
        if (!outputTensor) {
            throw runtime_error("Invalid output tensor");
        }

        outputTensors.emplace_back(parseTensor(outputTensor));
    }

    // Save the interpreter
    interpreters.insert(
        make_pair(modelId, make_tuple(std::move(interpreter), std::move(model),
                                      std::move(nativeModelBuf))));

    return {inputTensors, outputTensors};
}

void TFLite::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxInterpreters);

    auto it = interpreters.find(modelId);
    if (it == interpreters.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    interpreters.erase(it);
}

void TFLite::runJobVirtual(const uint64_t modelId,
                           const vector<Tensor>& inputTensors,
                           vector<Tensor>& outputTensors,
                           const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    // Get the FlatBufferModel and the associated Interpreter. We save local
    // references just in case someone wants to delete it while we run
    // inference.
    shared_ptr<tflite::Interpreter> interpreter;
    shared_ptr<tflite::FlatBufferModel> model;
    shared_ptr<vector<uint8_t>> modelBuf;
    {
        lock_guard<mutex> lockGuard(mtxInterpreters);
        auto it = interpreters.find(modelId);
        if (it == interpreters.end()) {
            // Should never happen...
            throw ModelNotFound("Model " + to_string(modelId) + " not found");
        }

        interpreter = get<0>(it->second);
        model = get<1>(it->second);
        modelBuf = get<2>(it->second);
    }

    // Check tensor sizes.
    vector<TfLiteTensor*> tfInputTensors;
    for (size_t i = 0; i < interpreter->inputs().size(); ++i) {
        TfLiteTensor* tfInputTensor =
            interpreter->tensor(interpreter->inputs()[i]);
        if (!tfInputTensor) {
            // Should never happen...
            throw runtime_error("Invalid input tensor obtained from TFLite");
        }

        if (tfInputTensor->bytes != inputTensors[i].getData().size()) {
            throw TensorMismatch("Mismatch in input tensor size (got " +
                                 to_string(inputTensors[i].getData().size()) +
                                 ", expected " +
                                 to_string(tfInputTensor->bytes) + ")");
        }

        tfInputTensors.push_back(tfInputTensor);
    }

    vector<TfLiteTensor*> tfOutputTensors;
    for (size_t i = 0; i < interpreter->outputs().size(); ++i) {
        TfLiteTensor* tfOutputTensor =
            interpreter->tensor(interpreter->outputs()[i]);
        if (!tfOutputTensor) {
            // Should never happen...
            throw runtime_error("Invalid output tensor obtained from TFLite");
        }

        if (tfOutputTensor->bytes != outputTensors[i].getData().size()) {
            throw TensorMismatch("Mismatch in output tensor size (got " +
                                 to_string(outputTensors[i].getData().size()) +
                                 ", expected " +
                                 to_string(tfOutputTensor->bytes) + ")");
        }

        tfOutputTensors.push_back(tfOutputTensor);
    }

    // We set TfLiteTensors' data pointers to our input and output data (with
    // correct allocated data sizes). We reset them after Invoke().

    // Read input.
    vector<const uint8_t*> oldTfInputTensorDataPtrs;
    for (size_t i = 0; i < tfInputTensors.size(); ++i) {
        oldTfInputTensorDataPtrs.push_back(tfInputTensors[i]->data.uint8);
        tfInputTensors[i]->data.uint8 = inputTensors[i].getData().data();
    }

    // Read output.
    vector<const uint8_t*> oldTfOutputTensorDataPtrs;
    for (size_t i = 0; i < tfOutputTensors.size(); ++i) {
        oldTfOutputTensorDataPtrs.push_back(tfOutputTensors[i]->data.uint8);
        tfOutputTensors[i]->data.uint8 = outputTensors[i].getData().data();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Starting job...");
    if (interpreter->Invoke() != kTfLiteOk) {
        throw runtime_error("Failure when invoking interpreter");
    }
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Job done");

    // Reset TfLiteTensors' data pointers (so that TFLite can deallocate them
    // and we don't leak memory).

    // Reset input.
    for (size_t i = 0; i < oldTfInputTensorDataPtrs.size(); ++i) {
        tfInputTensors[i]->data.uint8 =
            const_cast<uint8_t*>(oldTfInputTensorDataPtrs[i]);
    }

    // Reset output.
    for (size_t i = 0; i < oldTfOutputTensorDataPtrs.size(); ++i) {
        tfOutputTensors[i]->data.uint8 =
            const_cast<uint8_t*>(oldTfOutputTensorDataPtrs[i]);
    }
}

Tensor TFLite::parseTensor(const TfLiteTensor* tensor) {
    auto it = DATA_TYPES.find(tensor->type);
    larodTensorDataType larodType;
    if (it != DATA_TYPES.end()) {
        larodType = it->second;
    } else {
        larodType = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
    }

    // Convert the TFLite int dims array to a size_t array.
    vector<size_t> larodDims;
    for_each(
        tensor->dims->data, tensor->dims->data + tensor->dims->size,
        [&](const int& dim) { larodDims.push_back(static_cast<size_t>(dim)); });

    // Calculate pitches.
    size_t typeSize = 0;
    if (tflite::GetSizeOfType(nullptr, tensor->type, &typeSize) != kTfLiteOk) {
        throw runtime_error("Invalid tensor type.");
    }
    size_t dimsLen = larodDims.size();
    vector<size_t> pitches(dimsLen);
    if (dimsLen > 0) {
        pitches[dimsLen - 1] = larodDims[dimsLen - 1] * typeSize;
        for (size_t i = 1; i < dimsLen; ++i) {
            pitches[dimsLen - 1 - i] =
                pitches[dimsLen - i] * larodDims[dimsLen - 1 - i];
        }
    }

    // As of yet there is no notion of "layout" in tensorflow lite.
    return Tensor(larodType, LAROD_TENSOR_LAYOUT_UNSPECIFIED, larodDims,
                  pitches, tensor->bytes, tensor->name);
}

void TFLite::processJobQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<JobRequest> jobReq;
        try {
            jobReq = popJobQueueWait();
        } catch (InvalidJobRequest& e) {
            // We just ignore this particular request.
            LOG.warning(string("Skipping job request: ") + e.what());
            continue;
        }

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!jobReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Job request is null");
        } else if (jobReq->getException()) { // Check if the request is valid.
            jobReq->signal();
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Skipping job request; it had exceptions");
            continue;
        }

        try {
            runJobVirtual(jobReq->MODEL_ID, jobReq->INPUT_TENSORS,
                          jobReq->outputTensors, jobReq->PARAMS);
        } catch (...) {
            jobReq->setException(current_exception());
        }

        // Signal job has finished.
        jobReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

void TFLite::processLoadModelQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<LoadModelRequest> loadModelReq = popLoadModelQueueWait();

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!loadModelReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Load model request is null");
        }

        try {
            loadModelReq->setTensors(loadModelVirtual(
                const_cast<vector<uint8_t>&>(loadModelReq->DATA),
                loadModelReq->getModelId(), loadModelReq->PARAMS));
        } catch (...) {
            loadModelReq->setException(current_exception());
        }

        // Signal model has been loaded.
        loadModelReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

} // namespace backendunit
} // namespace larod
