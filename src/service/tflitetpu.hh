/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <edgetpu.h>
#include <string.h>

#include "log.hh"
#include "tflite.hh"

namespace larod {
namespace backendunit {

class TFLiteTPU : public TFLite {
public:
    TFLiteTPU();
    ~TFLiteTPU();

    TFLiteTPU(const TFLiteTPU&) = delete;
    TFLiteTPU& operator=(const TFLiteTPU&) = delete;

    larodChip getChip() override { return LAROD_CHIP_TPU; };

private:
    static inline const std::string THIS_NAMESPACE = "TFLiteTPU::";

    std::unique_ptr<edgetpu::EdgeTpuContext> tpuContext;

    std::unique_ptr<tflite::Interpreter>
        buildInterpreter(const tflite::FlatBufferModel& model) override;
};

} // namespace backendunit
} // namespace larod
