/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "cvflowvproc.hh"

#include <cavalry_mem.h>

#include "log.hh"

using namespace std;

namespace {

uint64_t inline align32(uint64_t val) {
    return ((val + 31) >> 5) << 5;
}

} // namespace

namespace larod {
namespace backendunit {

CvFlowVProc::CvFlowVProc() : bin(initVProc()), keepProcessingQueues(true) {
    // FIXME: Unsafe cast to uint32_t from unsigned long (Ambarella is the
    // culprit here...).
    if (vproc_load(getCavalryFd(), static_cast<uint8_t*>(bin.mem.getVirtAddr()),
                   static_cast<uint32_t>(bin.mem.getPhysAddr()),
                   static_cast<uint32_t>(bin.mem.getSize())) < 0) {
        throw runtime_error("Could not load VProc");
    }

    // Set up YUV (actually YCbCr color space defined by the JFIF file format
    // used in JPEG) to RGB conversion matrix.
    yuv2RgbMat.yc = 1;
    yuv2RgbMat.rv = 1.402f;
    yuv2RgbMat.gu = 0.344136f;
    yuv2RgbMat.gv = 0.714136f;
    yuv2RgbMat.bu = 1.772f;
    yuv2RgbMat.yb = 0;

    // Start queue processing threads.
    procQueueThreads.push_back(thread(&CvFlowVProc::processJobQueue, this));
    procQueueThreads.push_back(
        thread(&CvFlowVProc::processLoadModelQueue, this));

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

CvFlowVProc::~CvFlowVProc() {
    // Stop processing queues.
    keepProcessingQueues = false;

    // Eventually notify them if they are waiting.
    stopJobQueueWait();
    stopLoadModelQueueWait();

    for (thread& th : procQueueThreads) {
        try {
            th.join();
        } catch (system_error& e) {
            LOG.error("Could not join thread: " + string(e.what()));
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Stopped queue processing threads");

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

uint32_t CvFlowVProc::initVProc() {
    uint32_t size;
    if (vproc_init(BIN_NAME.c_str(), &size) < 0) {
        throw runtime_error("Could not initialize VProc");
    }

    return size;
}

CvFlowVProc::FirmwareBinary::~FirmwareBinary() {
    if (!mem.getSize()) {
        // If size has not been initiliazed (from initVProc() in
        // CvFlowVProc()), there is nothing to destruct.
        return;
    }

    if (vproc_exit()) {
        LOG.error("Could not exit VProc");
    }
}

CvFlowVProc::Vec::Vec(const size_t height, const size_t width,
                      const size_t pitch) {
    vec.pitch = static_cast<uint32_t>(pitch);
    vec.shape.h = static_cast<uint32_t>(height);
    vec.shape.w = static_cast<uint32_t>(width);
    vec.roi = {0, 0, vec.shape.w, vec.shape.h};
}

CvFlowVProc::Vec::Vec(const size_t height, const size_t width,
                      const size_t pitch,
                      const pair<bool, imageparams::CropInfo>& crop)
    : Vec(height, width, pitch) {
    if (!crop.first) {
        return;
    }

    vec.roi = {static_cast<uint32_t>(crop.second.x),
               static_cast<uint32_t>(crop.second.y),
               static_cast<uint32_t>(crop.second.w),
               static_cast<uint32_t>(crop.second.h)};
}

CvFlowVProc::YuvImg::YuvImg(const size_t height, const size_t width,
                            const size_t pitch)
    : y(height, width, pitch), uv(height / 2, width / 2, pitch) {
    y.vec.shape.d = 1;
    y.vec.color_space = CS_NV12;

    uv.vec.shape.d = 2;
    uv.vec.color_space = CS_NV12;

    byteSize = 3 * height * pitch / 2;
}

CvFlowVProc::RgbImg::RgbImg(size_t height, size_t width, size_t pitch)
    : rgb(height, width, pitch) {
    rgb.vec.shape.d = 3;
    rgb.vec.color_space = CS_RGB;

    byteSize = 3 * height * pitch;
}

void CvFlowVProc::checkScaleFactor(const double widthQuotient,
                                   const double heightQuotient,
                                   const double maxScaleFactor) {
    if ((widthQuotient > maxScaleFactor) ||
        (widthQuotient < (1.0 / maxScaleFactor))) {
        throw invalid_argument("This VProc op only supports up to " +
                               to_string(maxScaleFactor) +
                               "x width scale factors");
    }

    if ((heightQuotient > maxScaleFactor) ||
        (heightQuotient < (1.0 / maxScaleFactor))) {
        throw invalid_argument("This VProc op only supports up to " +
                               to_string(maxScaleFactor) +
                               "x height scale factors");
    }
}

CvFlowVProc::OpType CvFlowVProc::checkOpParams(
    const pair<imageparams::MetaData, imageparams::MetaData>& imgMetaData,
    const pair<bool, imageparams::CropInfo>& crop) {
    auto& [inputMetaData, outputMetaData] = imgMetaData;
    bool formatDiff = inputMetaData.format != outputMetaData.format;
    bool dimsDiff = (inputMetaData.width != outputMetaData.width) ||
                    (inputMetaData.height != outputMetaData.height);
    double maxScaleFactor;
    OpType opType;

    if (crop.first || dimsDiff) {
        if (formatDiff) {
            opType = OpType::DEFORMATION;
            maxScaleFactor = DEFORM_MAX_SCALE_FACTOR;
        } else {
            opType = OpType::CROP_SCALE;
            maxScaleFactor = CROP_SCALE_MAX_FACTOR;
        }
    } else {
        if (!formatDiff) {
            throw invalid_argument("This operation does nothing");
        }
        opType = OpType::CONVERT_FORMAT;
    }

    // Note that the casting is ok due to the limit checks done in
    // checkMetaData() when loading the model.
    if (crop.first) {
        // Compute quotients to compare with maximum scale factors allowed.
        checkScaleFactor(static_cast<double>(crop.second.w) /
                             static_cast<double>(outputMetaData.width),
                         static_cast<double>(crop.second.h) /
                             static_cast<double>(outputMetaData.height),
                         maxScaleFactor);
    } else if (dimsDiff) {
        // Compute quotients to compare with maximum scale factors allowed.
        checkScaleFactor(static_cast<double>(inputMetaData.width) /
                             static_cast<double>(outputMetaData.width),
                         static_cast<double>(inputMetaData.height) /
                             static_cast<double>(outputMetaData.height),
                         maxScaleFactor);
    }

    return opType;
}

void CvFlowVProc::checkMetaData(imageparams::MetaData& metaData,
                                const string& desc) {
    if (!metaData.pitchSpecified) {
        // Enforce the alignment required by the VProc hardware.
        metaData.rowPitch = align32(metaData.width);
    }

    // Check image alignment.
    if (metaData.format == imageparams::Format::NV12) {
        if (metaData.width % NV12_HW_ALIGNMENT) {
            throw invalid_argument(desc +
                                   " in NV12 must have width divisible by " +
                                   to_string(NV12_HW_ALIGNMENT) + " for VProc");
        }

        if (metaData.height % NV12_HW_ALIGNMENT) {
            throw invalid_argument(desc +
                                   " in NV12 must have height divisible by " +
                                   to_string(NV12_HW_ALIGNMENT) + " for VProc");
        }
    }

    if (metaData.rowPitch % PITCH_ALIGNMENT) {
        throw invalid_argument(desc + " must have pitch divisible by " +
                               to_string(PITCH_ALIGNMENT) + " for VProc");
    }

    // Check image format.
    if ((metaData.format != imageparams::Format::NV12) &&
        (metaData.format != imageparams::Format::RGB_PLANAR)) {
        throw invalid_argument(
            desc +
            " format must be in either NV12 or planar rbg format for VProc");
    }
}

CvFlowVProc::Context::Context(
    const pair<imageparams::MetaData, imageparams::MetaData>& metaData)
    : imgMetaData(metaData) {
    auto& [inputMetaData, outputMetaData] = imgMetaData;

    // Verify all general aspects of the meta data.
    checkMetaData(inputMetaData, "Input");
    checkMetaData(outputMetaData, "Output");

    // At this point we don't know if there will be cropping or scaling done
    // by the model, but we can detect whether it will convert format.
    if (inputMetaData.format != outputMetaData.format) {
        if (inputMetaData.format != imageparams::Format::NV12) {
            throw invalid_argument(
                "VProc convert format op only supports NV12 as input format");
        }

        if (outputMetaData.format != imageparams::Format::RGB_PLANAR) {
            throw invalid_argument(
                "VProc convert format op only supports planar "
                "RGB as output format");
        }
    }
}

pair<vector<Tensor>, vector<Tensor>>
    CvFlowVProc::loadModelVirtual(const span<uint8_t>& data,
                                  const uint64_t modelId,
                                  const ParamsMap& params) {
    if (data.size() != 0) {
        throw invalid_argument("VProc does not use model data");
    }

    lock_guard<mutex> lockGuard(mtxContexts);

    shared_ptr<Context> ctx =
        make_shared<Context>(imageparams::getInputOutputInfo(
            params, numeric_limits<uint32_t>::max()));

    if (!contexts
             .emplace(piecewise_construct, forward_as_tuple(modelId),
                      forward_as_tuple(ctx))
             .second) {
        throw runtime_error("Could not save context");
    }

    auto inputs = createTensorsFromMetaData(ctx->imgMetaData.first);
    auto outputs = createTensorsFromMetaData(ctx->imgMetaData.second);

    return {inputs, outputs};
}

vector<Tensor> CvFlowVProc::createTensorsFromMetaData(
    const imageparams::MetaData& metaData) {
    size_t byteSize = imageparams::getByteSize(metaData);

    switch (metaData.format) {
    case imageparams::Format::NV12: {
        return {Tensor(
            LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_420SP,
            {3, metaData.height, metaData.width},
            {byteSize, metaData.height * metaData.rowPitch, metaData.rowPitch},
            byteSize, "YUV420SP")};
    }
    case imageparams::Format::RGB_PLANAR: {
        return {Tensor(LAROD_TENSOR_DATA_TYPE_UINT8, LAROD_TENSOR_LAYOUT_NCHW,
                       {1, 3, metaData.height, metaData.width},
                       {1 * byteSize, byteSize,
                        metaData.height * metaData.rowPitch, metaData.rowPitch},
                       byteSize, "RGB")};
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid image format");
    }
}

void CvFlowVProc::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxContexts);

    auto it = contexts.find(modelId);
    if (it == contexts.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    contexts.erase(it);
}

void CvFlowVProc::runJobVirtual(const uint64_t modelId,
                                const vector<Tensor>& inputTensors,
                                vector<Tensor>& outputTensors,
                                const ParamsMap& params) {
    shared_ptr<Context> ctx;
    {
        lock_guard<mutex> lockGuard(mtxContexts);
        auto it = contexts.find(modelId);
        if (it == contexts.end()) {
            // Should never happen...
            throw ModelNotFound("Model " + to_string(modelId) + " not found");
        }
        ctx = it->second;
    }
    auto& [inputMetaData, outputMetaData] = ctx->imgMetaData;

    // Sanity check sizes (these should have already been checked
    // previously in the call chain).
    assert(inputTensors[0].getByteSize() ==
           imageparams::getByteSize(inputMetaData));
    assert(outputTensors[0].getData().size() ==
           imageparams::getByteSize(outputMetaData));
    assert(inputTensors[0].getPitches().back() == inputMetaData.rowPitch);
    assert(outputTensors[0].getPitches().back() == outputMetaData.rowPitch);

    auto crop = imageparams::getCropInfo(params, inputMetaData.width,
                                         inputMetaData.height,
                                         numeric_limits<uint32_t>::max());

    auto opType = checkOpParams(ctx->imgMetaData, crop);

    // Create CavalryMemory pointers to input and output.
    unique_ptr<CavalryMemory> input =
        make_unique<CavalryMemory>(imageparams::getByteSize(inputMetaData));
    unique_ptr<CavalryMemory> output =
        make_unique<CavalryMemory>(imageparams::getByteSize(outputMetaData));

    // Allocate and copy input data.
    copy(inputTensors[0].getData().begin(), inputTensors[0].getData().end(),
         static_cast<uint8_t*>(input->getVirtAddr()));
    input->flushCache();

    switch (opType) {
    case OpType::CONVERT_FORMAT:
        convertFormat(input, output, inputMetaData, outputMetaData);
        break;
    case OpType::CROP_SCALE:
        cropScale(input, output, inputMetaData, outputMetaData, crop);
        break;
    case OpType::DEFORMATION:
        cropScaleConvert(input, output, inputMetaData, outputMetaData, crop);
        break;
    default:
        // Should never happen...
        throw runtime_error("Invalid VProc operation");
    }

    // Copy output data.
    copy_n(static_cast<uint8_t*>(output->getVirtAddr()), output->getSize(),
           outputTensors[0].getData().begin());
}

void CvFlowVProc::convertFormat(unique_ptr<CavalryMemory>& input,
                                unique_ptr<CavalryMemory>& output,
                                const imageparams::MetaData& inputMetaData,
                                imageparams::MetaData& outputMetaData) {
    YuvImg yuvImg(inputMetaData.height, inputMetaData.width,
                  inputMetaData.rowPitch);
    RgbImg rgbImg(outputMetaData.height, outputMetaData.width,
                  outputMetaData.rowPitch);

    // FIXME: Unsafe casts to uint32_t below from unsigned long
    // (Ambarella is the culprit here...).
    yuvImg.y.vec.data_addr = static_cast<uint32_t>(input->getPhysAddr());
    yuvImg.uv.vec.data_addr = static_cast<uint32_t>(
        yuvImg.y.vec.data_addr + yuvImg.y.vec.shape.h * yuvImg.y.vec.pitch);
    rgbImg.rgb.vec.data_addr = static_cast<uint32_t>(output->getPhysAddr());

    int ret = vproc_yuv2rgb_420(&yuvImg.y.vec, &yuvImg.uv.vec, &rgbImg.rgb.vec,
                                &yuv2RgbMat);
    if (ret < 0) {
        throw runtime_error("Could not perform convert format op");
    }
}

void CvFlowVProc::cropScale(unique_ptr<CavalryMemory>& input,
                            unique_ptr<CavalryMemory>& output,
                            const imageparams::MetaData& inputMetaData,
                            imageparams::MetaData& outputMetaData,
                            const pair<bool, imageparams::CropInfo>& crop) {
    Vec inputImg(inputMetaData.height, inputMetaData.width,
                 inputMetaData.rowPitch, crop);
    Vec outputImg(outputMetaData.height, outputMetaData.width,
                  outputMetaData.rowPitch);

    // TODO: shape.d is set to 3 here as in dlp_preprocessing. It differs
    // from the convert format case as can be seen in the YuvImg constructor.
    inputImg.vec.shape.d = 3;
    outputImg.vec.shape.d = 3;

    switch (inputMetaData.format) {
    case imageparams::Format::NV12: {
        inputImg.vec.color_space = CS_NV12;
        outputImg.vec.color_space = CS_NV12;
        break;
    }
    case imageparams::Format::RGB_PLANAR: {
        inputImg.vec.color_space = CS_RGB;
        outputImg.vec.color_space = CS_RGB;
        break;
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid input image format");
    }

    // FIXME: Unsafe casts to uint32_t below from unsigned long (Ambarella
    // is the culprit here...).
    inputImg.vec.data_addr = static_cast<uint32_t>(input->getPhysAddr());
    outputImg.vec.data_addr = static_cast<uint32_t>(output->getPhysAddr());

    int ret = vproc_resize(&inputImg.vec, &outputImg.vec);
    if (ret < 0) {
        throw runtime_error("Vproc could not perform crop-scale");
    }
}

void CvFlowVProc::cropScaleConvert(
    unique_ptr<CavalryMemory>& input, unique_ptr<CavalryMemory>& output,
    const imageparams::MetaData& inputMetaData,
    imageparams::MetaData& outputMetaData,
    const pair<bool, imageparams::CropInfo>& crop) {
    Vec inputImg(inputMetaData.height, inputMetaData.width,
                 inputMetaData.rowPitch, crop);
    Vec outputImg(outputMetaData.height, outputMetaData.width,
                  outputMetaData.rowPitch);

    // shape.d is set to 3 here as in dlp_preprocessing. It differs from the
    // convert format case as can be seen in the YuvImg constructor.
    inputImg.vec.shape.d = 3;
    outputImg.vec.shape.d = 3;

    switch (inputMetaData.format) {
    case imageparams::Format::NV12: {
        inputImg.vec.color_space = CS_NV12;
        break;
    }
    case imageparams::Format::RGB_PLANAR: {
        inputImg.vec.color_space = CS_RGB;
        break;
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid input image format");
    }

    switch (outputMetaData.format) {
    case imageparams::Format::NV12: {
        outputImg.vec.color_space = CS_NV12;
        break;
    }
    case imageparams::Format::RGB_PLANAR: {
        outputImg.vec.color_space = CS_RGB;
        break;
    }
    default:
        // Should never happen...
        throw invalid_argument("Invalid output image format");
    }

    // FIXME: Unsafe casts to uint32_t below from unsigned long (Ambarella
    // is the culprit here...).
    inputImg.vec.data_addr = static_cast<uint32_t>(input->getPhysAddr());
    outputImg.vec.data_addr = static_cast<uint32_t>(output->getPhysAddr());

    // Unclear if and why this is needed...
    deformation_extra_t deformation_extra;
    memset(&deformation_extra, 0, sizeof(deformation_extra));

    int ret = vproc_image_deformation(&inputImg.vec, &outputImg.vec,
                                      &deformation_extra);
    if (ret < 0) {
        throw runtime_error("Could not perform crop scale convert op");
    }
}

void CvFlowVProc::processJobQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<JobRequest> jobReq;
        try {
            jobReq = popJobQueueWait();
        } catch (InvalidJobRequest& e) {
            // We just ignore this particular request.
            LOG.warning(string("Skipping job request: ") + e.what());
            continue;
        }

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!jobReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Job request is null");
        } else if (jobReq->getException()) { // Check if the request is valid.
            jobReq->signal();
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Skipping job request; it had exceptions");
            continue;
        }

        try {
            runJobVirtual(jobReq->MODEL_ID, jobReq->INPUT_TENSORS,
                          jobReq->outputTensors, jobReq->PARAMS);
        } catch (...) {
            jobReq->setException(current_exception());
        }

        // Signal job has finished.
        jobReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

void CvFlowVProc::processLoadModelQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<LoadModelRequest> loadModelReq = popLoadModelQueueWait();

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!loadModelReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Load model request is null");
        }

        try {
            loadModelReq->setTensors(loadModelVirtual(
                const_cast<vector<uint8_t>&>(loadModelReq->DATA),
                loadModelReq->getModelId(), loadModelReq->PARAMS));
        } catch (...) {
            loadModelReq->setException(current_exception());
        }

        // Signal model has been loaded.
        loadModelReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

} // namespace backendunit
} // namespace larod
