/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "cvflownnctrl.hh"

#include <cavalry/cavalry_ioctl.h>
#include <cavalry_mem.h>
#include <errno.h>
#include <math.h>

#include "model.hh"
#include "modelformat_generated.h"

using namespace std;

namespace larod {
namespace backendunit {

CvFlowNNCtrl::CvFlowNNCtrl() : keepProcessingQueues(true) {
    // Initialize nnctrl.
    if (nnctrl_init(getCavalryFd(), 0) < 0) {
        throw runtime_error("NNCtrl init failed");
    }

    // Start queue processing threads.
    procQueueThreads.push_back(thread(&CvFlowNNCtrl::processJobQueue, this));
    procQueueThreads.push_back(
        thread(&CvFlowNNCtrl::processLoadModelQueue, this));

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Constructed");
}

CvFlowNNCtrl::~CvFlowNNCtrl() {
    // Stop processing queues.
    keepProcessingQueues = false;

    // Eventually notify them if they are waiting.
    stopJobQueueWait();
    stopLoadModelQueueWait();

    for (thread& th : procQueueThreads) {
        try {
            th.join();
        } catch (system_error& e) {
            LOG.error("Could not join thread: " + string(e.what()));
        }
    }
    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Stopped queue processing threads");

    unique_lock<mutex> lock(mtxModels);
    models.clear();
    lock.unlock();

    nnctrl_exit();

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Destructed");
}

CvFlowNNCtrl::Net::Net(const uint8_t* data) : id(-1) {
    // Initialize network.
    net_cfg config{};
    config.net_feed_virt = data;
    config.reuse_mem = 1;
    id = nnctrl_init_net(&config, NULL, NULL);
    if (id < 0) {
        throw runtime_error("Could not initialize net");
    }

    idPtr.reset(&id);

    // Allocate network.
    buf = make_unique<CavalryMemory>(config.net_mem_total);
    net_mem memDesc{};
    memDesc.virt_addr = static_cast<uint8_t*>(buf->getVirtAddr());
    // FIXME: Unsafe cast to uint32_t from unsigned long (Ambarella is the
    // culprit here...).
    memDesc.phy_addr = static_cast<uint32_t>(buf->getPhysAddr());
    memDesc.mem_size = static_cast<uint32_t>(buf->getSize());

    net_input_cfg inputConfig = {};
    net_output_cfg outputConfig = {};
    if (nnctrl_get_net_io_cfg(*idPtr, &inputConfig, &outputConfig) < 0) {
        throw runtime_error("Could not get net configuration");
    }

    if (nnctrl_load_net(*idPtr, &memDesc, &inputConfig, &outputConfig) < 0) {
        throw runtime_error("Could not load net");
    }

    buf->flushCache();
}

larodTensorDataType CvFlowNNCtrl::getDataType(uint8_t sign, uint8_t sz,
                                              uint8_t expbits) {
    for (const auto& [fmt, type] : DATA_TYPES) {
        if (fmt.sign == sign && fmt.size == sz && fmt.expbits == expbits) {
            return type;
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ +
              "(): Unknown data type (sign=" + to_string(sign) +
              ", sz=" + to_string(sz) + ", expbits=" + to_string(expbits) +
              "), defaulting to unspecifed");

    return LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
}

template<typename T> Tensor CvFlowNNCtrl::createTensor(T desc) {
    auto larodType = getDataType(desc->data_fmt.sign, desc->data_fmt.size,
                                 desc->data_fmt.expbits);
    uint8_t sizeBytes = static_cast<uint8_t>(1 << desc->data_fmt.size);
    size_t sz = desc->size;

    // Calculate pitches.
    vector<size_t> pitches(4);
    pitches[3] =
        static_cast<size_t>(ceil(desc->dim.width * sizeBytes / 32.)) * 32;
    pitches[2] = pitches[3] * desc->dim.height;
    pitches[1] = pitches[2] * desc->dim.depth;
    pitches[0] = pitches[1] * desc->dim.plane;

    return Tensor(
        larodType, LAROD_TENSOR_LAYOUT_UNSPECIFIED,
        {desc->dim.plane, desc->dim.depth, desc->dim.height, desc->dim.width},
        pitches, sz, desc->name);
}

pair<vector<Tensor>, vector<Tensor>> CvFlowNNCtrl::getTensorInfo(int netId) {
    net_input_cfg netIn = {};
    net_output_cfg netOut = {};

    if (nnctrl_get_net_io_cfg(netId, &netIn, &netOut) < 0) {
        throw runtime_error("NNCtrl get net io failed");
    }

    vector<Tensor> inputTensors;
    for (unsigned int i = 0; i < netIn.in_num; i++) {
        inputTensors.push_back(createTensor(&netIn.in_desc[i]));
    }

    vector<Tensor> outputTensors;
    for (unsigned int i = 0; i < netOut.out_num; i++) {
        outputTensors.push_back(createTensor(&netOut.out_desc[i]));
    }

    return {inputTensors, outputTensors};
}

pair<vector<Tensor>, vector<Tensor>>
    CvFlowNNCtrl::loadModelVirtual(const span<uint8_t>& data,
                                   const uint64_t modelId,
                                   const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    // Check model format.
    const modelformat::Model* fbModel = verifyFbModel(data);

    const modelformat::CvFlow* fbCvFlow = fbModel->backEndData_as_CvFlow();
    if (!fbCvFlow) {
        throw invalid_argument(
            "Failed model format type check, got type: " +
            string(
                modelformat::EnumNamesBackEnd()[fbModel->backEndData_type()]) +
            " and expected " +
            string(
                modelformat::EnumNamesBackEnd()[modelformat::BackEnd_CvFlow]));
    }

    shared_ptr<Net> net = make_shared<Net>(fbCvFlow->nativeData()->data());

    // Extract tensor metadata
    auto [inputTensors, outputTensors] = getTensorInfo(net->getId());

    unique_lock<mutex> lock(mtxModels);
    bool ret = models.insert(make_pair(modelId, net)).second;
    lock.unlock();

    if (!ret) {
        // Should never happen...
        throw runtime_error("Could not load model: ID " + to_string(modelId) +
                            " already exists");
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Model " + to_string(modelId) +
              " loaded (id=" + to_string(net->getId()) + ")");

    return {inputTensors, outputTensors};
}

void CvFlowNNCtrl::deleteModelVirtual(const uint64_t modelId) {
    lock_guard<mutex> lockGuard(mtxModels);

    auto it = models.find(modelId);
    if (it == models.end()) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    models.erase(it);

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Deleted model " +
              to_string(modelId));
}

void CvFlowNNCtrl::runJobVirtual(const uint64_t modelId,
                                 const vector<Tensor>& inputTensors,
                                 vector<Tensor>& outputTensors,
                                 const ParamsMap& params) {
    // We don't support any additional parameters.
    if (params.size()) {
        throw runtime_error("Additional parameters are not supported");
    }

    // Get the Net. We save a local reference just in case someone wants to
    // delete it while we run inference.
    shared_ptr<Net> net;
    try {
        lock_guard<mutex> lockGuard(mtxModels);
        net = models.at(modelId);
    } catch (...) {
        // Should never happen...
        throw ModelNotFound("Model " + to_string(modelId) + " not found");
    }

    net_input_cfg netIn = {};
    net_output_cfg netOut = {};
    if (nnctrl_get_net_io_cfg(net->getId(), &netIn, &netOut) < 0) {
        throw runtime_error("NNCtrl get net io failed");
    }

    // Check nbr of input tensors.
    if (netIn.in_num != inputTensors.size()) {
        throw TensorMismatch("Mismatch in nbr of input tensors (got " +
                             to_string(inputTensors.size()) + ", expected " +
                             to_string(netIn.in_num));
    }

    // Check nbr of output tensors.
    if (netOut.out_num != outputTensors.size()) {
        throw TensorMismatch("Mismatch in nbr of output tensors (got " +
                             to_string(outputTensors.size()) + ", expected " +
                             to_string(netOut.out_num));
    }

    // Check and copy input tensor sizes.
    for (unsigned int i = 0; i < netIn.in_num; i++) {
        size_t sz = netIn.in_desc[i].size;
        if (sz != inputTensors[i].getData().size()) {
            throw TensorMismatch("Mismatch in input tensor size (got " +
                                 to_string(inputTensors[i].getData().size()) +
                                 ", expected " + to_string(sz) + ")");
        }

        auto src = inputTensors[i].getData().begin();
        auto dst = netIn.in_desc[i].virt;
        copy_n(src, netIn.in_desc[i].size, dst);

        if (cavalry_mem_sync_cache(netIn.in_desc[i].size, netIn.in_desc[i].addr,
                                   CAVALRY_MEM_CLEAN_CACHE,
                                   !CAVALRY_MEM_INVALIDATE_CACHE) < 0) {
            throw runtime_error("NNCtrl mem sync cach failed");
        }
    }

    // Check output tensor sizes.
    for (unsigned int i = 0; i < netOut.out_num; i++) {
        size_t sz = netOut.out_desc[i].size;
        if (sz != outputTensors[i].getData().size()) {
            throw TensorMismatch("Mismatch in output tensor size (got " +
                                 to_string(outputTensors[i].getData().size()) +
                                 ", expected " + to_string(sz) + ")");
        }
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Running job... ");

    net_result net_ret = {};
    int result = nnctrl_run_net(net->getId(), &net_ret, NULL, &netIn, &netOut);
    int retry = 10;
    while (result == -EAGAIN && retry) {
        result = nnctrl_resume_net(net->getId(), &net_ret);
        retry--;
    }

    if (result < 0) {
        throw runtime_error("NNCtrl run net failed: " + to_string(result));
    }

    // Copy output tensor.
    for (unsigned int i = 0; i < netOut.out_num; i++) {
        if (cavalry_mem_sync_cache(
                netOut.out_desc[i].size, netOut.out_desc[i].addr,
                !CAVALRY_MEM_CLEAN_CACHE, CAVALRY_MEM_INVALIDATE_CACHE) < 0) {
            throw runtime_error("NNCtrl mem sync cach failed");
        }

        auto src = netOut.out_desc[i].virt;
        auto dst = outputTensors[i].getData().begin();
        copy_n(src, netOut.out_desc[i].size, dst);
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Job done");
}

void CvFlowNNCtrl::processJobQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<JobRequest> jobReq;
        try {
            jobReq = popJobQueueWait();
        } catch (InvalidJobRequest& e) {
            // We just ignore this particular request.
            LOG.warning(string("Skipping job request: ") + e.what());
            continue;
        }

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!jobReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Job request is null");
        } else if (jobReq->getException()) { // Check if the request is valid.
            jobReq->signal();
            LOG.debug(THIS_NAMESPACE + __func__ +
                      "(): Skipping job request; it had exceptions");
            continue;
        }

        try {
            runJobVirtual(jobReq->MODEL_ID, jobReq->INPUT_TENSORS,
                          jobReq->outputTensors, jobReq->PARAMS);
        } catch (...) {
            jobReq->setException(current_exception());
        }

        // Signal job has finished.
        jobReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

void CvFlowNNCtrl::processLoadModelQueue() {
    LOG.debug(THIS_NAMESPACE + __func__ + "(): Started");

    while (keepProcessingQueues) {
        shared_ptr<LoadModelRequest> loadModelReq = popLoadModelQueueWait();

        // Check for nullptr, since that is returned if the wait is canceled.
        if (!loadModelReq) {
            if (!keepProcessingQueues) {
                // Returned from wait with signal to stop further processing.
                break;
            }

            throw runtime_error("Load model request is null");
        }

        try {
            loadModelReq->setTensors(loadModelVirtual(
                const_cast<vector<uint8_t>&>(loadModelReq->DATA),
                loadModelReq->getModelId(), loadModelReq->PARAMS));
        } catch (...) {
            loadModelReq->setException(current_exception());
        }

        // Signal model has been loaded.
        loadModelReq->signal();
    }

    LOG.debug(THIS_NAMESPACE + __func__ + "(): Stopped");
}

} // namespace backendunit
} // namespace larod
