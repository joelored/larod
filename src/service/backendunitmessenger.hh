/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "backendunit.hh"

namespace larod {

/**
 * @brief Proxy object for selectively grant member access to @c BackEndUnit.
 *
 * Only @c LoadModelRequest can use @ref addModel() to tell @c BackEndUnit to
 * post process a request.
 */
class BackEndUnitMessenger {
    friend class LoadModelRequest;

private:
    static void addModel(BackEndUnit* backEndUnit, LoadModelRequest* req) {
        backEndUnit->addModel(req);
    }
};

} // namespace larod
