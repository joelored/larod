/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <variant>
#include <vector>

#include "asyncmsg.hh"
#include "tensor.hh"

namespace larod {

class JobRequest : public AsyncMsg {
public:
    // Type to hold file descriptor, maximum accessable size (bytes) and data
    // start offset (bytes) from the beginning of the file.
    using FdInfo = std::tuple<int, const size_t, const int64_t>;

    using ParamsMap = std::unordered_map<
        std::string, std::variant<int64_t, std::string, std::vector<int64_t>>>;

    JobRequest(std::weak_ptr<Session> session, const uint64_t modelId,
               const std::vector<Tensor>& inputTensors,
               std::vector<std::vector<uint8_t>>&& inputTensorsData,
               const std::vector<Tensor>& outputTensors,
               std::vector<std::vector<uint8_t>>&& outputTensorsData,
               const std::vector<FdInfo>& outputFdInfos, const uint8_t priority,
               const uint64_t metaData, const ParamsMap&& params);
    ~JobRequest();

    void signal() override;

    bool operator<(const JobRequest& other) const {
        if (this->PRIORITY == other.PRIORITY) {
            // Compare if this is older than other.
            return this->CREATION_TIME > other.CREATION_TIME;
        }

        return this->PRIORITY < other.PRIORITY;
    }

    const uint64_t MODEL_ID;
    const std::vector<Tensor> INPUT_TENSORS;
    std::vector<Tensor> outputTensors;
    const std::vector<FdInfo> OUTPUT_FD_INFOS;
    const uint8_t PRIORITY;
    const uint64_t META_DATA;
    const ParamsMap PARAMS;

private:
    const std::vector<std::vector<uint8_t>> INPUT_TENSORS_DATA;
    std::vector<std::vector<uint8_t>> outputTensorsData;
};

} // namespace larod
