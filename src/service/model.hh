/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <functional>
#include <string>
#include <vector>

#include "larod.h"
#include "tensor.hh"

namespace larod {

/**
 * @brief Class to represent a model.
 *
 * This is almost just an aggregate class with some meta-data about a model. It
 * is used only for "bookkeeping" models (no actual binary model data or binary
 * tensor data is stored).
 */
class Model {
public:
    enum class Access {
        PRIVATE = LAROD_ACCESS_PRIVATE,
        PUBLIC = LAROD_ACCESS_PUBLIC
    };

    Model(const uint64_t id, const std::string name, const size_t sz,
          const Access access, const std::vector<Tensor> inputTensors,
          const std::vector<Tensor> outputTensors);

    bool operator==(const Model& other) const;
    operator std::string() const;

    uint64_t getId() const;
    std::string getName() const;
    size_t getSize() const;
    Access getAccessType() const;
    std::pair<std::vector<Tensor>, std::vector<Tensor>> getTensors() const {
        return {INPUT_TENSORS, OUTPUT_TENSORS};
    }

private:
    const uint64_t ID;
    const std::string NAME;
    const size_t SIZE;
    const Access ACCESS;
    const std::vector<Tensor> INPUT_TENSORS;
    const std::vector<Tensor> OUTPUT_TENSORS;
};

} // namespace larod
