/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

namespace larod {

typedef struct args_t {
    // fill eventually
} args_t;

int parseArgs(int argc, char** argv, args_t* args);

} // namespace larod
