/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <atomic>
#include <condition_variable>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <shared_mutex>
#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

#include "joborder.hh"
#include "jobrequest.hh"
#include "larod.h"
#include "loadmodelrequest.hh"
#include "model.hh"
#include "modelformat_generated.h"
#include "span.hh"

// Define a hasher for std::reference_wrapper<larod::Model>
namespace std {

template<> struct hash<reference_wrapper<larod::Model>> {
    size_t operator()(const reference_wrapper<larod::Model>& model) const {
        return hash<uint64_t>()(model.get().getId());
    }
};

} // namespace std

namespace larod {

// Define a comparison function for std::reference_wrapper<Model>
struct refModelIsEqual {
    bool operator()(const std::reference_wrapper<Model>& lhs,
                    const std::reference_wrapper<Model>& rhs) const {
        return lhs.get().getId() == rhs.get().getId();
    }
};

class BackEndUnit { // TODO: template modelId type and callerId type
    friend class BackEndUnitMessenger;

public:
    BackEndUnit();
    virtual ~BackEndUnit() = default;

    using ParamsMap = std::unordered_map<
        std::string, std::variant<int64_t, std::string, std::vector<int64_t>>>;

    Model loadModel(const uint64_t callerId, const span<uint8_t>& data,
                    const Model::Access access, const std::string& name,
                    const ParamsMap& params = {});
    void deleteModel(const uint64_t callerId, const uint64_t modelId);
    void runJob(const uint64_t callerId, const uint64_t modelId,
                const std::vector<Tensor>& inputTensors,
                std::vector<Tensor>& outputTensors,
                const ParamsMap& params = {});
    void queueJob(std::shared_ptr<JobOrder> jobOrder);
    void queueLoadModel(std::shared_ptr<LoadModelRequest> loadModelReq);
    std::unordered_map<uint64_t, Model> getModels() const;
    std::vector<size_t> getOutputByteSizes(const uint64_t modelId);
    virtual larodChip getChip() = 0;

protected:
    virtual std::pair<std::vector<Tensor>, std::vector<Tensor>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) = 0;
    virtual void deleteModelVirtual(const uint64_t modelId) = 0;
    virtual void runJobVirtual(const uint64_t modelId,
                               const std::vector<Tensor>& inputTensors,
                               std::vector<Tensor>& outputTensors,
                               const ParamsMap& params) = 0;
    virtual int getMinModelFormatVersion() const = 0;

    /**
     * @brief Parses a Flatbuffer representation of a model.
     *
     * In-memory parses a flatbuffer representation of a model. Also verifies
     * that the schema version is compatible with the backend minimum required
     * version (@see getMinModelFormatVersion()).
     *
     * This should be called from backends uses flatbuffers as a model format.
     * TODO: Redesign this for 2.0 so that we don't break the class call
     * hierarchy of object oriented design.
     *
     * @param model The raw data for the model.
     * @return The Flatbuffer structure representing the model.
     */
    virtual const modelformat::Model* verifyFbModel(const span<uint8_t>& data);

    /**
     * @brief Pops the job queue or waits for a request if empty.
     *
     * This function return the first JobRequest in line, if one exists.
     * If the queue is empty, it waits for until an JobRequest is
     * available. It also checks if the model for this JobRequest is valid
     * (ModelNotFound will be set as exception in returned JobRequest
     * otherwise) and if JobRequest has permissions to run the model
     * (PermissionDenied will be set as exception in returned JobRequest
     * otherwise).
     *
     * @return The JobRequest first in line.
     */
    std::shared_ptr<JobRequest> popJobQueueWait();

    std::shared_ptr<LoadModelRequest> popLoadModelQueueWait();
    void stopLoadModelQueueWait();
    void stopJobQueueWait();
    size_t getJobQueueSize() const;
    size_t getLoadModelQueueSize() const;

private:
    static inline std::string THIS_NAMESPACE = "BackEndUnit::";
    static inline uint64_t modelId = 0; ///< Unique model IDs

    /// Map mapping model ID to loaded models
    std::unordered_map<uint64_t, Model> models;
    /// Map mapping loaded models to creators
    std::unordered_map<std::reference_wrapper<Model>, uint64_t,
                       std::hash<std::reference_wrapper<Model>>,
                       refModelIsEqual>
        modelCreators;
    mutable std::mutex mtxModels; ///< Mutex for models map.
    mutable std::mutex mtxJobQueue;
    mutable std::mutex mtxLoadModelQueue; ///< Mutex for loading model queue.
    /// Queue for orders of asynchronous jobs.
    std::queue<std::shared_ptr<JobOrder>> jobQueue;
    /// Queue for asynchronous loading of models.
    std::queue<std::shared_ptr<LoadModelRequest>> loadModelQueue;

    std::condition_variable_any condVarJobQueue;
    std::condition_variable condVarLoadModelQueue;
    std::atomic<bool> waitForJobQueue;
    std::atomic<bool> waitForLoadModelQueue;

    /**
     * @brief Check if caller has permissions for operating on a model.
     *
     * If the model is public, this will always return true. Otherwise look up
     * the creator for the model and compare it with the caller.
     *
     * @param callerId ID of the caller.
     * @param model Model to check permissions on.
     * @return True if caller has permissions, otherwise false.
     */
    bool hasPermission(const uint64_t callerId, Model& modelId);
    Model& addModel(const uint64_t& callerId, const uint64_t& modelId,
                    const std::string& name, const size_t& size,
                    const Model::Access& access,
                    const std::vector<Tensor> inputTensors,
                    const std::vector<Tensor> outputTensors);

    /**
     * @brief Used by asynchronous requests which needs to post process after a
     * succesful load of a model.
     *
     * Accessible through proxy object @c BackEndUnitMessenger.
     *
     * @param req Request which has been processed and needs to update the model
     * collection.
     */
    void addModel(LoadModelRequest* req);

    /**
     * @brief Check if pairs of input and output tensors are matching.
     *
     * Checks if the pairs @p lhs of input and output tensors have the same
     * sizes and equal attributes (@c Tensor::assertEqual) as the @p rhs pairs
     * of tensors. This will throw if @p lhs differs from @p rhs.
     *
     * @param lhs Pairs of input and output tensors to check against @p rhs.
     * @param rhs Pairs of input and output tensors that @p lhs will be matched
     * against.
     */
    void checkTensors(
        const std::pair<std::vector<Tensor>, std::vector<Tensor>>& lhs,
        const std::pair<std::vector<Tensor>, std::vector<Tensor>>& rhs);

    /**
     * @brief Check if a job request is valid.
     *
     * This checks if a job request has a valid model, sufficient permissions
     * and more. It sets an appropriate exception on @p jobReq
     * (@c jobReq.setException(...)) in case of errors.
     *
     * @param jobReq Job request to check.
     */
    void checkJobRequest(JobRequest& jobReq);

    /**
     * @brief Pops a queue.
     *
     * Throws @c runtime_error if queue is empty.
     *
     * @param queue Queue to pop.
     * @return Element at the front of queue.
     */
    template<typename T> static T popQueue(std::queue<T>& queue);

    /**
     * @brief Pops a queue containing JobOrder and returns the JobRequest.
     *
     * This retrieves an JobRequest from the JobOrder. Throws @c runtime_error
     * if queue is empty or InvalidJobRequest if the Session associated with the
     * JobOrder has expired.
     *
     * @param queue Queue to pop.
     * @return JobRequest from the JobOrder at the front of queue.
     */
    static std::shared_ptr<JobRequest>
        popQueue(std::queue<std::shared_ptr<JobOrder>>& queue);

    /**
     * @brief Waits for a queue to be non-empty.
     *
     * The condition variable @p condVar is used for waiting using the lock @p
     * lock. The predicate for the wait is obviously the size of the queue @p
     * queue and an additionally an optional bool @p pred. If @p pred is false,
     * the wait stops (after a signal to @p condVar).
     *
     * @param queue Queue to wait for.
     * @param condVar Condition varaiable to use for the waiting.
     * @param lock Lock to use with @p condVar (should already be acquired).
     * @param pred Additional predicate to check for the wait predicate. If @p
     * queue is empty and @p pred is false, @p condVar stops waiting. Otherwise,
     * if @pred is true, it keeps waiting.
     */
    template<typename T, typename C, typename L>
    static void waitForQueue(std::queue<T>& queue, C& condVar,
                             std::unique_lock<L>& lock,
                             std::atomic<bool>& pred);

public:
    struct ModelNotFound : std::runtime_error {
        ModelNotFound(const std::string& msg) : runtime_error(msg) {}
    };

    struct PermissionDenied : std::runtime_error {
        PermissionDenied(const std::string& msg) : runtime_error(msg) {}
    };

    struct TensorMismatch : std::invalid_argument {
        TensorMismatch(const std::string& msg) : invalid_argument(msg) {}
    };

    /**
     * @brief Exception to represent an invalid job request.
     *
     * An invalid job request can be encountered for instance when a
     * Session has expired for a particular JobOrder.
     */
    struct InvalidJobRequest : std::runtime_error {
        InvalidJobRequest(const std::string& msg) : runtime_error(msg) {}
    };
};

} // namespace larod
