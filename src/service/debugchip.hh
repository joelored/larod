/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>
#include <thread>
#include <unordered_set>

#include "backendunit.hh"

namespace larod {
namespace backendunit {

class DebugChip : public BackEndUnit {
public:
    DebugChip();
    ~DebugChip();

    std::pair<std::vector<Tensor>, std::vector<Tensor>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       const std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;
    larodChip getChip() override { return LAROD_CHIP_DEBUG; };

    static inline const size_t NBR_OF_INPUT_TENSORS = 1;
    static inline const size_t NBR_OF_OUTPUT_TENSORS = 1;

    static inline const size_t INPUT_TENSOR_BYTE_SIZE = 1;
    static inline const size_t OUTPUT_TENSOR_BYTE_SIZE = 4096;
    static const larodTensorDataType TENSOR_DATA_TYPE =
        LAROD_TENSOR_DATA_TYPE_UINT8;

    static const larodTensorLayout INPUT_TENSOR_LAYOUT =
        LAROD_TENSOR_LAYOUT_UNSPECIFIED;
    static const larodTensorLayout OUTPUT_TENSOR_LAYOUT =
        LAROD_TENSOR_LAYOUT_UNSPECIFIED;

    static inline const std::vector<size_t> INPUT_TENSOR_DIMS = {
        INPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
    static inline const std::vector<size_t> INPUT_TENSOR_PITCHES = {
        INPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
    static inline const std::vector<size_t> OUTPUT_TENSOR_DIMS = {
        OUTPUT_TENSOR_BYTE_SIZE, 1, 1, 1};
    static inline const std::vector<size_t> OUTPUT_TENSOR_PITCHES = {
        OUTPUT_TENSOR_BYTE_SIZE, 1, 1, 1};

    static inline const std::string INPUT_TENSOR_NAME = "DebugChipInput";
    static inline const std::string OUTPUT_TENSOR_NAME = "DebugChipOutput";

    static inline const std::vector<Tensor> INPUT_TENSORS = {
        NBR_OF_INPUT_TENSORS,
        Tensor(TENSOR_DATA_TYPE, INPUT_TENSOR_LAYOUT, INPUT_TENSOR_DIMS,
               INPUT_TENSOR_PITCHES, INPUT_TENSOR_BYTE_SIZE,
               INPUT_TENSOR_NAME)};
    static inline const std::vector<Tensor> OUTPUT_TENSORS = {
        NBR_OF_OUTPUT_TENSORS,
        Tensor(TENSOR_DATA_TYPE, OUTPUT_TENSOR_LAYOUT, OUTPUT_TENSOR_DIMS,
               OUTPUT_TENSOR_PITCHES, OUTPUT_TENSOR_BYTE_SIZE,
               OUTPUT_TENSOR_NAME)};

private:
    static inline const std::string THIS_NAMESPACE = "DebugChip::";
    /// Number of simulating parallel jobs and load model threads.
    static const size_t HW_CONCURRENCY = 1;

    std::mutex mtxModels; ///< Mutex for model set.
    std::unordered_set<uint64_t> models;
    std::vector<std::thread> procJobQueueThreads;
    std::vector<std::thread> procLoadModelQueueThreads;
    std::atomic<bool> keepProcessingQueues;

    int getMinModelFormatVersion() const override { return 0; }
    void processJobQueue();
    void processLoadModelQueue();
};

} // namespace backendunit
} // namespace larod
