/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

/**
 * This message is sent (by sd-bus itself) when connection is reset by peer.
 */
#define LAROD_MSG_DISCONNECTED "Disconnected"

/**
 * client -> server:
 * s: unique D-Bus name
 */
#define LAROD_MSG_HANDSHAKE "Handshake"

/**
 * client -> server:
 * h: model fd, t: model size, t: chip ID, i: model access, s: model name,
 * t: callback pointer, a{sv}: additional parameters.
 *
 * The additional parameters dictionary at the end are optional. Server should
 * explicitly check for this before reading.
 *
 * server -> client:
 * r(
 *   r(t: model size, t: model ID, t: chip ID, i: chip type, i: model access,
 *     s: model name,
 *     a(r(i: data type, i: layout, t: byte size, a(t: dims), a(t: pitches))),
 *     a(r(i: data type, i: layout, t: byte size, a(t: dims), a(t: pitches)))
 *   ),
 *   t: callback pointer, i: error code, s: error message
 * )
 *
 * The inner struct contains the model size, model ID, chip ID, chip type
 * (larodChip enum), model access specifier, model name, array of input tensor
 * metadata and array of output tensor metadata. Tensor metadata arrays contains
 * data type, layout, byte size, array of dimensions and array of pitches.
 */
#define LAROD_MSG_QUEUE_LOAD_MODEL "QueueLoadModel"

/**
 * client -> server:
 * t: model ID, t: chip ID, y: priority, t: callback pointer,
 * a(
 *   r(i: data type, i: layout, a(t: dims), h: fd, t: fd size, x: fd offset)
 * ),
 * a(
 *   r(i: data type, i: layout, a(t: dims), h: fd, t: fd size, x: fd offset)
 * ),
 * a{sv}: additional parameters.
 *
 * The arrays contains structs describing input and output tensors' data
 * (respectively). The additional parameters at the end are optional. Server
 * should explicitly check for this before reading.
 *
 * server -> client:
 * t: callback pointer, i: error code, s: error message.
 */
#define LAROD_MSG_QUEUE_JOB "QueueJob"

/**
 * client -> server:
 * t: model ID, t: chip ID, t: callback pointer.
 *
 * server -> client:
 * t: callback pointer, i: error code, s: error message.
 */
#define LAROD_MSG_DELETE_MODEL "DeleteModel"
