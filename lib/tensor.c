/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tensor.h"

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <systemd/sd-bus.h>
#include <unistd.h>

#include "error.h"

#ifndef LAROD_UNIT_TEST
/**
 * @brief Create an emtpy @c larodTensor handle.
 *
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise pointer to @c larodTensor.
 */
static larodTensor* createTensor(larodError** error);

/**
 * @brief Destroy a single @c larodTensor.
 *
 * @param tensor The object to release. The function does nothing if @p tensor
 * is NULL.
 */
static void destroyTensor(larodTensor* tensor);

/**
 * @brief Retrieve fd size for tensor.
 *
 * Determines the maximum capacity (number of bytes that larod service is
 * allowed to access) for the file descriptor in a tensor. If client have
 * explicitly set the size then this size is returned. Otherwise lseek() to
 * the end of each fd is used to determine file size.
 *
 * @param tensor Pointer to tensor.
 * @param fdSize Ouput pointer, will be filled with tensor fd size.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return False if any error occurred, otherwise true.
 */
static bool getFdSize(const larodTensor* tensor, uint64_t* fdSize,
                      larodError** error);
#else
// Remove static declaration if LAROD_UNIT_TEST is defined in order to be able
// to unit test these functions.
larodTensor* createTensor(larodError** error);
void destroyTensor(larodTensor* tensor);
bool getFdSize(const larodTensor* tensor, uint64_t* fdSize, larodError** error);
#endif

/**
 * @brief Write tensor data to sd-bus message.
 *
 * @param msg Message to write tensor into.
 * @param tensor Tensor to be written into a message.
 * @param skipSizeSeek Set to true to skip trying to determine fd size if
 * tensor sizes are 0.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
static bool tensorToMsg(sd_bus_message* msg, const larodTensor* tensor,
                        bool skipSizeSeek, larodError** error);

/**
 * @brief Extract tensor meta data from D-Bus message.
 *
 * Reading from D-Bus array is handled in the following way: If a tensor (meta
 * data) was read from an array then @p tensorResult is filled with a valid
 * pointer and true is returned. If the D-Bus array reached the end then @p
 * tensorResult will be set to NULL and true is returned. Finally if some error
 * occurred then @p tensorResult will be untouched and false is returned,
 * possibly along with a newly created @c larodError. Tensor fd is set to -1 on
 * a successfully read tensor.
 *
 * @param msg Message to read tensor from.
 * @param tensorResult Output pointer to be filled with newly allocated tensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any error occurred, otherwise true.
 */
static bool tensorMetaDataFromMsg(sd_bus_message* msg,
                                  larodTensor** tensorResult,
                                  larodError** error);

larodTensor** tensorMetaDataArrayFromMsg(sd_bus_message* msg,
                                         size_t* numTensors,
                                         larodError** error) {
    assert(msg);
    assert(numTensors);

    size_t numRead = 0;
    larodTensor** tensors = NULL;

    int ret = sd_bus_message_enter_container(
        msg, 'a', "(" TENSOR_METADATA_FROM_SERVICE ")");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not enter tensor array: %s",
                         strerror(ret));

        goto error;
    }

    size_t arrayLen = 1;
    tensors = malloc(arrayLen * sizeof(larodTensor*));
    if (!tensors) {
        larodCreateError(error, ENOMEM, "Could not allocate tensor array");
        goto error;
    }

    while (true) {
        larodTensor* tensor = NULL;
        if (!tensorMetaDataFromMsg(msg, &tensor, error)) {
            goto error;
        }

        if (!tensor) {
            // End of array has been reached.
            break;
        }

        if (numRead >= arrayLen) {
            // Buffer is too small, reallocate
            arrayLen *= 2;
            larodTensor** newTensors =
                realloc(tensors, arrayLen * sizeof(larodTensor*));
            if (!newTensors) {
                destroyTensor(tensor);
                larodCreateError(error, ENOMEM,
                                 "Could not reallocate tensor array");

                goto error;
            }
            tensors = newTensors;
        }

        tensors[numRead] = tensor;
        numRead++;
    }

    // Exit output tensor array.
    ret = sd_bus_message_exit_container(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not exit tensor array: %s",
                         strerror(ret));

        goto error;
    }

    *numTensors = numRead;
    return tensors;

error:
    larodDestroyTensors(&tensors, numRead);
    return NULL;
}

/**
 * @brief: Extracts an array of size_t from sd-bus message
 *
 * Used to retrieve dimensions and pitches in tensorMetaDataFromMsg.
 */
bool readArrayFromMsg(sd_bus_message* msg, size_t* array, size_t* arrayLen,
                      const char* arrayName, larodError** error) {
    // Enter dims array.
    int ret = sd_bus_message_enter_container(msg, 'a', "t");

    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not enter %s array: %s", arrayName,
                         strerror(ret));

        return false;
    }

    while (true) {
        uint64_t element = 0;
        ret = sd_bus_message_read(msg, "t", &element);

        if (ret < 0) {
            ret = -ret;
            larodCreateError(error, ret, "Could not read %s element: %s",
                             arrayName, strerror(ret));

            return false;
        } else if (ret == 0) {
            // End of array reached
            break;
        }

        if (*arrayLen > LAROD_TENSOR_MAX_LEN) {
            larodCreateError(error, ERANGE,
                             "Too many %s in message when reading tensor",
                             arrayName);

            return false;
        }

        assert(element <= SIZE_MAX);

        // Cap element value to SIZE_MAX, if too big.
        array[*arrayLen] = element <= SIZE_MAX ? (size_t) element : SIZE_MAX;

        (*arrayLen)++;
    }

    // Exit dims array.
    ret = sd_bus_message_exit_container(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not exit %s array: %s", arrayName,
                         strerror(ret));

        return false;
    }

    return true;
}

bool tensorMetaDataFromMsg(sd_bus_message* msg, larodTensor** tensorResult,
                           larodError** error) {
    assert(msg);
    assert(tensorResult);

    int ret = sd_bus_message_enter_container(
        msg, 'r', "" TENSOR_METADATA_FROM_SERVICE "");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not enter tensor struct: %s",
                         strerror(ret));

        return false;
    } else if (ret == 0) {
        // End of sd-bus array.
        *tensorResult = NULL;
        return true;
    }

    larodTensor* tensor = createTensor(error);
    if (!tensor) {
        return false;
    }

    uint32_t dataType = 0;
    uint32_t layout = 0;
    uint64_t byteSize = 0;
    ret = sd_bus_message_read(msg, "iit", &dataType, &layout, &byteSize);

    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not read tensor metadata: %s",
                         strerror(ret));

        goto error;
    }

    if (!larodSetTensorDataType(tensor, dataType, error)) {
        goto error;
    }

    if (!larodSetTensorLayout(tensor, layout, error)) {
        goto error;
    }

    if (byteSize >= SIZE_MAX) {
        larodCreateError(error, EOVERFLOW,
                         "Tensor byte size larger than SIZE_MAX: %s",
                         strerror(EOVERFLOW));

        goto error;
    }
    tensor->byteSize = (size_t) byteSize;

    larodTensorDims dims = {{0}, 0};
    if (!readArrayFromMsg(msg, dims.dims, &dims.len, "dims", error)) {
        goto error;
    }

    if (!larodSetTensorDims(tensor, &dims, error)) {
        goto error;
    }

    larodTensorPitches pitches = {{0}, 0};
    if (!readArrayFromMsg(msg, pitches.pitches, &pitches.len, "pitches",
                          error)) {
        goto error;
    }

    if (!larodSetTensorPitches(tensor, &pitches, error)) {
        goto error;
    }

    const char* name = NULL;
    ret = sd_bus_message_read(msg, "s", &name);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not read tensor name: %s",
                         strerror(ret));
        goto error;
    }

    tensor->name = strndup(name, strlen(name));
    if (!tensor->name) {
        larodCreateError(error, ENOMEM,
                         "Could not allocate tensor name string: %s",
                         strerror(ENOMEM));
        goto error;
    }

    // Exit tensor struct.
    ret = sd_bus_message_exit_container(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not exit tensor struct: %s",
                         strerror(ret));

        goto error;
    }

    *tensorResult = tensor;

    return true;

error:
    destroyTensor(tensor);

    return false;
}

larodTensor* createTensor(larodError** error) {
    larodTensor* newTensor = calloc(1, sizeof(*newTensor));
    if (!newTensor) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));
        return NULL;
    }

    newTensor->fd = -1;
    newTensor->fdSize = 0;
    newTensor->fdOffset = 0;
    newTensor->dataType = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
    newTensor->layout = LAROD_TENSOR_LAYOUT_UNSPECIFIED;
    newTensor->byteSize = 0;
    newTensor->name = NULL;

    return newTensor;
}

larodTensor** larodCreateTensors(size_t numTensors, larodError** error) {
    size_t numCreated = 0;

    if (numTensors == 0) {
        larodCreateError(error, EINVAL,
                         "numTensors is 0, please create 1 tensor or more");

        return NULL;
    }

    larodTensor** tensors = calloc(numTensors + 1, sizeof(*tensors));
    if (!tensors) {
        larodCreateError(error, ENOMEM,
                         "Failed allocating memory for the tensor array");
        return NULL;
    }

    for (numCreated = 0; numCreated < numTensors; numCreated++) {
        larodTensor* t = createTensor(error);
        if (t == NULL) {
            goto error;
        }

        t->name = malloc(1);
        if (!t->name) {
            destroyTensor(t);
            larodCreateError(error, ENOMEM,
                             "Failed to allocate memory for tensor name");
            goto error;
        }
        t->name[0] = '\0';

        tensors[numCreated] = t;
    }
    // Terminate array with a NULL.
    tensors[numCreated] = NULL;

    return tensors;

error:
    larodDestroyTensors(&tensors, numCreated);
    return NULL;
}

larodTensor** copyTensorArray(larodTensor** sourceTensors, size_t numSrcTensors,
                              larodError** error) {
    size_t numCopied = 0;

    assert(sourceTensors);

    // Allocate space for every tensor and an additional NULL at the end.
    larodTensor** tensorCopies =
        calloc(numSrcTensors + 1, sizeof(*tensorCopies));
    if (!tensorCopies) {
        larodCreateError(error, ENOMEM,
                         "Failed allocating memory for the tensor array");
        return NULL;
    }

    for (numCopied = 0; numCopied < numSrcTensors; numCopied++) {
        larodTensor* t = copyTensor(sourceTensors[numCopied], error);
        if (t == NULL) {
            goto error;
        }
        tensorCopies[numCopied] = t;
    }
    tensorCopies[numCopied] = NULL;

    return tensorCopies;

error:
    larodDestroyTensors(&tensorCopies, numCopied);
    return NULL;
}

larodTensor* copyTensor(const larodTensor* tensor, larodError** error) {
    assert(tensor);

    larodTensor* newTensor = NULL;

    newTensor = malloc(sizeof(*newTensor));
    if (!newTensor) {
        larodCreateError(error, ENOMEM,
                         "Failed to allocate memory for new tensor: %s",
                         strerror(ENOMEM));
        return NULL;
    }

    memcpy(newTensor, tensor, sizeof(*tensor));

    newTensor->name = strndup(tensor->name, strlen(tensor->name));
    if (!newTensor->name) {
        larodCreateError(error, ENOMEM,
                         "Failed to allocate memory for tensor name: %s",
                         strerror(ENOMEM));
        destroyTensor(newTensor);
        return NULL;
    }

    return newTensor;
}

bool tensorToMsg(sd_bus_message* msg, const larodTensor* tensor,
                 bool skipSizeSeek, larodError** error) {
    uint64_t fdSize = 0;
    if (skipSizeSeek) {
        // For output tensors we accept explicitly set sizes as well as passing
        // size = 0. In the second case the service will just skip output size
        // checking.
        fdSize = tensor->fdSize;
    } else {
        // For input tensors we need to determine the fd size if client have not
        // explicitly set it.
        if (!getFdSize(tensor, &fdSize, error)) {
            return false;
        }
    }

    int ret = sd_bus_message_open_container(
        msg, 'r', TENSOR_METADATA_TO_SERVICE TENSOR_BUFFERDATA);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not enter tensor container: %s",
                         strerror(ret));

        return false;
    }

    ret = sd_bus_message_append(msg, "ii", tensor->dataType, tensor->layout);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not append tensor enums: %s",
                         strerror(ret));

        return false;
    }

    uint64_t localDims[LAROD_TENSOR_MAX_LEN];
    assert(tensor->dims.len <= LAROD_TENSOR_MAX_LEN);
    for (size_t i = 0; i < tensor->dims.len; i++) {
        localDims[i] = tensor->dims.dims[i];
    }
    ret = sd_bus_message_append_array(msg, 't', &localDims,
                                      tensor->dims.len * sizeof(uint64_t));
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not append tensor dimensions: %s",
                         strerror(ret));

        return false;
    }

    uint64_t localPitches[LAROD_TENSOR_MAX_LEN];
    assert(tensor->pitches.len <= LAROD_TENSOR_MAX_LEN);
    for (size_t i = 0; i < tensor->pitches.len; i++) {
        localPitches[i] = tensor->pitches.pitches[i];
    }
    ret = sd_bus_message_append_array(msg, 't', &localPitches,
                                      tensor->pitches.len * sizeof(uint64_t));
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not append tensor pitches: %s",
                         strerror(ret));

        return false;
    }

    ret = sd_bus_message_append(msg, TENSOR_BUFFERDATA, tensor->fd, fdSize,
                                tensor->fdOffset);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not append tensor fd details: %s",
                         strerror(ret));

        return false;
    }

    ret = sd_bus_message_close_container(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not exit tensor container: %s",
                         strerror(ret));

        return false;
    }
    return true;
}

bool getFdSize(const larodTensor* tensor, uint64_t* fdSize,
               larodError** error) {
    assert(tensor);
    assert(fdSize);

    if (tensor->fdSize > 0) {
        // User have explicitly set the size of this tensor. We simply use
        // this number.
        *fdSize = tensor->fdSize;
    } else {
        off_t filePos = lseek(tensor->fd, 0, SEEK_END);
        if (filePos < 0) {
            larodCreateError(error, errno,
                             "Could not get file size for tensor: %s",
                             strerror(errno));
            return false;
        }

        uint64_t fileSize = (uint64_t) filePos;

        // We leave the file pos at the end of file since we will always do
        // a seek in the service before reading or writing to the tensor.

        // Sanity check size vs offset, then subtract the offset since
        // 'size' is the number of bytes larod service is allowed to
        // actually access.
        assert(tensor->fdOffset >= 0);
        uint64_t offset = (uint64_t) tensor->fdOffset;
        if (offset >= fileSize) {
            larodCreateError(
                error, LAROD_ERROR_JOB,
                "Offset for tensor is larger or equal to the file size");
            return false;
        }

        fileSize -= offset;
        if (fileSize > SIZE_MAX) {
            larodCreateError(error, EINVAL,
                             "Destination file area for tensor is larger than "
                             "SIZE_MAX on this system");
            return false;
        }

        *fdSize = fileSize;
    }

    return true;
}

bool appendTensorsToMsg(sd_bus_message* msg, larodTensor** tensors,
                        size_t numTensors, bool skipSizeSeek,
                        larodError** error) {
    assert(msg);
    assert(tensors);

    // Enter tensor array.
    int ret = sd_bus_message_open_container(
        msg, 'a', "(" TENSOR_METADATA_TO_SERVICE TENSOR_BUFFERDATA ")");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret,
                         "Could not enter tensor meta data array: %s",
                         strerror(ret));

        goto error;
    }

    for (size_t i = 0; i < numTensors; i++) {
        const larodTensor* tensor = tensors[i];
        assert(tensor);

        if (!tensorToMsg(msg, tensor, skipSizeSeek, error)) {
            goto error;
        }
    }

    ret = sd_bus_message_close_container(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret,
                         "Could not exit tensor meta data array: %s",
                         strerror(ret));

        goto error;
    }

    return true;

error:
    return false;
}

bool larodSetTensorDims(larodTensor* tensor, const larodTensorDims* dims,
                        larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");

        return false;
    }

    if (!dims) {
        larodCreateError(error, EINVAL, "larodTensorDims is NULL");

        return false;
    }

    if (dims->len > LAROD_TENSOR_MAX_LEN) {
        larodCreateError(error, EINVAL, "dims len is too big (%zu > %u)",
                         dims->len, LAROD_TENSOR_MAX_LEN);

        return false;
    }

    tensor->dims = *dims;

    return true;
}

const larodTensorDims* larodGetTensorDims(const larodTensor* tensor,
                                          larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return NULL;
    }

    return &tensor->dims;
}

bool larodSetTensorPitches(larodTensor* tensor,
                           const larodTensorPitches* pitches,
                           larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");

        return false;
    }

    if (!pitches) {
        larodCreateError(error, EINVAL, "larodTensorPitches is NULL");

        return false;
    }

    if (pitches->len > LAROD_TENSOR_MAX_LEN) {
        larodCreateError(error, EINVAL, "pitches len is too big (%zu > %u)",
                         pitches->len, LAROD_TENSOR_MAX_LEN);

        return false;
    }

    tensor->pitches = *pitches;

    return true;
}

const larodTensorPitches* larodGetTensorPitches(const larodTensor* tensor,
                                                larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");

        return NULL;
    }

    return &tensor->pitches;
}

bool larodSetTensorDataType(larodTensor* tensor,
                            const larodTensorDataType dataType,
                            larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return false;
    }

    if (dataType == LAROD_TENSOR_DATA_TYPE_INVALID) {
        larodCreateError(error, EINVAL,
                         "LAROD_TENSOR_DATA_TYPE_INVALID should not be set "
                         "explicitly, it is reserved to indicate errors");
        return false;
    }

    if (dataType >= LAROD_TENSOR_DATA_TYPE_MAX) {
        larodCreateError(error, EINVAL,
                         "larodTensorDataType enum value out of range");
        return false;
    }
    tensor->dataType = dataType;

    return true;
}

larodTensorDataType larodGetTensorDataType(const larodTensor* tensor,
                                           larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return LAROD_TENSOR_DATA_TYPE_INVALID;
    }

    return tensor->dataType;
}

bool larodSetTensorLayout(larodTensor* tensor, const larodTensorLayout layout,
                          larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return false;
    }

    if (layout == LAROD_TENSOR_LAYOUT_INVALID) {
        larodCreateError(error, EINVAL,
                         "LAROD_TENSOR_LAYOUT_INVALID should not be set "
                         "explicitly, it is reserved to indicate errors");
        return false;
    }

    if (layout >= LAROD_TENSOR_LAYOUT_MAX) {
        larodCreateError(error, EINVAL,
                         "larodTensorLayout enum value out of range");
        return false;
    }

    tensor->layout = layout;

    return true;
}

larodTensorLayout larodGetTensorLayout(const larodTensor* tensor,
                                       larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return LAROD_TENSOR_LAYOUT_INVALID;
    }

    return tensor->layout;
}

bool larodSetTensorFd(larodTensor* tensor, const int fd, larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");

        return false;
    }

    if (fd < 0) {
        larodCreateError(error, EINVAL, "fd is negative");

        return false;
    }

    if (fd == LAROD_INVALID_FD) {
        larodCreateError(error, EINVAL,
                         "LAROD_INVALID_FD should not be set explicitly, it is "
                         "reserved to indicate errors");

        return false;
    }

    tensor->fd = fd;

    return true;
}

int larodGetTensorFd(const larodTensor* tensor, larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return LAROD_INVALID_FD;
    }

    return tensor->fd;
}

bool larodSetTensorFdSize(larodTensor* tensor, const size_t size,
                          larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return false;
    }

    tensor->fdSize = size;

    return true;
}

bool larodGetTensorFdSize(const larodTensor* tensor, size_t* size,
                          larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return false;
    }

    if (!size) {
        larodCreateError(error, EINVAL, "size is NULL");
        return false;
    }

    *size = tensor->fdSize;
    return true;
}

bool larodSetTensorFdOffset(larodTensor* tensor, const int64_t offset,
                            larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return false;
    }

    if (offset < 0) {
        larodCreateError(error, EINVAL,
                         "negative offset values are not allowed");
        return false;
    }

    tensor->fdOffset = offset;
    return true;
}

int64_t larodGetTensorFdOffset(const larodTensor* tensor, larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");
        return -1;
    }

    return tensor->fdOffset;
}

const char* larodGetTensorName(const larodTensor* tensor, larodError** error) {
    if (!tensor) {
        larodCreateError(error, EINVAL, "larodTensor is NULL");

        return NULL;
    }

    if (!tensor->name) {
        larodCreateError(error, EINVAL, "Tensor has no name");

        return NULL;
    }

    return tensor->name;
}

void destroyTensor(larodTensor* tensor) {
    if (!tensor) {
        return;
    }

    free(tensor->name);

    free(tensor);
}

void larodDestroyTensors(larodTensor*** tensors, size_t numTensors) {
    if (!tensors || !(*tensors)) {
        return;
    }

    for (size_t i = 0; i < numTensors; i++) {
        larodTensor* tensor = (*tensors)[i];
        destroyTensor(tensor);
    }

    free(*tensors);
    *tensors = NULL;
}

bool validateTensors(larodTensor** tensors, const size_t numTensors,
                     larodError** error) {
    assert(tensors);

    for (size_t i = 0; i < numTensors; ++i) {
        if (!tensors[i]) {
            larodCreateError(error, EINVAL, "Tensor %zu is null", i);

            return false;
        }

        // These should have already been checked by respective access function.
        assert(tensors[i]->fd >= 0);
        assert(tensors[i]->fdOffset >= 0);

        assert(tensors[i]->dims.len <= LAROD_TENSOR_MAX_LEN);

        assert(tensors[i]->dataType != LAROD_TENSOR_DATA_TYPE_INVALID);
        assert(tensors[i]->dataType < LAROD_TENSOR_DATA_TYPE_MAX);

        assert(tensors[i]->layout != LAROD_TENSOR_LAYOUT_INVALID);
        assert(tensors[i]->layout < LAROD_TENSOR_LAYOUT_MAX);

        // Layout NCHW requires dims len 0 or 4.
        if (tensors[i]->layout == LAROD_TENSOR_LAYOUT_NCHW &&
            (tensors[i]->dims.len != 0 && tensors[i]->dims.len != 4)) {
            larodCreateError(error, EINVAL,
                             "Tensor %zu has layout NCHW and expects dimension "
                             "length 4 or 0 (got %zu)",
                             i, tensors[i]->dims.len);

            return false;
        }

        // Layout NHWC requires dims len 0 or 4.
        if (tensors[i]->layout == LAROD_TENSOR_LAYOUT_NHWC &&
            (tensors[i]->dims.len != 0 && tensors[i]->dims.len != 4)) {
            larodCreateError(error, EINVAL,
                             "Tensor %zu has layout NCHW and expects dimension "
                             "length 4 or 0 (got %zu)",
                             i, tensors[i]->dims.len);

            return false;
        }

        // Pitches must be consistent with number of dims and total byte size.
        if (tensors[i]->pitches.len > 0) {
            if (tensors[i]->dims.len > 0 &&
                tensors[i]->pitches.len != tensors[i]->dims.len) {
                larodCreateError(error, EINVAL,
                                 "Tensor %zu has unequal number of dims (%zu) "
                                 "and pitches (%zu)",
                                 i, tensors[i]->dims.len,
                                 tensors[i]->pitches.len);

                return false;
            }

            if (tensors[i]->byteSize > 0 &&
                tensors[i]->pitches.pitches[0] != tensors[i]->byteSize) {
                larodCreateError(
                    error, EINVAL,
                    "Tensor %zu pitches component zero (%zu) does not "
                    "equal total byte size (%zu)",
                    i, tensors[i]->pitches.pitches[0], tensors[i]->byteSize);

                return false;
            }
        }
    }

    return true;
}
