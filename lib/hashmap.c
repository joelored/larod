/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "hashmap.h"

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>

#include "linkedlist.h"

#define INIT_NBR_OF_BUCKETS 16 // Should be a power of 2.
#define MAX_LOAD_FACTOR 0.75

struct HashMap {
    LinkedList** buckets;
    size_t size;         ///< Number of elements.
    size_t nbrOfBuckets; ///< Number of buckets.
};

/**
 * @brief Hash function.
 *
 * Based on Knuth's multiplicative method for 64-bit values.
 *
 * @param val Pointer value to hash.
 * @return Hash value.
 */
static inline uint64_t hash(uint64_t val) {
#define PHI_PRIME_64 UINT64_C(11400714819323198483)
    // We multiply the pointer value of val with 11400714819323198483 (a prime
    // number close to the fractional part of golden ratio times 2^64).
    return PHI_PRIME_64 * val;
}

/**
 * @brief Checks if a hash map needs rehashing.
 *
 * @param hashSet Hash map to check need to rehash.
 * @return True if @p hashSet needs rehashing, otherwise false.
 */
static inline bool needsRehashing(HashMap* hashMap) {
    return (float) hashMap->size / (float) hashMap->nbrOfBuckets >=
           MAX_LOAD_FACTOR;
}

/**
 * @brief Create a new hash map.
 *
 * @param hashMap Handle to a hash map to be allocated.
 * @param nbrOfBuckets Number of buckets to allocate for @p hashMap.
 * @return Positive errno style return code (zero means success).
 */
static int newHashMap(HashMap** hashMap, const size_t nbrOfBuckets);

/**
 * @brief Rehash a hash map.
 *
 * @param Handle to a hash map to rehash.
 * @return Positive errno style return code (zero means success).
 */
static int rehash(HashMap** hashMap);

static int createPair(uint64_t key, void* value, Pair** pair);
static int insertPair(HashMap* hashMap, Pair* pair);

/**
 * @brief Deallocate a hash map.
 *
 * @param freePairs If set to true, the pairs in the map will also be freed,
 * otherwise they will be not.
 */
static void deallocate(HashMap** hashMap, const bool freePairs);

static int newHashMap(HashMap** hashMap, const size_t nbrOfBuckets) {
    assert(hashMap);
    assert(nbrOfBuckets);

    *hashMap = malloc(sizeof(HashMap));
    if (!(*hashMap)) {
        return ENOMEM;
    }

    (*hashMap)->nbrOfBuckets = nbrOfBuckets;
    (*hashMap)->buckets =
        malloc((*hashMap)->nbrOfBuckets * sizeof(LinkedList*));
    if (!(*hashMap)->buckets) {
        free(*hashMap);
        return ENOMEM;
    }

    for (size_t i = 1; i < (*hashMap)->nbrOfBuckets; ++i) {
        (*hashMap)->buckets[i] = NULL;
    }

    int ret;
    for (size_t i = 0; i < (*hashMap)->nbrOfBuckets; ++i) {
        if ((ret = listConstruct(&(*hashMap)->buckets[i]))) {
            deallocate(hashMap, false);
            return ret;
        }
    }

    (*hashMap)->size = 0;

    return 0;
}

int hashMapConstruct(HashMap** hashMap) {
    if (!hashMap) {
        return EINVAL;
    }

    return newHashMap(hashMap, INIT_NBR_OF_BUCKETS);
}

void hashMapDestruct(HashMap** hashMap) {
    deallocate(hashMap, true);
}

static void deallocate(HashMap** hashMap, const bool freePairs) {
    if (!hashMap || !(*hashMap)) {
        return;
    }

    for (size_t i = 0; i < (*hashMap)->nbrOfBuckets; ++i) {
        if (freePairs) {
            // Free pairs.
            const Node* firstNode;
            int ret = listGetFirstNode((*hashMap)->buckets[i], &firstNode);
            if (ret) {
                // Should never happen.
                assert(!ret);
                abort();
            }

            for (const Node* node = firstNode; node != NULL;
                 node = node->next) {
                free(node->data);
            }
        }

        listDestruct(&(*hashMap)->buckets[i]);
    }
    free((*hashMap)->buckets);

    free(*hashMap);
    *hashMap = NULL;
}

static int createPair(uint64_t key, void* value, Pair** pair) {
    assert(pair);

    *pair = malloc(sizeof(Pair));
    if (!(*pair)) {
        return ENOMEM;
    }

    (*pair)->key = key;
    (*pair)->value = value;

    return 0;
}

int hashMapInsert(HashMap** hashMap, uint64_t key, void* value) {
    if (!hashMap || !(*hashMap)) {
        return EINVAL;
    }

    int ret;
    if (needsRehashing(*hashMap) && (ret = rehash(hashMap))) {
        return ret;
    }

    Pair* pair = NULL;
    ret = createPair(key, value, &pair);
    if (ret) {
        return ret;
    }

    ret = insertPair(*hashMap, pair);
    if (ret) {
        free(pair);
    }

    return ret;
}

static int insertPair(HashMap* hashMap, Pair* pair) {
    assert(hashMap);
    assert(pair);

    uint64_t hashedKey = hash(pair->key) & (hashMap->nbrOfBuckets - 1);

    // Check if key already exists.
    const Node* firstNode;
    int ret = listGetFirstNode(hashMap->buckets[hashedKey], &firstNode);
    if (ret) {
        // Should never happen.
        assert(!ret);

        return ret;
    }

    for (const Node* node = firstNode; node != NULL; node = node->next) {
        Pair* currPair = node->data;
        if (currPair->key == pair->key) {
            return EEXIST;
        }
    }

    ret = listPushBack(hashMap->buckets[hashedKey], pair);
    if (ret) {
        return ret;
    }

    hashMap->size += 1;

    return 0;
}

int hashMapErase(HashMap* hashMap, uint64_t key, void** erasedValue) {
    if (!hashMap) {
        return EINVAL;
    }

    uint64_t hashedKey = hash(key) & (hashMap->nbrOfBuckets - 1);

    // Find key in list.
    const Node* firstNode;
    int ret = listGetFirstNode(hashMap->buckets[hashedKey], &firstNode);
    if (ret) {
        // Should never happen.
        assert(!ret);

        return ret;
    }

    for (const Node* node = firstNode; node != NULL; node = node->next) {
        Pair* pair = node->data;
        if (pair->key == key) {
            ret = listErase(hashMap->buckets[hashedKey], pair);
            if (!ret) {
                if (erasedValue) {
                    *erasedValue = pair->value;
                }
                free(pair);

                hashMap->size -= 1;
            }

            return ret;
        }
    }

    if (erasedValue) {
        *erasedValue = NULL;
    }

    return ENODATA;
}

int hashMapGetSize(HashMap* hashMap, size_t* size) {
    if (!hashMap || !size) {
        return EINVAL;
    }

    *size = hashMap->size;

    return 0;
}

int hashMapExtractAll(HashMap* hashMap, const Pair*** pairs,
                      size_t* nbrOfPairs) {
    if (!hashMap || !pairs || !nbrOfPairs) {
        return EINVAL;
    }

    *nbrOfPairs = hashMap->size;
    *pairs = malloc(hashMap->size * sizeof(Pair*));
    if (!(*pairs)) {
        return ENOMEM;
    }

    const Pair** pairsPtr = *pairs;
    for (size_t i = 0; i < hashMap->nbrOfBuckets; ++i) {
        const Node* firstNode;
        int ret = listGetFirstNode(hashMap->buckets[i], &firstNode);
        if (ret) {
            // Should never happen.
            assert(!ret);
            free(*pairs);
            *pairs = NULL;
            *nbrOfPairs = 0;

            return ret;
        }

        for (const Node* node = firstNode; node != NULL; node = node->next) {
            *pairsPtr++ = node->data;
        }
    }

    return 0;
}

static int rehash(HashMap** hashMap) {
    HashMap* rehashedSet;
    int ret = newHashMap(&rehashedSet, (*hashMap)->nbrOfBuckets << 1);
    if (ret) {
        return ret;
    }

    for (size_t i = 0; i < (*hashMap)->nbrOfBuckets; ++i) {
        const Node* firstNode;
        ret = listGetFirstNode((*hashMap)->buckets[i], &firstNode);
        if (ret) {
            // Should never happen.
            assert(!ret);
            deallocate(&rehashedSet, false);

            return ret;
        }

        for (const Node* node = firstNode; node != NULL; node = node->next) {
            ret = insertPair(rehashedSet, (Pair*) node->data);
            if (ret) {
                deallocate(&rehashedSet, false);
                return ret;
            }
        }
    }

    deallocate(hashMap, false);
    *hashMap = rehashedSet;

    return 0;
}
