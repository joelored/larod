/**
 * @file larod.h
 * @brief Main larod header file.
 *
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 *
 * @note This library is not thread safe.
 */

#ifndef COM_AXIS_LAROD_H
#define COM_AXIS_LAROD_H

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "larod-version.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Maximum number of dimensions of a @c larodTensor.
 */
#define LAROD_TENSOR_MAX_LEN 12

/**
 * @brief Macro for invalid model ID.
 */
#define LAROD_INVALID_MODEL_ID UINT64_MAX

/**
 * @brief Macro for invalid tensor file descriptor.
 */
#define LAROD_INVALID_FD (INT_MIN)

/**
 * @brief Enum type with supported chips.
 *
 * The chips in this enum are supported by larod.
 */
typedef enum {
    /// Invalid chip.
    LAROD_CHIP_INVALID = 0,
    /// Dummy chip for debugging.
    LAROD_CHIP_DEBUG = 1,
    /// CPU with TensorFlow Lite.
    LAROD_CHIP_TFLITE_CPU = 2,
    /// Google TPU.
    LAROD_CHIP_TPU = 4,
    /// Ambarella CVFlow (NN).
    LAROD_CHIP_CVFLOW_NN = 6,
    /// GPU with TensorFlow Lite.
    /// WARNING: This is an experimental chip which is subject to change.
    LAROD_CHIP_TFLITE_GLGPU = 8,
    /// Ambarella CVFlow (proc).
    LAROD_CHIP_CVFLOW_PROC = 9,
    /// CPU with libyuv.
    LAROD_CHIP_LIBYUV = 11,
} larodChip;

/**
 * @brief Enum type for specifying access.
 *
 * Models can either be public or private; only the creator can modify private
 * models, while anyone can modify public models.
 */
typedef enum {
    LAROD_ACCESS_INVALID = 0, ///< Invalid access.
    LAROD_ACCESS_PRIVATE = 1, ///< Private access.
    LAROD_ACCESS_PUBLIC = 2   ///< Public access.
} larodAccess;

/**
 * @brief Enum type for error codes.
 *
 * These are set in @c larodError on errors. Note that only the negative
 * values are defined in this enum explicitly. @c larodErrorCode can also be a
 * positive value, this corresponds then to Linux style errno values.
 *
 * For example, if the value is 12 (positive), that is then value of the Linux
 * errno ENOMEM, which means "could not allocate memory". While a value of -4
 * (negative) is LAROD_ERROR_MODEL_NOT_FOUND defined in this enum.
 */
typedef enum {
    LAROD_ERROR_NONE = 0,               ///< No errors.
    LAROD_ERROR_JOB = -1,               ///< General error for job.
    LAROD_ERROR_LOAD_MODEL = -2,        ///< General error for loading model.
    LAROD_ERROR_FD = -3,                ///< Error regarding file descriptors.
    LAROD_ERROR_MODEL_NOT_FOUND = -4,   ///< Model was not found.
    LAROD_ERROR_PERMISSION = -5,        ///< Insufficient permissions.
    LAROD_ERROR_CONNECTION = -6,        ///< Invalid connection.
    LAROD_ERROR_CREATE_SESSION = -7,    ///< Could not create session.
    LAROD_ERROR_KILL_SESSION = -8,      ///< Failed killing session.
    LAROD_ERROR_INVALID_CHIP_ID = -9,   ///< Invalid chip ID.
    LAROD_ERROR_INVALID_ACCESS = -10,   ///< Invalid access specifier.
    LAROD_ERROR_DELETE_MODEL = -11,     ///< Error while deleting model.
    LAROD_ERROR_TENSOR_MISMATCH = -12,  ///< Tensor mismatch.
    LAROD_ERROR_VERSION_MISMATCH = -13, ///< Version mismatch.
    LAROD_ERROR_MAX_ERRNO = 1024        ///< Max errno value (dummy value).
} larodErrorCode;

/**
 * @brief Enum type for specifying tensor data type.
 *
 * As part of @c larodTensor these are used to describe the data type of a
 * tensor.
 *
 * If no data type is specified (@c LAROD_TENSOR_DATA_TYPE_UNSPECIFIED) for an
 * input or output tensor when running a job, there will be no sanity check that
 * it matches what the model expects.
 */
typedef enum {
    LAROD_TENSOR_DATA_TYPE_INVALID = 0, ///< Invalid data type.
    LAROD_TENSOR_DATA_TYPE_UNSPECIFIED, ///< No data type is specified.
    LAROD_TENSOR_DATA_TYPE_BOOL,        ///< 1-bit boolean.
    LAROD_TENSOR_DATA_TYPE_UINT8,       ///< 8-bit unsigned integer.
    LAROD_TENSOR_DATA_TYPE_INT8,        ///< 8-bit signed integer.
    LAROD_TENSOR_DATA_TYPE_UINT16,      ///< 16-bit unsigned integer.
    LAROD_TENSOR_DATA_TYPE_INT16,       ///< 16-bit signed integer.
    LAROD_TENSOR_DATA_TYPE_UINT32,      ///< 32-bit unsigned integer.
    LAROD_TENSOR_DATA_TYPE_INT32,       ///< 32-bit signed integer.
    LAROD_TENSOR_DATA_TYPE_UINT64,      ///< 64-bit unsigned integer.
    LAROD_TENSOR_DATA_TYPE_INT64,       ///< 64-bit signed integer.
    LAROD_TENSOR_DATA_TYPE_FLOAT16,     ///< 16-bit floating point.
    LAROD_TENSOR_DATA_TYPE_FLOAT32,     ///< 32-bit floating point.
    LAROD_TENSOR_DATA_TYPE_FLOAT64,     ///< 64-bit floating point.
    LAROD_TENSOR_DATA_TYPE_MAX,         ///< Max enum (dummy value).
} larodTensorDataType;

/**
 * @brief Enum type for specifying tensor layout.
 *
 * As part of @c larodTensor these are used to describe how to interpret the
 * dimensions of a tensor.
 *
 * N, H, W, C stands for batch size, height, width and channels respectively.
 * However, the semantic interpretation of the dimensions can of course differ
 * when working on image data that is not laid out in memory this way. The
 * important thing is that input tensors sent for jobs have the same layout as
 * what the model they are sent to expects, and similarly that outputs are
 * interpreted according to the model's output layout.
 *
 * @c LAROD_TENSOR_LAYOUT_420SP represents a semiplanar data layout with
 * subsampling 4:2:0, such as for example the NV12 color format. With this
 * layout the interpretations of what @c larodTensorDims and @c
 * larodTensorPitches represent differ from the other layouts. The @c
 * larodTensorPitches is expected to have 3 elements. The "rightmost" element,
 * pitches[2], represents the total byte size of a single row in the image for
 * both the plane separated data part and the plane interleaved part. The middle
 * element, pitches[1], correspond to the total byte size of the plane separated
 * data, including padding on the end, i.e. it is the offset in bytes to the
 * plane interleaved part of the data. The first element, pitches[0], describe
 * the total byte size of the data, all padding included. This also accounts for
 * extra padding on the end of the buffer. For an explicit example, see @c
 * larodTensorPitches. The @c larodTensorDims is also expected to have 3
 * elements. The first element dims[0] represents the number of channels and
 * should always be equal to 3. The second element dims[1] corresponds to the
 * height in pixels of the image, and dims[2] the width in pixels of the image.
 * This layout is experimental.
 *
 * If no layout is specified (@c LAROD_TENSOR_LAYOUT_UNSPECIFIED) for an input
 * or output tensor when running a job, there will be no sanity check that
 * it matches what the model expects.
 */
typedef enum {
    LAROD_TENSOR_LAYOUT_INVALID = 0, ///< Invalid layout.
    LAROD_TENSOR_LAYOUT_UNSPECIFIED, ///< No layout is specified.
    LAROD_TENSOR_LAYOUT_NHWC,        ///< Tensor is laid out with dimensions
                                     ///< NxHxWxC, this is also referred to as
                                     ///< "color interleaved" image data. The @c
                                     ///< len member of the @c larodTensorDims
                                     ///< must be 4 or 0 ("unspecified") for
                                     ///< this kind of layout.
    LAROD_TENSOR_LAYOUT_NCHW,        ///< Tensor is laid out with dimensions
                                     ///< NxCxHxW, this is also referred to as
                                     ///< "plane separated color" image data.
                                     ///< The @c len member of the @c
                                     ///< larodTensorDims must be 4 or 0
                                     ///< ("unspecified") for this kind of
                                     ///< layout.
    LAROD_TENSOR_LAYOUT_420SP,       ///< Image format, subsampling 4:2:0,
                                     ///< semiplanar data layout (experimental).
    LAROD_TENSOR_LAYOUT_MAX,         ///< Max enum (dummy value).
} larodTensorLayout;

/**
 * @brief Type containing error information.
 *
 * This is passed as an pointer argument to most library functions. An
 * initialized pointer of this type needs to be deallocated with
 * @c larodClearError(). The field @c msg will always be non-NULL on errors.
 */
typedef struct {
    larodErrorCode code; ///< Error code.
    const char* msg;     ///< Error message.
} larodError;

/**
 * @brief A type containing information about a tensor's dimensions.
 *
 * As part of @c larodTensor this is used to define dimensions of a tensor. For
 * most values of @c larodTensorLayout the tensor can be viewed as a
 * multidimensional array, where the size of each dimension is described by the
 * corresponding number in the @c larodTensorDims struct. An exception to this
 * is @c LAROD_TENSOR_LAYOUT_420SP. For more info, see @c larodTensorLayout.
 *
 * If @c len is set to 0 for an input or output tensor when running a job,
 * there will be no sanity check that it matches what the model expects.
 *
 * If @c len is specified, it must be equal to the @c len of the tensor's
 * pitches if that is also specified; otherwise one gets an error when creating
 * job requests. See @c larodTensorPitches for more details of how they are
 * related.
 */
typedef struct {
    size_t dims[LAROD_TENSOR_MAX_LEN];
    size_t len;                        ///< Length of dims array.
} larodTensorDims;

/**
 * @brief A type containing information about a tensor's pitches.
 *
 * As part of @c larodTensor this is used to define pitches of a tensor. For
 * most layouts pitches should be interpreted as the byte size of the elements
 * plus any padding added because of some alignment requirement in each
 * dimension; i.e. as many bytes as one has to traverse to reach the next
 * element of the neighboring left dimension. This is calculated going from
 * right to left. Note that the first element, pitches[0], corresponds to the
 * total byte size of the tensor's data. This interpretation is modified when @c
 * LAROD_TENSOR_LAYOUT_420SP is used. For more info, see @c larodTensorLayout,
 * and example 3 below.
 *
 * This general definition permits alignment requirements in all dimensions.
 * Often, one only deals with alignment in the width dimension and refers to
 * "row pitch" as the pitch in that specific dimension.
 *
 * Example 1: Consider a tensor with layout @c LAROD_TENSOR_LAYOUT_NCHW, data
 * type float32, dims (1, 3, 250, 250) and an alignment requirement of 32 bytes
 * in the 4th dimension. Starting from the right-most dimension, the 4th pitch
 * is then equal to the byte size of one row: ceil(250 * sizeof(float32) / 32) *
 * 32 = 32 * 32 = 1024. The 4th pitch would then correspond to the "row pitch"
 * for this given @c larodTensorLayout. The rest of the pitches are calculated
 * as (1 * 3 * 250 * 1024, 3 * 250 * 1024, 250 * 1024, 1024).
 *
 * Example 2: A tensor with layout @c LAROD_TENSOR_LAYOUT_NHWC, dims (1, 224,
 * 300, 3), uint8 as data type, and alignment requirements of 32 and 4 bytes in
 * the 3rd and 4th dimensions respectively, gives the pitches (1 * 224 * 1216,
 * 224 * 1216, 1216, 4), since ceil(3 * sizeof(uint8) / 4) * 4 = 4 and ceil(300
 * * 4 / 32) * 32 = 1216. In this example, the "row pitch" would be the 3rd
 * pitch for this layout.
 *
 * Example 3: As an example of @c LAROD_TENSOR_LAYOUT_420SP consider an image
 * with data type uint8, pixel height of 224, pixel width of 300, an alignment
 * requirement on the width of 32 bytes, and an alignment requirement on the
 * plane separated data part of 32 bytes. The last element, pitches[2],
 * describing the total size of a row, would be 320, since ceil(300 *
 * sizeof(uint8) / 32) * 32 = 320. The second element, pitches[1], describing
 * the total size of the separated data plane would be given by ceil(224 * 320 /
 * 32) * 32 = 224 * 320 = 71680. Finally the first element of pitches would be
 * given by the sum of the two parts of the data, the separated part, and the
 * interleaved part, possibly with additional padding on the end.  In this case
 * it would simply be 224 * 320 + 224 * 320 / 2 = 107520.
 *
 * If @c len is set to 0 for an input or output tensor when running a job, there
 * will be no sanity check that it matches what the model expects. If @c len is
 * specified, it must be equal to the @c len of its dimensions if that is also
 * specified; otherwise one gets an error when creating requests.
 */
typedef struct {
    size_t pitches[LAROD_TENSOR_MAX_LEN];
    size_t len;                           ///< Length of pitches array.
} larodTensorPitches;

/**
 * @brief A type representing a model.
 *
 * This type represents a model that has been loaded in the service. It contains
 * information about the model, such as how many input and output tensor handles
 * it requires for running a job, what ID it has been designated by the service
 * etcetera. The properties of a model is accessed by various access functions
 * (e.g. @c larodGetModelId()).
 */
typedef struct larodModel larodModel;

/**
 * @brief Callback used in larodLoadModelAsync().
 *
 * Callback function should not carry out relatively extensive blocking tasks
 * (it will block the main poll thread for the message bus). Especially note
 * that it must not call the synchronous calls @c larodRunJob() and
 * @c larodDeleteModel().
 *
 * @param model Handle to the loaded model. Caller must release this handle
 * using @c larodDestroyModel() when it's not needed any more.
 * @param userData User data.
 * @param error An initialized error handle when errors occurred (@p model is
 * is NULL). @p error will be NULL on success. In this callback function, one
 * must not free this handle with @c larodClearError(), since the library will
 * do it when returning from this callback.
 */
typedef void (*larodLoadModelCallback)(larodModel* model, void* userData,
                                       larodError* error);
/**
 * @brief Callback used in larodRunJobAsync().
 *
 * Callback function should not carry out relatively extensive blocking tasks
 * (it will block the main poll thread for the message bus). Especially note
 * that it must not call the synchronous calls @c larodRunJob() and
 * @c larodDeleteModel().
 *
 * @param userData User data.
 * @param error An initialized error handle if any errors occur while running
 * the job. Otherwise, it will be NULL. In this callback function, one must
 * not free this handle with @c larodClearError(), since the library will do
 * it when returning from this callback.
 */
typedef void (*larodRunJobCallback)(void* userData, larodError* error);

/**
 * @brief Connection handle type for the larod service.
 *
 * This connection handle type is used to connect to and interfacing the larod
 * service.
 */
typedef struct larodConnection larodConnection;

/**
 * @brief Type describing a job request.
 *
 * This is used when running jobs such as pre-processing or inference.
 */
typedef struct larodJobRequest larodJobRequest;

/**
 * @brief A type representing a tensor.
 *
 * This type contains various information about a tensor, such as dimensions,
 * data type, data file descriptor etcetera. The properties of a tensor is
 * accessed by get and set functions (e.g. @c larodGetTensorDims()).
 */
typedef struct larodTensor larodTensor;

/**
 * @brief A type containing key-value pairs.
 *
 * This type can hold key-value pairs, where the key is a string and the value
 * can be any of a given number of types. It should be created using @c
 * larodCreateMap(), destroyed using @c larodDestroyMap(), and accessed
 * using the various access functions such as @c larodMapSetInt(), @c
 * larodMapGetStr(), etcetera.
 */
typedef struct larodMap larodMap;

/**
 * @brief Deallocate an error handle.
 *
 * Functions that has an argument of type @c larodError** allocates @c
 * larodError when errors occur. This function should then later be used to
 * deallocate it.
 *
 * @param error An initialized error handle or NULL. If @p error is NULL this
 * function will do nothing. @p *error will be set to NULL after this call.
 */
void larodClearError(larodError** error);

/**
 * @brief Connect to larod.
 *
 * Connects to larod and creates a new session. Upon success, this allocates the
 * connection handle @p conn and must thus be freed afterwards with with
 * larodDisconnect().
 *
 * @param conn An uninitialized connection handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodConnect(larodConnection** conn, larodError** error);

/**
 * @brief Disconnect from larod.
 *
 * Disconnects from larod and kills the corresponding session. This will always
 * deallocate the connection handle @p conn and set it to NULL (as well as other
 * allocated stuff). The return value indicates if the corresponding session in
 * larod was successfully killed.
 *
 * @param conn An initialized connection handle. The dereferenced @p conn will
 * be set to NULL after the connection is released. If @p conn is NULL this
 * function will do nothing.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodDisconnect(larodConnection** conn, larodError** error);

/**
 * @brief Get number of currently active sessions in larod.
 *
 * @param conn An initialized connection handle.
 * @param numSessions Pointer to the variable where the result will be stored.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodGetNumSessions(larodConnection* conn, uint64_t* numSessions,
                         larodError** error);

/**
 * @brief Get name of a chip.
 *
 * This is a convenience function to get the string representation of a chip. If
 * @p chip has an invalid value (not defined in enum type @c larodChip),
 * "Invalid chip" will be returned.
 *
 * @param chip Chip to get the name of.
 * @return String representation of @p chip.
 */
const char* larodGetChipName(const larodChip chip);

/**
 * @brief List available chips.
 *
 * Fills @p chips with a list of available @c larodChip sorted in no particular
 * order.
 *
 * @param conn An initialized connection handle.
 * @param chips Pointer to a pointer where an array of returned chips will be
 * allocated to. After a successful call, you should thus free @p *chips!
 * @param numChips Output pointer that will be filled with number of chips.
 * This can be set to NULL if client does not need to retrieve this information.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodListChips(larodConnection* conn, larodChip** chips, size_t* numChips,
                    larodError** error);

/**
 * @brief Load a new model.
 *
 * This function returns a model handle. Use this for referencing the loaded
 * model in other functions such as @ref larodDeleteModel(), @ref
 * larodCreateJobRequest() etcetera. Note that the file descriptor @p fd
 * for the binary model file must have valid reading permissions.
 *
 * @param conn An initialized connection handle.
 * @param fd File descriptor to the model data. This can be set to a negative
 * value (i.e. invalid) if @p params is also specified (not NULL).
 * @param chip Chip type to load model onto.
 * @param access Access specifier for the model.
 * @param name Name for the model.
 * @param params Additional optional parameters (can be NULL). Please refer to
 * the documentation for the @p chip you are using to see what key-value pairs
 * are applicable.
 * @param error An uninitialized handle to an error. @p error
 * can also be NULL if one does not want any error information. In case of
 * errors (when return value is NULL) it must later be deallocated with @c
 * larodClearError().
 * @return NULL if any errors occur, otherwise a valid pointer to @c larodModel.
 * The returned pointer should be released with @c larodDestroyModel() when it
 * is no longer needed.
 */
larodModel* larodLoadModel(larodConnection* conn, const int fd,
                           const larodChip chip, const larodAccess access,
                           const char* name, const larodMap* params,
                           larodError** error);

/**
 * @brief Load a new model asynchronously.
 *
 * This is the asynchronous version of larodLoadModel(). The callback function
 * @p callback will be called as soon as the model has been loaded.
 *
 * @param conn An initialized connection handle.
 * @param fd File descriptor to the model data. This can be set to a negative
 * value (i.e. invalid) if @p params is also specified (not NULL).
 * @param chip Chip type to load model onto.
 * @param access Access specifier for the model.
 * @param name Name for the model.
 * @param params Additional optional parameters (can be NULL). Please refer to
 * the documentation for the @p chip you are using to see what key-value pairs
 * are applicable.
 * @param callback Callback function.
 * @param userData User data that will be passed to the callback function.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodLoadModelAsync(larodConnection* conn, const int fd,
                         const larodChip chip, const larodAccess access,
                         const char* name, const larodMap* params,
                         larodLoadModelCallback callback, void* userData,
                         larodError** error);

/**
 * @brief Get handle to a model by model ID.
 *
 * The model ID could for example be retrieved with @c larodGetModelId().
 *
 * @param conn An initialized connection handle.
 * @param modelId Model ID to get the corresponding model handle for.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if any errors occur, otherwise a valid pointer to @c larodModel.
 * The returned pointer should be released with @c larodDestroyModel() when it
 * is no longer needed.
 */
larodModel* larodGetModel(larodConnection* conn, const uint64_t modelId,
                          larodError** error);

/**
 * @brief Get all loaded models.
 *
 * Retrieve a list of all loaded models. Caller needs to free this list
 * afterwards with the function @c larodDestroyModels().
 *
 * If any of the returned models' sizes exceeds the address space size of this
 * system, then SIZE_MAX will be the listed as the model size (see @c
 * larodGetModelSize()).
 *
 * @param conn An initialized connection handle.
 * @param numModels Output pointer that will be filled with number of entries
 * in the returned array. If the number of loaded models is 0 in the server,
 * this pointer will be set accordingly. If an error occurred the target of this
 * pointer remains untouched.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if any errors occurred, an array with a single element NULL if
 * the number of loaded models is 0 in the service, and otherwise an array of
 * pointers to @c larodModel.
 */
larodModel** larodGetModels(larodConnection* conn, size_t* numModels,
                            larodError** error);

/**
 * @brief Free an allocated @c larodModel handle.
 *
 * Release the memory for a model handle. @p model must not be used after this
 * call. The model will not be deleted in the server by this call. Use @c
 * larodDeleteModel() to accomplish this.
 *
 * @param model Pointer to model (allocated from e.g a @c larodLoadModel() call)
 * or NULL. The model handle will be released and then the dereferenced @p model
 * will be set to NULL. If @p model is NULL this function will do nothing.
 */
void larodDestroyModel(larodModel** model);

/**
 * @brief Destroy a list of model objects.
 *
 * Release all data associated with @p models (which was returned by @c
 * larodGetModels()). The pointer @p models must not be used after this call.
 *
 * @param models Pointer to allocated (with @c larodGetModels()) list of
 * models or NULL. The dereferenced @p models will be set to NULL after release
 * of the list. If @p models is NULL this function will do nothing.
 * @param numModels Number of models in the @p models array.
 */
void larodDestroyModels(larodModel*** models, size_t numModels);

/**
 * @brief Delete a loaded model.
 *
 * A public model can be deleted by anyone. However, for private models, only
 * the creator has permission to delete it (if the creator disconnects, the
 * private models will automatically get deleted).
 *
 * The model handle @p model will not be released after a successful call.
 * Consequently @c larodDestroyModel() should be called for @p model when no
 * longer needed.
 *
 * @param conn An initialized connection handle.
 * @param model Model to delete.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodDeleteModel(larodConnection* conn, larodModel* model,
                      larodError** error);

/**
 * @brief Get model ID.
 *
 * All models have a unique ID.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is LAROD_INVALID_MODEL_ID), it must later be deallocated with @c
 * larodClearError().
 * @return LAROD_INVALID_MODEL_ID if error has occurred, otherwise a valid model
 * ID.
 */
uint64_t larodGetModelId(const larodModel* model, larodError** error);

/**
 * @brief Get chip for a model.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is LAROD_INVALID_MODEL_ID), it must later be deallocated with @c
 * larodClearError().
 * @return LAROD_CHIP_INVALID if error has occurred, otherwise the associated
 * chip for @p model.
 */
larodChip larodGetModelChip(const larodModel* model, larodError** error);

/**
 * @brief Get model size.
 *
 * Get size (in bytes) of a loaded model.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors, it must later
 * be deallocated with @c larodClearError().
 * @return Model size in bytes or 0 if error occurred.
 */
size_t larodGetModelSize(const larodModel* model, larodError** error);

/**
 * @brief Get model name.
 *
 * The returned pointer is valid as long as @p model handle is valid, the caller
 * does not need to deallocate it.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return Model name as NULL-terminated string, or NULL if error occurred.
 */
const char* larodGetModelName(const larodModel* model, larodError** error);

/**
 * @brief Get model access mode.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is LAROD_ACCESS_INVALID), it must later be deallocated with @c
 * larodClearError().
 * @return LAROD_ACCESS_INVALID if error has occurred, otherwise a valid model
 * access.
 */
larodAccess larodGetModelAccess(const larodModel* model, larodError** error);

/**
 * @brief Get number of input tensors for a model.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is 0), it must later be deallocated with @c larodClearError().
 * @return Number of input tensors or 0 if error occurred.
 */
size_t larodGetModelNumInputs(const larodModel* model, larodError** error);

/**
 * @brief Get number of output tensors for a model.
 *
 * @param model Pointer to an initialized model handle.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is 0), it must later be deallocated with @c larodClearError().
 * @return Number of output tensors or 0 if error occurred.
 */
size_t larodGetModelNumOutputs(const larodModel* model, larodError** error);

/**
 * @brief Get input tensor byte sizes for a model.
 *
 * Allocate input tensors byte sizes based on a @c larodModel. The array of byte
 * sizes needs to be released when it is no longer needed by calling @c free().
 *
 * @param model Pointer to an initialized model handle.
 * @param numInputs Output pointer that will be filled with number of input
 * tensors. This can be set to NULL if client does not need to retrieve this
 * information (e.g. if the number is already known).
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of input tensor
 * byte sizes.
 */
size_t* larodGetModelInputByteSizes(const larodModel* model, size_t* numInputs,
                                    larodError** error);

/**
 * @brief Get output tensor byte sizes for a model.
 *
 * Allocate output tensors byte sizes based on a @c larodModel. The array of
 * byte sizes needs to be released when it is no longer needed by calling @c
 * free().
 *
 * @param model Pointer to an initialized model handle.
 * @param numOutputs Output pointer that will be filled with number of output
 * tensors. This can be set to NULL if client does not need to retrieve this
 * information (e.g. if the number is already known).
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of output
 * tensor byte sizes.
 */
size_t* larodGetModelOutputByteSizes(const larodModel* model,
                                     size_t* numOutputs, larodError** error);

/**
 * @brief Create input tensors from a model.
 *
 * Allocate input tensors based on a @c larodModel. The tensors will be
 * initialized with data type, dims, layout etcetera based on information from
 * the model. The allocated tensors will have file descriptors set to -1. The
 * array of tensors needs to be released when it is no longer needed using @c
 * larodDestroyTensors().
 *
 * @param model Pointer to an initialized model handle.
 * @param numTensors Output pointer that will be filled with number of entries
 * in the returned array. This can be set to NULL if client does not need to
 * retrieve this information (e.g. if the number is already known).
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of @c
 * larodTensor pointers.
 */
larodTensor** larodCreateModelInputs(const larodModel* model,
                                     size_t* numTensors, larodError** error);

/**
 * @brief Create output tensors from a model.
 *
 * Allocate output tensors based on a @c larodModel. The tensors will be
 * initialized with data type, dims, layout etcetera based on information from
 * the model. The allocated tensors will have file descriptors set to -1. The
 * array of tensors needs to be released when it is no longer needed using @c
 * larodDestroyTensors().
 *
 * @param model Pointer to an initialized model handle.
 * @param numTensors Output pointer that will be filled with number of entries
 * in the returned array. This can be set to NULL if client does not need to
 * retrieve this information (e.g. if the number is already known).
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of @c
 * larodTensor pointers.
 */
larodTensor** larodCreateModelOutputs(const larodModel* model,
                                      size_t* numTensors, larodError** error);

/**
 * @brief Create a list of empty @c larodTensor handles.
 *
 * Creates a list of @c larodTensor. After a successful call, various attributes
 * can be set on these tensors. Properties will be initialized to "unspecified"
 * or similar. The returned allocated array needs to be released with @c
 * larodDestroyTensors() when no longer needed.
 *
 * New tensors created with this function will have an empty name string.
 *
 * @param numTensors Number of tensors to create.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of @c
 * larodTensor pointers.
 */
larodTensor** larodCreateTensors(size_t numTensors, larodError** error);

/**
 * @brief Destroy a list of @c larodTensor.
 *
 * Releases the memory associated with a list of @c larodTensor handles. Neither
 * the pointers in the @p tensors array nor the array itself can be used after
 * this call.
 *
 * @param tensors Pointer to array of @c larodTensor pointers or NULL. The
 * dereferenced @p tensors will be set to NULL after the list is released. If @p
 * tensors is NULL, this function will do nothing.
 * @param numTensors Number of tensors in the @p tensors array.
 */
void larodDestroyTensors(larodTensor*** tensors, size_t numTensors);

/**
 * @brief Set the dimensions of a tensor.
 *
 * If the @p dims member @c len is set to 0 no sanity checks for the tensor
 * dimensions (i.e. comparisons with what the model expects of the tensor
 * dimensions) will be performed when running a job.
 *
 * If @c len is specified, it must be equal to the @c len of the tensor's
 * pitches if that is also specified; otherwise one gets an error when creating
 * job requests. See @c larodTensorPitches for more details of how they are
 * related.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param dims Pointer to @p larodTensorDims. The contents of the struct will be
 * copied into @p tensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodSetTensorDims(larodTensor* tensor, const larodTensorDims* dims,
                        larodError** error);

/**
 * @brief Get dimensions of a tensor.
 *
 * See @c larodTensorDims for definition of dimensions and @c larodTensorPitches
 * for how they are related to tensors' pitches.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise a pointer to @c larodTensorDims
 * instance. This pointer is valid as long as @p tensor is valid.
 */
const larodTensorDims* larodGetTensorDims(const larodTensor* tensor,
                                          larodError** error);

/**
 * @brief Set pitches of a tensor.
 *
 * If the @p pitches member @c len is set to 0 no sanity checks for the tensor
 * pitches (i.e. comparisons with what the model expects of the tensor pitches)
 * will be performed when running a job. See @c larodTensorPitches for
 * the definition of pitches.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param pitches Pointer to @p larodTensorPitches. The contents of the struct
 * will be copied into @p tensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodSetTensorPitches(larodTensor* tensor,
                           const larodTensorPitches* pitches,
                           larodError** error);

/**
 * @brief Get pitches of a tensor.
 *
 * See @c larodTensorPitches for the definition of pitches.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return NULL if error occurred, otherwise a pointer to @c larodTensorDims
 * instance. This pointer is valid as long as @p tensor is valid.
 */
const larodTensorPitches* larodGetTensorPitches(const larodTensor* tensor,
                                                larodError** error);

/**
 * @brief Set data type of a tensor.
 *
 * If the data type is set to @c LAROD_TENSOR_DATA_TYPE_UNSPECIFIED no sanity
 * checks for the tensor data type (i.e. comparisons with what the model expects
 * of the tensor data type) will be performed when running a job.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param dataType Data type for the tensor (see @c larodTensorDataType).
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodSetTensorDataType(larodTensor* tensor,
                            const larodTensorDataType dataType,
                            larodError** error);

/**
 * @brief Get data type of a tensor.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is @c LAROD_TENSOR_DATA_TYPE_INVALID), it must later be deallocated
 * with @c larodClearError().
 * @return @c LAROD_TENSOR_DATA_TYPE_INVALID if error has occurred, otherwise a
 * valid tensor data type.
 */
larodTensorDataType larodGetTensorDataType(const larodTensor* tensor,
                                           larodError** error);

/**
 * @brief Set layout of a tensor.
 *
 * If the layout is set to @c LAROD_TENSOR_LAYOUT_UNSPECIFIED no sanity checks
 * for the tensor layout (i.e. comparisons with what the model expects of the
 * tensor layout) will be performed when running a job.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param layout Layout as specified by @c larodTensorLayout.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodSetTensorLayout(larodTensor* tensor, const larodTensorLayout layout,
                          larodError** error);

/**
 * @brief Get layout of a tensor.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is LAROD_TENSOR_LAYOUT_INVALID), it must later be deallocated with @c
 * larodClearError().
 * @return LAROD_TENSOR_LAYOUT_INVALID if error occurred, otherwise a valid @c
 * larodTensorLayout.
 */
larodTensorLayout larodGetTensorLayout(const larodTensor* tensor,
                                       larodError** error);

/**
 * @brief Set file descriptor of a tensor.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param fd File descriptor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodSetTensorFd(larodTensor* tensor, const int fd, larodError** error);

/**
 * @brief Get file descriptor of a tensor.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is LAROD_INVALID_FD), it must later be deallocated with @c
 * larodClearError().
 * @return LAROD_INVALID_FD if error occurred, otherwise a valid file
 * descriptor.
 */
int larodGetTensorFd(const larodTensor* tensor, larodError** error);

/**
 * @brief Set file descriptor maximum capacity (bytes) for a tensor.
 *
 * Sets the maximum number of bytes that larod service is allowed to read or
 * write to the file descriptor associated with @p tensor. If @p size is set to
 * 0 for an input tensor (default), then larod will use lseek() or similar to
 * determine the full size of the buffer backing the fd. If @p size is set to 0
 * for an output tensor (default), there will be no size check when larod is
 * writing to the fd. It is valid to set an fd offset using @c
 * larodSetTensorFdOffset() even when @p size is 0.

 * Note that for input tensors, larod will always read the full input area from
 * the tensor fd at the start of running a job, even if tensor dimensions and
 * data type may indicate smaller amount of data.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param size Maximum capacity (bytes). Setting this to 0 will cause larod to
 * try determining size at runtime (default).
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodSetTensorFdSize(larodTensor* tensor, const size_t size,
                          larodError** error);

/**
 * @brief Get file descriptor maximum capacity (bytes) for a tensor.
 *
 * Retrieves the user-supplied file descriptor maximum capacity (bytes) of a
 * tensor, i.e. the value set by @c larodSetTensorFdSize() or default value
 * 0. Please refer to @c larodSetTensorFdSize() for details about the size
 * value.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param size Output pointer, the dereferenced pointer will be updated with
 * currently set size.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodGetTensorFdSize(const larodTensor* tensor, size_t* size,
                          larodError** error);

/**
 * @brief Set file offset for a tensor.
 *
 * Set the file offset (bytes) that will be applied when larod is accessing
 * (reading/writing) the tensor's file descriptor during a job call. The offset
 * is specified from the beginning of the file. Negative @p offset values are
 * not allowed. The default value is 0.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param offset Offset from the start of tensor's file descriptor.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodSetTensorFdOffset(larodTensor* tensor, const int64_t offset,
                            larodError** error);

/**
 * @brief Get file offset for a tensor.
 *
 * Retrieves the user-supplied file offset (bytes) of the file descriptor
 * associated with @p tensor, i.e. the value set by @c larodSetTensorFdOffset()
 * or default value 0.
 *
 * @param tensor Pointer to already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return -1 if error occurred, otherwise the configured offset value.
 */
int64_t larodGetTensorFdOffset(const larodTensor* tensor, larodError** error);

/**
 * @brief Get name of tensor.
 *
 * Returns a reference to a string representing the name of the supplied tensor.
 * The returned string pointer is valid as long as @p tensor is valid.
 *
 * @param tensor Pointer to an already allocated @c larodTensor.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors
 * (when return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a pointer to an initialized string
 * representing the name of @p tensor.
 */
const char* larodGetTensorName(const larodTensor* tensor, larodError** error);

/**
 * @brief Create new @c larodMap.
 *
 * Creates and initializes a handle to a mapping of key-value pairs. This struct
 * is used to send additional information to the service, see for instance @c
 * larodLoadModel. In particular, this struct may contain information that is
 * relevant to only one, or a few backends.
 *
 * It can hold various different data types as values and each value is
 * identified via a key string. Adding key-value pairs to the mapping should be
 * done using the set functions @c larodMapSetStr(), @c larodMapSetInt(),
 * etcetera, and accessing the elements should be done via the get functions @c
 * larodMapGetStr(), @c larodMapGetInt(), and so on. Strings added to this map
 * has a maximum allowed length of 128 bytes (including terminating NULL-byte).
 *
 * Once the mapping is no longer needed it should be destroyed using @c
 * larodDestroyMap(). When key-value pairs are added to the map copies of the
 * data are created, and the allocated memory is managed internally.
 *
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors
 * (when return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a pointer to an initialized @c
 * larodMap struct. Should be destroyed using @c larodDestroyMap when no longer
 * needed.
 */
larodMap* larodCreateMap(larodError** error);

/**
 * @brief Destroy a @c larodMap.
 *
 * Releases all memory associated with a @c larodMap, including allocated
 * key-value pairs. @p map should not be used after this call.
 *
 * @param map Pointer to an initialized handle of a @c larodMap or NULL.
 */
void larodDestroyMap(larodMap** map);

/**
 * @brief Add a string to a @c larodMap object.
 *
 * Adds a copy of the string pointed to by @p value to a @c larodMap object
 * and associates the value with a string given by @p key. If a string type
 * value already exists for the given key it will be replaced by the new value.
 * If a value already exists for the given key, but the value is of another
 * type an error will be reported. Both key and value strings should be at
 * maximum 128 bytes long (including terminating NULL-byte).
 *
 * @param map Handle to a @c larodMap object.
 * @param key String key to identify @p value with.
 * @param value String value to associate with @p key.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodMapSetStr(larodMap* map, const char* key, const char* value,
                    larodError** error);

/**
 * @brief Add an integer to a @c larodMap object.
 *
 * Adds a 64-bit integer to a @c larodMap object and associates the value
 * with a string given by @p key. If an integer type value already exists for
 * the given key it will be replaced by the new value. If a value already
 * exists for the given key, but the value is of another type an error will be
 * reported. The key string should be at maximum 128 bytes long (including
 * terminating NULL-byte).
 *
 * @param map Handle to a @c larodMap object.
 * @param key String key to identify @p value with.
 * @param value Value to associate with @p key.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodMapSetInt(larodMap* map, const char* key, const int64_t value,
                    larodError** error);

/**
 * @brief Add an integer array of two elements to a @c larodMap object.
 *
 * Adds a 64-bit integer array of two elements to a @c larodMap object and
 * associates it with a string given by @p key. If a two element integer array
 * value already exists for the given key it will be replaced by the new value.
 * If a value already exists for the given key, but the value is of another
 * type an error will be reported. The key string should be at maximum 128 bytes
 * long (including terminating NULL-byte).
 *
 * @param map Handle to a @c larodMap object.
 * @param key String key to identify @p value with.
 * @param value0 First array value to associate with @p key.
 * @param value1 Second array value to associate with @p key.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodMapSetIntArr2(larodMap* map, const char* key, const int64_t value0,
                        const int64_t value1, larodError** error);

/**
 * @brief Add an integer array of four elements to a @c larodMap object.
 *
 * Adds a 64-bit integer array of four elements to a @c larodMap object and
 * associates it with a string given by @p key. If a four element integer array
 * value already exists for the given key it will be replaced by the new value.
 * If a value already exists for the given key, but the value is of another
 * type an error will be reported. The key string should be at maximum 128 bytes
 * long (including terminating NULL-byte).
 *
 * @param map Handle to a @c larodMap object.
 * @param key String key to identify @p value with.
 * @param value0 First array value to associate with @p key.
 * @param value1 Second array value to associate with @p key.
 * @param value2 Third array value to associate with @p key.
 * @param value3 Fourth array value to associate with @p key.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodMapSetIntArr4(larodMap* map, const char* key, const int64_t value0,
                        const int64_t value1, const int64_t value2,
                        const int64_t value3, larodError** error);

/**
 * @brief Get string type value tied to given key.
 *
 * Get a string value corresponding to the given key. If the key is not found
 * in @p map, or if the key exists, but the value is not a string type an error
 * is reported. The key string should be at maximum 128 bytes long (including
 * terminating NULL-byte).
 *
 * @param map Object to search for the key.
 * @param key Key string to search for.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a string corresponding to the given
 * key. Should not be freed by the user.
 */
const char* larodMapGetStr(larodMap* map, const char* key, larodError** error);

/**
 * @brief Get an integer type value tied to given key.
 *
 * Get a 64-bit integer corresponding to the given key. If the key is not found
 * in @p map, or if the key exists, but the value is not an integer type an
 * error is reported. The key string should be at maximum 128 bytes long
 * (including terminating NULL-byte).
 *
 * @param map Object to search for the key.
 * @param key Key string to search for.
 * @param value Output pointer for value to be extracted. The referenced memory
 * will be set to the value of the requested key if call is successful,
 * otherwise it will not be changed.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodMapGetInt(larodMap* map, const char* key, int64_t* value,
                    larodError** error);

/**
 * @brief Get an integer array of two elements tied to given key.
 *
 * Get a 64-bit integer array of two elements, corresponding to the given key.
 * If the key is not found in @p map, or if the key exists, but the value is not
 * an integer array of two elements an error is reported. The key string should
 * be at maximum 128 bytes long (including terminating NULL-byte).
 *
 * @param map Object to search for the key.
 * @param key Key string to search for.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of two elements
 * corresponding to the given key. Should not be freed by the user.
 */
const int64_t* larodMapGetIntArr2(larodMap* map, const char* key,
                                  larodError** error);

/**
 * @brief Get an integer array of four elements tied to given key.
 *
 * Get a 64-bit integer array of four elements, corresponding to the given key.
 * If the key is not found in @p map, or if the key exists, but the value is not
 * an integer array of four elements an error is reported. The key string should
 * be at maximum 128 bytes long (including terminating NULL-byte).
 *
 * @param map Object to search for the key.
 * @param key Key string to search for.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise pointer to an array of four
 * elements corresponding to the given key. Should not be freed by the user.
 */
const int64_t* larodMapGetIntArr4(larodMap* map, const char* key,
                                  larodError** error);

/**
 * @brief Create and initialize a job request handle.
 *
 * Creates and initializes a job request handle with a minimum set of arguments
 * needed to run a job. The job request needs to be deallocated with @c
 * larodDestroyJobRequest() when no longer needed.
 *
 * The tensor data and model information is copied into the @c larodJobRequest
 * when it is created. The input and output file descriptors specified in the
 * tensors @c inputTensors and @c outputTensors must have reading and writing
 * permissions respectively when running the job. Moreover, @p inputTensors file
 * descriptors' file offsets will be set to the beginning before reading, while
 * @p outputTensors file descriptors' file offsets will be untouched before
 * writing (and left at the resulting position after the write).
 *
 * @param model Loaded model to run the job on.
 * @param inputTensors Array of input tensors.
 * @param numInputs Number of input tensors.
 * @param outputTensors Array of output tensors.
 * @param numOutputs Number of output tensors.
 * @param params Additional optional parameters (can be NULL). Please refer to
 * the documentation for the @p chip you are using to see what key-value pairs
 * are applicable.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a pointer to an initialized
 * request.
 */
larodJobRequest* larodCreateJobRequest(const larodModel* model,
                                       larodTensor** inputTensors,
                                       size_t numInputs,
                                       larodTensor** outputTensors,
                                       size_t numOutputs, larodMap* params,
                                       larodError** error);

/**
 * @brief Destroy a request handle.
 *
 * @p jobReq should not be used after this call.
 *
 * @param jobReq Pointer to an initialized handle or NULL. The dereferenced @p
 * jobReq will be set to NULL after the job request is released. If @p jobReq is
 * NULL this function will nothing.
 */
void larodDestroyJobRequest(larodJobRequest** jobReq);

/**
 * @brief Set model in a request.
 *
 * Specifies which model to run the job request on. Copies the @p model
 * information into the job request. The existing model information in @p req
 * will be overwritten.
 *
 * @param jobReq Pointer to an already allocated job request.
 * @param model Model to copy into the request.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is false), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodSetJobRequestModel(larodJobRequest* jobReq, const larodModel* model,
                             larodError** error);

/**
 * @brief Set input tensor array in a request.
 *
 * Copies the @p tensors array into the request. The existing tensor array in @p
 * jobReq will be overwritten.
 *
 * @param jobReq Pointer to an already allocated job request.
 * @param tensors Tensor array to copy into the request.
 * @param numTensors Numbers of tensor entries in the tensor array.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is false), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodSetJobRequestInputs(larodJobRequest* jobReq, larodTensor** tensors,
                              const size_t numTensors, larodError** error);

/**
 * @brief Set output tensor array in a job request.
 *
 * Copies the @p tensors array into the job request. The existing tensor
 * array in @p jobReq will be overwritten.
 *
 * @param jobReq Job request handle.
 * @param tensors Tensor array to copy into the job request.
 * @param numTensors Numbers of tensor entries in the tensor array.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is false), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodSetJobRequestOutputs(larodJobRequest* jobReq, larodTensor** tensors,
                               const size_t numTensors, larodError** error);

/**
 * @brief Set an priority of a job request.
 *
 * Only applicable when used with asynchronous jobs. 0 means lowest priority
 * while 100 means highest priority.
 *
 * When the service processes requests for a given session, it will attempt to
 * run higher priority job requests before lower priority requests.
 * Specifically, the service will dispatch jobs to the underlying chip framework
 * in order of priority. As a result of the dependency on the underlying chip
 * framework, the service cannot guarantee that jobs will be finished in
 * priority order.
 *
 * As an illustrative example, consider a case where multiple low priority
 * requests are enqueued, followed by a high priority request, followed by
 * another set of low priority requests. In this case, after the high priority
 * request is enqueued, it is guaranteed to be the next request to be dispatched
 * to the chip framework. Thus, the high priority request may be processed
 * before some of the earlier low priority ones. Also, the high priority request
 * is guaranteed be processed before any low priority requests that were
 * enqueued later.
 *
 * Note also that priorities are only valid within the set of requests for a
 * given session. The priority does not influence the ordering in different
 * connections! Default priority is 50 when a request is created (@see
 * larodCreateJobRequest() and @see larodCreateEmptyJobRequest()).
 *
 * @param jobReq Pointer to an initialized handle.
 * @param priority The priority to attach to @p jobReq.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodSetJobRequestPriority(larodJobRequest* jobReq, const uint8_t priority,
                                larodError** error);

/**
 * @brief Set additional parameters to a job request.
 *
 * Copies the @p params map into the job request. The existing parameters map in
 * @p jobReq will be overwritten.
 *
 * @param jobReq Job request handle.
 * @param params Additional optional parameters. Please refer to the
 * documentation for the @p chip you are using to see what key-value pairs are
 * applicable.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is false), it must later be deallocated with @c
 * larodClearError().
 * @return False if error occurred, otherwise true.
 */
bool larodSetJobRequestParams(larodJobRequest* jobReq, const larodMap* params,
                              larodError** error);

/**
 * @brief Run a job on a loaded model.
 *
 * Data will be read or written using the tensors' file descriptors. The maximum
 * number of bytes that the larod service will access (read or write) can be set
 * by @c larodSetTensorFdSize() for each tensor. Offsets into the file
 * descriptors can be configured using @c larodSetTensorFdOffset(). If the size
 * value for a tensor is 0 (default value) then larod will use file operations
 * to determine the file descriptor size. The larod service will also use file
 * operations to read and write tensor data, thus the current file positions of
 * tensor file descriptors will be updated by the job call. The client
 * should not access the file descriptors during this call.
 *
 * @param conn An initialized connection handle.
 * @param jobReq An initialized job request handle. The input and output file
 * descriptors specified in the request must have reading and writing
 * permissions, respectively. Moreover, the input file descriptors' file offsets
 * will be set to the beginning before reading, while the output file
 * descriptors' file offsets will be untouched before writing (and left at the
 * resulting position after the write).
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodRunJob(larodConnection* conn, const larodJobRequest* jobReq,
                 larodError** error);

/**
 * @brief Run a job on a loaded model asynchronously.
 *
 * This is the asynchronous version of larodRunJob(). The same set of
 * parameters as larodRunJob() must be explicitly set in @p jobReq.
 *
 * Data will be accessed for read or write using the tensor file
 * descriptors. Offsets into the file descriptors can be configured using @c
 * larodSetTensorFdOffset(). The maximum number of bytes that the larod service
 * will access (read or write) can be set by @c larodSetTensorFdSize() for each
 * tensor. If the size value for a tensor is 0 (default value) then larod will
 * use file operations to determine the file descriptor size. The larod service
 * will also use file operations to read and write tensor data, thus the current
 * file positions of tensor file descriptors will be updated by the job call.
 * The client should not access the file descriptors while an asynchronous job
 * is running.
 *
 * The callback function @p callback will be called as soon as the job has
 * finished. The callbacks are not guaranteed to be called in any order; @p
 * userData can be used to tag different requests.
 *
 * @param conn An initialized connection handle.
 * @param jobReq An initialized job request handle. The input and output
 * file descriptors specified in the request must have reading and writing
 * permissions, respectively. Moreover, the input file descriptors' file offsets
 * will be set to the beginning before reading, while the output file
 * descriptors' file offsets will be untouched before writing (and left at the
 * resulting position after the write).
 * @param callback Callback function.
 * @param userData User data that will be passed to the callback function.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool larodRunJobAsync(larodConnection* conn, const larodJobRequest* jobReq,
                      larodRunJobCallback callback, void* userData,
                      larodError** error);

#ifdef __cplusplus
}
#endif

#endif // COM_AXIS_LAROD_H
