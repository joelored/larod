/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct HashMap HashMap;

typedef struct {
    uint64_t key;
    void* value;
} Pair;

/**
 * @brief Construct an empty hash map.
 *
 * This allocates a handle that needs to be freed with @ref hashMapDestruct()
 * afterwards.
 *
 * @param hashMap An uninitialized handle to a hash map.
 * @return Positive errno style return code (zero means success).
 */
int hashMapConstruct(HashMap** hashMap);

/**
 * @brief Destructs the hash map.
 *
 * @param hashMap Handle to a hash map to deallocate.
 * @return Positive errno style return code (zero means success).
 */
void hashMapDestruct(HashMap** hashMap);

/**
 * @brief Inserts an element.
 *
 * Note that only unique keys are allowed.
 *
 * @param hashMap Hash map to insert an element to.
 * @param key A unique key.
 * @param value Value to map the key to.
 * @return Positive errno style return code (zero means success). If key already
 * exists, @c EEXIST is returned.
 */
int hashMapInsert(HashMap** hashMap, uint64_t key, void* value);

/**
 * @brief Erases an element.
 *
 * @param hashMap Hash map to erase an element from.
 * @param key Key to remove.
 * @param erasedValue The value that the erased key mapped to (can also be set
 * to NULL).
 * @return Positive errno style return code (zero means success). If @p key
 * could not be found, @c ENODATA is returned.
 */
int hashMapErase(HashMap* hashMap, uint64_t key, void** erasedValue);

/**
 * @brief Get value of element with a specific key.
 *
 * @param hashMap Hash map to get value from.
 * @param key Key to get the value for.
 * @param retValue The retrieved value.
 * @return Positive errno style return code (zero means success). If @p key
 * could not be found, @c ENODATA is returned.
 */
int hashMapGetValue(HashMap* hashMap, uint64_t key, void** retValue);

/**
 * @brief Get the number of elements.
 *
 * @param hashMap Hash map to get the total number of elements from.
 * @param size Pointer to the returned size.
 * @return Positive errno style return code (zero means success).
 */
int hashMapGetSize(HashMap* hashMap, size_t* size);

/**
 * @brief Extract (by copying) all key-value pairs from the hash map.
 *
 * This will return a list of pointers to the pairs in the hash map.
 *
 * @param hashMap Hash map to extract from.
 * @param pairs Pointer to an array of Pair pointers where the extracted pairs
 * will be allocated. After a successful call, you should thus free @p pairs!
 * @param nbrOfPairs nbrOfPairs in @p *pairs.
 * @return Positive errno style return code (zero means success).
 */
int hashMapExtractAll(HashMap* hashMap, const Pair*** pairs,
                      size_t* nbrOfPairs);

#ifdef __cplusplus
}
#endif
