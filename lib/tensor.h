/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <systemd/sd-bus.h>

#include "error.h"
#include "larod.h"

/**
 * @brief D-Bus signature for tensor meta data.
 *
 * i: larodTensorDataType enum.
 * i: larodTensorLayout enum.
 * t: byte size.
 * at: dims array.
 * at: pitches array.
 * s: Tensor name.
 */
#define TENSOR_METADATA_FROM_SERVICE "iitatats"

/**
 * @brief D-Bus signature for tensor meta data.
 *
 * i: larodTensorDataType enum.
 * i: larodTensorLayout enum.
 * at: dims array.
 * at: pitches array.
 */
#define TENSOR_METADATA_TO_SERVICE "iiatat"

/**
 * @brief D-Bus signature for tensor fd, its size and offset.
 *
 * h: File descriptor.
 * t: Size.
 * x: Offset.
 */
#define TENSOR_BUFFERDATA "htx"

/**
 * @brief Type representing a tensor.
 */
struct larodTensor {
    int fd;                       ///< File descriptor representing the data.
    size_t fdSize;                ///< Maximum accessible area in the fd.
    int64_t fdOffset;             ///< Offset from the beginning of the fd to
                                  ///< the tensor data.
    larodTensorDims dims;         ///< Struct containing tensor dimensions data.
    larodTensorPitches pitches;   ///< Struct containing tensor pitch data.
    larodTensorDataType dataType; ///< Data type of the tensor data.
    larodTensorLayout layout;     ///< Layout of the tensor.
    size_t byteSize;              ///< Byte size of the tensor.
    char* name;                   ///< Name of tensor.
};

/**
 * @brief Read array of tensor meta data structs from D-Bus message.
 *
 * @param msg Message to read array of tensor meta data from.
 * @param numTensors Output pointer, will be filled with number of retrieved
 * tensors.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return NULL if any error occurred, otherwise a pointer to newly allocated
 * array of @c larodTensor pointers.
 */
larodTensor** tensorMetaDataArrayFromMsg(sd_bus_message* msg,
                                         size_t* numTensors,
                                         larodError** error);

/**
 * @brief Write tensors to sd-bus message.
 *
 * Write an array of larodTensor objects into a suitable D-Bus array.
 *
 * @param msg Message to write to.
 * @param tensors Tensors to write into message.
 * @param numTensors Number of entries in @p tensors array.
 * @param skipSizeSeek Set to true to skip trying to determine fd size if tensor
 * sizes is 0.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is false), it must later be deallocated with @c larodClearError().
 * @return False if any errors occur, otherwise true.
 */
bool appendTensorsToMsg(sd_bus_message* msg, larodTensor** tensors,
                        size_t numTensors, bool skipSizeSeek,
                        larodError** error);

/**
 * @brief Create identical tensor from an existing one.
 *
 * Allocate memory for a new tensor and copy all elements of @p tensor to
 * the new one, including the file descriptor value. It must be released
 * with @c larodDestroyTensor() when no longer needed.
 *
 * @param tensor Tensor to be duplicated.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a valid @c larodTensor pointer.
 */
larodTensor* copyTensor(const larodTensor* tensor, larodError** error);

/**
 * @brief Duplicate an array of tensors.
 *
 * Create a new array of tensor pointers and fill it with copies of each tensor
 * in the original array.
 *
 * @param sourceTensors Array of tensor objects to duplicate.
 * @param numSrcTensors Number of tensors in the array.
 * @param error An uninitialized handle to an error. @p error can also be
 * NULL if one does not want any error information. In case of errors (when
 * return value is NULL), it must later be deallocated with @c
 * larodClearError().
 * @return NULL if error occurred, otherwise a valid array of @c larodTensor
 * objects.
 */
larodTensor** copyTensorArray(larodTensor** sourceTensors, size_t numSrcTensors,
                              larodError** error);

/**
 * @brief Validate tensors.
 *
 * @param tensors List of tensors to validate.
 * @param numTensors Number of tensors in @p tensors.
 * @param error An uninitialized handle to an error. @p error can also be NULL
 * if one does not want any error information. In case of errors (when return
 * value is NULL), it must later be deallocated with @c larodClearError().
 * @return True if @p tensors does not contain any errors, otherwise false.
 */
bool validateTensors(larodTensor** tensors, const size_t numTensors,
                     larodError** error);
