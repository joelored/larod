/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "larod.h"

#include <assert.h>
#include <errno.h>
#include <poll.h>
#include <pthread.h> // TODO: use <threads.h> with glibc 2.28
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/eventfd.h>
#include <sys/mman.h>
#include <systemd/sd-bus.h>
#include <unistd.h>

#include "error.h"
#include "hashset.h"
#include "jobrequest.h"
#include "message.h"
#include "model.h"
#include "tensor.h"
#include "version.h"

#define LAROD_SERVICE_NAME "com.axis.Larod1"

#define LAROD_SERVICE_INTERFACE "com.axis.Larod1.Service"
#define LAROD_SERVICE_OBJ_PATH "/com/axis/Larod1"

#define LAROD_SESSION_INTERFACE "com.axis.Larod1.Session"
#define LAROD_SESSION_OBJ_PATH "/com/axis/Larod1/Session"

#define DBUS_TIMEOUT_SEC 120

// The function larodListChips is updated to fill in an output pointer with the
// number of chips instead of the byte size of the list.
__asm__(".symver larodListChips_v1, larodListChips@LIBLAROD_1.0.0");
__asm__(".symver larodListChips_v2, larodListChips@@LIBLAROD_1.3.0");
// The functions larodLoadModel and larodLoadModelAsync are updated to also take
// a chip ID parameter and a larodMap parameter.
__asm__(".symver larodLoadModel_v1, larodLoadModel@LIBLAROD_1.0.0");
__asm__(".symver larodLoadModel_v2, larodLoadModel@@LIBLAROD_2.0.0");
__asm__(".symver larodLoadModelAsync_v1, larodLoadModelAsync@LIBLAROD_1.0.0");
__asm__(".symver larodLoadModelAsync_v2, larodLoadModelAsync@@LIBLAROD_2.0.0");

// InferenceRequest is renamed to JobRequest.
__asm__(".symver larodRunJob, larodRunInference@LIBLAROD_1.0.0");
__asm__(".symver larodRunJobAsync, larodRunInferenceAsync@LIBLAROD_1.0.0");

/**
 * @brief Type containing connection information.
 */
struct larodConnection {
    sd_bus* asyncReadBus;  ///< sd-bus asynchronous connection for reading.
    sd_bus* asyncWriteBus; ///< sd-bus asynchronous connection for writing.
    sd_bus* bus;           ///< sd-bus synchronous connection.
    sd_bus_message* msg;   ///< sd-bus message.
    sd_bus_error error;    ///< sd-bus error.

    pthread_t busProcThread; ///< Asynchronous bus processing thread.
    int busProcEventFd;      ///< eventfd to release busProcThread from ppoll().
    atomic_bool stopBusProc; ///< Flag to signal busProcThread to exit.
    HashSet* requests;       ///< Set of sent asynchronous requests.
    pthread_mutex_t reqMtx;  ///< Mutex for accessing the set of sent requests.
    pthread_cond_t cond;     ///< Condition variable for signaling callbacks.
    pthread_mutex_t condMtx; ///< Mutex for callback condition variable.

    uint64_t id;          ///< Unique session ID.

    // Parameters for backwards compatibility for when one used to set/get chip
    // of each session.
    uint64_t legacyCurrChipId; ///< Current session chip ID.

    char* sessionObjPath; ///< D-Bus object path for session.
};

/**
 * @brief Enum type for asynchronous message requests.
 */
typedef enum {
    REQUEST_QUEUE_LOAD_MODEL, ///< Queue load model.
    REQUEST_QUEUE_JOB,        ///< Queue running jobs.
    REQUEST_DELETE_MODEL      ///< Delete model.
} RequestType;

typedef void (*FuncPtr)(void); // Generic function pointer typedef

/**
 * @brief Type containing callback information.
 */
typedef struct {
    FuncPtr func;        ///< Callback function.
    void* userData;      ///< User data.
    RequestType request; ///< Request type.
} CallbackInfo;

/**
 * @brief User data for @c larodLoadModel() callback.
 *
 * When loading model synchronously with @c larodLoadModel(), this struct is
 * used as user data for the underlying asynchronous request's callback.
 */
typedef struct {
    larodModel* model;
    larodConnection* conn;
    larodError** error;
    unsigned int refCount;
    bool done;
} LoadModelSyncCallbackData;

/**
 * @brief User data for callback functions.
 *
 * When running synchronous functions, this struct is used as user data for the
 * underlying asynchronous request's callback.
 */
typedef struct {
    larodConnection* conn;
    larodError** error;
    unsigned int refCount;
    bool done;
    bool ret;
} AsyncStatusCallbackData;

/**
 * @brief Removes a callback from the hash set in @c larodConnection.
 *
 * This erases the request @p callbakcInfo from @p larodConnection.requests.
 *
 * @param conn An initialized connection handle.
 * @param callbackInfo Pointer to CallbackInfo.
 */
static void eraseCallbackFromHashSet(larodConnection* conn,
                                     CallbackInfo* callbackInfo);

/**
 * @brief Callback for asynchronous message requests regarding loading models.
 *
 * @param callbackInfo Pointer to CallbackInfo.
 * @param model Pointer to model object.
 * @param error Pointer to larodError object, which is freed in this function.
 */
static void loadModelAsyncCallback(CallbackInfo* callbackInfo,
                                   larodModel* model, larodError* error);

/**
 * @brief Callback for @c larodLoadModel().
 *
 * This callback is used for underlying asynchronous load model request for the
 * synchronous function @c larodLoadModel(). This is a @c
 * larodLoadModelCallback().
 */
static void loadModelSyncCallback(larodModel* model, void* userData,
                                  larodError* error);

/**
 * @brief Callback for asynchronous message requests regarding jobs.
 *
 * @param callbackInfo Pointer to CallbackInfo.
 * @param error Pointer to larodError object, which is freed in this function.
 */
static void runJobAsyncCallback(CallbackInfo* callbackInfo, larodError* error);

/**
 * @brief Callback for @c larodRunJob().
 *
 * This callback is used for underlying asynchronous job requests for the
 * synchronous function @c larodRunJob(). This is a @c larodRunJobCallback().
 */
static void runJobSyncCallback(void* userData, larodError* error);

/**
 * @brief Runs callback function.
 *
 * @param callbackInfo Pointer to CallbackInfo.
 * @param error Pointer to larodError object, which is freed in this function.
 */
static void deleteModelAsyncCallback(CallbackInfo* callbackInfo,
                                     larodError* error);

/**
 * @brief Callback for @c larodDeleteModel().
 *
 * This callback is used for underlying asynchronous delete model request for
 * the synchronous function @c larodDeleteModel().
 */
static void deleteModelSyncCallback(void* userData, larodError* error);

/**
 * @brief Free a connection handle.
 *
 * This will disconnect from the sd-bus bus as well as free allocated
 * resources. @p *conn will be set to NULL afterwards.
 *
 * @param conn An initialized connection handle. Will do nothing if it is NULL.
 */
static void freeConn(larodConnection** conn);

/**
 * @brief Create a sd-bus bus with a given file descriptor.
 *
 * This will allocate and start a new sd-bus bus. It will also set the option to
 * allow sending of file descriptors over the bus.
 *
 * @param bus Pointer to a pointer of an uninitialized sd-bus.
 * @param fd File descriptor to use with @p bus.
 * @return Positive errno style return code (zero means success).
 */
static int createAsyncBus(sd_bus** bus, int fd);

/**
 * @brief Bus polling and processing thread.
 *
 * Polls the bus (using sd_bus_wait()) and processes it (using sd_bus_process())
 * forever. This is used on the asynchronous connection for triggering the
 * callbacks.
 *
 * @param arg Bus to process (will be casted to sd_bus*).
 * @return Returns NULL.
 */
static void* processBus(void* arg);

/**
 * @brief Release the reference to a shared pointer.
 *
 * Uses a reference-counting solution to release a shared resource. The
 * reference counter will be decremented and the pointer that data points to
 * will be set to NULL. If the reference counter becomes zero as a result from
 * calling this function, the data will be freed as well (by using free()).
 *
 * @param data Pointer to the pointer to delete the reference to.
 * @param refCount A pointer to the reference counter.
 */
static void releaseReference(void** data, unsigned int* refCount);

/**
 * @brief Get the chip ID of larodChip.
 *
 * Finds the first instance of the given @c larodChip and sets @p chipId.
 *
 * @param conn An initialized connection handle.
 * @param chip Chip to find first instance of.
 * @param chipId Variable to be set.
 * @return True if chip is found; false otherwise.
 */
static bool getFirstChipId(larodConnection* conn, const larodChip chip,
                           uint64_t* chipId);

/**
 * @brief Get chip type from legacy chip ID.
 *
 * Finds what @c larodChip that corresponds to the @c legacyCurrChipId in @p
 * conn. Only legacy functions should use this.
 *
 * @param conn An initialized connection handle.
 * @param chipType Pointer to variable to be set.
 * @return False is an error occurs; true otherwise.
 */
static bool getLegacyChip(larodConnection* conn, larodChip* chipType);

bool larodConnect(larodConnection** conn, larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    *conn = malloc(sizeof(larodConnection));
    if (!(*conn)) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return false;
    }

    (*conn)->asyncReadBus = NULL;
    (*conn)->asyncWriteBus = NULL;
    (*conn)->bus = NULL;
    (*conn)->msg = NULL;
    (*conn)->error = SD_BUS_ERROR_NULL;
    (*conn)->sessionObjPath = NULL;
    (*conn)->busProcEventFd = -1;
    (*conn)->stopBusProc = false;
    (*conn)->requests = NULL;
    (*conn)->legacyCurrChipId = 0;

    // Used in handshaking at the end of this function.
    sd_bus_message* handshakeMsg = NULL;

    // Initialize set of sent asynchronous requests.
    int ret = hashSetConstruct(&(*conn)->requests);
    if (ret) {
        larodCreateError(error, ret, "Could not create hash set: %s",
                         strerror(ret));

        goto error;
    }

    ret = pthread_mutex_init(&(*conn)->reqMtx, NULL);
    if (ret) {
        larodCreateError(error, ret, "Could not initialize mutex: %s",
                         strerror(ret));

        goto error;
    }

    pthread_condattr_t attr;

    ret = pthread_condattr_init(&attr);
    if (ret) {
        larodCreateError(error, ret, "Could not initialize condattr: %s",
                         strerror(ret));

        goto error;
    }

    ret = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    if (ret) {
        larodCreateError(error, ret, "Could not set condattr clock: %s",
                         strerror(ret));

        goto error;
    }

    ret = pthread_cond_init(&(*conn)->cond, &attr);
    if (ret) {
        larodCreateError(error, ret,
                         "Could not initialize condition variable: %s",
                         strerror(ret));

        goto error;
    }

    ret = pthread_mutex_init(&(*conn)->condMtx, NULL);
    if (ret) {
        larodCreateError(error, ret, "Could not initialize mutex: %s",
                         strerror(ret));

        goto error;
    }

    // Connect to bus
#ifdef LAROD_USE_SESSION_BUS
    ret = sd_bus_open_user(&(*conn)->bus);
#else
    ret = sd_bus_open_system(&(*conn)->bus);
#endif
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not open bus: %s", strerror(ret));

        goto error;
    }

    // Used in handshaking.
    const char* dbusConnName = NULL;
    ret = sd_bus_get_unique_name((*conn)->bus, &dbusConnName);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not get bus name: %s",
                         strerror(ret));

        goto error;
    }

    // Create a new session.
    // We pass on the name of our part of the D-Bus connection to the service as
    // a way of identification and also send the library version information.
    ret = sd_bus_call_method((*conn)->bus, LAROD_SERVICE_NAME,
                             LAROD_SERVICE_OBJ_PATH, LAROD_SERVICE_INTERFACE,
                             "CreateSession", &(*conn)->error, &(*conn)->msg,
                             "suuu", dbusConnName, LAROD_VERSION_MAJOR,
                             LAROD_VERSION_MINOR, LAROD_VERSION_PATCH);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create session: %s",
                         (*conn)->error.message);

        goto error;
    }

    ret = sd_bus_message_enter_container((*conn)->msg, 'r', "thhis");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not parse message: %s",
                         strerror(ret));

        goto error;
    }

    int asyncReadFd;
    int asyncWriteFd;
    int32_t errCode = LAROD_ERROR_CONNECTION;
    char* errMsg = NULL;
    ret = sd_bus_message_read((*conn)->msg, "thhis", &(*conn)->id, &asyncReadFd,
                              &asyncWriteFd, &errCode, &errMsg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not read message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_message_exit_container((*conn)->msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not parse message: %s",
                         strerror(ret));

        goto error;
    }

    ret = errCode;
    if (ret) {
        larodCreateError(error, ret, "%s", errMsg);

        goto error;
    }

    // Copy asynchronous file descriptors since sd-bus closes them when the
    // message is freed (sd-bus will later close them on "unref" on the
    // corresponding buses).
    asyncReadFd = dup(asyncReadFd);
    if (asyncReadFd < 0) {
        ret = errno;
        larodCreateError(error, ret, "Could not duplicate file descriptor: %s",
                         strerror(ret));

        goto error;
    }

    asyncWriteFd = dup(asyncWriteFd);
    if (asyncWriteFd < 0) {
        ret = errno;
        close(asyncReadFd);

        larodCreateError(error, ret, "Could not duplicate file descriptor: %s",
                         strerror(ret));

        goto error;
    }

    (*conn)->msg = sd_bus_message_unref((*conn)->msg);

    // Connect to asynchronous buses
    ret = createAsyncBus(&(*conn)->asyncReadBus, asyncReadFd);
    if (ret) {
        close(asyncReadFd);
        close(asyncWriteFd);

        larodCreateError(error, ret, "Could not create asynchronous bus: %s",
                         strerror(ret));

        goto error;
    }

    ret = createAsyncBus(&(*conn)->asyncWriteBus, asyncWriteFd);
    if (ret) {
        close(asyncWriteFd);

        larodCreateError(error, ret, "Could not create asynchronous bus: %s",
                         strerror(ret));

        goto error;
    }

    // Create eventfd for read bus processing thread. This is also used as a
    // flag for asynchronous read bus processing thread; if
    // (*conn)->busProcEventFd is non-negative, that indicates that
    // (*conn)->busProcThread has been created, otherwise it has not.
    (*conn)->busProcEventFd = eventfd(0, 0);
    if ((*conn)->busProcEventFd < 0) {
        ret = errno;
        larodCreateError(error, ret,
                         "Could not create event file descriptor: %s",
                         strerror(ret));

        goto error;
    }

    // Start asynchronous read bus processing thread.
    ret = pthread_create(&(*conn)->busProcThread, NULL, processBus, *conn);
    if (ret) {
        // Indicate that thread has not been created.
        close((*conn)->busProcEventFd);
        (*conn)->busProcEventFd = -1;

        larodCreateError(
            error, ret,
            "Could not create asynchronous bus processing thread: %s",
            strerror(ret));

        goto error;
    }

    // Parse session id
    int len =
        snprintf(NULL, 0, "%s/%" PRIu64, LAROD_SESSION_OBJ_PATH, (*conn)->id);
    if (len < 0) {
        ret = errno;
        larodCreateError(error, ret, "Could not parse session ID: %s",
                         strerror(ret));

        goto error;
    }

    (*conn)->sessionObjPath = malloc((size_t)(len + 1));
    if (!(*conn)->sessionObjPath) {
        ret = ENOMEM;
        larodCreateError(error, ret, "%s", strerror(ret));

        goto error;
    }
    int lenWritten =
        snprintf((*conn)->sessionObjPath, (size_t)(len + 1), "%s/%" PRIu64,
                 LAROD_SESSION_OBJ_PATH, (*conn)->id);
    if (lenWritten < 0 || lenWritten != len) {
        ret = errno;
        larodCreateError(error, ret, "Could not save session ID: %s",
                         strerror(ret));

        goto error;
    }

    // Handshake with server, telling it we successfully set up the asynchronous
    // file descriptors recieved.
    ret = sd_bus_message_new_method_call(
        (*conn)->asyncWriteBus, &handshakeMsg, LAROD_SERVICE_NAME,
        (*conn)->sessionObjPath, LAROD_SESSION_INTERFACE, LAROD_MSG_HANDSHAKE);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create handshake message: %s",
                         strerror(ret));

        goto error;
    }

    // Send our D-Bus connection name as payload.
    ret = sd_bus_message_append(handshakeMsg, "s", dbusConnName);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create handshake message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_send((*conn)->asyncWriteBus, handshakeMsg, NULL);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send handshake message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_flush((*conn)->asyncWriteBus);
    handshakeMsg = sd_bus_message_unref(handshakeMsg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send handshake message: %s",
                         strerror(ret));

        goto error;
    }

    goto end;

error:
    handshakeMsg = sd_bus_message_unref(handshakeMsg);
    freeConn(conn);

end:
    return ret == 0;
}

bool larodDisconnect(larodConnection** conn,
                     __attribute__((unused)) larodError** error) {
    if (!conn || !(*conn)) {
        return true;
    }

    freeConn(conn);

    return true;
}

static void freeConn(larodConnection** conn) {
    if (!conn || !(*conn)) {
        return;
    }

    // Stop bus processing thread
    (*conn)->stopBusProc = true;

    // Check if read bus processing thread has been created.
    if ((*conn)->busProcEventFd >= 0) {
        // Signal the eventfd to release the thread blocking on ppoll().
        uint64_t value = 1;
        ssize_t writtenBytes =
            write((*conn)->busProcEventFd, &value, sizeof(value));
        if (writtenBytes == sizeof(value)) {
            pthread_join((*conn)->busProcThread, NULL);
        } else {
            pthread_cancel((*conn)->busProcThread);
        }
    }

    // Disconnect from buses
    sd_bus_error_free(&(*conn)->error);
    (*conn)->msg = sd_bus_message_unref((*conn)->msg);
    (*conn)->bus = sd_bus_close_unref((*conn)->bus);
    (*conn)->asyncReadBus = sd_bus_close_unref((*conn)->asyncReadBus);
    (*conn)->asyncWriteBus = sd_bus_close_unref((*conn)->asyncWriteBus);

    // Free stuff
    if ((*conn)->busProcEventFd >= 0) {
        close((*conn)->busProcEventFd);
        (*conn)->busProcEventFd = -1;
    }

    // Assuming no one else is using the set during this call.
    hashSetDestruct(&(*conn)->requests);
    pthread_mutex_destroy(&(*conn)->reqMtx);
    pthread_cond_destroy(&(*conn)->cond);
    pthread_mutex_destroy(&(*conn)->condMtx);

    free((*conn)->sessionObjPath);
    free(*conn);
    *conn = NULL;
}

static int createAsyncBus(sd_bus** bus, int fd) {
    int ret = sd_bus_new(bus);
    if (ret < 0) {
        goto end;
    }

    ret = sd_bus_set_fd(*bus, fd, fd);
    if (ret < 0) {
        goto unref;
    }

    ret = sd_bus_negotiate_fds(*bus, true);
    if (ret < 0) {
        goto unref;
    }

    ret = sd_bus_set_anonymous(*bus, false);
    if (ret < 0) {
        goto unref;
    }

    ret = sd_bus_start(*bus);
    if (ret < 0) {
        goto unref;
    }

    goto end;

unref:
    sd_bus_close_unref(*bus);

end:
    return ret < 0 ? -ret : 0;
}

bool larodGetNumSessions(larodConnection* conn, uint64_t* numSessions,
                         larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!numSessions) {
        larodCreateError(error, EINVAL, "Pointer argument numSessions is NULL");

        return false;
    }

    sd_bus_error_free(&conn->error);

    int ret =
        sd_bus_get_property(conn->bus, LAROD_SERVICE_NAME,
                            LAROD_SERVICE_OBJ_PATH, LAROD_SERVICE_INTERFACE,
                            "NbrOfSessions", &(conn->error), &(conn->msg), "t");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not get number of sessions: %s",
                         conn->error.message);

        goto end;
    }

    *numSessions = 0;
    ret = sd_bus_message_read(conn->msg, "t", numSessions);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not read message: %s",
                         strerror(ret));

        goto end;
    }

    // sd-bus may return a positive integer on success.
    ret = 0;

end:
    conn->msg = sd_bus_message_unref(conn->msg);

    return ret == 0;
}

bool larodListChips_v1(larodConnection* conn, larodChip** chips, size_t* size,
                       larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!chips) {
        larodCreateError(error, EINVAL,
                         "Pointer to pointer argument chips is NULL");

        return false;
    }

    if (!size) {
        larodCreateError(error, EINVAL, "Pointer argument size is NULL");

        return false;
    }

    sd_bus_error_free(&conn->error);

    int ret = sd_bus_get_property(conn->bus, LAROD_SERVICE_NAME,
                                  LAROD_SERVICE_OBJ_PATH,
                                  LAROD_SERVICE_INTERFACE, "AvailableChips",
                                  &(conn->error), &(conn->msg), "ai");
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not get chips: %s",
                         conn->error.message);

        goto end;
    }

    int32_t* buf;
    ret = sd_bus_message_read_array(conn->msg, 'i', (const void**) &buf, size);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not read message: %s",
                         strerror(ret));

        goto end;
    }

    // sd-bus may return a positive integer on success.
    ret = 0;

    *chips = malloc(*size);
    if (!(*chips) && *size) {
        ret = ENOMEM;
        larodCreateError(error, ret, "%s", strerror(ret));

        goto end;
    }

    memcpy(*chips, buf, *size);

end:
    conn->msg = sd_bus_message_unref(conn->msg);

    return ret == 0;
}

bool larodListChips_v2(larodConnection* conn, larodChip** chips,
                       size_t* numChips, larodError** error) {
    if (!numChips) {
        larodCreateError(error, EINVAL, "Pointer argument numChips is NULL");

        return false;
    }

    size_t byteSize = 0;
    if (!larodListChips_v1(conn, chips, &byteSize, error)) {
        return false;
    }

    *numChips = byteSize / sizeof(int32_t);

    return true;
}

bool larodGetChipType(larodConnection* conn, larodChip* chipType,
                      larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!chipType) {
        larodCreateError(error, EINVAL, "Pointer argument chipType is NULL");

        return false;
    }

    larodChip* chips = NULL;
    size_t listEntries = 0;
    bool ret = larodListChips(conn, &chips, &listEntries, error);
    if (!ret) {
        goto end;
    }

    assert(conn->legacyCurrChipId < listEntries);

    *chipType = chips[conn->legacyCurrChipId];
end:
    free(chips);

    return ret;
}

const char* larodGetChipName(const larodChip chip) {
    switch (chip) {
    case LAROD_CHIP_DEBUG:
        return "Debug chip";
    case LAROD_CHIP_TFLITE_CPU:
        return "CPU with TensorFlow Lite";
    case LAROD_CHIP_TPU:
        return "Google TPU";
    case LAROD_CHIP_TFLITE_GLGPU:
        return "GPU with TensorFlow Lite (experimental)";
    case LAROD_CHIP_CVFLOW_NN:
        return "Ambarella CVFlow (NN)";
    case LAROD_CHIP_CVFLOW_PROC:
        return "Ambarella CVFlow (proc)";
    case LAROD_CHIP_LIBYUV:
        return "CPU with libyuv";
    default:
        return "Invalid chip";
    }
}

bool larodGetChipId(larodConnection* conn, uint64_t* chipId,
                    larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!chipId) {
        larodCreateError(error, EINVAL, "Pointer argument chipId is NULL");

        return false;
    }

    *chipId = conn->legacyCurrChipId;

    return true;
}

bool larodSetChipId(larodConnection* conn, const uint64_t chipId,
                    larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    larodChip* chips = NULL;
    size_t listEntries = 0;
    bool ret = larodListChips(conn, &chips, &listEntries, error);
    if (!ret) {
        goto end;
    }

    if (chipId > listEntries) {
        larodCreateError(error, EINVAL,
                         "chipId is too large (%" PRIu64 " > %zu)", chipId,
                         listEntries);
        ret = false;
        goto end;
    }

    conn->legacyCurrChipId = chipId;

end:
    free(chips);

    return ret;
}

bool getLegacyChip(larodConnection* conn, larodChip* chipType) {
    assert(conn);
    assert(chipType);

    // Get list of chips.
    larodChip* chips = NULL;
    size_t listEntries = 0;
    bool ret = true;
    if (!larodListChips(conn, &chips, &listEntries, NULL)) {
        ret = false;

        goto end;
    }
    assert(conn->legacyCurrChipId < listEntries);

    // Set what chip type the chip ID corresponds to.
    *chipType = chips[conn->legacyCurrChipId];

end:
    free(chips);

    return ret;
}

bool getFirstChipId(larodConnection* conn, const larodChip chip,
                    uint64_t* chipId) {
    assert(conn);
    assert(chipId);

    // Get list of chips.
    larodChip* chips = NULL;
    size_t listEntries = 0;
    bool ret = true;
    if (!larodListChips(conn, &chips, &listEntries, NULL)) {
        ret = false;

        goto end;
    }

    // Get index of chip.
    size_t i;
    for (i = 0; i < listEntries && chips[i] != chip; ++i) {
    }

    // Return false if not found.
    if (i == listEntries) {
        ret = false;

        goto end;
    }

    *chipId = (uint64_t) i;

end:
    free(chips);

    return ret;
}

bool larodSetChip(larodConnection* conn, const larodChip chip,
                  larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    // See if chip is valid.
    larodChip* chips = NULL;
    size_t listEntries = 0;
    bool ret = larodListChips(conn, &chips, &listEntries, error);
    if (!ret) {
        goto end;
    }
    for (size_t i = 0; i < listEntries; ++i) {
        if (chip == chips[i]) {
            conn->legacyCurrChipId = i;
            goto end;
        }
    }

    larodCreateError(error, ENODEV, "Could not set chip %s: no such chip found",
                     larodGetChipName(chip));
    ret = false;

end:
    free(chips);

    return ret;
}

void releaseReference(void** data, unsigned int* refCount) {
    assert(refCount);
    assert(*refCount);

    *refCount -= 1;
    if (*refCount == 0) {
        free(*data);
    }

    *data = NULL;
}

void loadModelSyncCallback(larodModel* model, void* userData,
                           larodError* error) {
    assert(userData);
    LoadModelSyncCallbackData* callbackData =
        (LoadModelSyncCallbackData*) userData;

    pthread_mutex_lock(&callbackData->conn->condMtx);

    callbackData->done = true;
    if (error) {
        larodCreateError(callbackData->error, error->code,
                         "Could not load model: %s", error->msg);
    } else {
        callbackData->model = model;
    }

    larodConnection* conn = callbackData->conn;

    releaseReference((void**) &callbackData, &callbackData->refCount);

    pthread_cond_broadcast(&conn->cond);
    pthread_mutex_unlock(&conn->condMtx);
}

bool larodLoadModelAsync_v2(larodConnection* conn, const int inFd,
                            const larodChip chip, const larodAccess access,
                            const char* name, const larodMap* params,
                            larodLoadModelCallback callback, void* userData,
                            larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!callback) {
        larodCreateError(error, EINVAL, "Callback function pointer is NULL");

        return false;
    }

    // This must be freed in the callback function receiving it.
    // Replacing the malloc call with a memory-pool solution was tried;
    // this did not decrease execution time noticeably.
    CallbackInfo* callbackInfo = malloc(sizeof(CallbackInfo));
    if (!callbackInfo) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return false;
    }

    callbackInfo->func = (FuncPtr) callback;
    callbackInfo->userData = userData;
    callbackInfo->request = REQUEST_QUEUE_LOAD_MODEL;

    pthread_mutex_lock(&conn->reqMtx);
    int ret = hashSetInsert(&conn->requests, callbackInfo);
    pthread_mutex_unlock(&conn->reqMtx);
    if (ret) {
        free(callbackInfo);
        larodCreateError(error, ret, "Could not insert into hash set: %s",
                         strerror(ret));

        return false;
    }

    // Used below to construct the sd-bus message. Defined here to allow `goto
    // error.`
    sd_bus_message* msg = NULL;

    // Prepare fd.
    int fd = -1;
    if (inFd >= 0) {
        fd = dup(inFd);
        if (fd < 0) {
            larodCreateError(error, errno, "Could not dup fd: %s",
                             strerror(errno));

            goto error;
        }
    } else if (!params) {
        larodCreateError(error, EBADF, "fd is negative while params is NULL");

        goto error;
    } else if (!params->numElems) {
        larodCreateError(error, EINVAL, "fd is negative while params is empty");

        goto error;
    } else {
        // We allow a negative fd if params is specified and non-empty. Let's
        // create a temporary fd to let sd-bus serialize and send a fd
        // successfully.
        fd = memfd_create(conn->sessionObjPath, MFD_CLOEXEC);
        if (fd < 0) {
            larodCreateError(error, EINVAL,
                             "Could not create temporary file: %s",
                             strerror(errno));

            goto error;
        }
    }

    // Get file size
    off_t sz = lseek(fd, 0, SEEK_END);
    if (sz < 0) {
        ret = errno;
        larodCreateError(error, ret, "Could not get file size: %s",
                         strerror(ret));

        goto error;
    }

    // Rewind file offset
    if (lseek(fd, 0, SEEK_SET) < 0) {
        ret = errno;
        larodCreateError(error, ret, "Could not rewind file offset: %s",
                         strerror(ret));

        goto error;
    }

    if (inFd >= 0 && sz == 0) {
        larodCreateError(error, EINVAL, "Model is of size 0");

        goto error;
    }

    ret = sd_bus_message_new_method_call(
        conn->asyncWriteBus, &msg, LAROD_SERVICE_NAME, conn->sessionObjPath,
        LAROD_SESSION_INTERFACE, LAROD_MSG_QUEUE_LOAD_MODEL);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create message: %s",
                         strerror(ret));

        goto error;
    }

    // Get chip ID of first instance of larodChip.
    uint64_t chipId = 0;
    if (!getFirstChipId(conn, chip, &chipId)) {
        larodCreateError(error, EINVAL, "Could not find chip");

        goto error;
    }

    ret = sd_bus_message_append(msg, "httist", fd, (uint64_t) sz, chipId,
                                (int32_t) access, name,
                                (uint64_t)(uintptr_t) callbackInfo);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create message: %s",
                         strerror(ret));

        goto error;
    }

    // Append additonal parameters.
    if (params && !appendMapToMsg(msg, params, error)) {
        goto error;
    }

    ret = sd_bus_send(conn->asyncWriteBus, msg, NULL);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_flush(conn->asyncWriteBus);
    msg = sd_bus_message_unref(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send message: %s",
                         strerror(ret));

        goto error;
    }

    close(fd);

    return true;

error:
    eraseCallbackFromHashSet(conn, callbackInfo);
    sd_bus_message_unref(msg);
    free(callbackInfo);
    if (fd) {
        close(fd);
    }

    return false;
}

bool larodLoadModelAsync_v1(larodConnection* conn, const int fd,
                            const larodAccess access, const char* name,
                            larodLoadModelCallback callback, void* userData,
                            larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    larodChip chipType = LAROD_CHIP_INVALID;
    bool ret = getLegacyChip(conn, &chipType);
    assert(ret);

    ret = larodLoadModelAsync_v2(conn, fd, chipType, access, name, NULL,
                                 callback, userData, error);

    return ret;
}

larodModel* larodLoadModel_v2(larodConnection* conn, const int fd,
                              const larodChip chip, const larodAccess access,
                              const char* name, const larodMap* params,
                              larodError** error) {
    LoadModelSyncCallbackData* userData =
        malloc(sizeof(LoadModelSyncCallbackData));
    userData->model = NULL;
    userData->conn = conn;
    userData->error = error;
    userData->done = false;
    // userData is referenced from this thread and loadModelSyncCallback.
    userData->refCount = 2;

    if (!larodLoadModelAsync_v2(conn, fd, chip, access, name, params,
                                loadModelSyncCallback, userData, error)) {
        free(userData);
        return NULL;
    }

    // Wait for load model callback.
    struct timespec timeout;
    timespec_get(&timeout, TIME_UTC);
    timeout.tv_sec += DBUS_TIMEOUT_SEC;

    pthread_mutex_lock(&conn->condMtx);
    while (!userData->done) {
        int ret = pthread_cond_timedwait(&conn->cond, &conn->condMtx, &timeout);
        if (ret == ETIMEDOUT) {
            larodCreateError(error, ret, "Could not load model: %s",
                             strerror(ret));
            userData->done = true;
        } else if (ret) {
            larodCreateError(error, ret, "Could not wait for callback: %s",
                             strerror(ret));
            userData->done = true;
        }
    }

    larodModel* ret = userData->model;

    releaseReference((void**) &userData, &userData->refCount);

    pthread_mutex_unlock(&conn->condMtx);

    return ret;
}

larodModel* larodLoadModel_v1(larodConnection* conn, const int fd,
                              const larodAccess access, const char* name,
                              larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return NULL;
    }

    larodChip chipType = LAROD_CHIP_INVALID;
    __attribute__((unused)) bool ret = getLegacyChip(conn, &chipType);
    assert(ret);

    return larodLoadModel_v2(conn, fd, chipType, access, name, NULL, error);
}

larodModel* larodGetModel(larodConnection* conn, const uint64_t modelId,
                          larodError** error) {
    size_t numModels = 0;
    larodModel** models = larodGetModels(conn, &numModels, error);
    if (!models) {
        return NULL;
    }

    larodModel* model = NULL;

    for (size_t i = 0; i < numModels; ++i) {
        if (models[i]->id == modelId) {
            // Get hold of the model we are interested in and set the
            // corresponding array position to NULL. The larodDestroyModel()
            // function is NULL-safe thus that array position will be just
            // skipped in the larodDestroyModels() call.
            model = models[i];
            models[i] = NULL;
            break;
        }
    }

    if (!model) {
        larodCreateError(error, LAROD_ERROR_MODEL_NOT_FOUND,
                         "Could not find model with ID %" PRIu64, modelId);
    }

    larodDestroyModels(&models, numModels);

    return model;
}

larodModel** larodGetModels(larodConnection* conn, size_t* numModels,
                            larodError** error) {
    size_t nbrModels = 0;
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return NULL;
    }

    if (!numModels) {
        larodCreateError(error, EINVAL, "numModels is NULL");

        return NULL;
    }

    sd_bus_error_free(&conn->error);

    larodModel** models = NULL;

    int ret = sd_bus_get_property(
        conn->bus, LAROD_SERVICE_NAME, conn->sessionObjPath,
        LAROD_SESSION_INTERFACE, "Models", &(conn->error), &(conn->msg),
        "a(" MODEL_SIGNATURE "a(" TENSOR_METADATA_FROM_SERVICE
        ")a(" TENSOR_METADATA_FROM_SERVICE "))");
    if (ret < 0) {
        larodCreateError(error, -ret, "Could not get models: %s",
                         conn->error.message);

        goto end;
    }

    models = malloc(sizeof(larodModel*) * (nbrModels + 1));
    if (!models) {
        larodCreateError(error, errno, "Failed to allocate models list");

        goto end;
    }

    // Enter model array.
    ret = sd_bus_message_enter_container(
        conn->msg, 'a',
        "(" MODEL_SIGNATURE "a(" TENSOR_METADATA_FROM_SERVICE
        ")a(" TENSOR_METADATA_FROM_SERVICE "))");
    if (ret < 0) {
        larodCreateError(error, -ret, "Could not enter model array: %s",
                         strerror(-ret));

        goto end;
    }

    while (true) {
        larodModel* model = NULL;
        if (!readModelFromMessage(conn->msg, &model, error)) {
            goto error;
        }

        if (!model) {
            // We reached the end in the array.
            break;
        }

        larodModel** newModels =
            realloc(models, sizeof(larodModel*) * (nbrModels + 2));
        if (!newModels) {
            larodCreateError(error, errno, "Failed to reallocate models list");

            goto error;
        }
        models = newModels;
        models[nbrModels] = model;
        nbrModels++;
    }
    // Model array should be ended with a NULL.
    models[nbrModels] = NULL;

    // Exit model array.
    ret = sd_bus_message_exit_container(conn->msg);
    if (ret < 0) {
        larodCreateError(error, -ret, "Could not exit model array: %s",
                         strerror(-ret));

        goto error;
    }

end:
    conn->msg = sd_bus_message_unref(conn->msg);

    *numModels = nbrModels;
    return models;

error:
    larodDestroyModels(&models, nbrModels);
    conn->msg = sd_bus_message_unref(conn->msg);

    return NULL;
}

void deleteModelSyncCallback(void* userData, larodError* error) {
    assert(userData);
    AsyncStatusCallbackData* callbackData = (AsyncStatusCallbackData*) userData;

    pthread_mutex_lock(&callbackData->conn->condMtx);

    callbackData->done = true;
    if (error) {
        larodCreateError(callbackData->error, error->code, "%s", error->msg);
        callbackData->ret = false;
    } else {
        callbackData->ret = true;
    }

    larodConnection* conn = callbackData->conn;

    releaseReference((void**) &callbackData, &callbackData->refCount);

    pthread_cond_broadcast(&conn->cond);
    pthread_mutex_unlock(&conn->condMtx);
}

bool larodDeleteModel(larodConnection* conn, larodModel* model,
                      larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!model) {
        larodCreateError(error, EINVAL, "larodModel is NULL");

        return false;
    }

    // This must be freed in the callback function receiving it.
    // Replacing the malloc call with a memory-pool solution was tried;
    // this did not decrease execution time noticeably.
    CallbackInfo* callbackInfo = malloc(sizeof(CallbackInfo));
    if (!callbackInfo) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return false;
    }

    AsyncStatusCallbackData* userData = malloc(sizeof(AsyncStatusCallbackData));
    userData->conn = conn;
    userData->error = error;
    userData->done = false;
    userData->ret = false;
    // userData is referenced from this thread and deleteModelSyncCallback.
    userData->refCount = 2;
    callbackInfo->func = (FuncPtr) deleteModelSyncCallback;
    callbackInfo->userData = userData;
    callbackInfo->request = REQUEST_DELETE_MODEL;

    pthread_mutex_lock(&conn->reqMtx);
    int ret = hashSetInsert(&conn->requests, callbackInfo);
    pthread_mutex_unlock(&conn->reqMtx);
    if (ret) {
        free(callbackInfo);
        free(userData);
        larodCreateError(error, ret, "Could not insert into hash set: %s",
                         strerror(ret));

        return false;
    }

    sd_bus_message* msg = NULL;
    ret = sd_bus_message_new_method_call(
        conn->asyncWriteBus, &msg, LAROD_SERVICE_NAME, conn->sessionObjPath,
        LAROD_SESSION_INTERFACE, LAROD_MSG_DELETE_MODEL);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_message_append(msg, "ttt", model->id, model->chipId,
                                (uint64_t)(uintptr_t) callbackInfo);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_send(conn->asyncWriteBus, msg, NULL);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_flush(conn->asyncWriteBus);
    msg = sd_bus_message_unref(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send message: %s",
                         strerror(ret));

        goto error;
    }

    // Wait for delete model callback.
    struct timespec timeout;
    clock_gettime(CLOCK_MONOTONIC, &timeout);
    timeout.tv_sec += DBUS_TIMEOUT_SEC;

    pthread_mutex_lock(&conn->condMtx);
    while (!userData->done) {
        ret = pthread_cond_timedwait(&conn->cond, &conn->condMtx, &timeout);
        if (ret == ETIMEDOUT) {
            larodCreateError(error, ret, "Could not delete model: %s",
                             strerror(ret));
            userData->done = true;
        } else if (ret) {
            larodCreateError(error, ret,
                             "Could not wait for delete model callback: %s",
                             strerror(ret));
            userData->done = true;
        }
    }

    ret = userData->ret;
    releaseReference((void**) &userData, &userData->refCount);

    pthread_mutex_unlock(&conn->condMtx);

    return ret;

error:
    eraseCallbackFromHashSet(conn, (CallbackInfo*) callbackInfo);
    sd_bus_message_unref(msg);
    free(callbackInfo);
    free(userData);

    return false;
}

bool larodRunJob(larodConnection* conn, const larodJobRequest* jobReq,
                 larodError** error) {
    AsyncStatusCallbackData* userData = malloc(sizeof(AsyncStatusCallbackData));
    userData->conn = conn;
    userData->error = error;
    userData->done = false;
    userData->ret = false;
    // userData is referenced from this thread and runJobSyncCallback.
    userData->refCount = 2;

    if (!larodRunJobAsync(conn, jobReq, runJobSyncCallback, userData, error)) {
        free(userData);
        return false;
    }

    // Wait for job request callback.
    struct timespec timeout;
    clock_gettime(CLOCK_MONOTONIC, &timeout);
    timeout.tv_sec += DBUS_TIMEOUT_SEC;

    pthread_mutex_lock(&conn->condMtx);
    while (!userData->done) {
        int ret = pthread_cond_timedwait(&conn->cond, &conn->condMtx, &timeout);
        if (ret == ETIMEDOUT) {
            larodCreateError(error, ret, "Could not run job: %s",
                             strerror(ret));
            userData->done = true;
        } else if (ret) {
            larodCreateError(error, ret, "Could not wait for callback: %s",
                             strerror(ret));
            userData->done = true;
        }
    }

    bool ret = userData->ret;

    releaseReference((void**) &userData, &userData->refCount);

    pthread_mutex_unlock(&conn->condMtx);

    return ret;
}

void runJobSyncCallback(void* userData, larodError* error) {
    assert(userData);
    AsyncStatusCallbackData* callbackData = (AsyncStatusCallbackData*) userData;

    pthread_mutex_lock(&callbackData->conn->condMtx);

    callbackData->done = true;
    if (error) {
        larodCreateError(callbackData->error, error->code, "%s", error->msg);
        callbackData->ret = false;
    } else {
        callbackData->ret = true;
    }

    larodConnection* conn = callbackData->conn;

    releaseReference((void**) &callbackData, &callbackData->refCount);

    pthread_cond_broadcast(&conn->cond);
    pthread_mutex_unlock(&conn->condMtx);
}

bool larodRunJobAsync(larodConnection* conn, const larodJobRequest* jobReq,
                      larodRunJobCallback callback, void* userData,
                      larodError** error) {
    if (!conn) {
        larodCreateError(error, EINVAL, "larodConnection is NULL");

        return false;
    }

    if (!jobReq) {
        larodCreateError(error, EINVAL, "Pointer argument jobReq is NULL");

        return false;
    }

    if (!callback) {
        larodCreateError(error, EINVAL, "Callback function pointer is NULL");

        return false;
    }

    if (!validateJobReqOpt(jobReq)) {
        larodCreateError(error, EINVAL, "Job request contains invalid options");

        return false;
    }

    // This must be freed in the callback function receiving it.
    // Replacing the malloc call with a memory-pool solution was tried;
    // this did not decrease execution time noticeably.
    CallbackInfo* callbackInfo = malloc(sizeof(CallbackInfo));
    if (!callbackInfo) {
        larodCreateError(error, ENOMEM, "%s", strerror(ENOMEM));

        return false;
    }

    callbackInfo->func = (FuncPtr) callback;
    callbackInfo->userData = userData;
    callbackInfo->request = REQUEST_QUEUE_JOB;

    pthread_mutex_lock(&conn->reqMtx);
    int ret = hashSetInsert(&conn->requests, callbackInfo);
    pthread_mutex_unlock(&conn->reqMtx);
    if (ret) {
        free(callbackInfo);

        larodCreateError(error, ret, "Could not insert into hash set: %s",
                         strerror(ret));

        return false;
    }

    sd_bus_message* msg = NULL;
    ret = sd_bus_message_new_method_call(
        conn->asyncWriteBus, &msg, LAROD_SERVICE_NAME, conn->sessionObjPath,
        LAROD_SESSION_INTERFACE, LAROD_MSG_QUEUE_JOB);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not create message: %s",
                         strerror(ret));

        goto error;
    }

    // Sd-bus message signature: "ttyt a(iiathtx) a(iiathtx) a{sv}"
    // t              - model ID
    // t              - chip ID
    // y              - priority
    // t              - cookie/user-pointer
    // a(iiathtx)     - array of tensors (data type, data layout, array-of-dims,
    //                  fd, size, offset)
    // a{sv}          - additional parameters.

    ret = sd_bus_message_append(msg, "ttyt", jobReq->modelId, jobReq->chipId,
                                jobReq->priority,
                                (uint64_t)(uintptr_t) callbackInfo);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(
            error, ret,
            "Could not append job request information to message: %s",
            strerror(ret));

        goto error;
    }

    // Write inputs to message, determine fd sizes if necessary.
    if (!appendTensorsToMsg(msg, jobReq->inputs, jobReq->numInputs, false,
                            error)) {
        goto error;
    }

    // Write outputs to message. Skip fd size lseek().
    if (!appendTensorsToMsg(msg, jobReq->outputs, jobReq->numOutputs, true,
                            error)) {
        goto error;
    }

    // Append additonal parameters.
    if (jobReq->params && !appendMapToMsg(msg, jobReq->params, error)) {
        goto error;
    }

    ret = sd_bus_send(conn->asyncWriteBus, msg, NULL);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send message: %s",
                         strerror(ret));

        goto error;
    }

    ret = sd_bus_flush(conn->asyncWriteBus);
    msg = sd_bus_message_unref(msg);
    if (ret < 0) {
        ret = -ret;
        larodCreateError(error, ret, "Could not send message: %s",
                         strerror(ret));

        goto error;
    }

    return true;

error:
    eraseCallbackFromHashSet(conn, callbackInfo);
    sd_bus_message_unref(msg);
    free(callbackInfo);

    return false;
}

static void loadModelAsyncCallback(CallbackInfo* callbackInfo,
                                   larodModel* model, larodError* error) {
    assert(callbackInfo);
    assert(callbackInfo->request == REQUEST_QUEUE_LOAD_MODEL);

    larodLoadModelCallback userCallback =
        (larodLoadModelCallback) callbackInfo->func;
    userCallback(model, callbackInfo->userData, error);

    free(callbackInfo);
    larodClearError(&error); // User should not already have freed this.
}

static void runJobAsyncCallback(CallbackInfo* callbackInfo, larodError* error) {
    assert(callbackInfo);
    assert(callbackInfo->request == REQUEST_QUEUE_JOB);

    larodRunJobCallback userCallback = (larodRunJobCallback) callbackInfo->func;
    userCallback(callbackInfo->userData, error);

    free(callbackInfo);
    larodClearError(&error); // User should not already have freed this.
}

static void deleteModelAsyncCallback(CallbackInfo* callbackInfo,
                                     larodError* error) {
    assert(callbackInfo);
    assert(callbackInfo->request == REQUEST_DELETE_MODEL);

    typedef void (*larodDeleteModelCallback)(void* userData, larodError* error);
    larodDeleteModelCallback userCallback =
        (larodDeleteModelCallback) callbackInfo->func;
    userCallback(callbackInfo->userData, error);

    free(callbackInfo);
    larodClearError(&error); // User should not already have freed this.
}

static void eraseCallbackFromHashSet(larodConnection* conn,
                                     CallbackInfo* callbackInfo) {
    pthread_mutex_lock(&conn->reqMtx);
    int eraseRet = hashSetErase(conn->requests, callbackInfo);
    if (eraseRet) {
        // Should never happen.
        assert(!eraseRet);
        abort();
    }
    pthread_mutex_unlock(&conn->reqMtx);
}

static void* processBus(void* arg) {
    assert(arg);

    larodConnection* conn = arg;
    sd_bus* bus = conn->asyncReadBus;

#define NBR_OF_FDS (2)

    struct pollfd pfd[NBR_OF_FDS] = {0};
    pfd[0].fd = sd_bus_get_fd(bus);
    pfd[1].fd = conn->busProcEventFd;
    pfd[0].events = pfd[1].events =
        EPOLLIN | EPOLLPRI | EPOLLRDHUP | EPOLLERR | EPOLLHUP | POLLNVAL;

    while (!conn->stopBusProc) {
        sd_bus_message* msg = NULL;
        int ret = sd_bus_process(bus, &msg);
        if (ret < 0) {
            conn->stopBusProc = true;
        }

        if (msg) {
            const char* msgMember = sd_bus_message_get_member(msg);

            if (strcmp(msgMember, LAROD_MSG_HANDSHAKE) == 0) {
                // The first message from the service is the response of our
                // handshake with the session ID as payload.
                uint64_t id;
                ret = sd_bus_message_read(msg, "t", &id);
                if (ret < 0) {
                    goto unref;
                }

                assert(conn->id == id);
            } else if (strcmp(msgMember, LAROD_MSG_QUEUE_LOAD_MODEL) == 0) {
                // Parse message.
                ret = sd_bus_message_enter_container(
                    msg, 'r',
                    "(" MODEL_SIGNATURE "a(" TENSOR_METADATA_FROM_SERVICE
                    ")a(" TENSOR_METADATA_FROM_SERVICE "))tis");
                if (ret < 0) {
                    goto unref;
                }

                // Read model.
                larodModel* model = NULL;
                if (!readModelFromMessage(msg, &model, NULL)) {
                    goto unref;
                }

                // Read callback and return info.
                uintptr_t callbackInfo = 0;
                int32_t errCode = LAROD_ERROR_LOAD_MODEL;
                char* errMsg = NULL;
                ret = sd_bus_message_read(msg, "tis", &callbackInfo, &errCode,
                                          &errMsg);
                if (ret < 0) {
                    larodDestroyModel(&model);

                    goto unref;
                }

                ret = sd_bus_message_exit_container(msg);
                if (ret < 0) {
                    larodDestroyModel(&model);

                    goto unref;
                }

                larodError* error = NULL;
                if (errCode) {
                    larodCreateError(&error, errCode, "%s", errMsg);

                    // Make sure model is NULL on error.
                    larodDestroyModel(&model);
                }

                eraseCallbackFromHashSet(conn, (CallbackInfo*) callbackInfo);

                loadModelAsyncCallback((CallbackInfo*) callbackInfo, model,
                                       error);
            } else if (strcmp(msgMember, LAROD_MSG_QUEUE_JOB) == 0) {
                uintptr_t callbackInfo;
                larodErrorCode errCode;
                const char* errMsg;
                if (sd_bus_message_read(msg, "tis", &callbackInfo, &errCode,
                                        &errMsg) < 0) {
                    goto unref;
                }

                eraseCallbackFromHashSet(conn, (CallbackInfo*) callbackInfo);

                larodError* error = NULL;
                if (errCode) {
                    larodCreateError(&error, errCode, "%s", errMsg);
                }

                runJobAsyncCallback((CallbackInfo*) callbackInfo, error);
            } else if (strcmp(msgMember, LAROD_MSG_DELETE_MODEL) == 0) {
                uintptr_t callbackInfo;
                larodErrorCode errCode;
                const char* errMsg;
                if (sd_bus_message_read(msg, "tis", &callbackInfo, &errCode,
                                        &errMsg) < 0) {
                    goto unref;
                }

                eraseCallbackFromHashSet(conn, (CallbackInfo*) callbackInfo);

                larodError* error = NULL;
                if (errCode) {
                    larodCreateError(&error, errCode, "%s", errMsg);
                }

                deleteModelAsyncCallback((CallbackInfo*) callbackInfo, error);
            } else if (strcmp(msgMember, LAROD_MSG_DISCONNECTED) == 0) {
                // This message is sent (by sd-bus itself) when connection is
                // reset by peer. We just ignore it (it will be handled in
                // sd_bus_process() on next iteration).
            } else {
                // Unknown message (should never happen).
                assert(0);
            }
        }

    unref:
        msg = sd_bus_message_unref(msg);

        if (ret == 0) {
            // No new messages, wait for some on the bus.
            pfd[0].revents = pfd[1].revents = 0;
            int pollRet = ppoll(pfd, NBR_OF_FDS, NULL, NULL);

            if (pollRet < 0) {
                conn->stopBusProc = true;
            } else if (pfd[1].revents != 0) {
                // Something triggered the eventfd. Stop the processing.
                conn->stopBusProc = true;
            }
        }
    }

    // Handle outstanding requests.
    void** sentRequests;
    size_t nbrOfRequests;
    CallbackInfo* callbackInfo;
    larodError* error = NULL;
    char* errMsg = "Asynchronous connection has been closed";
    pthread_mutex_lock(&conn->reqMtx);
    hashSetExtractAll(conn->requests, &sentRequests, &nbrOfRequests);
    for (size_t i = 0; i < nbrOfRequests; ++i) {
        callbackInfo = (CallbackInfo*) sentRequests[i];

        larodCreateError(&error, LAROD_ERROR_CONNECTION, "%s", errMsg);

        if (callbackInfo->request == REQUEST_QUEUE_LOAD_MODEL) {
            loadModelAsyncCallback(callbackInfo, NULL, error);
        } else if (callbackInfo->request == REQUEST_QUEUE_JOB) {
            runJobAsyncCallback(callbackInfo, error);
        } else if (callbackInfo->request == REQUEST_DELETE_MODEL) {
            deleteModelAsyncCallback(callbackInfo, error);
        } else {
            // Unknown request (should never happen).
            assert(0);
        }
    }
    pthread_mutex_unlock(&conn->reqMtx);
    free(sentRequests);

    return NULL;
}
