/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <string>
#include <thread>
#include <unordered_set>

#include "backendunit.hh"

namespace larod {
namespace backendunit {

class UnitTestChip : public BackEndUnit {
public:
    std::pair<std::vector<Tensor>, std::vector<Tensor>>
        loadModelVirtual(const span<uint8_t>& data, const uint64_t modelId,
                         const ParamsMap& params) override;
    void deleteModelVirtual(const uint64_t modelId) override;
    void runJobVirtual(const uint64_t modelId,
                       const std::vector<Tensor>& inputTensors,
                       std::vector<Tensor>& outputTensors,
                       const ParamsMap& params) override;
    larodChip getChip() override { return LAROD_CHIP_DEBUG; };

    // These functions expose protected methods in the BackEndUnit class to be
    // accessible in unit tests.
    std::shared_ptr<JobRequest> popJobQueue();
    std::shared_ptr<JobRequest> popJobQueueWait();
    std::shared_ptr<LoadModelRequest> popLoadModelQueue();
    std::shared_ptr<LoadModelRequest> popLoadModelQueueWait();

    size_t getJobQueueSize() const;
    size_t getLoadModelQueueSize() const;
    void setMinVersion(const int version) { minModelFormatVersion = version; }

    const modelformat::Model* parseModel(const span<uint8_t>& model,
                                         int version) const;

    // Functions to simulate processing of RunJob and LoadModel async
    // processing. startProcJobRequest() will pop a request from the queue
    // and store it in class shared ptr. Then test case calls
    // finishLoadModelRequest() to finish mocked processing by signalling the
    // request.
    void startProcJobRequest();
    void startProcLoadModelRequest();
    void finishJobRequest();
    void finishLoadModelRequest();

    static const size_t NBR_OF_INPUT_TENSORS = 1;
    static const size_t NBR_OF_OUTPUT_TENSORS = 1;
    static const size_t INPUT_TENSOR_BYTE_SIZE = 1;
    static const size_t OUTPUT_TENSOR_BYTE_SIZE = 1;
    static const larodTensorDataType TENSOR_DATA_TYPE =
        LAROD_TENSOR_DATA_TYPE_UINT8;
    static const larodTensorLayout INPUT_TENSOR_LAYOUT =
        LAROD_TENSOR_LAYOUT_UNSPECIFIED;
    static const larodTensorLayout OUTPUT_TENSOR_LAYOUT =
        LAROD_TENSOR_LAYOUT_UNSPECIFIED;
    static const std::vector<size_t> INPUT_TENSOR_DIMS;
    static const std::vector<size_t> OUTPUT_TENSOR_DIMS;
    static const std::vector<size_t> INPUT_TENSOR_PITCHES;
    static const std::vector<size_t> OUTPUT_TENSOR_PITCHES;
    static const std::string INPUT_TENSOR_NAME;
    static const std::string OUTPUT_TENSOR_NAME;
    static const std::vector<Tensor> INPUT_TENSORS;
    static const std::vector<Tensor> OUTPUT_TENSORS;

private:
    static const std::string THIS_NAMESPACE;

    // State variables to 'mock' async inference and load-model operations being
    // in progress in the accelerator
    std::shared_ptr<JobRequest> jobInProgress;
    std::shared_ptr<LoadModelRequest> loadModelInProgress;
    int minModelFormatVersion = 0;

    int getMinModelFormatVersion() const override {
        return minModelFormatVersion;
    }
};

} // namespace backendunit
} // namespace larod
