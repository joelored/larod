/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "backendunit.hh"

#include <gtest/gtest.h>

#include "debugchip.hh"
#include "model.hh"
#include "span.hh"
#include "testutils.hh"
#include "unittestchip.hh"

using namespace std;
using namespace larod;
using namespace larod::test;

/**
 * @brief Test fixture for BackEndUnit unit tests.
 */
class BackEndUnitTest : public ::testing::Test {
public:
    BackEndUnitTest() : MODEL_DATA(VALID_MODEL_BUF) {
        // TODO: Generalize (DebugChip has only one input and output for now).
        inputTensors[0].setData(inputTensorData);
        outputTensors[0].setData(outputTensorData);
    }

    backendunit::DebugChip debugChip;
    backendunit::UnitTestChip unitTestChip;
    const span<uint8_t> MODEL_DATA;
    vector<uint8_t> inputTensorData =
        vector<uint8_t>(backendunit::DebugChip::INPUT_TENSOR_BYTE_SIZE, 1);
    vector<Tensor> inputTensors = backendunit::DebugChip::INPUT_TENSORS;
    vector<uint8_t> outputTensorData =
        vector<uint8_t>(backendunit::DebugChip::OUTPUT_TENSOR_BYTE_SIZE, 1);
    vector<Tensor> outputTensors = backendunit::DebugChip::OUTPUT_TENSORS;
};

TEST_F(BackEndUnitTest, LoadInvalidModel) {
    vector<uint8_t> invalidModelData = {1, 2, 3};
    larod::span<uint8_t> modelData(invalidModelData);
    ASSERT_THROW(
        unitTestChip.loadModel(0, modelData, Model::Access::PUBLIC, ""),
        invalid_argument);
}

TEST_F(BackEndUnitTest, LoadModelVersion7OnMinVersion0) {
    // Recall that the version field is 7 for MODEL_DATA (see testmodel.h).
    ASSERT_NO_THROW(
        unitTestChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadModelVersion7OnMinVersion6) {
    // Recall that the version field is 7 for MODEL_DATA (see testmodel.h).
    unitTestChip.setMinVersion(6);
    ASSERT_NO_THROW(
        unitTestChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadModelVersion7OnMinVersion8) {
    // Recall that the version field is 7 for MODEL_DATA (see testmodel.h).
    unitTestChip.setMinVersion(8);
    ASSERT_THROW(
        unitTestChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""),
        invalid_argument);
}

TEST_F(BackEndUnitTest, LoadPublicModel) {
    ASSERT_NO_THROW(
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, ""));
}

TEST_F(BackEndUnitTest, LoadPrivateModel) {
    ASSERT_NO_THROW(
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, ""));
}

TEST_F(BackEndUnitTest, LoadModelGivesUniqueIDs) {
    Model model1 =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    Model model2 =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");

    ASSERT_NE(model1.getId(), model2.getId());
}

TEST_F(BackEndUnitTest, DeletePrivateModel) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    ASSERT_NO_THROW(debugChip.deleteModel(0, model.getId()));
}

TEST_F(BackEndUnitTest, DeleteOthersPrivateModel) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    ASSERT_ANY_THROW(debugChip.deleteModel(1, model.getId()));
}

TEST_F(BackEndUnitTest, DeleteOthersPublicModel) {
    Model model = debugChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, "");
    ASSERT_NO_THROW(debugChip.deleteModel(1, model.getId()));
}

TEST_F(BackEndUnitTest, DeleteNonExistentModel) {
    ASSERT_ANY_THROW(debugChip.deleteModel(0, numeric_limits<uint64_t>::max()));
}

TEST_F(BackEndUnitTest, RunJobOnPublicModel) {
    Model model = debugChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, "");
    ASSERT_NO_THROW(
        debugChip.runJob(0, model.getId(), inputTensors, outputTensors));
}

TEST_F(BackEndUnitTest, RunJobOnPrivateModel) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    ASSERT_NO_THROW(
        debugChip.runJob(0, model.getId(), inputTensors, outputTensors));
}

TEST_F(BackEndUnitTest, RunJobOnOthersPublicModel) {
    Model model = debugChip.loadModel(0, MODEL_DATA, Model::Access::PUBLIC, "");
    ASSERT_NO_THROW(
        debugChip.runJob(1, model.getId(), inputTensors, outputTensors));
}

TEST_F(BackEndUnitTest, RunJobOnOthersPrivateModel) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    ASSERT_ANY_THROW(
        debugChip.runJob(1, model.getId(), inputTensors, outputTensors));
}

TEST_F(BackEndUnitTest, RunJobOnNonExistentModel) {
    ASSERT_ANY_THROW(debugChip.runJob(0, numeric_limits<uint64_t>::max(),
                                      inputTensors, outputTensors));
}

TEST_F(BackEndUnitTest, GetOutputByteSizes) {
    Model model =
        debugChip.loadModel(0, MODEL_DATA, Model::Access::PRIVATE, "");
    vector<size_t> outputSizes = debugChip.getOutputByteSizes(model.getId());
    ASSERT_EQ(outputSizes.size(),
              backendunit::DebugChip::NBR_OF_OUTPUT_TENSORS);
    // TODO: Generalize (DebugChip has only one output for now).
    ASSERT_EQ(outputSizes[0], backendunit::DebugChip::OUTPUT_TENSOR_BYTE_SIZE);
}

TEST_F(BackEndUnitTest, GetOutputByteSizesOnNonExistentModel) {
    ASSERT_THROW(debugChip.getOutputByteSizes(numeric_limits<uint64_t>::max()),
                 BackEndUnit::ModelNotFound);
}
