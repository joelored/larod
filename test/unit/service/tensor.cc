/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "tensor.hh"

#include <cmath>
#include <gtest/gtest.h>

#include "larod.h"

using namespace std;
using namespace larod;

/**
 * @brief Test fixture for Tensor unit tests.
 *
 * Sets up some mock data used in the tests.
 */
class TensorTest : public ::testing::Test {
public:
    // Valid constructor input.
    const larodTensorDataType VALID_DATA_TYPE = LAROD_TENSOR_DATA_TYPE_INT8;
    const larodTensorLayout VALID_LAYOUT = LAROD_TENSOR_LAYOUT_NCHW;
    const vector<size_t> VALID_DIMS = {1, 224, 224, 3};
    const vector<size_t> VALID_PITCHES = {224 * 224 * 3, 224 * 224 * 3, 224 * 3,
                                          3};
    const size_t VALID_BYTE_SIZE = 1 * 224 * 224 * 3 * 1;
    const string VALID_NAME = "Tensor Name";
    const span<uint8_t> VALID_DATA;

    // Valid but != to valid input data above.
    const larodTensorDataType DIFFERENT_DATA_TYPE =
        LAROD_TENSOR_DATA_TYPE_FLOAT32;
    const larodTensorLayout DIFFERENT_LAYOUT = LAROD_TENSOR_LAYOUT_NHWC;
    const size_t DIFFERENT_BYTE_SIZE = 1 * 224 * 300 * 3 * 4;
    const vector<size_t> DIFFERENT_DIMS = {1, 224, 300, 3};
    const vector<size_t> DIFFERENT_PITCHES = {
        224 * 300 * 3 * 4, 224 * 300 * 3 * 4, 300 * 3 * 4, 3 * 4};

    const Tensor VALID_TENSOR =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, VALID_PITCHES,
               VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);
};

TEST_F(TensorTest, ConstructorValidInput) {
    ASSERT_NO_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                           VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME,
                           VALID_DATA));
}

TEST_F(TensorTest, ConstructorUnspecifiedDims) {
    ASSERT_NO_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, {}, VALID_PITCHES,
                           VALID_BYTE_SIZE, VALID_NAME, VALID_DATA));
}

TEST_F(TensorTest, ConstructorUnspecifiedPitches) {
    ASSERT_NO_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, {},
                           VALID_BYTE_SIZE, VALID_NAME, VALID_DATA));
}

TEST_F(TensorTest, ConstructorInvalidDataType) {
    ASSERT_THROW(Tensor(LAROD_TENSOR_DATA_TYPE_INVALID, VALID_LAYOUT,
                        VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME,
                        VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorTooLargeDataType) {
    ASSERT_THROW(
        Tensor(static_cast<larodTensorDataType>(LAROD_TENSOR_DATA_TYPE_MAX + 1),
               VALID_LAYOUT, VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE,
               VALID_NAME, VALID_DATA),
        invalid_argument);
}

TEST_F(TensorTest, ConstructorInvalidLayout) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_INVALID,
                        VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME,
                        VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorTooLargeLayout) {
    ASSERT_THROW(
        Tensor(VALID_DATA_TYPE,
               static_cast<larodTensorLayout>(LAROD_TENSOR_LAYOUT_MAX + 1),
               VALID_DIMS, VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME,
               VALID_DATA),
        invalid_argument);
}

TEST_F(TensorTest, ConstructorInvalidDims) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, {1, 0, 224, 3},
                        VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorTooLongDims) {
    vector<size_t> TOO_LONG_DIMS(LAROD_TENSOR_MAX_LEN + 1, 1);

    ASSERT_THROW(Tensor(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_UNSPECIFIED,
                        TOO_LONG_DIMS, VALID_PITCHES, VALID_BYTE_SIZE,
                        VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorNCHWWithDimsMismatch) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_NCHW, {1, 224, 3},
                        VALID_PITCHES, 0, VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorNHWCWithDimsMismatch) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_NHWC, {1, 224, 3},
                        VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorTooShortPitches) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                        {224 * 224, 224 * 224, 224}, VALID_BYTE_SIZE,
                        VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorTooLongPitches) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                        {224 * 224 * 3, 224 * 224 * 3, 224 * 3, 3, 1},
                        VALID_BYTE_SIZE, VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorInvalidZeroPitches) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                        {0, 224 * 224 * 3, 224 * 3, 3}, VALID_BYTE_SIZE,
                        VALID_NAME, VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, ConstructorInvalidPitches) {
    ASSERT_THROW(Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS,
                        DIFFERENT_PITCHES, VALID_BYTE_SIZE, VALID_NAME,
                        VALID_DATA),
                 invalid_argument);
}

TEST_F(TensorTest, AssertEqualSameInput) {
    const Tensor VALID_TENSOR_COPY =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, VALID_PITCHES,
               VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_NO_THROW(Tensor::assertEqual(VALID_TENSOR, VALID_TENSOR_COPY));
}

TEST_F(TensorTest, AssertEqualDifferentDataType) {
    const Tensor DIFFERENT_DATA_TYPE_TENSOR =
        Tensor(DIFFERENT_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, VALID_PITCHES,
               VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_THROW(Tensor::assertEqual(VALID_TENSOR, DIFFERENT_DATA_TYPE_TENSOR),
                 invalid_argument);
}

TEST_F(TensorTest, AssertEqualDifferentLayout) {
    const Tensor DIFFERENT_LAYOUT_TENSOR =
        Tensor(VALID_DATA_TYPE, DIFFERENT_LAYOUT, VALID_DIMS, VALID_PITCHES,
               VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_THROW(Tensor::assertEqual(VALID_TENSOR, DIFFERENT_LAYOUT_TENSOR),
                 invalid_argument);
}

TEST_F(TensorTest, AssertEqualDifferentDims) {
    const Tensor DIFFERENT_DIMS_TENSOR =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, DIFFERENT_DIMS, VALID_PITCHES,
               VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_THROW(Tensor::assertEqual(VALID_TENSOR, DIFFERENT_DIMS_TENSOR),
                 invalid_argument);
}

TEST_F(TensorTest, AssertEqualDifferentPitches) {
    vector<size_t> ALIGNED_PITCHES(4);
    ALIGNED_PITCHES[3] =
        static_cast<size_t>(ceil(static_cast<float>(VALID_DIMS[3]) / 32.f)) *
        32;
    ALIGNED_PITCHES[2] = ALIGNED_PITCHES[3] * VALID_DIMS[2];
    ALIGNED_PITCHES[1] = ALIGNED_PITCHES[2] * VALID_DIMS[1];
    ALIGNED_PITCHES[0] = ALIGNED_PITCHES[1] * VALID_DIMS[0];
    const Tensor DIFFERENT_PITCHES_TENSOR =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, ALIGNED_PITCHES, 0,
               VALID_NAME, VALID_DATA);

    ASSERT_THROW(Tensor::assertEqual(VALID_TENSOR, DIFFERENT_PITCHES_TENSOR),
                 invalid_argument);
}

TEST_F(TensorTest, AssertEqualDifferentByteSize) {
    const Tensor DIFFERENT_BYTE_SIZE_TENSOR =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, {},
               DIFFERENT_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_THROW(Tensor::assertEqual(VALID_TENSOR, DIFFERENT_BYTE_SIZE_TENSOR),
                 invalid_argument);
}

TEST_F(TensorTest, AssertEqualUnspecifiedDataType) {
    const Tensor UNSPECIFIED_DATA_TYPE_TENSOR =
        Tensor(LAROD_TENSOR_DATA_TYPE_UNSPECIFIED, VALID_LAYOUT, VALID_DIMS,
               VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_NO_THROW(
        Tensor::assertEqual(VALID_TENSOR, UNSPECIFIED_DATA_TYPE_TENSOR));
}

TEST_F(TensorTest, AssertEqualUnspecifiedLayout) {
    const Tensor UNSPECIFIED_LAYOUT_TENSOR =
        Tensor(VALID_DATA_TYPE, LAROD_TENSOR_LAYOUT_UNSPECIFIED, VALID_DIMS,
               VALID_PITCHES, VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_NO_THROW(
        Tensor::assertEqual(VALID_TENSOR, UNSPECIFIED_LAYOUT_TENSOR));
}

TEST_F(TensorTest, AssertEqualUnspecifiedDims) {
    const Tensor UNSPECIFIED_DIMS_TENSOR =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, {}, VALID_PITCHES,
               VALID_BYTE_SIZE, VALID_NAME, VALID_DATA);

    ASSERT_NO_THROW(Tensor::assertEqual(VALID_TENSOR, UNSPECIFIED_DIMS_TENSOR));
}

TEST_F(TensorTest, AssertEqualUnspecifiedPitches) {
    const Tensor UNSPECIFIED_DIMS_TENSOR =
        Tensor(VALID_DATA_TYPE, VALID_LAYOUT, VALID_DIMS, {}, VALID_BYTE_SIZE,
               VALID_NAME, VALID_DATA);

    ASSERT_NO_THROW(Tensor::assertEqual(VALID_TENSOR, UNSPECIFIED_DIMS_TENSOR));
}
