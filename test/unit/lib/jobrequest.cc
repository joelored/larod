/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gtest/gtest.h>

#include "larod.h"

extern "C" {
#include "jobrequest.h"
}

using namespace std;

class JobReqTest : public ::testing::Test {
protected:
    JobReqTest() {
        // Set up up some legal dummy values.
        req.modelId = 1;
        req.inputs = reinterpret_cast<larodTensor**>(1);
        req.outputs = reinterpret_cast<larodTensor**>(1);
        req.numInputs = 1;
        req.numOutputs = 1;
        // DEFAULT_JOB_PRIORITY = 50
        req.priority = 50;
    }

    larodJobRequest req;
};

TEST_F(JobReqTest, validateJobReqOptInvalidModelId) {
    req.modelId = LAROD_INVALID_MODEL_ID;

    ASSERT_FALSE(validateJobReqOpt(&req));
}

TEST_F(JobReqTest, validateJobReqOptInvalidNumInputs) {
    req.numInputs = 0;

    ASSERT_FALSE(validateJobReqOpt(&req));
}

TEST_F(JobReqTest, validateJobReqOptInvalidNumOutputs) {
    req.numOutputs = 0;

    ASSERT_FALSE(validateJobReqOpt(&req));
}

TEST_F(JobReqTest, validateJobReqOptInvalidPriority) {
    // MAXIMUM_JOB_PRIORITY = 100
    req.priority = 100 + 1;

    ASSERT_FALSE(validateJobReqOpt(&req));
}

TEST_F(JobReqTest, validateJobReqOptValidInput) {
    ASSERT_TRUE(validateJobReqOpt(&req));
}
