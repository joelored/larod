/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <cstdio>
#include <gtest/gtest.h>
#include <vector>

extern "C" {
#include "tensor.h"
}
#include "larod.h"
#include "testmacros.h"
#include "testutils.hh"

using namespace std;
using namespace larod::test;

// Declarations for functions which are normally private (i.e. static) in
// lib/tensor.c.
extern "C" {
larodTensor* createTensor(larodError** error);
void destroyTensor(larodTensor* tensor);
bool getFdSize(const larodTensor* tensor, uint64_t* fdSize, larodError** error);
}

class TensorUnitTest : public ::testing::Test {
protected:
    larodError* error = nullptr;
    static constexpr size_t NUM_TENSORS = 3;
    static constexpr size_t DIMS_LEN = 4;
    larodTensor** tensors = nullptr;
    larodTensorDims dims;
    uint64_t fdSize = 0;
    vector<int> openFds;

    void SetUp() override {
        tensors = larodCreateTensors(NUM_TENSORS, &error);
        ASSERT_NE(tensors, nullptr);
        ASSERT_EQ(error, nullptr);

        larodTensorDims dims;
        dims.len = DIMS_LEN;
        for (size_t i = 0; i < dims.len; ++i) {
            dims.dims[i] = i;
        }
        larodTensorPitches pitches;
        pitches.len = DIMS_LEN;
        pitches.pitches[DIMS_LEN - 1] = dims.dims[DIMS_LEN - 1];
        for (size_t i = 1; i < DIMS_LEN; ++i) {
            pitches.pitches[DIMS_LEN - 1 - i] =
                pitches.pitches[DIMS_LEN - i] * dims.dims[DIMS_LEN - 1 - i];
        }

        for (size_t i = 0; i < NUM_TENSORS; ++i) {
            tensors[i]->dataType = LAROD_TENSOR_DATA_TYPE_UINT8;
            tensors[i]->layout = LAROD_TENSOR_LAYOUT_NCHW;

            bool ret = larodSetTensorDims(tensors[i], &dims, &error);
            ASSERT_TRUE(ret);
            ASSERT_EQ(error, nullptr);

            ret = larodSetTensorPitches(tensors[i], &pitches, &error);
            ASSERT_TRUE(ret);
            ASSERT_EQ(error, nullptr);

            // Verity that tensors created by larodCreateTensors() have their
            // names set to '\0'.
            EXPECT_NE(tensors[i]->name, nullptr);
            EXPECT_STREQ(tensors[i]->name, "");

            const size_t namelen = static_cast<size_t>(
                snprintf(NULL, 0, "Tensor %u", (unsigned int) i + 1) + 1);
            tensors[i]->name =
                reinterpret_cast<char*>(realloc(tensors[i]->name, namelen));
            snprintf(tensors[i]->name, namelen, "Tensor %u",
                     (unsigned int) i + 1);

            // We want to set a valid fd with non-zero size to the tensor. Since
            // getTestModelFd() provides just such an fd (using memfd_create
            // syscall and writing some data to it) we use this function here.
            // The fd returned form getTestModelFd() will have size
            // VALID_MODEL_SIZE.
            int fd = getTestModelFd();
            ASSERT_GE(fd, 0);

            ret = larodSetTensorFd(tensors[i], fd, &error);
            ASSERT_TRUE(ret);
            ASSERT_NE(tensors[i]->fd, -1);
            ASSERT_EQ(error, nullptr);

            openFds.push_back(fd);
        }
    }

    void TearDown() override {
        larodDestroyTensors(&tensors, NUM_TENSORS);
        for (int fd : openFds) {
            close(fd);
        }
    }
};

TEST_F(TensorUnitTest, CreateTensor) {
    larodTensor* tensor = createTensor(&error);
    ASSERT_EQ(error, nullptr);
    ASSERT_NE(tensor, nullptr);

    EXPECT_EQ(tensor->dataType, LAROD_TENSOR_DATA_TYPE_UNSPECIFIED);
    EXPECT_EQ(tensor->layout, LAROD_TENSOR_LAYOUT_UNSPECIFIED);
    EXPECT_EQ(tensor->dims.len, 0);
    EXPECT_EQ(tensor->pitches.len, 0);
    EXPECT_EQ(tensor->byteSize, 0);
    EXPECT_EQ(tensor->name, nullptr);

    destroyTensor(tensor);
}

TEST_F(TensorUnitTest, DestroyTensor) {
    ASSERT_EXIT(destroyTensor(nullptr); exit(EXIT_SUCCESS),
                                        ::testing::ExitedWithCode(EXIT_SUCCESS),
                                        "");
}

TEST_F(TensorUnitTest, CopyTensor) {
    larodTensor* tensorCpy = copyTensor(tensors[0], &error);
    ASSERT_NE(tensorCpy, nullptr);
    ASSERT_EQ(error, nullptr);

    EXPECT_EQ(tensorCpy->dataType, tensors[0]->dataType);
    EXPECT_EQ(tensorCpy->layout, tensors[0]->layout);
    EXPECT_EQ(tensorCpy->dims.len, tensors[0]->dims.len);
    for (size_t i = 0; i < tensorCpy->dims.len; ++i) {
        EXPECT_EQ(tensorCpy->dims.dims[i], tensors[0]->dims.dims[i]);
    }
    EXPECT_EQ(tensorCpy->pitches.len, tensors[0]->dims.len);
    for (size_t i = 0; i < tensorCpy->pitches.len; ++i) {
        EXPECT_EQ(tensorCpy->pitches.pitches[i],
                  tensors[0]->pitches.pitches[i]);
    }

    EXPECT_NE(tensorCpy->name, tensors[0]->name);
    EXPECT_STREQ(tensorCpy->name, tensors[0]->name);

    destroyTensor(tensorCpy);
}

TEST_F(TensorUnitTest, CopyTensorArray) {
    larodTensor** tensorsCpy = copyTensorArray(tensors, NUM_TENSORS, &error);
    ASSERT_NE(tensorsCpy, nullptr);
    ASSERT_EQ(error, nullptr);

    for (size_t i = 0; i < NUM_TENSORS; ++i) {
        ASSERT_NE(tensors[i], nullptr);
        EXPECT_EQ(tensors[i]->dataType, tensorsCpy[i]->dataType);
        EXPECT_EQ(tensors[i]->layout, tensorsCpy[i]->layout);
        EXPECT_EQ(tensors[i]->dims.len, tensorsCpy[i]->dims.len);
        for (size_t j = 0; j < tensors[i]->dims.len; ++j) {
            EXPECT_EQ(tensors[i]->dims.dims[j], tensorsCpy[i]->dims.dims[j]);
        }
        EXPECT_EQ(tensors[i]->pitches.len, tensorsCpy[i]->pitches.len);
        for (size_t j = 0; j < tensors[i]->pitches.len; ++j) {
            EXPECT_EQ(tensors[i]->pitches.pitches[j],
                      tensorsCpy[i]->pitches.pitches[j]);
        }
        EXPECT_NE(tensors[i]->name, tensorsCpy[i]->name);
        EXPECT_STREQ(tensors[i]->name, tensorsCpy[i]->name);
    }

    larodDestroyTensors(&tensorsCpy, NUM_TENSORS);
}

TEST_F(TensorUnitTest, GetFdSize) {
    bool ret = getFdSize(tensors[0], &fdSize, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);
    ASSERT_EQ(fdSize, VALID_MODEL_BUF.size());
}

TEST_F(TensorUnitTest, GetFdSizeTooLargeOffset) {
    ASSERT_LE(VALID_MODEL_BUF.size(), INT64_MAX);
    tensors[0]->fdOffset = static_cast<int64_t>(VALID_MODEL_BUF.size());
    bool ret = getFdSize(tensors[0], &fdSize, &error);
    ASSERT_FALSE(ret);
    ASSERT_NE(error, nullptr);
    larodClearError(&error);
}

TEST_F(TensorUnitTest, GetFdSizeInvalidFd) {
    tensors[0]->fd = -1;
    bool ret = getFdSize(tensors[0], &fdSize, &error);
    ASSERT_FALSE(ret);
    ASSERT_NE(error, nullptr);
    larodClearError(&error);
}

TEST_F(TensorUnitTest, ValidateTensors) {
    bool ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    tensors[NUM_TENSORS - 1]->layout = LAROD_TENSOR_LAYOUT_NHWC;
    ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    tensors[NUM_TENSORS - 1]->dataType = LAROD_TENSOR_DATA_TYPE_UNSPECIFIED;
    tensors[NUM_TENSORS - 1]->layout = LAROD_TENSOR_LAYOUT_UNSPECIFIED;
    tensors[NUM_TENSORS - 1]->dims.len = 0;
    tensors[NUM_TENSORS - 1]->pitches.len = 0;
    ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);
}

TEST_F(TensorUnitTest, ValidateInvalidTensors) {
    // Verify error reported when NULL encountered in array before reaching
    // tensor number NUM_TENSORS.
    bool ret = validateTensors(tensors, NUM_TENSORS + 1, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(TensorUnitTest, ValidateTensorsInvalidDimsLenNCHW) {
    // Verify error when a tensor has layout NCHW with dims.len = 2.
    dims.len = 2;
    tensors[NUM_TENSORS - 1]->dims = dims;
    bool ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(TensorUnitTest, ValidateTensorsInvalidDimsLenNHWC) {
    // Verify error when a tensor has layout NHWC with dims.len = 2.
    dims.len = 2;
    tensors[NUM_TENSORS - 1]->dims = dims;
    tensors[NUM_TENSORS - 1]->layout = LAROD_TENSOR_LAYOUT_NHWC;
    bool ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(TensorUnitTest, ValidateTensorsInvalidDimsPitches) {
    // Verify error when a tensor has mismatch of dims/pitches lengths.
    tensors[NUM_TENSORS - 1]->pitches.len = 2;
    bool ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(TensorUnitTest, ValidateTensorsInvalidPitchesByteSize) {
    // Verify error when a tensor has mismatch of pitches and byte size.
    tensors[NUM_TENSORS - 1]->byteSize = 0;
    bool ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    tensors[NUM_TENSORS - 1]->byteSize =
        tensors[NUM_TENSORS - 1]->pitches.pitches[0];
    ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    tensors[NUM_TENSORS - 1]->byteSize =
        tensors[NUM_TENSORS - 1]->pitches.pitches[0] - 1;
    ret = validateTensors(tensors, NUM_TENSORS, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}
