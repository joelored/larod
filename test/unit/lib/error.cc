/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <gtest/gtest.h>

extern "C" {
#include "error.h"
}

TEST(ErrorTest, CreateError) {
    larodError* error = NULL;

    int ret = larodCreateError(&error, LAROD_ERROR_JOB, "msg %d", 1);
    ASSERT_EQ(ret, 0);
    ASSERT_NE(error, nullptr);
    ASSERT_STREQ(error->msg, "msg 1");

    larodClearError(&error);
    ASSERT_EQ(error, nullptr);
}

TEST(ErrorTest, CreateErrorWithNULL) {
    int ret = larodCreateError(NULL, LAROD_ERROR_JOB, "NOPP");
    ASSERT_EQ(ret, EINVAL);
}

TEST(ErrorTest, PrependErrorMessages) {
#define FIRST_MSG "first: error0"
#define SECOND_MSG "second: error1: "

    larodError* error = NULL;
    int ret = larodCreateError(&error, LAROD_ERROR_JOB, FIRST_MSG);
    ASSERT_EQ(ret, 0);
    ASSERT_NE(error, nullptr);
    ASSERT_STREQ(error->msg, FIRST_MSG);

    ret = larodPrependErrorMsg(&error, SECOND_MSG);
    ASSERT_EQ(ret, 0);
    ASSERT_NE(error, nullptr);
    ASSERT_STREQ(error->msg, SECOND_MSG FIRST_MSG);

    larodClearError(&error);
    ASSERT_EQ(error, nullptr);
}
