/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "hashmap.h"

#include <gtest/gtest.h>
#include <unordered_map>

class HashMapTest : public ::testing::Test {
protected:
    HashMapTest()
        : map(nullptr), key1(1), key2(2), key3(3),
          value1(reinterpret_cast<void*>(0x01)),
          value2(reinterpret_cast<void*>(0x02)),
          value3(reinterpret_cast<void*>(0x03)) {
        hashMapConstruct(&map);

        keyValueMap[key1] = value1;
        keyValueMap[key2] = value2;
        keyValueMap[key3] = value3;
    };
    ~HashMapTest() { hashMapDestruct(&map); }

    HashMap* map;
    uint64_t key1;
    uint64_t key2;
    uint64_t key3;
    void* value1;
    void* value2;
    void* value3;

    /// Map used as ground truth.
    std::unordered_map<uint64_t, void*> keyValueMap;
};

TEST_F(HashMapTest, SizeIsZeroInitially) {
    size_t sz;
    ASSERT_EQ(hashMapGetSize(map, &sz), 0);
    ASSERT_EQ(sz, 0);

    const Pair** pairs;
    ASSERT_EQ(hashMapExtractAll(map, &pairs, &sz), 0);
    ASSERT_EQ(sz, 0);
    free(pairs);
}

TEST_F(HashMapTest, EraseNonExistent) {
    ASSERT_EQ(hashMapErase(map, key1, nullptr), ENODATA);
}

TEST_F(HashMapTest, InsertNull) {
    ASSERT_EQ(hashMapInsert(&map, 0, nullptr), 0);
}

TEST_F(HashMapTest, InsertOne) {
    ASSERT_EQ(hashMapInsert(&map, key1, value1), 0);

    size_t sz;
    ASSERT_EQ(hashMapGetSize(map, &sz), 0);
    ASSERT_EQ(sz, 1);

    const Pair** pairs;
    ASSERT_EQ(hashMapExtractAll(map, &pairs, &sz), 0);
    ASSERT_EQ(sz, 1);
    ASSERT_EQ(pairs[0]->key, key1);
    ASSERT_EQ(pairs[0]->value, value1);
    free(pairs);
}

TEST_F(HashMapTest, InsertExistent) {
    ASSERT_EQ(hashMapInsert(&map, key1, value1), 0);
    ASSERT_EQ(hashMapInsert(&map, key1, value1), EEXIST);
}

TEST_F(HashMapTest, InsertSeveral) {
    ASSERT_EQ(hashMapInsert(&map, key1, value1), 0);
    ASSERT_EQ(hashMapInsert(&map, key2, value2), 0);
    ASSERT_EQ(hashMapInsert(&map, key3, value3), 0);

    size_t sz;
    ASSERT_EQ(hashMapGetSize(map, &sz), 0);
    ASSERT_EQ(sz, 3);

    const Pair** pairs;
    ASSERT_EQ(hashMapExtractAll(map, &pairs, &sz), 0);
    ASSERT_EQ(sz, 3);
    bool hasKey[3] = {false, false, false};
    for (size_t i = 0; i < 3; ++i) {
        hasKey[pairs[i]->key - 1] = true;
        ASSERT_EQ(keyValueMap[pairs[i]->key], pairs[i]->value);
    }
    ASSERT_TRUE(hasKey[0]);
    ASSERT_TRUE(hasKey[1]);
    ASSERT_TRUE(hasKey[2]);
    free(pairs);
}

TEST_F(HashMapTest, InsertSeveralExistent) {
    ASSERT_EQ(hashMapInsert(&map, key1, value1), 0);
    ASSERT_EQ(hashMapInsert(&map, key2, value2), 0);
    ASSERT_EQ(hashMapInsert(&map, key3, value3), 0);

    ASSERT_EQ(hashMapInsert(&map, key1, value1), EEXIST);
    ASSERT_EQ(hashMapInsert(&map, key2, value2), EEXIST);
    ASSERT_EQ(hashMapInsert(&map, key3, value3), EEXIST);
}

TEST_F(HashMapTest, EraseOne) {
    ASSERT_EQ(hashMapInsert(&map, key1, value1), 0);

    void* erasedValue;
    ASSERT_EQ(hashMapErase(map, key1, &erasedValue), 0);
    ASSERT_EQ(value1, erasedValue);

    size_t sz;
    ASSERT_EQ(hashMapGetSize(map, &sz), 0);
    ASSERT_EQ(sz, 0);

    const Pair** pairs;
    ASSERT_EQ(hashMapExtractAll(map, &pairs, &sz), 0);
    ASSERT_EQ(sz, 0);
    free(pairs);
}

TEST_F(HashMapTest, EraseSeveral) {
    ASSERT_EQ(hashMapInsert(&map, key1, value1), 0);
    ASSERT_EQ(hashMapInsert(&map, key2, value2), 0);
    ASSERT_EQ(hashMapInsert(&map, key3, value3), 0);

    void* erasedValue;
    ASSERT_EQ(hashMapErase(map, key1, &erasedValue), 0);
    ASSERT_EQ(value1, erasedValue);
    ASSERT_EQ(hashMapErase(map, key2, &erasedValue), 0);
    ASSERT_EQ(value2, erasedValue);
    ASSERT_EQ(hashMapErase(map, key3, &erasedValue), 0);
    ASSERT_EQ(value3, erasedValue);

    size_t sz;
    ASSERT_EQ(hashMapGetSize(map, &sz), 0);
    ASSERT_EQ(sz, 0);

    const Pair** pairs;
    ASSERT_EQ(hashMapExtractAll(map, &pairs, &sz), 0);
    ASSERT_EQ(sz, 0);
    free(pairs);
}

TEST_F(HashMapTest, RehashingWorks) {
    static constexpr size_t NBR_OF_ELEMENTS = 130;

    for (size_t i = 1; i <= NBR_OF_ELEMENTS; ++i) {
        ASSERT_EQ(hashMapInsert(&map, i, reinterpret_cast<void*>(i)), 0);
    }

    size_t sz;
    ASSERT_EQ(hashMapGetSize(map, &sz), 0);
    ASSERT_EQ(sz, NBR_OF_ELEMENTS);
}
