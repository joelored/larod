/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once
#include <gtest/gtest.h>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "callbackhandler.hh"
#include "larod.h"

/**
 * @brief Test fixture for job tests.
 *
 * GoogleTest fixture for job tests. Loads/creates necessary file
 * descriptors and releases them when class is destroyed. Test cases creates one
 * or several Larod connections by calling addConnection() - these connections
 * are then cleaned up when test case is done.
 */
class JobFuncTest : public ::testing::Test {
public:
    /**
     * @brief Container for a Larod connection.
     *
     * Container to group an open Larod connection with a job request etc.
     */
    struct Connection {
        larodConnection* larodConn;
        larodJobRequest* jobReq;
        larodModel* modelPtr;
        size_t numInputs;
        larodTensor** inputTensors;
        size_t numOutputs;
        larodTensor** outputTensors;
        // Note that fds should not be shared among connections since reads on
        // the service side of the fds' contents may happen concurrently and
        // cause file position race conditions.
        std::vector<int> inputFileFds;
        std::vector<int> outputFileFds;
        std::shared_ptr<CallBackHandler> cbh;

        Connection(larodConnection* conn, larodModel* model, size_t numInputs,
                   larodTensor** inputTensors, size_t numOutputs,
                   larodTensor** outputTensors, std::vector<int>&& inputFileFds,
                   std::vector<int>&& outputFileFds,
                   std::shared_ptr<CallBackHandler> handler)
            : larodConn(conn), jobReq(nullptr), modelPtr(model),
              numInputs(numInputs), inputTensors(inputTensors),
              numOutputs(numOutputs), outputTensors(outputTensors),
              inputFileFds(inputFileFds), outputFileFds(outputFileFds),
              cbh(handler) {}

        larodJobRequest* createJobReq();
    };

    JobFuncTest();
    void TearDown() override;

    // Command-line arguments - these are populated by the main function based
    // on the argparse info.
    static bool debugPrints;
    static larodChip chip;
    static std::string modelFile;
    static std::vector<std::string> inputFiles;
    static std::vector<std::string> verificationFiles;
    static larodMap* modelParams;
    static larodMap* jobParams;

protected:
    /**
     * @brief Open a larod connection, get graph and tensors.
     *
     * Open a larod connection and loads the graph specified by the client in @c
     * modelFile. Tensor input and verification files will be loaded using @p
     * getFileDescriptors() if not already done. The created @c Connection
     * instance will have tensors created based on the model info and populated
     * with corresponding file descriptors.
     *
     * @param chHandlerId Id number for the associated callback handler object.
     * @return Connection object reference if success, otherwise exception is
     * thrown.
     */
    std::shared_ptr<Connection> addConnection(unsigned int chHandlerId);

    void gtestDebugPrint(std::string msg);

    /**
     * @brief Opens all files needed for the fixture.
     *
     * Opens/creates input fd:s, output fd:s and verification fd:s. If client
     * has provided input file names in @c inputFiles these will be opened. If
     * no input file names are provided there will be temp files created based
     * on the @p inputs tensor information.
     * Temp files with size 0 are created for the output tensors. The tensor
     * arrays @p inputs and @p outputs are updated with the newly opened fd:s.
     * If client has provided files for verification these will be opened and
     * stored in the @c verificationFileFds.
     *
     * @param numInputs Number of tensors in the @p inputs array.
     * @param inputByteSizes Byte sizes of input tensors.
     * @param numOutputs Number of tensors in the @p outputs array.
     * @param outputByteSizes Byte sizes of output tensors.
     * @return A pair of vectors containing fds corresponding to the open input
     * and output files respectively.
     */
    std::pair<std::vector<int>, std::vector<int>>
        getFileDescriptors(size_t numInputs, size_t* inputByteSizes,
                           size_t numOutputs, size_t* outputByteSizes);

    /**
     * @brief Create a temp file with given size.
     *
     * Creates a temp file and truncates it to @p size bytes. The created fd is
     * pushed to the end of the @c fdTracker vector to be cleaned up in the
     * fixture tear-down method.
     *
     * @param size Created fd will be truncated to this size.
     * @return File descriptor for the newly created temp file. Exception is
     * thrown upon error.
     */
    int createTempFile(size_t size);

    /**
     * @brief Read (parts) of an open file to buffer.
     *
     * @param fd File to read data from.
     * @param offset Offset where reading should start.
     * @param size Number of bytes to read. Set this to 0 zero for system to
     * determine the current size of the file.
     * @return Vector holding the read bytes. Exception is thrown on error.
     */
    std::vector<uint8_t> readFileToVector(int fd, off_t offset, size_t size);

    /**
     * @brief Create a temp file and insert data.
     *
     * Creates an empty temp file with size @p offset. The full contents of @p
     * inputFd will be copied into the newly created temp file starting at @p
     * offset.
     * @param fd File to read data from.
     * @param offset Offset where writing should start.
     * @return Tuple containing the newly created file descriptor and the
     * number of bytes copied from @p inputFd at the end of new temp file.
     */
    std::tuple<int, size_t> buildTempFileWithOffset(int inputFd, off_t offset);

protected:
    int modelFd;                          ///< Fd containing the model data.
    size_t* inputByteSizes;               ///< Byte sizes for input tensors.
    size_t* outputByteSizes;              ///< Byte sizes for output tensors.
    std::vector<int> verificationFileFds; ///< Fds with verification data.
    std::vector<int> fdTracker;           ///< Fds in this vector will closed in
                                          ///< the fixture tear-down method.
    std::vector<std::shared_ptr<Connection>>
        larodConns; ///< Vector of connection objects.

    const size_t AUTO_SIZE = 0;
};
