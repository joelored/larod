/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <deque>
#include <fcntl.h>
#include <fstream>
#include <functional>
#include <gtest/gtest.h>
#include <mutex>
#include <random>
#include <stdexcept>

#include "argparse.h"
#include "callbackhandler.hh"
#include "jobfunctest.hh"
#include "larod.h"
#include "testmacros.h"
#include "testutils.hh"

using namespace std;
using namespace std::chrono;
using namespace larod::test;

// Decides how much bytes of the output from a job may differ from a precomputed
// "correct" output. Needed since different HW produces slightly different
// output (but are very similar).
#define MAX_ALLOWED_DIFF 15

// Globals used to pass cmd-line arguments to test cases.
static bool verifySequence;

/**
 * @brief Empty Larod job callback.
 *
 * Empty callback for Larod async jobs to be used with the 'warm-up' jobs.
 */
void jobCallbackNoOp(void*, larodError*) {
}

/**
 * @brief Larod job callback with book-keeping.
 *
 * Larod async job callback that collects info about the job that was
 * finished by calling into the associated CallBackHandler.
 */
void jobCallbackCount(void* userData, larodError* error) {
    CallBackHandler::CallbackItem* cbi =
        static_cast<CallBackHandler::CallbackItem*>(userData);

    if (error) {
        cbi->cbh->callBackReceived(cbi->prio, cbi->sequenceNumber, error->code);
    } else {
        cbi->cbh->callBackReceived(cbi->prio, cbi->sequenceNumber,
                                   LAROD_ERROR_NONE);
    }

    delete cbi;
}

/**
 * @brief Get byte size of @c larodTensorDataType.
 *
 * @param dataType Data type of a tensor.
 * @param size Reference to the variable where the result will be stored.
 * @return False if any errors occur; otherwise true.
 */
bool getDataTypeSize(const larodTensorDataType& dataType, size_t& size) {
    switch (dataType) {
    case LAROD_TENSOR_DATA_TYPE_BOOL:
        size = sizeof(bool);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT8:
        size = sizeof(uint8_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT8:
        size = sizeof(int8_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT16:
        size = sizeof(uint16_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT16:
        size = sizeof(int16_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT32:
        size = sizeof(uint32_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT32:
        size = sizeof(int32_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_UINT64:
        size = sizeof(uint64_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_INT64:
        size = sizeof(int64_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_FLOAT16:
        // TfLiteFloat16 uses a struct containing a uint16_t. That could be
        // padded though...
        size = sizeof(uint16_t);
        return true;
    case LAROD_TENSOR_DATA_TYPE_FLOAT32:
        size = sizeof(float);
        return true;
    case LAROD_TENSOR_DATA_TYPE_FLOAT64:
        size = sizeof(double);
        return true;
    default:
        return false;
    }
}

/**
 * @brief: Retrieves unpadded data.
 *
 * Fills @p unpaddedData with the data in @p data, without the elements that
 * correspond to padding.
 *
 * This function recursively calls itself until it reaches the last dimension;
 * where it inserts a row of elements. The result is a nest of loops (one for
 * each dimension). The @p dim denotes the dimension level and @p pos denotes
 * the "element position"; these should be set to 0 in the first call of
 * getUnpaddedData() to retrieve all of the data.
 */
void getUnpaddedData(vector<uint8_t>& unpaddedData, const vector<uint8_t>& data,
                     const vector<size_t>& dims, const vector<size_t>& pitches,
                     size_t dataTypeSize, size_t dim, size_t pos) {
    assert(dim <= dims.size() - 1);
    for (size_t i = 0; i < dims[dim]; ++i) {
        if (dim == dims.size() - 1) {
            assert(pos + (i + 1) * dataTypeSize <= data.size());
            unpaddedData.insert(
                unpaddedData.end(),
                data.begin() + static_cast<ptrdiff_t>(pos + i * dataTypeSize),
                data.begin() +
                    static_cast<ptrdiff_t>(pos + (i + 1) * dataTypeSize));
        } else {
            getUnpaddedData(unpaddedData, data, dims, pitches, dataTypeSize,
                            dim + 1, pos + i * pitches[dim + 1]);
        }
    }
}

/**
 * @brief: Removes any padding from a tensor data vector.
 *
 * Removes padding from @p tensorData and also checks that the attributes of the
 * tensor are consistent with one another. The @p tensor must have specified
 * data type, dims and pitches.
 */
void removeAnyPadding(vector<uint8_t>& tensorData, const larodTensor* tensor,
                      larodError* error) {
    const larodTensorDims* dimsPtr = larodGetTensorDims(tensor, &error);
    ASSERT_EQ(error, nullptr);
    const larodTensorPitches* pitchesPtr =
        larodGetTensorPitches(tensor, &error);
    ASSERT_EQ(error, nullptr);
    ASSERT_NE(dimsPtr, nullptr);
    ASSERT_NE(pitchesPtr, nullptr);
    ASSERT_GT(dimsPtr->len, 0);
    ASSERT_EQ(dimsPtr->len, pitchesPtr->len);

    // Creating std::vector pitches/dims.
    size_t dimensions = dimsPtr->len;
    vector<size_t> pitches(pitchesPtr->pitches,
                           pitchesPtr->pitches + dimensions);
    ASSERT_EQ(pitches[0], tensorData.size());
    vector<size_t> dims(dimsPtr->dims, dimsPtr->dims + dimensions);

    // Retrieve data type byte size of tensor.
    larodTensorDataType dataType = larodGetTensorDataType(tensor, &error);
    ASSERT_EQ(error, nullptr);
    size_t typeSize = 0;
    bool ret = getDataTypeSize(dataType, typeSize);
    ASSERT_EQ(ret, true);

    // Calculating byte size of unpadded tensor.
    size_t num_elements = accumulate(
        dims.begin(), dims.end(), static_cast<size_t>(1), multiplies<size_t>());
    ASSERT_NE(num_elements, 0);
    size_t unpaddedSize = num_elements * typeSize;

    // Fill vector with unpadded data.
    vector<uint8_t> data;
    getUnpaddedData(data, tensorData, dims, pitches, typeSize, 0, 0);

    ASSERT_EQ(data.size(), unpaddedSize);

    // Substitute tensorData with the unpadded data.
    tensorData = std::move(data);
}

void verifyTensorData(vector<uint8_t>& tensorData,
                      vector<uint8_t>& tensorRefData, const larodTensor* tensor,
                      larodError* error) {
    larodTensorLayout layout = larodGetTensorLayout(tensor, &error);
    ASSERT_EQ(error, nullptr);
    ASSERT_NE(layout, LAROD_TENSOR_LAYOUT_INVALID);

    if (layout == LAROD_TENSOR_LAYOUT_420SP) {
        // Retrieve data type byte size of tensor.
        larodTensorDataType dataType = larodGetTensorDataType(tensor, &error);
        ASSERT_EQ(error, nullptr);
        size_t typeSize = 0;
        bool ret = getDataTypeSize(dataType, typeSize);
        ASSERT_EQ(ret, true);

        // Get dims and pitches
        const larodTensorDims* dimsPtr = larodGetTensorDims(tensor, &error);
        ASSERT_EQ(error, nullptr);
        ASSERT_NE(dimsPtr, nullptr);
        ASSERT_GT(dimsPtr->len, 0);
        const larodTensorPitches* pitchesPtr =
            larodGetTensorPitches(tensor, &error);
        ASSERT_EQ(error, nullptr);
        ASSERT_NE(pitchesPtr, nullptr);
        ASSERT_EQ(pitchesPtr->len, dimsPtr->len);

        // Calculate width, height, pitch.
        size_t bufferWidth = dimsPtr->dims[2] * typeSize;
        size_t bufferHeight = 3 * dimsPtr->dims[1] / 2;
        size_t pitch = pitchesPtr->pitches[2];

        for (size_t i = 0; i < bufferHeight; i++) {
            for (size_t j = 0; j < bufferWidth; j++) {
                ASSERT_LE(abs(tensorData[i * pitch + j] -
                              tensorRefData[i * pitch + j]),
                          MAX_ALLOWED_DIFF);
            }
        }
    } else {
        removeAnyPadding(tensorData, tensor, error);
        removeAnyPadding(tensorRefData, tensor, error);

        ASSERT_EQ(tensorData.size(), tensorRefData.size());

        for (size_t j = 0; j < tensorRefData.size(); j++) {
            ASSERT_LE(abs(tensorData[j] - tensorRefData[j]), MAX_ALLOWED_DIFF);
        }
    }
}

// Verify basic functionality of job related API calls
TEST_F(JobFuncTest, JobRequest) {
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    larodConnection* conn = c->larodConn;

    larodJobRequest* jobReq = c->createJobReq();

    bool ret = larodRunJob(conn, jobReq, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    size_t numOutputs = 0;
    larodTensor** outputTensors =
        larodCreateModelOutputs(c->modelPtr, &numOutputs, &error);
    ASSERT_NE(outputTensors, nullptr);
    ASSERT_EQ(error, nullptr);
    ASSERT_GE(numOutputs, 1);

    if (!verificationFiles.empty()) {
        ASSERT_EQ(numOutputs, verificationFiles.size());
    }

    size_t* outputByteSizes =
        larodGetModelOutputByteSizes(c->modelPtr, nullptr, &error);
    ASSERT_NE(outputByteSizes, nullptr);
    ASSERT_EQ(error, nullptr);

    // Verify that expected number of bytes was written to each output file.
    for (size_t i = 0; i < numOutputs; i++) {
        struct stat stat_buf;
        ASSERT_EQ(fstat(c->outputFileFds[i], &stat_buf), 0);
        ASSERT_EQ(stat_buf.st_size, outputByteSizes[i]);

        if (!verificationFileFds.empty()) {
            vector<uint8_t> verificationVector =
                readFileToVector(verificationFileFds[i], 0, AUTO_SIZE);
            ASSERT_GT(verificationVector.size(), 0);

            vector<uint8_t> outputVector =
                readFileToVector(c->outputFileFds[i], 0, AUTO_SIZE);
            ASSERT_GT(outputVector.size(), 0);

            ASSERT_EQ(outputVector.size(), verificationVector.size());

            verifyTensorData(outputVector, verificationVector, outputTensors[i],
                             error);
        }
    }

    larodDestroyTensors(&outputTensors, numOutputs);
    free(outputByteSizes);
}

/**
 * @brief Test to verify Larod async priorities.
 *
 * This test case verifies that queued job requests are processed and reported
 * back in an order that satisfies larod's priority guarantees, namely that a
 * high priority request may only be ordered past lower priority requests. In
 * other words, high priority requests may never be reordered _after_ lower
 * priority requests.
 *
 * 1. A set of Larod connections are created.
 * 2. X number of low priority job requests are made,
 *    followed by 1 high priority request (per larodConnection),
 *    followed by another X low priority requests.
 * 3. A number of 'dummy' warm-up jobs are queued to add some items in
 *    Larod service queue.
 * 4. Items created in step 2 are queued in random order on the Larod
 *    connections. The only requests that are not randomized are the high
 *    priority ones, since we want to verify that they are not moved down the
 *    list.
 * 5. The test case blocks until all job callbacks are received.
 * 6. The priority and sequence ordering for all the Larod connections are
 *    verified.
 */
TEST_F(JobFuncTest, AsyncPrioMulti) {
    // Too many concurrent async jobs will max out larod's fd pool.
    const unsigned int NUM_CONNECTIONS = 3;
    const unsigned int NUM_JOBS_PER_CONNECTION = 10;

    const uint8_t LOW_PRIO = 1;
    const uint8_t HIGH_PRIO = 99;

    bool ret;
    larodError* error = nullptr;

    vector<pair<unsigned int, uint8_t>> jobs;

    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        addConnection(i);
    }

    // Add low prio jobs.
    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        for (unsigned int j = 0; j < NUM_JOBS_PER_CONNECTION; ++j) {
            jobs.emplace_back(i, LOW_PRIO);
        }
    }

    // Add high prio jobs.
    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        jobs.emplace_back(i, HIGH_PRIO);
    }

    // Add more low prio jobs.
    for (unsigned int i = 0; i < NUM_CONNECTIONS; ++i) {
        for (unsigned int j = 0; j < NUM_JOBS_PER_CONNECTION; ++j) {
            jobs.emplace_back(i, LOW_PRIO);
        }
    }

    gtestDebugPrint("Issuing 'warmup jobs'...");
    for (auto conn : larodConns) {
        const unsigned int WARMUP_JOBS = 3;
        conn->createJobReq();

        for (unsigned int i = 0; i < WARMUP_JOBS; i++) {
            ret = larodRunJobAsync(conn->larodConn, conn->jobReq,
                                   jobCallbackNoOp, nullptr, &error);
            ASSERT_TRUE(ret);
            ASSERT_EQ(error, nullptr);
        }
    }

    // Always print the rand seed to log to be able to re-create failed Jenkins
    // tests.
    unsigned int seed = (unsigned int) (time(nullptr));
    gtestPrint(string("Using random seed: ") + to_string(seed));

    gtestDebugPrint("Randomizing jobs...");
    random_device rd;
    mt19937 g(rd());
    g.seed(seed);
    auto begin = jobs.begin();
    auto high_jobs_begin = begin + NUM_CONNECTIONS * NUM_JOBS_PER_CONNECTION;
    auto high_jobs_end =
        begin + NUM_CONNECTIONS * (NUM_JOBS_PER_CONNECTION + 1);
    auto end = jobs.end();

    // Shuffle first batch of LOW_PRIO jobs.
    shuffle(begin, high_jobs_begin, g);

    // Shuffle batch of HIGH_PRIO jobs.
    shuffle(high_jobs_begin, high_jobs_end, g);

    // Shuffle second batch of LOW_PRIO jobs.
    shuffle(high_jobs_end, end, g);

    for (auto job : jobs) {
        auto conn = larodConns[job.first];
        unsigned int seqNum = conn->cbh->getSequenceNumber();

        gtestDebugPrint(string("  Adding req seqnum=") + to_string(seqNum) +
                        " prio=" + to_string((unsigned int) job.second) +
                        " connection=" + to_string(conn->cbh->ID));

        void* cbParam =
            new CallBackHandler::CallbackItem(job.second, seqNum, conn->cbh);

        ret = larodSetJobRequestPriority(conn->jobReq, job.second, &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(error, nullptr);

        ret = larodRunJobAsync(conn->larodConn, conn->jobReq, jobCallbackCount,
                               cbParam, &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(error, nullptr);
    }

    // Wait until all requests on all Larod connections has finished.
    gtestDebugPrint("Waiting for jobs to finish.");
    for (auto conn : larodConns) {
        shared_ptr<CallBackHandler> cbh = conn->cbh;
        unique_lock<mutex> lk(cbh->callbackMutex);
        cbh->callbackCond.wait(
            lk, [cbh] { return (cbh->numPendingCallbacks <= 0); });
    }

    gtestDebugPrint("Verifying callbacks..");
    for (auto conn : larodConns) {
        gtestDebugPrint(string("  conn id: ") + to_string(conn->cbh->ID));
        shared_ptr<CallBackHandler> cbh = conn->cbh;

        unsigned int outSeqNum = 0;
        for (auto cbItem : cbh->receivedCallbacks) {
            gtestDebugPrint(string("    prio: ") +
                            to_string((unsigned int) cbItem.priority) +
                            " seq num: " + to_string(cbItem.sequenceNumber));

            ASSERT_EQ(cbItem.returnCode, LAROD_ERROR_NONE);

            // Whenever we get a HIGH_PRIO request, verify that it was handled
            // before the second batch of LOW_PRIO requests.
            if (cbItem.priority == HIGH_PRIO) {
                ASSERT_LE(outSeqNum, cbItem.sequenceNumber);
            }

            outSeqNum += 1;
        }
    }
}

TEST_F(JobFuncTest, AsyncModelAccessDenied) {
    bool ret;
    larodError* error = nullptr;

    gtestDebugPrint("Creating Larod connections...");
    shared_ptr<Connection> connA = addConnection(0);
    connA->createJobReq();
    shared_ptr<Connection> connB = addConnection(1);
    connB->createJobReq();

    // Execute a couple of jobs on each connection to 'warm up' before issuing
    // the jobs where we check order etc. This gets the backend unit in Larod
    // service busy, making sure that the interesting jobs we add will be placed
    // in the order we expect rather than being picked up directly by busy
    // execution units in the backend.
    const unsigned int INITIAL_JOBS = 5;
    uint8_t prio = 50;
    void* jobParam = nullptr;

    ret = larodSetJobRequestPriority(connA->jobReq, prio, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    for (unsigned int i = 0; i < INITIAL_JOBS; i++) {
        jobParam = new CallBackHandler::CallbackItem(
            prio, connA->cbh->getSequenceNumber(), connA->cbh);

        ret = larodRunJobAsync(connA->larodConn, connA->jobReq,
                               jobCallbackCount, jobParam, &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(error, nullptr);
    }

    // Now enqueue a high-prio request on connection A but use model pointer
    // from connection B.
    prio = 90;
    ret = larodSetJobRequestPriority(connA->jobReq, prio, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetJobRequestModel(connA->jobReq, connB->modelPtr, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    jobParam = new CallBackHandler::CallbackItem(
        prio, connA->cbh->getSequenceNumber(), connA->cbh);
    ret = larodRunJobAsync(connA->larodConn, connA->jobReq, jobCallbackCount,
                           jobParam, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    gtestDebugPrint("Waiting for jobs to finish.");
    {
        unique_lock<mutex> lk(connA->cbh->callbackMutex);
        connA->cbh->callbackCond.wait(
            lk, [connA] { return (connA->cbh->numPendingCallbacks <= 0); });
    }

    bool failedCallbackFound = false;
    for (auto cbitem : connA->cbh->receivedCallbacks) {
        if (cbitem.priority == prio) {
            ASSERT_EQ(cbitem.returnCode, LAROD_ERROR_PERMISSION);
            failedCallbackFound = true;
            break;
        }
    }
    ASSERT_EQ(failedCallbackFound, true);
}

// Exercise jobReq setters.
TEST_F(JobFuncTest, jobReqSetters) {
    bool ret;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    larodConnection* conn = c->larodConn;
    larodJobRequest* jobReq = c->createJobReq();

    size_t numInputs = 0;
    larodTensor** inputTensors =
        larodCreateModelInputs(c->modelPtr, &numInputs, &error);
    ASSERT_EQ(numInputs, c->inputFileFds.size());
    ASSERT_NE(inputTensors, nullptr);
    ASSERT_EQ(error, nullptr);

    size_t numOutputs = 0;
    larodTensor** outputTensors =
        larodCreateModelOutputs(c->modelPtr, &numOutputs, &error);
    ASSERT_EQ(numOutputs, c->outputFileFds.size());
    ASSERT_NE(outputTensors, nullptr);
    ASSERT_EQ(error, nullptr);

    for (size_t i = 0; i < numInputs; i++) {
        ret = larodSetTensorFd(inputTensors[i], c->inputFileFds[i], &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(error, nullptr);
    }

    for (size_t i = 0; i < numOutputs; i++) {
        ret = larodSetTensorFd(outputTensors[i], c->outputFileFds[i], &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(error, nullptr);
    }

    ret = larodSetJobRequestModel(jobReq, c->modelPtr, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetJobRequestInputs(jobReq, inputTensors, numInputs, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetJobRequestOutputs(jobReq, outputTensors, numOutputs, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodRunJob(conn, jobReq, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    larodDestroyTensors(&inputTensors, numInputs);
    ASSERT_EQ(inputTensors, nullptr);
    larodDestroyTensors(&outputTensors, numOutputs);
    ASSERT_EQ(outputTensors, nullptr);
}

/**
 * @brief Offset on input tensor.
 */
TEST_F(JobFuncTest, InputWithOffset) {
    bool ret = false;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    auto [inputFd, ignore] = buildTempFileWithOffset(c->inputFileFds[0], 11111);
    ASSERT_GE(inputFd, 0);

    ASSERT_TRUE(larodSetTensorFd(c->inputTensors[0], inputFd, nullptr));
    ASSERT_TRUE(larodSetTensorFdOffset(c->inputTensors[0], 11111, nullptr));
    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector =
            readFileToVector(verificationFileFds[0], 0, AUTO_SIZE);
        ASSERT_GT(verificationVector.size(), 0);

        vector<uint8_t> outputVector =
            readFileToVector(c->outputFileFds[0], 0, verificationVector.size());
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

/**
 * @brief Offset on output tensor.
 */
TEST_F(JobFuncTest, OutputWithOffset) {
    bool ret = false;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    int outputFd = createTempFile(11111);
    ASSERT_GE(outputFd, 0);

    ASSERT_TRUE(larodSetTensorFd(c->outputTensors[0], outputFd, nullptr));
    ASSERT_TRUE(larodSetTensorFdOffset(c->outputTensors[0], 11111, nullptr));
    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector =
            readFileToVector(verificationFileFds[0], 0, AUTO_SIZE);
        ASSERT_GT(verificationVector.size(), 0);

        vector<uint8_t> outputVector =
            readFileToVector(outputFd, 11111, verificationVector.size());
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

TEST_F(JobFuncTest, InputWithTooLargeOffset) {
    bool ret = false;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    int inputFd = createTempFile(11111);
    ASSERT_GE(inputFd, 0);

    // Set input fd offset (22222) to beyond the file end.
    ASSERT_TRUE(larodSetTensorFd(c->inputTensors[0], inputFd, nullptr));
    ASSERT_TRUE(larodSetTensorFdOffset(c->inputTensors[0], 22222, nullptr));
    c->createJobReq();

    // Job should fail: Input tensor offset is too big.
    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, InputWithSize) {
    bool ret = false;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    auto [inputFd, inputSize] = buildTempFileWithOffset(c->inputFileFds[0], 0);
    ASSERT_GE(inputFd, 0);
    ASSERT_GT(inputSize, 0);

    ASSERT_TRUE(larodSetTensorFd(c->inputTensors[0], inputFd, nullptr));
    ASSERT_TRUE(larodSetTensorFdSize(c->inputTensors[0], inputSize, nullptr));
    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector =
            readFileToVector(verificationFileFds[0], 0, AUTO_SIZE);
        ASSERT_GT(verificationVector.size(), 0);

        vector<uint8_t> outputVector =
            readFileToVector(c->outputFileFds[0], 0, verificationVector.size());
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

TEST_F(JobFuncTest, OutputWithSize) {
    bool ret = -1;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    auto [outputFd, outputSize] =
        buildTempFileWithOffset(c->outputFileFds[0], 0);
    ASSERT_GE(outputFd, 0);
    ASSERT_GT(outputSize, 0);

    ASSERT_TRUE(larodSetTensorFd(c->outputTensors[0], outputFd, nullptr));
    ASSERT_TRUE(larodSetTensorFdSize(c->outputTensors[0], outputSize, nullptr));
    c->createJobReq();

    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    if (verificationFileFds.size() > 0) {
        vector<uint8_t> verificationVector =
            readFileToVector(verificationFileFds[0], 0, AUTO_SIZE);
        ASSERT_GT(verificationVector.size(), 0);

        vector<uint8_t> outputVector =
            readFileToVector(outputFd, 0, verificationVector.size());
        ASSERT_GT(outputVector.size(), 0);

        ASSERT_EQ(outputVector.size(), verificationVector.size());

        verifyTensorData(outputVector, verificationVector, c->outputTensors[0],
                         error);
    } else {
        gtestPrint("Skipping file verification -- missing verification file");
    }
}

TEST_F(JobFuncTest, InputWithBadSize) {
    bool ret = false;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    auto [inputFd, inputSize] = buildTempFileWithOffset(c->inputFileFds[0], 0);
    ASSERT_GE(inputFd, 0);
    ASSERT_GT(inputSize, 0);

    ASSERT_TRUE(larodSetTensorFd(c->inputTensors[0], inputFd, nullptr));
    ASSERT_TRUE(
        larodSetTensorFdSize(c->inputTensors[0], inputSize / 2, nullptr));
    c->createJobReq();

    // Don't run this test with debug chip since its input size is so small.
    if (JobFuncTest::chip == LAROD_CHIP_DEBUG) {
        return;
    }

    // Job should fail: Input tensor size is too small.
    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, OutputWithBadSize) {
    bool ret = -1;
    larodError* error = nullptr;

    shared_ptr<Connection> c = addConnection(0);
    auto [outputFd, outputSize] =
        buildTempFileWithOffset(c->outputFileFds[0], 0);
    ASSERT_GE(outputFd, 0);
    ASSERT_GT(outputSize, 0);

    ASSERT_TRUE(larodSetTensorFd(c->outputTensors[0], outputFd, nullptr));
    ASSERT_TRUE(
        larodSetTensorFdSize(c->outputTensors[0], outputSize / 2, nullptr));
    c->createJobReq();

    // Job should fail: Output tensor size is too small.
    ret = larodRunJob(c->larodConn, c->jobReq, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, CreateInputInvalidDimsNCHW) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->inputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->inputTensors[0], LAROD_TENSOR_LAYOUT_NCHW,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    larodJobRequest* jobReq = larodCreateJobRequest(
        c->modelPtr, c->inputTensors, c->numInputs, c->outputTensors,
        c->numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, CreateInputInvalidDimsNHWC) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->inputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->inputTensors[0], LAROD_TENSOR_LAYOUT_NHWC,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    larodJobRequest* jobReq = larodCreateJobRequest(
        c->modelPtr, c->inputTensors, c->numInputs, c->outputTensors,
        c->numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, CreateOutputInvalidDimsNCHW) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->outputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->outputTensors[0], LAROD_TENSOR_LAYOUT_NCHW,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    larodJobRequest* jobReq = nullptr;
    jobReq = larodCreateJobRequest(c->modelPtr, c->inputTensors, c->numInputs,
                                   c->outputTensors, c->numOutputs, jobParams,
                                   &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, CreateOutputInvalidDimsNHWC) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->outputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->outputTensors[0], LAROD_TENSOR_LAYOUT_NHWC,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    larodJobRequest* jobReq = larodCreateJobRequest(
        c->modelPtr, c->inputTensors, c->numInputs, c->outputTensors,
        c->numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, SetInputInvalidDimsNCHW) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodJobRequest* jobReq = c->createJobReq();

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->inputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->inputTensors[0], LAROD_TENSOR_LAYOUT_NCHW,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret =
        larodSetJobRequestInputs(jobReq, c->inputTensors, c->numInputs, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, SetInputInvalidDimsNHWC) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodJobRequest* jobReq = c->createJobReq();

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->inputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->inputTensors[0], LAROD_TENSOR_LAYOUT_NHWC,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret =
        larodSetJobRequestInputs(jobReq, c->inputTensors, c->numInputs, &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, SetOutputInvalidDimsNCHW) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodJobRequest* jobReq = c->createJobReq();

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->outputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->outputTensors[0], LAROD_TENSOR_LAYOUT_NCHW,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetJobRequestInputs(jobReq, c->outputTensors, c->numOutputs,
                                   &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, SetOutputInvalidDimsNHWC) {
    larodError* error = nullptr;
    bool ret = false;

    shared_ptr<Connection> c = addConnection(0);

    larodJobRequest* jobReq = c->createJobReq();

    larodTensorDims dims = {{0}, 2};
    ret = larodSetTensorDims(c->outputTensors[0], &dims, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorLayout(c->outputTensors[0], LAROD_TENSOR_LAYOUT_NHWC,
                               &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetJobRequestInputs(jobReq, c->outputTensors, c->numOutputs,
                                   &error);
    ASSERT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

// Check input parameter validation of larodRunJob
TEST_F(JobFuncTest, larodRunJobInvalidParameter) {
    larodError* error = nullptr;
    shared_ptr<Connection> c = addConnection(0);

    // Connection=NULL, 'valid' request ptr
    bool ret = larodRunJob(nullptr, c->jobReq, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    // 'Valid' connection but request ptr is NULL
    ret = larodRunJob(c->larodConn, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    // Both params NULL
    ret = larodRunJob(nullptr, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

// Check input parameter validation of larodRunJobAsync
TEST_F(JobFuncTest, larodRunJobAsyncInvalidParameter) {
    larodError* error = nullptr;
    shared_ptr<Connection> c = addConnection(0);

    bool ret =
        larodRunJobAsync(nullptr, c->jobReq, jobCallbackNoOp, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    ret = larodRunJobAsync(c->larodConn, nullptr, jobCallbackNoOp, nullptr,
                           &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    ret = larodRunJobAsync(c->larodConn, c->jobReq, nullptr, nullptr, &error);
    EXPECT_FALSE(ret);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

TEST_F(JobFuncTest, CreateJobRequestInvalidParameter) {
    larodError* error = nullptr;
    larodJobRequest* jobReq = nullptr;

    shared_ptr<Connection> c = addConnection(0);

    jobReq = larodCreateJobRequest(nullptr, c->inputTensors, c->numInputs,
                                   c->outputTensors, c->numOutputs, jobParams,
                                   &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    jobReq = larodCreateJobRequest(c->modelPtr, nullptr, c->numInputs,
                                   c->outputTensors, c->numOutputs, jobParams,
                                   &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);

    jobReq = larodCreateJobRequest(c->modelPtr, c->inputTensors, c->numInputs,
                                   nullptr, c->numOutputs, jobParams, &error);
    ASSERT_EQ(jobReq, nullptr);
    ASSERT_LAROD_ERROR(error);
    larodClearError(&error);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

    args_t args;

    if (parseArgs(argc, argv, &args) != 0) {
        destroyArgs(&args);
        exit(EXIT_FAILURE);
    }
    JobFuncTest::chip = args.chip;
    JobFuncTest::debugPrints = args.debugPrints;
    JobFuncTest::modelParams = args.modelParams;
    JobFuncTest::jobParams = args.jobParams;
    verifySequence = args.verifySequence;
    if (args.model) {
        JobFuncTest::modelFile = args.model;
    }
    if (args.numInputFiles > 0) {
        JobFuncTest::inputFiles = vector<string>(
            args.inputFiles, args.inputFiles + args.numInputFiles);
    }
    if (args.numVerificationFiles > 0) {
        JobFuncTest::verificationFiles =
            vector<string>(args.verificationFiles,
                           args.verificationFiles + args.numVerificationFiles);
    }

    int ret = RUN_ALL_TESTS();

    destroyArgs(&args);

    return ret;
}
