/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "jobfunctest.hh"

#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <gtest/gtest.h>
#include <iostream>
#include <stdexcept>

#include "testutils.hh"

using namespace std;
using namespace larod::test;

// Parameters from argparse.
string JobFuncTest::modelFile;
vector<string> JobFuncTest::inputFiles;
vector<string> JobFuncTest::verificationFiles;
bool JobFuncTest::debugPrints = false;
larodChip JobFuncTest::chip;
larodMap* JobFuncTest::modelParams = nullptr;
larodMap* JobFuncTest::jobParams = nullptr;

larodJobRequest* JobFuncTest::Connection::createJobReq() {
    larodError* error = nullptr;
    if (jobReq != nullptr) {
        larodDestroyJobRequest(&jobReq);
    }

    larodJobRequest* jobPtr =
        larodCreateJobRequest(modelPtr, inputTensors, numInputs, outputTensors,
                              numOutputs, jobParams, &error);
    if (!jobPtr) {
        throw runtime_error("Failed creating job request: " +
                            string(error->msg));
    }

    jobReq = jobPtr;
    return jobReq;
}

JobFuncTest::JobFuncTest()
    : modelFd(-1), inputByteSizes(nullptr), outputByteSizes(nullptr) {
}

void JobFuncTest::TearDown() {
    if (modelFd >= 0) {
        close(modelFd);
    }
    for (int fd : fdTracker) {
        close(fd);
    }

    free(inputByteSizes);
    free(outputByteSizes);

    for (auto conn : larodConns) {
        larodDestroyTensors(&conn->inputTensors, conn->numInputs);
        larodDestroyTensors(&conn->outputTensors, conn->numOutputs);

        larodDestroyJobRequest(&conn->jobReq);

        if (!larodDeleteModel(conn->larodConn, conn->modelPtr, NULL)) {
            throw runtime_error("Failed deleting model!");
        }

        larodDestroyModel(&conn->modelPtr);

        if (!larodDisconnect(&conn->larodConn, NULL)) {
            throw runtime_error("Failed closing larod connection!");
        }
    }

    // Call base GoogleTest Test::TearDown() for framework cleanup.
    Test::TearDown();
}

shared_ptr<JobFuncTest::Connection>
    JobFuncTest::addConnection(unsigned int chHandlerId) {
    larodConnection* conn = nullptr;
    larodError* error = nullptr;

    bool ret = larodConnect(&conn, &error);
    if (!ret || !conn) {
        throw runtime_error("Failed to open larod connection: " +
                            getErrMsgAndClear(&error));
    }

    // Get hold input model file if file name was supplied by the user on
    // command line.
    if (modelFile.size()) {
        modelFd = open(modelFile.c_str(), O_RDONLY);
        if (modelFd < 0) {
            throw runtime_error("Failed to open model file: " +
                                getErrMsgAndClear(&error));
        }
    } else if (chip == LAROD_CHIP_DEBUG) {
        const size_t dummyModelSize = 20; // Also debugchip need some data.
        modelFd = createTempFile(dummyModelSize);
    } else {
        // File descriptor is negative if no model file is specified.
        modelFd = -1;
    }

    larodModel* model =
        larodLoadModel(conn, modelFd, chip, LAROD_ACCESS_PRIVATE, "larod-model",
                       modelParams, &error);
    if (!model) {
        throw runtime_error("Failed loading model: " +
                            getErrMsgAndClear(&error));
    }

    size_t numInputs = 0;
    larodTensor** inputs = larodCreateModelInputs(model, &numInputs, &error);
    if (inputs == NULL) {
        throw runtime_error("Failed fetching model inputs: " +
                            getErrMsgAndClear(&error));
    }

    size_t numOutputs = 0;
    larodTensor** outputs = larodCreateModelOutputs(model, &numOutputs, &error);
    if (outputs == NULL) {
        throw runtime_error("Failed fetching model outputs: " +
                            getErrMsgAndClear(&error));
    }

    // Load input files when first model is loaded. At this time we have access
    // to the model tensors, and we know that same model is opened every time.
    if (larodConns.empty()) {
        inputByteSizes = larodGetModelInputByteSizes(model, nullptr, nullptr);
        outputByteSizes = larodGetModelOutputByteSizes(model, nullptr, nullptr);

        if (!inputByteSizes || !outputByteSizes) {
            throw runtime_error("Failed getting tensor byte sizes");
        }
    }

    auto [inputFileFds, outputFileFds] = getFileDescriptors(
        numInputs, inputByteSizes, numOutputs, outputByteSizes);

    // Populate input and output file descriptors in larod tensors.
    for (size_t i = 0; i < numInputs; i++) {
        if (!larodSetTensorFd(inputs[i], inputFileFds[i], &error)) {
            throw runtime_error("Failed setting fd on input tensor " +
                                to_string(i) + ": " +
                                getErrMsgAndClear(&error));
        }
        // Makes sure that lseek is not being run on fds in lib at the same time
        // the service is reading from them. This could otherwise happen since
        // we're using the same fds for all jobs per connection.
        if (!larodSetTensorFdSize(inputs[i], inputByteSizes[i], &error)) {
            throw runtime_error("Failed setting fd on input tensor " +
                                to_string(i) + ": " +
                                getErrMsgAndClear(&error));
        }
    }

    for (size_t i = 0; i < numOutputs; i++) {
        if (!larodSetTensorFd(outputs[i], outputFileFds[i], &error)) {
            throw runtime_error("Failed setting fd on output tensor " +
                                to_string(i) + ": " +
                                getErrMsgAndClear(&error));
        }
    }

    // Adding larodClearError() mostly for the record: exceptions are thrown on
    // error thus error will always be NULL here...
    larodClearError(&error);

    return larodConns.emplace_back(make_shared<JobFuncTest::Connection>(
        conn, model, numInputs, inputs, numOutputs, outputs,
        std::move(inputFileFds), std::move(outputFileFds),
        make_shared<struct CallBackHandler>(chHandlerId)));
}

void JobFuncTest::gtestDebugPrint(string msg) {
    if (debugPrints) {
        gtestPrint(msg);
    }
}

int JobFuncTest::createTempFile(size_t size) {
    char fileName[] = "/tmp/larod-job-XXXXXX";
    int fd = mkstemp(fileName);
    if (fd < 0) {
        throw runtime_error("Failed to create temp file");
    }
    if (size > 0) {
        // FIXME: Unsafe cast to off_t from size_t.
        if (ftruncate(fd, static_cast<off_t>(size)) != 0) {
            throw runtime_error("Failed ftruncate on tmp file with size " +
                                to_string(size));
        }
    }
    remove(fileName);

    fdTracker.push_back(fd);

    return fd;
}

pair<vector<int>, vector<int>>
    JobFuncTest::getFileDescriptors(size_t numInputs, size_t* inputByteSizes,
                                    size_t numOutputs,
                                    size_t* outputByteSizes) {
    vector<int> inputFileFds;
    vector<int> outputFileFds;

    if (inputFiles.empty()) {
        // No input files provided by client. Create temp files resized based on
        // the tensor info.
        for (size_t i = 0; i < numInputs; i++) {
            int fd = createTempFile(inputByteSizes[i]);
            inputFileFds.push_back(fd);
        }
    } else {
        // If user has provided input files then they must match the number of
        // input tensors.
        if (inputFiles.size() != numInputs) {
            throw runtime_error("User provided " +
                                to_string(inputFiles.size()) +
                                " files but model have " +
                                to_string(numInputs) + " input tensors");
        }

        for (string fileName : inputFiles) {
            size_t i = inputFileFds.size();

            int fd = open(fileName.c_str(), O_RDONLY);
            if (fd < 0) {
                throw runtime_error("Failed to open input file");
            }
            inputFileFds.push_back(fd);
            fdTracker.push_back(fd);

            // Figure out and update the input tensor sizes. The sizes we get
            // from larod can't be trusted when loading a pre-processing model,
            // since the tensor with dynamic parameters has size 0.
            off_t filePos = lseek(fd, 0, SEEK_END);
            if (filePos < 0) {
                throw runtime_error("Failed to seek to end of input file");
            }
            if (i < numInputs) {
                inputByteSizes[i] = (size_t) filePos;
            }
            if (lseek(fd, 0, SEEK_SET) != 0) {
                throw runtime_error("Failed lseek() call");
            }
        }
    }

    // Create temp output files.
    for (size_t i = 0; i < numOutputs; i++) {
        // Create an empty temp file and track the fd.
        int fd = createTempFile(outputByteSizes[i]);
        outputFileFds.push_back(fd);
    }

    // Read all verification files.
    for (const auto& fileName : verificationFiles) {
        int fd = open(fileName.c_str(), O_RDONLY);
        if (fd < 0) {
            throw runtime_error("Failed to open input file " + fileName);
        }
        verificationFileFds.push_back(fd);
        fdTracker.push_back(fd);
    }

    return {inputFileFds, outputFileFds};
}

tuple<int, size_t> JobFuncTest::buildTempFileWithOffset(int inputFd,
                                                        off_t offset) {
    assert(offset >= 0);
    int inputFdWithOffset = createTempFile(static_cast<size_t>(offset));
    if (lseek(inputFdWithOffset, offset, SEEK_SET) != offset) {
        throw runtime_error("Failed lseek() call");
    }

    ssize_t totalBytesWritten = 0;
    ssize_t bytesRead = 0;
    do {
        uint8_t buf[4096];
        bytesRead = read(inputFd, buf, sizeof(buf));
        if (bytesRead < 0) {
            throw runtime_error("Read failed");
        }

        ssize_t bytesWritten = 0;
        while (bytesWritten < bytesRead) {
            ssize_t writeRet =
                write(inputFdWithOffset, buf + bytesWritten,
                      static_cast<size_t>(bytesRead - bytesWritten));
            if (writeRet < 0) {
                throw runtime_error("Write failed");
            }
            bytesWritten += writeRet;
        }

        totalBytesWritten += bytesWritten;
    } while (bytesRead > 0);

    return {inputFdWithOffset, totalBytesWritten};
}

vector<uint8_t> JobFuncTest::readFileToVector(int fd, off_t offset,
                                              size_t size) {
    vector<uint8_t> fileContents;

    if (size == 0) {
        struct stat buf;
        if (fstat(fd, &buf) != 0) {
            throw runtime_error("Failed to stat file");
        }
        size = static_cast<size_t>(buf.st_size);
        assert(offset >= 0);
        size -= static_cast<size_t>(offset);
    }

    if (lseek(fd, offset, SEEK_SET) != offset) {
        throw runtime_error("Failed lseek() call.");
    }

    fileContents.resize(size);
    ssize_t readBytes = read(fd, fileContents.data(), size);
    if (readBytes < 0) {
        throw runtime_error("Failed read() call.");
    }

    if (static_cast<size_t>(readBytes) != size) {
        throw runtime_error("Failed to read complete file");
    }

    return fileContents;
}
