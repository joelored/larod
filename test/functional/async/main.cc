/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */
#include <vector>

#include "argparse.h"
#include "log.h"
#include "process-handler.hh"
#include "unistd.h"

// Macros to use in calls to runCmdChildren() to increase readability
#define WAIT_VERIFY (true)
#define DONT_WAIT (false)
#define EXEC_PARALLEL (true)
#define EXEC_SERIALIZED (false)

// === Local function declarations ===
int main(int argc, char** argv);
static int runLoadModelJob(args_t* cmdLineArgs);
static int runCrashingClients(args_t* cmdLineArgs);

/**
 * @brief Execute a command in a set of child processes.
 *
 * @param ph The ProcessHandler book-keeping struct.
 * @param procIdArray Array of child process indices.
 * @param cmd The command to execute in the child processes.
 * @param waitAndVerify Flag to decide if we should wait until the command
 *                      finished in the child process. Also verify the result
 *                      of the command.
 * @param execInParallel Set this to execute the command in parallel in all the
 *                       child processes.
 * @param preDelayMs Delay before each command iteration in the child proc.
 * @param postDelayMs Delay after each command iteration in the child.
 * @param numIterations Number of iterations to run the command in the child.
 */
static void runCmdChildren(ProcessHandler_t* ph,
                           const std::vector<unsigned int>& procIdArray,
                           CommandId_t cmd, bool waitAndVerify,
                           bool execInParallel, unsigned int preDelayMs,
                           unsigned int postDelayMs,
                           unsigned int numIterations);

// Helper macro for running a simple command:
//   0ms pre-delay
//   0ms post-delay
//   1 iteration
#define RUN_CMD_SIMPLE(handler, procArray, cmd)                                \
    runCmdChildren(handler, procArray, cmd, WAIT_VERIFY, EXEC_SERIALIZED, 0,   \
                   0, 1)

void runCmdChildren(ProcessHandler_t* ph,
                    const std::vector<unsigned int>& procIdArray,
                    CommandId_t cmd, bool waitAndVerify, bool execInParallel,
                    unsigned int preDelayMs, unsigned int postDelayMs,
                    unsigned int numIterations) {
    for (const auto& procId : procIdArray) {
        sendMessage(ph, cmd, procId, preDelayMs, postDelayMs, numIterations);

        // Wait until the child process have responded to the command
        if (waitAndVerify && !execInParallel) {
            waitChildResult(ph, procId);
        }
    }
    // If we run the command in all children in parallel we wait for the
    // result here.
    if (waitAndVerify && execInParallel) {
        for (const auto& procId : procIdArray) {
            waitChildResult(ph, procId);
        }
    }
}

static void fillVector(std::vector<unsigned int>& v, unsigned int initialValue,
                       unsigned int numElements) {
    unsigned int currentValue = initialValue;
    for (unsigned int i = 0; i < numElements; i++) {
        v.push_back(currentValue++);
    }
}

int runLoadModelJob(args_t* cmdLineArgs) {
    unsigned int numProcsLoadmodel = 0;
    std::vector<unsigned int> loadModelProcs;
    unsigned int numProcsJob = 0;
    std::vector<unsigned int> runJobProcs;
    int result = EXIT_FAILURE;

    switch (cmdLineArgs->mode) {
    case MODE_LOAD_MODELS:
        numProcsLoadmodel = cmdLineArgs->numProcesses;
        fillVector(loadModelProcs, 0, numProcsLoadmodel);
        break;
    case MODE_RUN_JOB:
        numProcsJob = cmdLineArgs->numProcesses;
        fillVector(runJobProcs, 0, numProcsJob);
        break;
    case MODE_MIX:
        // For mixed mode we use half of the processes to each task
        numProcsLoadmodel = cmdLineArgs->numProcesses / 2;
        fillVector(loadModelProcs, 0, numProcsLoadmodel);
        numProcsJob = cmdLineArgs->numProcesses - numProcsLoadmodel;
        fillVector(runJobProcs, numProcsLoadmodel, numProcsJob);
        break;
    default:
        logError("%s: Internal error: Unkown test mode: %d", __func__,
                 cmdLineArgs->mode);
        return EXIT_FAILURE;
        break;
    }

    std::vector<unsigned int> allProcs;
    fillVector(allProcs, 0, cmdLineArgs->numProcesses);

    logDebug(cmdLineArgs->procHandlerDebug, "Forking processes...");
    ProcessHandler_t* ph =
        forkProcesses(cmdLineArgs->numProcesses, cmdLineArgs->procHandlerDebug,
                      cmdLineArgs->modelPath, cmdLineArgs->chip,
                      cmdLineArgs->modelParams, cmdLineArgs->jobParams);
    if (ph == NULL) {
        logError("%s: Failed creating process handler", __func__);
        return EXIT_FAILURE;
    }

    try {
        logDebug(cmdLineArgs->procHandlerDebug, "Opening larod connections...");
        RUN_CMD_SIMPLE(ph, allProcs, CMD_OPENLARODCONN);

        // === Load models ===
        if (numProcsJob > 0) {
            logDebug(cmdLineArgs->procHandlerDebug,
                     "Loading models on all processes dedicated to "
                     "run_job...");
            RUN_CMD_SIMPLE(ph, runJobProcs, CMD_LOADMODEL);
        }

        for (unsigned int j = 0; j < cmdLineArgs->numLoops; j++) {
            logDebug(cmdLineArgs->procHandlerDebug,
                     "=== Iteration %u out of %u ===", (j + 1),
                     cmdLineArgs->numLoops);

            // === Send commands to start job =====
            logDebug(cmdLineArgs->procHandlerDebug,
                     "Running %u procs with jobs, %u jobs each... ",
                     numProcsJob, cmdLineArgs->numCallsPerLoop);

            runCmdChildren(ph, runJobProcs,
                           (cmdLineArgs->useAsync) ? CMD_RUNJOB_ASYNC :
                                                     CMD_RUNJOB,
                           DONT_WAIT, EXEC_PARALLEL,
                           5,  // Pre-delay ms
                           10, // Post-delay ms
                           cmdLineArgs->numCallsPerLoop);

            // === Send commands to load models =====
            logDebug(cmdLineArgs->procHandlerDebug,
                     "Running %u procs with %u model operations",
                     numProcsLoadmodel, cmdLineArgs->numCallsPerLoop);

            runCmdChildren(ph, loadModelProcs,
                           (cmdLineArgs->useAsync) ? CMD_LOADMODEL_ASYNC :
                                                     CMD_LOADMODEL,
                           DONT_WAIT, EXEC_PARALLEL,
                           0, // Pre-delay ms
                           0, // Post-delay ms
                           cmdLineArgs->numCallsPerLoop);

            // === Wait for child processes to signal 'ready' =====
            logDebug(cmdLineArgs->procHandlerDebug,
                     "Waiting for procs to be done (loop %u of %u)...", j,
                     cmdLineArgs->numLoops);

            // === Check results from all child processes =====
            for (const auto& procId : allProcs) {
                waitChildResult(ph, procId);
            }
        }

        logDebug(cmdLineArgs->procHandlerDebug, "Closing larod connections...");
        RUN_CMD_SIMPLE(ph, allProcs, CMD_CLOSELARODCONN);

        // No errors encountered!
        result = EXIT_SUCCESS;
    } catch (const std::exception& e) {
        logError("%s", e.what());
    }

    logDebug(cmdLineArgs->procHandlerDebug, "Shutting down processes...");
    shutdownProcesses(ph);
    logDebug(cmdLineArgs->procHandlerDebug, "Processes shut down.");

    return result;
}

int runCrashingClients(args_t* cmdLineArgs) {
    std::vector<unsigned int> allProcs;

    // Assign the first half of children in allProcs to be used for running
    // jobs.
    unsigned int numProcsJob = cmdLineArgs->numProcesses / 2;
    std::vector<unsigned int> runJobProcs;

    unsigned int numProcsCrash = cmdLineArgs->numProcesses - numProcsJob;
    std::vector<unsigned int> crashProcs;

    fillVector(allProcs, 0, cmdLineArgs->numProcesses);
    fillVector(runJobProcs, 0, numProcsJob);
    fillVector(crashProcs, numProcsJob, numProcsCrash);

    ProcessHandler_t* ph = NULL;
    int result = EXIT_SUCCESS;

    for (unsigned int i = 0;
         i < cmdLineArgs->numLoops && result == EXIT_SUCCESS; i++) {
        try {
            logDebug(cmdLineArgs->procHandlerDebug,
                     "=== Iteration %u out of %u ===", (i + 1),
                     cmdLineArgs->numLoops);

            logDebug(cmdLineArgs->procHandlerDebug, "Forking processes...");
            ph = forkProcesses(
                cmdLineArgs->numProcesses, cmdLineArgs->procHandlerDebug,
                cmdLineArgs->modelPath, cmdLineArgs->chip,
                cmdLineArgs->modelParams, cmdLineArgs->jobParams);
            if (ph == NULL) {
                logError("%s: Failed creating process handler", __func__);
                return EXIT_FAILURE;
            }

            logDebug(cmdLineArgs->procHandlerDebug,
                     "Opening larod connections...");
            RUN_CMD_SIMPLE(ph, allProcs, CMD_OPENLARODCONN);

            // === Load models in all processes ===
            logDebug(cmdLineArgs->procHandlerDebug, "Loading models...");
            RUN_CMD_SIMPLE(ph, allProcs, CMD_LOADMODEL);

            // Initiate jobs in first half of the procs.
            logDebug(cmdLineArgs->procHandlerDebug, "Starting jobs procs...");
            runCmdChildren(ph, runJobProcs,
                           (cmdLineArgs->useAsync) ? CMD_RUNJOB_ASYNC :
                                                     CMD_RUNJOB,
                           DONT_WAIT, EXEC_PARALLEL,
                           5,  // Pre-delay ms
                           10, // Post-delay ms
                           cmdLineArgs->numCallsPerLoop);

            // Start async jobs in the 'crash' set of child processes.
            logDebug(cmdLineArgs->procHandlerDebug,
                     "Async job before crash...");
            constexpr unsigned int preDelayMs = 5;
            constexpr unsigned int postDelayMs = 10;
            runCmdChildren(ph, crashProcs, CMD_RUNJOB_ASYNC, DONT_WAIT,
                           EXEC_PARALLEL, preDelayMs, postDelayMs,
                           cmdLineArgs->numCallsPerLoop);

            // Allow the jobs to run in child processes for an
            // "appropriate" of time - we'd like about half the requests to
            // be processed to ensure that jobs and crashes overlap.
            unsigned int jobTimeMs = 5;
            unsigned int totalJobTimeMs =
                (preDelayMs + jobTimeMs + postDelayMs) *
                cmdLineArgs->numCallsPerLoop;
            usleep((totalJobTimeMs / 2) * 1000);

            // Now we crash a subset of the child processes. Async jobs should
            // still be 'in flight' for the corresponding sessions in the larod
            // service.
            logDebug(cmdLineArgs->procHandlerDebug,
                     "Sending 'crash' commands!");
            runCmdChildren(ph, crashProcs, CMD_ABORT_PROCESS, DONT_WAIT,
                           EXEC_PARALLEL,
                           0, // Pre-delay ms
                           0, // Post-delay ms
                           1);

            // The set of processes running jobs should not have been
            // affected by the set of crashes. Now verify that the jobs
            // finished.
            for (unsigned int j = 0; j < numProcsJob; j++) {
                waitChildResult(ph, runJobProcs[j]);
            }

            // Finally close the larod connections for the 'job' set of
            // processes.
            RUN_CMD_SIMPLE(ph, runJobProcs, CMD_CLOSELARODCONN);

        } catch (const std::exception& e) {
            logError("%s", e.what());
            result = EXIT_FAILURE;
        }
        logDebug(cmdLineArgs->procHandlerDebug, "Shutting down processes...");
        shutdownProcesses(ph);
        ph = NULL;
        logDebug(cmdLineArgs->procHandlerDebug, "Processes shut down.");
    }

    return result;
}

int main(int argc, char** argv) {
    args_t cmdLineArgs;

    if (parseArgs(argc, argv, &cmdLineArgs) != 0) {
        destroyArgs(&cmdLineArgs);
        exit(EXIT_FAILURE);
    }

    int ret = -1;
    if (cmdLineArgs.mode == MODE_CRASH) {
        ret = runCrashingClients(&cmdLineArgs);
    } else {
        ret = runLoadModelJob(&cmdLineArgs);
    }

    destroyArgs(&cmdLineArgs);
    return ret;
}
