/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "argparse.h"

#include <argp.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

#define KEY_USAGE (127)

static int parseOpt(int key, char* arg, struct argp_state* state);

/**
 * @brief Parses a string as an unsigned long long
 *
 * @param arg String to parse.
 * @param i Pointer to the number being the result of parsing.
 * @return Positive errno style return code (zero means success).
 */
static int parsePosInt(char* arg, unsigned long long* i);

const struct argp_option opts[] = {
    {"chip", 'c', "CHIP", 0,
     "Chooses chip CHIP to run tests on, where CHIP is the enum type "
     "larodChip from the library. If not specified, the default chip for a "
     "new connection will be used.",
     0},
    {"async", 'a', NULL, 0,
     "Use async calls. Default is to use synchronous calls.", 0},
    {"mode", 'm', "MODE", 0,
     "Test mode, possible values: load_models, run_job, mix, crash. "
     "Specifies what the child processes should execute. 'mix' will put half of"
     " the processes to load models while the rest of the processes will run "
     "jobs. 'crash' lets half the set of processes run jobs, "
     "the other half run few jobs then crash. Default is 'load_models'.",
     0},
    {"model", 'g', "MODEL", 0,
     "Specifies from which path to load a model. Can be left blank if and only "
     "if the chip used is the debug chip.",
     0},
    {"model-params", 'b', "KEY:VAL", 0,
     "Add additonal parameters when loading model (c.f. larodLoadModel()). "
     "KEY should be a string and VAL should either be a string, integer or an "
     "array of integers separated by \",\". For example "
     "\"image.input.format:rgb-interleaved\", \"image.input.row-pitch:224\" or "
     "\"image.input.size:224,224\".",
     0},
    {"job-params", 'e', "KEY:VAL", 0,
     "Add additional parameters when running a job (c.f. "
     "larodCreateJobRequest()). See option \"--model-params\" for a description"
     " of KEY and VAL.",
     0},
    {"debug", 'd', NULL, 0, "Enable debug prints.", 0},
    {"help", 'h', NULL, 0, "Print this help text and exit.", 0},
    {"usage", KEY_USAGE, NULL, 0, "Print short usage message and exit.", 0},
    {0}};
const struct argp argp = {
    opts,
    parseOpt,
    "NUM_PROCESSES NUM_LOOPS CALLS_PER_LOOP",
    "Executes the larod async functional tests. Enough data to load a model "
    "must be provided: either by supplying a model file name or by supplying "
    "enough model parameters for the specified chip.",
    NULL,
    NULL,
    NULL};

int parseArgs(int argc, char** argv, args_t* args) {
    int ret = argp_parse(&argp, argc, argv, ARGP_NO_HELP, NULL, args);
    if (ret) {
        return ret;
    }

    // Make sure a model path or model parameters are provided.
    if (!args->modelPath && !args->hasModelParams) {
        fprintf(stderr, "%s: Neither model-file nor model params specified\n",
                argv[0]);
        return 1;
    }

    return 0;
}

int parseOpt(int key, char* arg, struct argp_state* state) {
    args_t* args = state->input;
    switch (key) {
    case 'c': {
        unsigned long long chip;
        int ret = parsePosInt(arg, &chip);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret, "invalid chip type");
        }
        // Make sure we don't overflow when casting to enum below.
        if (chip > INT_MAX) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ERANGE,
                         "chip type value too large");
        }
        args->chip = (larodChip) chip;
        break;
    }
    case 'd':
        args->procHandlerDebug = true;
        break;
    case 'a':
        args->useAsync = true;
        break;
    case 'g':
        args->modelPath = arg;
        break;
    case 'm': {
        const size_t MAX_ARG_LEN = 14; // 'run_job' + null-char
        if (arg == NULL) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, EINVAL,
                         "empty mode value provided");
        } else {
            size_t i;
            for (i = 0; (i < MAX_ARG_LEN) && (arg[i] != '\0'); i++) {
                arg[i] = (char) tolower(arg[i]);
            }
            if (i == MAX_ARG_LEN) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, EINVAL,
                             "wrong parameter for argument mode");
            } else {
                if (strncmp("load_model", arg, MAX_ARG_LEN) == 0) {
                    args->mode = MODE_LOAD_MODELS;
                } else if (strncmp("run_job", arg, MAX_ARG_LEN) == 0) {
                    args->mode = MODE_RUN_JOB;
                } else if (strncmp("mix", arg, MAX_ARG_LEN) == 0) {
                    args->mode = MODE_MIX;
                } else if (strncmp("crash", arg, MAX_ARG_LEN) == 0) {
                    args->mode = MODE_CRASH;
                } else {
                    destroyArgs(args);
                    argp_failure(state, EXIT_FAILURE, EINVAL,
                                 "unknown mode parameter");
                }
            }
        }
        break;
    }
    case 'b': {
        int ret = addParam(args->modelParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to model larodMap", arg);
        }
        args->hasModelParams = true;
        break;
    }
    case 'e': {
        int ret = addParam(args->jobParams, arg);
        if (ret) {
            destroyArgs(args);
            argp_failure(state, EXIT_FAILURE, ret,
                         "could not add \"%s\" to job larodMap", arg);
        }
        break;
    }
    case 'h':
        argp_state_help(state, stdout, ARGP_HELP_STD_HELP);
        break;
    case ARGP_KEY_ARG:
        if (state->arg_num == 0) {
            unsigned long long numProcs;
            int ret = parsePosInt(arg, &numProcs);
            if (ret) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, ret,
                             "invalid number of processes");
            }
            // Make sure we don't overflow when casting to uint below.
            if (numProcs > UINT_MAX) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, ERANGE,
                             "number of processes must be less than %u",
                             UINT_MAX);
            }
            args->numProcesses = (unsigned int) numProcs;
        } else if (state->arg_num == 1) {
            unsigned long long numLoops;
            int ret = parsePosInt(arg, &numLoops);
            if (ret) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, ret,
                             "invalid number of loops");
            }
            // Make sure we don't overflow when casting to uint below.
            if (numLoops > UINT_MAX) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, ERANGE,
                             "number of loops must be less than %u", UINT_MAX);
            }
            args->numLoops = (unsigned int) numLoops;
        } else if (state->arg_num == 2) {
            unsigned long long numCallsPerLoop;
            int ret = parsePosInt(arg, &numCallsPerLoop);
            if (ret) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, ret,
                             "invalid number of calls-per-loop");
            }
            // Make sure we don't overflow when casting to uint below.
            if (numCallsPerLoop > UINT_MAX) {
                destroyArgs(args);
                argp_failure(state, EXIT_FAILURE, ERANGE,
                             "number of calls-per-loop must be less than %u",
                             UINT_MAX);
            }
            args->numCallsPerLoop = (unsigned int) numCallsPerLoop;
        } else {
            destroyArgs(args);
            argp_error(state, "Too many arguments given");
        }
        break;
    case KEY_USAGE:
        argp_state_help(state, stdout, ARGP_HELP_USAGE | ARGP_HELP_EXIT_OK);
        break;
    case ARGP_KEY_INIT: {
        larodError* error = NULL;

        args->modelPath = NULL;
        args->chip = LAROD_CHIP_DEBUG;
        args->numProcesses = 0;
        args->numLoops = 0;
        args->numCallsPerLoop = 0;
        args->useAsync = false;
        args->mode = MODE_LOAD_MODELS;
        args->procHandlerDebug = false;

        args->hasModelParams = false;
        args->modelParams = larodCreateMap(&error);
        if (!args->modelParams) {
            destroyArgs(args);
            // larodClearError() deliberately skipped here to be able to print
            // error message.
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not create larodMap: %s", error->msg);
        }
        args->jobParams = larodCreateMap(&error);
        if (!args->jobParams) {
            destroyArgs(args);
            // larodClearError() deliberately skipped here to be able to print
            // error message.
            argp_failure(state, EXIT_FAILURE, ENOMEM,
                         "could not create larodMap: %s", error->msg);
        }
    } break;
    case ARGP_KEY_END:
        if (state->arg_num != 3) {
            argp_error(state, "Invalid number of arguments given");
        }
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static int parsePosInt(char* arg, unsigned long long* i) {
    char* endPtr;
    *i = strtoull(arg, &endPtr, 0);
    if (*endPtr != '\0') {
        return EINVAL;
    } else if (arg[0] == '-' || *i == 0) {
        return EINVAL;
    } else if (*i == ULLONG_MAX) {
        return ERANGE;
    }

    return 0;
}

void destroyArgs(args_t* args) {
    larodDestroyMap(&args->modelParams);
    larodDestroyMap(&args->jobParams);
}
