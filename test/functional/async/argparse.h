/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    MODE_LOAD_MODELS, ///< Test will stress loading models
    MODE_RUN_JOB,     ///< Stress job calls
    MODE_MIX,         ///< Half the set of process will load models
                      ///< while the rest of the processes runs
                      ///< jobs
    MODE_CRASH,       ///< Run jobs and crashing clients simultaneously
} AsyncTestMode_t;

typedef struct args_t {
    char* modelPath;              ///< Model path used by all the child procs.
    bool hasModelParams;          ///< Indicates if client supplied modelParams.
    larodMap* modelParams;        ///< Static model params when loading model.
    larodMap* jobParams;          ///< Dynamic job params sent each inference.
    larodChip chip;               ///< Chip used by all the child processes
    unsigned int numProcesses;    ///< Number of child processes to spawn
    unsigned int numLoops;        ///< Number of API call package loop
    unsigned int numCallsPerLoop; ///< Number of calls per loop
    AsyncTestMode_t mode;         ///< Loading models or running jobs etc
    bool useAsync;                ///< Sync or async calls
    bool procHandlerDebug;        ///< Debug prints from process handler
} args_t;

int parseArgs(int argc, char** argv, args_t* args);
void destroyArgs(args_t* args);

#ifdef __cplusplus
}
#endif
