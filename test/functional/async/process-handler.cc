/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "process-handler.hh"

#include <assert.h>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <limits.h>
#include <mutex>
#include <semaphore.h>
#include <sstream>
#include <stdexcept>
#include <string.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <vector>

#include "argparse.h"
#include "larod.h"
#include "log.h"

#define SHM_NAME ("/LarodAsyncTestShm")
#define SEM_IS_SHARED (1)
#define PROCESS_EXIT_FAILURE (1)
#define INVALID_FD (-1)

typedef struct {
    int* fdList;
    size_t numOpenFiles;
} OpenFilesList_t;

static bool debugPrints = false;

/**
 * @brief Struct for IPC mailbox.
 *
 * Struct to hold a mailbox between the parent process and remote worker
 * process. An array of ChildProcMailbox_t structs are created in the shm
 * IPC shared memory area set up by the 'server' process.
 */
typedef struct {
    unsigned int workerId; ///< Index of slave process (0, 1, 2...).
    pid_t pid;             ///< Slave process id.

    // Server -> child process
    sem_t mailFlagToChild; ///< Semaphore to indicate mail from server to child.
    CommandId_t command;   ///< Command id.
    unsigned int preDelayMs;  ///< Delay to wait before larod call.
    unsigned int postDelayMs; ///< Delay to wait after larod call.
    unsigned int numCycles;   ///< Number of repetitions for a command.

    // Child proc -> server
    bool childResult;       ///< Result of larod operation from child to server.
    sem_t mailFlagToServer; ///< Semaphore indicating operation done from child
                            ///< to server.
} ChildProcMailbox_t;

typedef struct {
    ChildProcMailbox_t* mailbox;     ///< Pointer to the mailbox in IPC
                                     ///< shared mem area.
    larodConnection* larodConn;      ///< larod connection.
    char* modelPath;                 ///< Model path for loading from disk.
    larodChip chip;                  ///< larod chip to use.
    larodMap* modelParams;           ///< Params for loading model.
    larodMap* jobParams;             ///< Params to use when running job.
    OpenFilesList_t modelFiles;      ///< Opened model files.
    std::vector<larodModel*> models; ///< larod loaded models if any.
    OpenFilesList_t sourceFiles;     ///< Source buffers for job calls.
    OpenFilesList_t resultFiles;     ///< Result buffers for jobs.
    std::atomic_uint
        numLoadModelsQueued;        ///< Number of async load-models queued.
    std::atomic_uint numJobsQueued; ///< Number of async jobs queued.
    std::mutex mtxDeleteModels;     ///< Protecting deleteModels.
    std::condition_variable deleteModels; ///< Notify to delete models.
} ChildProc_t;

struct ProcessHandler {
    int shmFd;                      ///< shmem file descriptor.
    ChildProcMailbox_t* mailboxShm; ///< Mapped memory area for the shmFd.
                                    ///< This area contains an array of
                                    ///< ChildProcMailbox_t structs.
    size_t memmapSizeAligned;       ///< Size of mmap() in mailboxShm.
    unsigned int numChildren;       ///< Number of entries in the
                                    ///< mailboxShm array.
};

// Dummy model buffer size for use with DummyBackend. Must be > 0.
static const size_t DUMMY_MODEL_SIZE = 1 * 1024 * 1024;

// Child process functions
static void childProcEntryFcn(ChildProc_t* procInfo);
static void createEmptyFileList(OpenFilesList_t* fileList,
                                const size_t numFiles, const size_t numCycles);
static bool openFilesOrTemp(OpenFilesList_t* fileList, const size_t numFiles,
                            const size_t numCycles, const char* path,
                            const size_t* tempSizes);
static void closeFiles(OpenFilesList_t* fileList);
static void mailboxPreDelay(ChildProcMailbox_t* mBox);
static void mailboxPostDelay(ChildProcMailbox_t* mBox);
static inline void childSignalMboxSem(ChildProcMailbox_t* mBox);
static void childOpenLarodConnection(ChildProc_t* procInfo);
static void childCloseLarodConnection(ChildProc_t* procInfo);

static void childLoadModel(ChildProc_t* procInfo);
static void childLoadModelAsync(ChildProc_t* procInfo);
static void childLoadModelCb(larodModel* model, void* userData,
                             larodError* error);
static bool childDeleteAllModels(ChildProc_t* procInfo);

static void childRunJob(ChildProc_t* procInfo, bool useAsync);
static void childJobCallback(void* userData, larodError* error);
static bool setTensorFds(larodTensor** tensors, size_t numTensors,
                         unsigned int cycle, ChildProc_t* procInfo,
                         OpenFilesList_t* fdBuffer);

ProcessHandler_t* forkProcesses(unsigned int numChild, bool enableDebugPrints,
                                char* modelPath, larodChip chip,
                                larodMap* modelParams, larodMap* jobParams) {
    debugPrints = enableDebugPrints;
    auto shmFd = shm_open(SHM_NAME, O_RDWR | O_CREAT, 0644);

    if (shmFd < 0) {
        logError("Could not get file descriptor for model file, ret=%d.",
                 shmFd);
        return NULL;
    }

    if (shm_unlink(SHM_NAME) < 0) {
        logError("Failed unlink() on shmem name, errstr=%s.", strerror(errno));
    }

    size_t pagesize = (size_t) sysconf(_SC_PAGE_SIZE);
    size_t totalMailboxSize = (sizeof(ChildProcMailbox_t) * numChild);
    auto memmapSizeAligned = (totalMailboxSize + pagesize) & ~(pagesize - 1);

    if (ftruncate(shmFd, (ssize_t) memmapSizeAligned) < 0) {
        logError("Failed ftruncate() on shmem fd, errstr=%s.", strerror(errno));
        return NULL;
    }

    ProcessHandler_t* handler = new ProcessHandler_t();

    handler->shmFd = shmFd;
    handler->numChildren = numChild;
    handler->memmapSizeAligned = memmapSizeAligned;
    auto mailboxShm = static_cast<ChildProcMailbox_t*>(
        mmap(NULL, handler->memmapSizeAligned, PROT_READ | PROT_WRITE,
             MAP_SHARED, handler->shmFd, 0x0));
    if (mailboxShm == MAP_FAILED) {
        logError("Failed mapping memory, errstr=%s.", strerror(errno));
        goto errorExit;
    }
    handler->mailboxShm = mailboxShm;

    memset(handler->mailboxShm, 0x0, handler->memmapSizeAligned);

    // Fork and init new processes
    for (unsigned int i = 0; i < handler->numChildren; i++) {
        ChildProcMailbox_t* mBox = &handler->mailboxShm[i];
        mBox->workerId = i;

        if (sem_init(&mBox->mailFlagToServer, SEM_IS_SHARED, 0) < 0) {
            logError(
                "Failed initializing mailFlagToServer semaphore, errstr=%s.",
                strerror(errno));
            goto errorExit;
        }

        if (sem_init(&mBox->mailFlagToChild, SEM_IS_SHARED, 0) < 0) {
            logError(
                "Failed initializing mailFlagToChild semaphore, errstr=%s.",
                strerror(errno));
            goto errorExit;
        }

        pid_t pidAfterFork = fork();

        if (pidAfterFork == -1) {
            logError("Failed forking new process, errstr=%s.", strerror(errno));
            goto errorExit;
        } else if (pidAfterFork == 0) {
            // Start executing 'main' function for the new thread.
            ChildProc_t childProc = {};

            childProc.mailbox = &handler->mailboxShm[i];
            childProc.modelPath = modelPath;
            childProc.chip = chip;
            childProc.modelParams = modelParams;
            childProc.jobParams = jobParams;
            childProc.modelFiles.fdList = NULL;
            childProc.modelFiles.numOpenFiles = 0;
            childProc.sourceFiles.fdList = NULL;
            childProc.sourceFiles.numOpenFiles = 0;
            childProc.resultFiles.fdList = NULL;
            childProc.resultFiles.numOpenFiles = 0;
            childProc.numJobsQueued = 0;
            childProc.numLoadModelsQueued = 0;

            childProcEntryFcn(&childProc); // childProcEntryFcn will call exit()

        } else {
            logDebug(debugPrints, "Forked new process with pid %d.",
                     pidAfterFork);
            mBox->pid = pidAfterFork;
        }
    }
    return handler;

errorExit:;
    shutdownProcesses(handler);
    return NULL;
}

void shutdownProcesses(ProcessHandler_t* handler) {
    if (handler == NULL)
        return;

    if (handler->mailboxShm != NULL) {
        for (unsigned int i = 0; i < handler->numChildren; i++) {
            ChildProcMailbox_t* mBox = &handler->mailboxShm[i];
            if (mBox->pid != 0) {
                try {
                    SENDMSG_SIMPLE(handler, CMD_SHUTDOWN, i);
                } catch (std::exception& e) {
                    logError("Failed to send message to worker %u", i);
                }
                logDebug(debugPrints, "%s: waitpid() for pid %u.", __func__,
                         mBox->workerId);
                int wstatus = -1;
                if (waitpid(mBox->pid, &wstatus, 0) < 0) {
                    logError("Failed waitpid() for worker %u, errstr=%s.", i,
                             strerror(errno));
                }

                if (!WIFEXITED(wstatus)) {
                    logWarning("Worker process #%u did not terminate "
                               "correctly...",
                               i);
                } else {
                    int returnCode = WEXITSTATUS(wstatus);
                    if (returnCode != 0)
                        logWarning("Worker process #%u terminated with "
                                   "non-zero value: %d.",
                                   i, returnCode);
                }
            }
        }
        if (munmap(handler->mailboxShm, handler->memmapSizeAligned) < 0) {
            logError("Failed munmap() on shared mailbox mem :/");
        }
    }

    if (handler->shmFd >= 0) {
        close(handler->shmFd);
    }

    delete handler;
}

void sendMessage(ProcessHandler_t* handler, CommandId_t cmd,
                 unsigned int childIndex, unsigned int preDelayMs,
                 unsigned int postDelayMs, unsigned int numIterations) {
    ChildProcMailbox_t* mBox = &handler->mailboxShm[childIndex];

    if (numIterations <= 0) {
        logError("Number of iterations should be at least 1!");
        throw std::runtime_error("Number of iterations should be at least 1");
    }

    mBox->command = cmd;
    mBox->preDelayMs = preDelayMs;
    mBox->postDelayMs = postDelayMs;
    mBox->numCycles = numIterations;

    if (sem_post(&mBox->mailFlagToChild) < 0) {
        std::stringstream errMsg;
        errMsg << "Failed triggering child " << mBox->workerId
               << " mailbox semaphore, errstr = " << strerror(errno);
        logError("%s", errMsg.str().c_str());
        throw std::runtime_error(errMsg.str());
    }
}

void waitChildResult(ProcessHandler_t* handler, unsigned int childIndex) {
    ChildProcMailbox_t* mBox = &handler->mailboxShm[childIndex];

    if (sem_wait(&mBox->mailFlagToServer) < 0) {
        std::stringstream errMsg;
        errMsg << "Error when waiting on child " << childIndex
               << " mailbox semaphore";
        throw std::runtime_error(errMsg.str());
    }
    if (!mBox->childResult) {
        std::stringstream errMsg;
        errMsg << "Child " << childIndex << " failed to execute operation";
        throw std::runtime_error(errMsg.str());
    }
}

static void childProcEntryFcn(ChildProc_t* procInfo) {
    // This is the entry function for a child process!
    ChildProcMailbox_t* mBox = procInfo->mailbox;

    bool exitCondition = false;
    int retVal = 0;
    const bool USE_ASYNC = true;
    logDebug(debugPrints, "Thread worker #%u is alive!", mBox->workerId);

    try {
        while (!exitCondition) {
            if (sem_wait(&mBox->mailFlagToChild) < 0) {
                logError("%s: procId %u: Error when waiting on semaphore.",
                         __func__, mBox->workerId);
                retVal = PROCESS_EXIT_FAILURE;
                break;
            }

            logDebug(debugPrints, "%s: procid %u: Got command '%s'.", __func__,
                     mBox->workerId, commandName[mBox->command]);

            switch (mBox->command) {
            case CMD_SHUTDOWN: {
                exitCondition = true;
                break;
            }

            case CMD_OPENLARODCONN: {
                childOpenLarodConnection(procInfo);
                break;
            }

            case CMD_CLOSELARODCONN: {
                childCloseLarodConnection(procInfo);
                break;
            }

            case CMD_LOADMODEL: {
                childLoadModel(procInfo);
                break;
            }

            case CMD_LOADMODEL_ASYNC: {
                childLoadModelAsync(procInfo);
                break;
            }

            case CMD_RUNJOB: {
                childRunJob(procInfo, !USE_ASYNC);
                break;
            }

            case CMD_RUNJOB_ASYNC: {
                childRunJob(procInfo, USE_ASYNC);
                break;
            }

            case CMD_ABORT_PROCESS: {
                // 'Crash' the process for testing larod robustness to crashing
                // clients.
                exit(0);
                break;
            }

            default: {
                logError("%s: procid %u: Received unknown child command %d :/",
                         __func__, mBox->workerId, mBox->command);
                exitCondition = true;
                retVal = PROCESS_EXIT_FAILURE;
                break;
            }
            }

        } // end process main while()
    } catch (const std::exception& e) {
        logError("%s", e.what());
        retVal = PROCESS_EXIT_FAILURE;
    }

    // All test command handlers are supposed to close files they have loaded
    assert(procInfo->modelFiles.numOpenFiles == 0);
    assert(procInfo->sourceFiles.numOpenFiles == 0);
    assert(procInfo->resultFiles.numOpenFiles == 0);

    exit(retVal);
}

static void childSignalMboxSem(ChildProcMailbox_t* mBox) {
    if (sem_post(&mBox->mailFlagToServer) < 0) {
        std::stringstream errMsg;
        errMsg << "Failed triggering child mailbox-to-server for worker "
               << mBox->workerId;
        logError("%s", errMsg.str().c_str());
        throw std::runtime_error(errMsg.str());
    }
}

static void mailboxPreDelay(ChildProcMailbox_t* mBox) {
    if (mBox->preDelayMs > 0)
        usleep(1000 * mBox->preDelayMs);
}

static void mailboxPostDelay(ChildProcMailbox_t* mBox) {
    if (mBox->postDelayMs > 0)
        usleep(1000 * mBox->postDelayMs);
}

void createEmptyFileList(OpenFilesList_t* fileList, const size_t numFiles,
                         const size_t numCycles) {
    assert(fileList);
    assert(fileList->numOpenFiles == 0);
    assert(fileList->fdList == nullptr);

    fileList->fdList = new int[numCycles * numFiles];

    // We need to initialize the file descriptor array to
    // a value that cannot represent an open file, otherwise
    // the error handling will not work.
    for (size_t i = 0; i < numCycles * numFiles; i++) {
        fileList->fdList[i] = INVALID_FD;
    }
}

bool openFilesOrTemp(OpenFilesList_t* fileList, const size_t numFiles,
                     const size_t numCycles, const char* path,
                     const size_t* tempSizes) {
    assert(fileList);
    createEmptyFileList(fileList, numFiles, numCycles);

    for (size_t i = 0; i < numCycles; i++) {
        for (size_t j = 0; j < numFiles; j++) {
            int fd = INVALID_FD;

            if (path == NULL) {
                // No path supplied, create temp file instead
                char pattern[] = "/tmp/larod-asynctest-XXXXXX";
                fd = mkstemp(pattern);
                if (fd < 0) {
                    logError("%s:Failed mkstemp(), strerror=%s.", __func__,
                             strerror(errno));
                    goto error;
                }

                if (ftruncate(fd, (ssize_t) tempSizes[j]) < 0) {
                    logError("%s:Failed ftruncate(), strerror=%s.", __func__,
                             strerror(errno));
                    goto error;
                }

                unlink(pattern);
            } else {
                // Ordinary open from file system
                fd = open(path, O_RDONLY);

                if (fd < 0) {
                    logError("%s:Failed open(), strerror=%s.", __func__,
                             strerror(errno));
                    goto error;
                }
            }

            fileList->fdList[i * numFiles + j] = fd;
            fileList->numOpenFiles++;
        }
    }

    return true;

error:
    closeFiles(fileList);

    return false;
}

void closeFiles(OpenFilesList_t* fileList) {
    assert(fileList);

    for (size_t i = 0; i < fileList->numOpenFiles; i++) {
        if ((fileList->fdList[i] >= 0) && (close(fileList->fdList[i]) < 0)) {
            logError("%s:Failed close(fd=%d), strerror=%s.", __func__,
                     fileList->fdList[i], strerror(errno));
        }
    }

    fileList->numOpenFiles = 0;
    delete[] fileList->fdList;
    fileList->fdList = nullptr;
}

void childOpenLarodConnection(ChildProc_t* procInfo) {
    ChildProcMailbox_t* mBox = procInfo->mailbox;
    bool larodResult = true;

    for (unsigned int i = 0; (i < mBox->numCycles) && larodResult; i++) {
        mailboxPreDelay(mBox);

        // We repeat open/close if server requested several iterations
        if (procInfo->larodConn != NULL) {
            larodResult = larodDisconnect(&procInfo->larodConn, NULL);
        }

        if (larodResult) {
            larodResult = larodConnect(&procInfo->larodConn, NULL);
        }

        mailboxPostDelay(mBox);
    }

    mBox->childResult = larodResult;

    // Report result back to parent process
    childSignalMboxSem(mBox);
}

void childCloseLarodConnection(ChildProc_t* procInfo) {
    ChildProcMailbox_t* mBox = procInfo->mailbox;

    childDeleteAllModels(procInfo);

    if (mBox->numCycles > 1) {
        logError(
            "%s: Multiple iterations not supported for CMD_CLOSELARODCONN!",
            __func__);
        mBox->childResult = false;
    } else {
        mailboxPreDelay(mBox);
        mBox->childResult = larodDisconnect(&procInfo->larodConn, NULL);
        mailboxPostDelay(mBox);
    }

    // Report result back to parent process
    childSignalMboxSem(mBox);
}

void childLoadModel(ChildProc_t* procInfo) {
    ChildProcMailbox_t* mBox = procInfo->mailbox;
    bool result = true;

    if (procInfo->modelPath != NULL) {
        // Client supplied a model file on disk. Open fd:s to that file.
        if (!openFilesOrTemp(&procInfo->modelFiles, 1, mBox->numCycles,
                             procInfo->modelPath, &DUMMY_MODEL_SIZE)) {
            logError("%s: procid %u: Failed loading model file %s!", __func__,
                     mBox->workerId, procInfo->modelPath);
            result = false;
        }
    } else {
        // Client only supplied parameters for the model load.
        createEmptyFileList(&procInfo->modelFiles, 1, mBox->numCycles);
    }

    for (unsigned int i = 0; (i < mBox->numCycles) && result; i++) {
        mailboxPreDelay(mBox);

        // If server requested multiple iterations in the CMD_OPENMODEL
        // message we close the model handle in each loop. We then leave the
        // function with procInfo->models containing the last created model
        // handle.
        result = childDeleteAllModels(procInfo);

        if (result) {
            larodError* error = NULL;

            procInfo->models.push_back(larodLoadModel(
                procInfo->larodConn, procInfo->modelFiles.fdList[0],
                procInfo->chip, LAROD_ACCESS_PRIVATE, "ChildModel",
                procInfo->modelParams, &error));

            if (!procInfo->models.back()) {
                logError("%s: procid %u: model load errstring [%s].", __func__,
                         mBox->workerId, error->msg);
                result = false;
            }

            larodClearError(&error);
        }

        mailboxPostDelay(mBox);
    }

    closeFiles(&procInfo->modelFiles);
    mBox->childResult = result;

    // Report result back to parent process
    childSignalMboxSem(mBox);
}

void childLoadModelAsync(ChildProc_t* procInfo) {
    ChildProcMailbox_t* mBox = procInfo->mailbox;
    bool result = true;

    if (procInfo->modelPath != NULL) {
        // Client supplied a model file on disk. Open fd:s to that file.
        if (!openFilesOrTemp(&procInfo->modelFiles, 1, mBox->numCycles,
                             procInfo->modelPath, &DUMMY_MODEL_SIZE)) {
            logError("%s: procid %u: Failed loading model files from path %s!",
                     __func__, mBox->workerId, procInfo->modelPath);
            result = false;
        }
    } else {
        // Client only supplied parameters for the model load.
        createEmptyFileList(&procInfo->modelFiles, 1, mBox->numCycles);
    }

    procInfo->numLoadModelsQueued = mBox->numCycles;

    for (unsigned int i = 0; (i < mBox->numCycles) && result; i++) {
        mailboxPreDelay(mBox);

        result = larodLoadModelAsync(
            procInfo->larodConn, procInfo->modelFiles.fdList[i], procInfo->chip,
            LAROD_ACCESS_PRIVATE, "AsyncChildModel", procInfo->modelParams,
            childLoadModelCb, procInfo, NULL);

        mailboxPostDelay(mBox);
    }

    std::unique_lock<std::mutex> lock(procInfo->mtxDeleteModels);
    procInfo->deleteModels.wait_for(
        lock, std::chrono::seconds(60),
        [&procInfo]() { return procInfo->numLoadModelsQueued == 0; });
    childDeleteAllModels(procInfo);

    closeFiles(&procInfo->modelFiles);
    mBox->childResult = result;

    // If async request failed in this step we report back to server
    // directly...
    if (!mBox->childResult) {
        procInfo->numLoadModelsQueued = 0;
        childSignalMboxSem(mBox);
    }
}

void childLoadModelCb(larodModel* model, void* userData, larodError* error) {
    bool success = false;
    ChildProc_t* procInfo = static_cast<ChildProc_t*>(userData);
    ChildProcMailbox_t* mBox = procInfo->mailbox;

    procInfo->numLoadModelsQueued--;

    unsigned int numLoadModelsQueuedToPrint = procInfo->numLoadModelsQueued;
    logDebug(debugPrints,
             "%s: procid %u: LoadModel cb from larod, "
             "outstanding=%u.",
             __func__, mBox->workerId, numLoadModelsQueuedToPrint);

    if (error) {
        logError("%s: procid %u: larod reported error (%d): %s .", __func__,
                 mBox->workerId, error->code, error->msg);
        success = false;
    } else {
        success = true;
        procInfo->models.push_back(model);
    }

    if (!success || (procInfo->numLoadModelsQueued == 0)) {
        mBox->childResult = success;
        procInfo->deleteModels.notify_all();
        childSignalMboxSem(mBox);
    }
}

static bool childDeleteAllModels(ChildProc_t* procInfo) {
    bool deleteModelSucceeded = true;
    for (auto& model : procInfo->models) {
        if (!larodDeleteModel(procInfo->larodConn, model, NULL)) {
            logError("%s: procid %u: Failed deleting model.", __func__,
                     procInfo->mailbox->workerId);
            deleteModelSucceeded = false;
        }
        larodDestroyModel(&model);
    }
    procInfo->models.clear();
    return deleteModelSucceeded;
}

bool setTensorFds(larodTensor** tensors, size_t numTensors, unsigned int cycle,
                  ChildProc_t* procInfo, OpenFilesList_t* fdBuffer) {
    larodError* error = NULL;
    ChildProcMailbox_t* mBox = procInfo->mailbox;

    for (size_t j = 0; j < numTensors; j++) {
        if (!larodSetTensorFd(
                tensors[j], fdBuffer->fdList[cycle * numTensors + j], &error)) {
            logError("%s: procid %u: Failed updating tensor fd: %s.", __func__,
                     mBox->workerId, error->msg);
            larodClearError(&error);

            return false;
        }
    }

    return true;
}

void childRunJob(ChildProc_t* procInfo, bool useAsync) {
    larodError* error = NULL;
    ChildProcMailbox_t* mBox = procInfo->mailbox;
    larodJobRequest* jobReq = NULL;
    larodModel* model = procInfo->models.back();
    larodTensor** inputTensors = NULL;
    larodTensor** outputTensors = NULL;
    size_t* inputByteSizes = NULL;
    size_t* outputByteSizes = NULL;
    size_t numInputs = 0;
    size_t numOutputs = 0;
    bool result = true;

    inputTensors = larodCreateModelInputs(model, &numInputs, &error);
    if (!inputTensors) {
        logError("%s: procid %u: Failed creating input tensors: %s.", __func__,
                 mBox->workerId, error->msg);
        larodClearError(&error);

        goto end;
    }

    outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
    if (!outputTensors) {
        logError("%s: procid %u: Failed creating output tensors: %s.", __func__,
                 mBox->workerId, error->msg);
        larodClearError(&error);

        goto end;
    }

    inputByteSizes = larodGetModelInputByteSizes(model, NULL, &error);
    if (!inputByteSizes) {
        logError("%s: procid %u: Failed getting input byte sizes: %s.",
                 __func__, mBox->workerId, error->msg);
        larodClearError(&error);

        goto end;
    }

    outputByteSizes = larodGetModelOutputByteSizes(model, NULL, &error);
    if (!outputByteSizes) {
        logError("%s: procid %u: Failed getting output byte sizes: %s.",
                 __func__, mBox->workerId, error->msg);
        larodClearError(&error);

        goto end;
    }

    if (!openFilesOrTemp(&procInfo->sourceFiles, numInputs, mBox->numCycles,
                         NULL, inputByteSizes)) {
        logError("%s: procid %u: Failed creating input temp files!", __func__,
                 mBox->workerId);

        goto end;
    }

    if (!openFilesOrTemp(&procInfo->resultFiles, numOutputs, mBox->numCycles,
                         NULL, outputByteSizes)) {
        logError("%s: procid %u: Failed creating output temp files!", __func__,
                 mBox->workerId);
        closeFiles(&procInfo->sourceFiles);

        goto end;
    }

    if (useAsync) {
        procInfo->numJobsQueued = mBox->numCycles;
    }

    for (unsigned int i = 0; (i < mBox->numCycles) && result; i++) {
        result = setTensorFds(inputTensors, numInputs, i, procInfo,
                              &procInfo->sourceFiles);
        if (!result) {
            break;
        }

        result = setTensorFds(outputTensors, numOutputs, i, procInfo,
                              &procInfo->resultFiles);
        if (!result) {
            break;
        }

        jobReq =
            larodCreateJobRequest(model, inputTensors, numInputs, outputTensors,
                                  numOutputs, procInfo->jobParams, &error);
        if (!jobReq) {
            logError("%s: procid %u: Failed creating job request: %s.",
                     __func__, mBox->workerId, error->msg);
            larodClearError(&error);

            break;
        }

        mailboxPreDelay(mBox);

        if (useAsync) {
            result = larodRunJobAsync(procInfo->larodConn, jobReq,
                                      childJobCallback, procInfo, &error);
            if (!result) {
                logError("%s: procid %u: Failed starting async job: %s.",
                         __func__, mBox->workerId, error->msg);
            }
        } else {
            result = larodRunJob(procInfo->larodConn, jobReq, &error);
            if (!result) {
                logError("%s: procid %u: Failed running job: %s.", __func__,
                         mBox->workerId, error->msg);
            }
        }
        larodClearError(&error);

        mailboxPostDelay(mBox);

        larodDestroyJobRequest(&jobReq);
    }

    closeFiles(&procInfo->sourceFiles);
    closeFiles(&procInfo->resultFiles);

end:
    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);

    free(inputByteSizes);
    free(outputByteSizes);

    mBox->childResult = result;

    if (useAsync) {
        // Async case: If everything went well we wait for callbacks to
        // arrive and signal the semaphore once all callbacks have arrived.
        // If request failed already in in this step we report back to
        // server directly without waiting for the callbacks.
        if (!mBox->childResult) {
            procInfo->numJobsQueued = 0;
            childSignalMboxSem(mBox);
        }
    } else {
        // Synchronous api case: Signal back to server, all the jobs are
        // done at this point.
        childSignalMboxSem(mBox);
    }
}

void childJobCallback(void* userData, larodError* error) {
    ChildProc_t* procInfo = static_cast<ChildProc_t*>(userData);
    ChildProcMailbox_t* mBox = procInfo->mailbox;

    procInfo->numJobsQueued--;

    unsigned int numJobsQueuedToPrint = procInfo->numJobsQueued;
    logDebug(debugPrints,
             "%s: procid %u: Got callback from larod, "
             "outstanding=%u.",
             __func__, mBox->workerId, numJobsQueuedToPrint);

    if (error) {
        logError("%s: procid %u: larod reported error: %s.", __func__,
                 mBox->workerId, error->msg);
    }

    if (error || (procInfo->numJobsQueued == 0)) {
        mBox->childResult = (error == NULL);
        childSignalMboxSem(mBox);
    }
}
