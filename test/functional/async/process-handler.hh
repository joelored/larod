/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "larod.h"

/**
 * @brief Enum type with subproc commands.
 *
 * Commands that can be sent to child workers/processes.
 */
typedef enum {
    CMD_SHUTDOWN,        ///< Exit the main worker loop.
    CMD_OPENLARODCONN,   ///< Open a connection to Larod service.
    CMD_CLOSELARODCONN,  ///< Close an existing Larod connection.
    CMD_LOADMODEL,       ///< Load a model.
    CMD_LOADMODEL_ASYNC, ///< Load a model asynchronously.
    CMD_RUNJOB,          ///< Run job(s) on a loaded model.
    CMD_RUNJOB_ASYNC,    ///< Issue async job request(s).
    CMD_ABORT_PROCESS    ///< Exit/crash the child process.
} CommandId_t;

static const char* const commandName[] = {
    "Shutdown",         "Open Connection", "Close Connection", "Load Model",
    "Load Model Async", "Run Job",         "Run Job Async",    "Abort Process"};

/**
 * @brief A type for ProcessHandle book-keeping.
 */
typedef struct ProcessHandler ProcessHandler_t;

/**
 * @brief Create ProcessHandler and start child processes.
 *
 * Allocates/initializes a ProcessHandler_t struct and forks @p numChild
 * new processes. The processes goes into while loop and blocks on a
 * semaphore waiting for commands from the 'server'.
 *
 * @param numChild Number of new processes to fork.
 * @param enableDebugPrints Controls printing of debug info from child
 * processes.
 * @param modelPath File path for loading model. Can be NULL.
 * @param modelParams Parameters to supply in load-model calls.
 * @param jobParams Dynamic parameter to supply in runJob calls..
 * @return ProcessHandler_t pointer or NULL on error.
 */
ProcessHandler_t* forkProcesses(unsigned int numChild, bool enableDebugPrints,
                                char* modelPath, larodChip chip,
                                larodMap* modelParams, larodMap* jobParams);
/**
 * @brief Notify children and wait for termination.
 *
 * All child processes are sent the CMD_SHUTDOWN and then the main
 * process is waiting until all children have terminated.

 * @param handler The ProcessHandler book-keeping struct.
 */
void shutdownProcesses(ProcessHandler_t* handler);

/**
 * @brief Send a message to child process.
 *
 * Writes command @p cmd into the mailbox for child process @p childIndex,
 * then triggers the corresponding mailbox semaphore.
 *
 * @param handler The ProcessHandler book-keeping struct.
 * @param cmd Command id.
 * @param childIndex Numerical index of destination child process.
 * @param preDelayMs Delay to wait in child proc before executing desired
 * operation.
 * @param postDelay Delay in milliseconds to wait after the Larod call has
 * been issued.
 * @param numIterations Number of calls to be made to Larod for the desired
 * command.
 */
void sendMessage(ProcessHandler_t* handler, CommandId_t cmd,
                 unsigned int childIndex, unsigned int preDelayMs,
                 unsigned int postDelayMs, unsigned int numIterations);

#define SENDMSG_SIMPLE(handler, cmd, childIndex)                               \
    sendMessage((handler), (cmd), (childIndex), 0, 0, 1)

/**
 * @brief Waits for a child process command result.
 *
 * Waits for and verifies that integer result from the mailbox of child
 * process.
 * @p childIndex is successful - if not throws std::runtime_error.
 *
 * @param handler The ProcessHandler book-keeping struct.
 * @param childIndex Numerical index of destination child process.
 */
void waitChildResult(ProcessHandler_t* handler, unsigned int childIndex);
