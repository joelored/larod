/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once
#include <functional>
#include <gtest/gtest.h>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "larod.h"

/**
 * @brief Tuple containing information describing a tensor.
 */
using TensorTuple = std::tuple<larodTensorDataType, larodTensorLayout,
                               larodTensorDims, larodTensorPitches>;

class TensorFormatTest : public ::testing::Test {
public:
    TensorFormatTest();
    void SetUp() override;
    void TearDown() override;

    // Command line arguments.
    static std::string modelFile;
    static larodChip chip;
    static larodMap* modelParams;
    static larodMap* jobParams;
    static std::vector<std::string> inputFormats;
    static size_t numInputs;
    static std::vector<std::string> outputFormats;
    static size_t numOutputs;

    /**
     * @brief Map from data type string to corresponding larodTensorDataType
     */
    static const std::unordered_map<std::string, larodTensorDataType>
        dataTypeMap;

    /**
     * @brief Map from layout string to corresponding larodTensorLayout
     */
    static const std::unordered_map<std::string, larodTensorLayout> layoutMap;

protected:
    // Used in test cases in main.cc to label positions in a vector.
    static constexpr size_t INPUT_TUPLES = 0;
    static constexpr size_t OUTPUT_TUPLES = 1;

    int modelFd;
    larodConnection* larodConn;
    larodModel* modelPtr;

    /**
     * @brief Deduces byte size of a tensor from a @c TensorTuple.
     *
     * Extracts the total byte size of a tensor (including any padding). This
     * information is retrieved from the @c larodTensorPitches component of
     * @p tensorTuple, since the first pitch is equal to the total byte size of
     * the tensor.
     *
     * @param tensorTuple Tensor information to be used for deducing byte size
     * of the tensor.
     * @return Returns pair, where the first member is a bool, and the second is
     * a size_t. The bool reports whether or not the tensor size could be
     * determined, where true denotes that it could. The size_t is the deduced
     * size of the tensor when deducing it was possible.
     */
    static std::pair<bool, size_t>
        tensorSizeBytes(const TensorTuple& tensorTuple);

    /**
     * @brief Create @c TensorTuples from command line format strings.
     *
     * @param tensorFormat String describing a tensor.
     * @return Vector of @c TensorTuples containing enums and larodTensorDims
     * objects describing tensors.
     */
    static std::vector<TensorTuple>
        parseTensors(std::vector<std::string> tensorFormats);

    static bool compareTensor(TensorTuple tensorTuple, larodTensor* tensor);

    /**
     * @brief Alias for reference_wrapper used in tensorFormatHelper().
     */
    using TensorTuplesVector = std::reference_wrapper<std::vector<TensorTuple>>;

    /**
     * @brief Run a job and report how result compares to expectations.
     *
     * Sets up and runs a job using tensors created from @c TensorTuples.
     * The tensors can be modified by providing an appropriate definition of @p
     * tweakFormats.
     *
     * Additionally @p tweakFormats takes an input bool reference. This can be
     * used by the lambda function to determine if a particular type of
     * modification to a tensor should be skipped. This is relevant if the input
     * format given at command line is such that the tensor should not be tested
     * in a some certain way. E.g. if the input tensor of a model has an
     * unspecified data type, changing the data type should not be tested.
     *
     * The value of @p expectedOutcome should be the value that is
     * expected to be reported by @c larodRunJob() in the current test
     * case.
     *
     * @param tweakFormats Lambda function that should be used to modify tensors
     * according to what is supposed to be tested.
     * @param expectedOutcome Bool that specifies what the test expects @c
     * larodRunJob() to return.
     * @return Returns success or failure of the test.
     */
    bool tensorFormatHelper(
        std::function<void(std::vector<TensorTuplesVector>&, bool&)>
            tweakFormats,
        const bool expectedOutcome);

    /**
     * @brief Create a temporary file for a tensor.
     *
     * @param size Size in bytes of file to be created.
     * @return Returns file descriptor of the temporary file.
     */
    int createTempFile(size_t size);

    void tupleToTensor(TensorTuple formatTuple, larodTensor* tensor);
};
