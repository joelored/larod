/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "larod.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_NUM_FORMATS 30

typedef struct args_t {
    char* modelFile;
    larodChip chip;
    larodMap* modelParams;
    larodMap* jobParams;
    bool hasModelParams;
    char* inputFormats[MAX_NUM_FORMATS];
    size_t numInputs;
    char* outputFormats[MAX_NUM_FORMATS];
    size_t numOutputs;
    bool debug;
} args_t;

bool parseArgs(int argc, char** argv, args_t* args);

void destroyArgs(args_t* args);

#ifdef __cplusplus
}
#endif
