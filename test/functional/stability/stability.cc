/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <gtest/gtest.h>
#include <mutex>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <thread>
#include <unistd.h>

#include "larod.h"

using namespace std::chrono_literals;

class CrashFuncTest : public ::testing::Test {
protected:
    std::mutex mtxCallbackCounter;
    std::condition_variable condVarNumCallbacks;
    std::atomic<unsigned> receivedCallbacks;
    std::atomic<bool> callbackErrFlag;

public:
    CrashFuncTest() : receivedCallbacks(0), callbackErrFlag(false) {}

    void incNumCallbacks(bool errOccured) {
        if (errOccured) {
            callbackErrFlag = true;
        }
        receivedCallbacks++;
        condVarNumCallbacks.notify_one();
    }

protected:
    static int createTempFile(size_t size) {
        char fileName[] = "/tmp/larod-crashtest-XXXXXX";
        int fd = mkstemp(fileName);
        if (fd >= 0) {
            // FIXME: Unsafe cast from size_t to off_t.
            int ret = ftruncate(fd, static_cast<off_t>(size));
            if (ret) {
                close(fd);
                return ret;
            }
            std::remove(fileName);
        }
        return fd;
    }

    static void jobCallback(void* userData, larodError* error) {
        CrashFuncTest* self = reinterpret_cast<CrashFuncTest*>(userData);

        // We expect to recieve LAROD_ERROR_NONE while the Larod service is
        // still alive, then LAROD_ERROR_CONNECTION when the Larod service gets
        // killed.
        // Just before the service shuts down larod might read only 0 bytes
        // (EOF) from input tensor file descriptors, resulting in LAROD_ERROR_FD
        // being returned. Thus we accept that error as well.
        bool unexpectedError = false;
        if (error) {
            unexpectedError = (error->code != LAROD_ERROR_NONE) &&
                              (error->code != LAROD_ERROR_CONNECTION) &&
                              (error->code != LAROD_ERROR_FD);
        }
        self->incNumCallbacks(unexpectedError);
    }
};

// Verify that the service deallocates resources after a client
// has unexpectedly terminated.
TEST_F(CrashFuncTest, ClientTerminated) {
    larodError* error = nullptr;
    larodConnection* conn = nullptr;

    // Connect to the service.
    bool ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Check the initial number of sessions.
    uint64_t initialNbrOfSessions;
    ret = larodGetNumSessions(conn, &initialNbrOfSessions, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(initialNbrOfSessions, 0);
    ASSERT_EQ(error, nullptr);

    pid_t child = fork();
    ASSERT_GE(child, 0);
    if (child) {
        // Wait for the child process to terminate and check status.
        int wStatus;
        wait(&wStatus);
        ASSERT_TRUE(WIFEXITED(wStatus));
        ASSERT_EQ(WEXITSTATUS(wStatus), 0);

        // Somewhat ad-hoc sleep time - we need to make sure the
        // service has time to clean up after a disconnected client.
        sleep(10);

        // Check that the current number of sessions equals the
        // initial number - this proves that the "dangling" session
        // created by the child process has been destroyed.
        uint64_t finalNbrOfSessions;
        ret = larodGetNumSessions(conn, &finalNbrOfSessions, &error);
        ASSERT_TRUE(ret);
        EXPECT_EQ(initialNbrOfSessions, finalNbrOfSessions);
        ASSERT_EQ(error, nullptr);

        // Make sure we can close our connection.
        ret = larodDisconnect(&conn, &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(conn, nullptr);
        ASSERT_EQ(error, nullptr);
    } else {
        // Connect to the service, but exit before disconnecting.
        ret = larodConnect(&conn, &error);
        ASSERT_TRUE(ret);
        ASSERT_NE(conn, nullptr);
        ASSERT_EQ(error, nullptr);
        // Explicitly terminate the child process to avoid that it
        // proceeds to execute further testcases that might be
        // added in future patches.
        exit(0);
    }
}

// Trigger a number of async jobs on debug chip and kill the Larod
// service concurrently. Verify that API calls return sane values and that
// correct number of callbacks are received by the application.
TEST_F(CrashFuncTest, LarodServiceCrashed) {
    bool ret;
    larodError* error = nullptr;
    larodConnection* conn = nullptr;
    const size_t MODEL_SIZE = 32 * 1024; // Must be > 0.
    const unsigned NUM_CRASH_JOB_REQUESTS = 60;

    // Connect to the service.
    ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Check the initial number of sessions
    uint64_t initialNbrOfSessions;
    ret = larodGetNumSessions(conn, &initialNbrOfSessions, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    int modelFd = createTempFile(MODEL_SIZE);
    ASSERT_GE(modelFd, 0);

    // Make sure we are using the debug chip to have known behavior for the
    // async calls.
    larodModel* model =
        larodLoadModel(conn, modelFd, LAROD_CHIP_DEBUG, LAROD_ACCESS_PRIVATE,
                       "crashtest", nullptr, &error);
    ASSERT_NE(model, nullptr);
    ASSERT_EQ(error, nullptr);

    size_t* inputByteSize = larodGetModelInputByteSizes(model, nullptr, &error);
    ASSERT_NE(inputByteSize, nullptr);
    ASSERT_EQ(error, nullptr);

    size_t* outputByteSize =
        larodGetModelOutputByteSizes(model, nullptr, &error);
    ASSERT_NE(outputByteSize, nullptr);
    ASSERT_EQ(error, nullptr);

    int inputFd = createTempFile(*inputByteSize);
    ASSERT_GE(inputFd, 0);

    int outputFd = createTempFile(*outputByteSize);
    ASSERT_GE(outputFd, 0);

    larodTensor** inputTensors = larodCreateTensors(1, &error);
    ASSERT_NE(inputTensors, nullptr);
    ASSERT_EQ(error, nullptr);

    larodTensor** outputTensors = larodCreateTensors(1, &error);
    ASSERT_NE(outputTensors, nullptr);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorFd(inputTensors[0], inputFd, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    ret = larodSetTensorFd(outputTensors[0], outputFd, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(error, nullptr);

    larodJobRequest* jobReq = larodCreateJobRequest(
        model, inputTensors, 1, outputTensors, 1, nullptr, &error);
    ASSERT_NE(jobReq, nullptr);
    ASSERT_EQ(error, nullptr);

    // Fire away a number of requests.
    for (unsigned i = 0; i < NUM_CRASH_JOB_REQUESTS; i++) {
        ret = larodRunJobAsync(conn, jobReq, jobCallback, this, &error);
        ASSERT_TRUE(ret);
        ASSERT_EQ(error, nullptr);
    }

    // Allow the Larod process some execution time to start processing the
    // inference requests.
    std::this_thread::sleep_for(40ms);

    // Kill the Larod service. We expect systemd to restart it.
    ASSERT_EQ(system("kill $(pidof larod)"), 0);

    // Allow systemd some time to restart the killed process.
    std::this_thread::sleep_for(500ms);

    // Wait until all the job requests has been delivered. Timeout is set
    // arbitrarily to 3.5 seconds, effectively the callbacks are delivered in
    // milliseconds.
    std::unique_lock<std::mutex> lock(mtxCallbackCounter);
    condVarNumCallbacks.wait_for(lock, 3500ms, [this] {
        return receivedCallbacks >= NUM_CRASH_JOB_REQUESTS;
    });
    lock.unlock();

    ASSERT_EQ(receivedCallbacks, NUM_CRASH_JOB_REQUESTS);
    ASSERT_EQ(callbackErrFlag, false);

    // We expect running job call to fail since the DBUS peer-to-peer socket was
    // lost when Larod service was killed.
    ret = larodRunJobAsync(conn, jobReq, jobCallback, this, &error);
    ASSERT_FALSE(ret);
    ASSERT_NE(error, nullptr);
    larodClearError(&error);

    // Disconnect should still clean up successfully.
    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    // Wait some time to allow systemd to restart the service. Then verify that
    // we can connect to restarted Larod.
    bool systemdRestarted = false;
    unsigned int retryCounter = 0;
    const unsigned int MAX_RETRIES = 5;
    if (retryCounter++ < MAX_RETRIES && !systemdRestarted) {
        std::this_thread::sleep_for(100ms);
        int shellResult = system("pidof larod > /dev/null");
        systemdRestarted = (shellResult == 0);
    }
    ASSERT_LT(retryCounter, MAX_RETRIES);

    ret = larodConnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_NE(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    uint64_t nbrOfSessions;
    ret = larodGetNumSessions(conn, &nbrOfSessions, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(nbrOfSessions, initialNbrOfSessions);
    ASSERT_EQ(error, nullptr);

    ret = larodDisconnect(&conn, &error);
    ASSERT_TRUE(ret);
    ASSERT_EQ(conn, nullptr);
    ASSERT_EQ(error, nullptr);

    larodDestroyModel(&model);
    larodDestroyTensors(&inputTensors, 1);
    larodDestroyTensors(&outputTensors, 1);
    larodDestroyJobRequest(&jobReq);
    free(inputByteSize);
    free(outputByteSize);

    close(inputFd);
    close(outputFd);
    close(modelFd);
}
