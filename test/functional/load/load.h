/**
 * Copyright 2019 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include "argparse.h"

#ifdef __cplusplus
extern "C" {
#endif

int doLoad(args_t* args);

#ifdef __cplusplus
}
#endif
