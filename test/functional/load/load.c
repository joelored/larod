/**
 * Copyright 2018 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <libgen.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "argparse.h"
#include "larod.h"
#include "log.h"

#define NSEC_PER_SEC (1e9L)
#define NSEC_PER_MSEC (1e6L)

static void signalHandler(int sig);

bool loadModel(larodConnection* conn, char* modelFile, const larodChip chip,
               const larodAccess access, const char* modelName,
               larodModel** model, larodMap* params);
bool runJob(larodConnection* conn, const larodJobRequest* const jobReq,
            unsigned long long rounds, FILE* csvFile);
bool runJobAsync(larodConnection* conn, const larodJobRequest* const jobReq,
                 const unsigned long long rounds, const unsigned int sleepTime,
                 FILE* csvFile);
void jobCallback(void* data, larodError* error);
bool logTimeToCSV(FILE* csvFile, unsigned long long jobTimeUs);

bool debug = false;
volatile sig_atomic_t isRunning = false;
static unsigned long long totalJobTimeNs = 0;
static unsigned long long nbrCallbacksDone = 0;

// This struct contains an approximate time stamp for when the currently
// executing job started. This variable is updated, for each job request, in one
// of the two following ways:
//   1. From inside runJobAsync(). This occurs if the previous job has finished
//      before the client has woken up to send the next job request.
//   2. From the previous job's callback function, jobCallback(). This occurs if
//      the client has already sent the request for the next job. This is based
//      on the assumption that the next job will start immediately after the one
//      before finished.
static struct timespec nextJobStartTime;

pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condVar = PTHREAD_COND_INITIALIZER;

static void signalHandler(int sig) {
    static volatile sig_atomic_t signalHandled = 0;
    if (signalHandled) {
        signal(sig, SIG_DFL);
        raise(sig);

        return;
    }

    signalHandled = 1;

    isRunning = false;
}

// Call with rounds = 0 to run function until SIGINT
bool doLoad(args_t* args) {
    bool ret = false;
    larodError* error = NULL;
    larodConnection* conn = NULL;
    larodModel* model = NULL;
    larodJobRequest* jobReq = NULL;
    larodTensor** inputTensors = NULL;
    larodTensor** outputTensors = NULL;
    size_t* inputByteSizes = NULL;
    size_t numInputs = 0;
    size_t numOutputs = 0;
    FILE** fpInputs = NULL;
    FILE** fpOutputs = NULL;
    size_t allocedOutputFiles = 0;
    FILE* csvFile = NULL;

    debug = args->debug;

    // Connect
    logInfo("Connecting to larod...");
    if (!larodConnect(&conn, &error)) {
        logError("Could not connect to larod (%d): %s", error->code,
                 error->msg);
        larodClearError(&error);
        goto end;
    }
    logInfo("Connected");

    // Load model
    if (!loadModel(conn, args->modelFile, args->chip, args->modelAccess,
                   args->modelName, &model, args->modelParams)) {
        goto end;
    }

    inputTensors = larodCreateModelInputs(model, &numInputs, &error);
    if (!inputTensors) {
        logError("Could not create input tensor (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    if (args->numInputs != numInputs) {
        logError("Provided %zu inputs but model requires %zu", args->numInputs,
                 numInputs);

        goto end;
    }

    outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
    if (!outputTensors) {
        logError("Could not create output tensor (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    if (args->numOutputs < numOutputs) {
        // Files were not specified for all output tensors. Use the first input
        // file's name with suffix ".outX" for integers X = numUserOutputs,
        // numUserOutputs + 1,...
        char** newOutputFiles =
            realloc(args->outputFiles, numOutputs * sizeof(char*));
        if (!newOutputFiles) {
            logError("Could not allocate memory for output file names array");

            goto end;
        }
        args->outputFiles = newOutputFiles;

        const size_t MAX_FILE_SUFFIX_LEN = 32;
        char fileSuffix[MAX_FILE_SUFFIX_LEN];
        for (size_t i = args->numOutputs; i < numOutputs; ++i) {
            int len = snprintf(fileSuffix, MAX_FILE_SUFFIX_LEN, ".out%zu", i);
            if (len < 0) {
                logError("Could not encode file suffix string");

                goto end;
            } else if ((size_t) len >= MAX_FILE_SUFFIX_LEN) {
                logError("File suffix buffer is too small (need %d)", len + 1);

                goto end;
            }

            const char* inputBaseName = basename(args->inputFiles[0]);

            args->outputFiles[i] =
                malloc(1 + strlen(inputBaseName) + strlen(fileSuffix));
            if (!args->outputFiles[i]) {
                logError("Could not allocate memory for output file name");

                goto end;
            }
            ++allocedOutputFiles;

            strcpy(args->outputFiles[i], inputBaseName);
            strcat(args->outputFiles[i], fileSuffix);
        }
    } else if (args->numOutputs != numOutputs) {
        logError("Provided %zu outputs but model requires %zu",
                 args->numOutputs, numOutputs);

        goto end;
    }

    fpInputs = calloc(numInputs, sizeof(FILE*));
    if (!fpInputs) {
        logError("Could not allocate memory for input files array");

        goto end;
    }
    fpOutputs = calloc(numOutputs, sizeof(FILE*));
    if (!fpOutputs) {
        logError("Could not allocate memory for output files array");

        goto end;
    }

    inputByteSizes = larodGetModelInputByteSizes(model, NULL, &error);
    if (!inputByteSizes) {
        logError("Could not allocate get input byte sizes (%d): %s",
                 error->code, error->msg);

        goto end;
    }

    for (size_t i = 0; i < numInputs; ++i) {
        assert(args->inputFiles[i]);

        fpInputs[i] = fopen(args->inputFiles[i], "rb");
        if (!fpInputs[i]) {
            logError("Could not open input file %s: %s", args->inputFiles[i],
                     strerror(errno));

            goto end;
        }

        const int inFd = fileno(fpInputs[i]);
        if (inFd < 0) {
            logError("Could not get file descriptor for input file: %s",
                     strerror(errno));

            goto end;
        }

        if (!larodSetTensorFd(inputTensors[i], inFd, &error)) {
            logError("Could not set input tensor fd (%d): %s", error->code,
                     error->msg);

            goto end;
        }

        // Set size of fd in input tensor. If left unspecified, the size of a
        // input tensor's fd will be retrieved in larodRunJobAsync(). This
        // is problematic if one quickly queues up job requests containing
        // input tensors with the same fd; since then the lib and service can
        // perform a lseek/read at the same time.  For debug chip, we set it to
        // what the model expects so that one can give it any file as input.
        size_t fileSize = 0;
        if (args->chip == LAROD_CHIP_DEBUG) {
            fileSize = inputByteSizes[i];
        } else {
            off_t filePos = lseek(inFd, 0, SEEK_END);
            if (filePos < 0) {
                logError("Could not get file size of fd: %s", strerror(errno));

                goto end;
            }

            fileSize = (size_t) filePos;
        }
        if (!larodSetTensorFdSize(inputTensors[i], fileSize, &error)) {
            logError("Could not set input tensor fd size (%d): %s", error->code,
                     error->msg);

            goto end;
        }
    }

    for (size_t i = 0; i < numOutputs; ++i) {
        assert(args->outputFiles[i]);

        fpOutputs[i] = fopen(args->outputFiles[i], "wb");
        if (!fpOutputs[i]) {
            logError("Could not open output file %s: %s", args->outputFiles[i],
                     strerror(errno));

            goto end;
        }

        const int outFd = fileno(fpOutputs[i]);
        if (outFd < 0) {
            logError("Could not get file descriptor for output file: %s",
                     strerror(errno));

            goto end;
        }

        if (!larodSetTensorFd(outputTensors[i], outFd, &error)) {
            logError("Could not set output tensor fd (%d): %s", error->code,
                     error->msg);

            goto end;
        }
    }

    logDebug(debug, "Will write output to:");
    for (size_t i = 0; i < numOutputs; ++i) {
        logDebug(debug, "\t%s", args->outputFiles[i]);
    }

    jobReq =
        larodCreateJobRequest(model, inputTensors, numInputs, outputTensors,
                              numOutputs, args->jobParams, &error);
    if (!jobReq) {
        logError("Could not create job request (%d): %s", error->code,
                 error->msg);

        goto end;
    }

    signal(SIGINT, signalHandler);
    isRunning = true;

    // There's typically extra time needed to run the first job so we do a
    // warmup so that time measurement isn't affected. Furthermore this protects
    // against fd overflow in larod for backends that are VERY slow on the first
    // job.
    logInfo("Running first job as warm up...");
    if (!larodRunJob(conn, jobReq, &error)) {
        logError("Could not run first job (%d): %s", error->code, error->msg);
        goto end;
    }

    if (args->csvPath) {
        csvFile = fopen(args->csvPath, "wb");
        if (!csvFile) {
            logError("Could not open CSV output file %s: %s", args->csvPath,
                     strerror(errno));

            goto end;
        }

        logInfo("Will write job times to file \"%s\"", args->csvPath);
    }

    struct timespec startTime;
    if (args->timeJob) {
        clock_gettime(CLOCK_MONOTONIC, &startTime);
    }

    // Run job
    if (args->async) {
        logInfo("Running continuous job asynchronously...");
        ret = runJobAsync(conn, jobReq, args->rounds, args->sleepTime, csvFile);
    } else {
        logInfo("Running continuous job...");
        ret = runJob(conn, jobReq, args->rounds, csvFile);
    }
    if (!ret) {
        goto end;
    }

    bool measureTime = args->timeJob || args->jobTimeLimit < ULONG_MAX;
    if (measureTime) {
        double meanJobTimeMs = (double) totalJobTimeNs /
                               (double) NSEC_PER_MSEC / (double) args->rounds;

        if (args->timeJob) {
            logInfo("Mean execution time for job: %.2lf ms", meanJobTimeMs);
        }

        double timeLimit = (double) args->jobTimeLimit;
        if (meanJobTimeMs > timeLimit) {
            logError("Job time limit exceeded: Took %.2lf ms but needs to be "
                     "less than or equal to %.lf ms",
                     meanJobTimeMs, timeLimit);
            ret = false;
            goto end;
        }
    }

    logInfo("Test terminated");

end:
    // Close files
    if (fpInputs) {
        for (size_t i = 0; i < numInputs; ++i) {
            if (fpInputs[i] && fclose(fpInputs[i])) {
                logWarning("Could not close file %s", args->inputFiles[i]);
            }
        }
        free(fpInputs);
    }

    if (fpOutputs) {
        for (size_t i = 0; i < numOutputs; ++i) {
            if (fpOutputs[i] && fclose(fpOutputs[i])) {
                logWarning("Could not close file %s", args->outputFiles[i]);
            }
        }
        free(fpOutputs);
    }

    for (size_t i = 0; i < allocedOutputFiles; ++i) {
        free(args->outputFiles[args->numOutputs + i]);
    }

    if (csvFile) {
        fclose(csvFile);
    }

    // Delete model.
    if (model && !larodDeleteModel(conn, model, &error)) {
        logError("Could not delete model (%d): %s", error->code, error->msg);
    }

    // Disconnect
    if (conn && !larodDisconnect(&conn, &error)) {
        logError("Could not disconnect (%d): %s", error->code, error->msg);
    }

    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);
    larodDestroyJobRequest(&jobReq);
    larodDestroyModel(&model);
    larodClearError(&error);
    free(inputByteSizes);

    return ret;
}

bool loadModel(larodConnection* conn, char* modelFile, const larodChip chip,
               const larodAccess access, const char* modelName,
               larodModel** model, larodMap* params) {
    logDebug(debug, "Loading model...");
    larodError* error = NULL;

    // File descriptor is negative if no model file is specified.
    int fd = -1;
    FILE* fpModel = NULL;

    if (modelFile) {
        fpModel = fopen(modelFile, "rb");
        if (!fpModel) {
            logError("Could not open model file %s: %s", modelFile,
                     strerror(errno));

            return false;
        }

        fd = fileno(fpModel);
        if (fd < 0) {
            logError("Could not get file descriptor for model file: %s",
                     strerror(errno));
            fclose(fpModel);

            return false;
        }

        if (!modelName) {
            modelName = basename(modelFile);
        }
    } else if (!modelName) {
        modelName = "params-model";
    }

    *model = larodLoadModel(conn, fd, chip, access, modelName, params, &error);
    if (!*model) {
        logError("When loading model (%d): %s", error->code, error->msg);
        larodClearError(&error);

        return false;
    }

    if (modelFile && fclose(fpModel)) {
        logWarning("Could not close file %s", modelFile);
    }

    logInfo("Model %s loaded", modelName);

    return true;
}

bool runJob(larodConnection* conn, const larodJobRequest* jobReq,
            unsigned long long rounds, FILE* csvFile) {
    logDebug(debug, "Running job...");

    bool ret = true;

    while (isRunning) {
        struct timespec jobStartTime;
        clock_gettime(CLOCK_MONOTONIC, &jobStartTime);

        larodError* error = NULL;
        if (!larodRunJob(conn, jobReq, &error)) {
            logError("When running job (%d): %s", error->code, error->msg);
            larodClearError(&error);

            ret = false;
            break;
        }

        struct timespec jobEndTime;
        clock_gettime(CLOCK_MONOTONIC, &jobEndTime);

        // Compute the time taken for this job.
        unsigned long long jobTimeNs =
            (unsigned long long) (jobEndTime.tv_sec - jobStartTime.tv_sec) *
            (unsigned long long) NSEC_PER_SEC;
        jobTimeNs +=
            (unsigned long long) (jobEndTime.tv_nsec - jobStartTime.tv_nsec);

        totalJobTimeNs += jobTimeNs;

        if (csvFile) {
            ret = logTimeToCSV(csvFile, jobTimeNs / 1000);
            if (!ret) {
                break;
            }
        }

        logDebug(debug, "Output written (%llu us)", jobTimeNs / 1000);

        // Decrement rounds and update isRunning if rounds != 0.
        isRunning = rounds ? isRunning && (--rounds > 0) : isRunning;
    }

    return ret;
}

typedef struct {
    unsigned long long reqNbr;
    unsigned long long rounds;
    bool* hasRecvAll;
    bool* hasFailed;
    FILE* csvFile;
} CallbackData;

bool runJobAsync(larodConnection* conn, const larodJobRequest* const jobReq,
                 const unsigned long long rounds, const unsigned int sleepTime,
                 FILE* csvFile) {
    larodError* error = NULL;
    bool hasRecvAll = false;
    bool hasFailed = false;
    CallbackData* callbackData;
    bool ret = true;

    for (unsigned long long i = 1; isRunning; ++i) {
        callbackData = malloc(sizeof(CallbackData));
        if (!callbackData) {
            isRunning = false;
            ret = false;
            break;
        }

        callbackData->reqNbr = i;
        callbackData->rounds = rounds;
        callbackData->hasRecvAll = &hasRecvAll;
        callbackData->hasFailed = &hasFailed;
        callbackData->csvFile = csvFile;

        // If all outgoing requests are completed, set nextInfStartTime here
        // because time has passed since the last callback updated it.
        pthread_mutex_lock(&mtx);
        if (nbrCallbacksDone == i - 1) {
            clock_gettime(CLOCK_MONOTONIC, &nextJobStartTime);
        }
        pthread_mutex_unlock(&mtx);

        ret = larodRunJobAsync(conn, jobReq, jobCallback, callbackData, &error);
        if (!ret) {
            logError("Could not run job: (%d): %s", error->code, error->msg);
            larodClearError(&error);

            isRunning = false;
            break;
        }

        logDebug(debug, "Sent job request %llu", i);

        if (rounds && i == rounds) {
            break;
        }

        if (sleepTime) {
            const unsigned int SLEEP_SEC = sleepTime / 1000;
            const unsigned int SLEEP_MSEC = sleepTime % 1000;
            if (SLEEP_SEC) {
                // Use sleep() for the seconds.
                sleep(SLEEP_SEC);
            }

            if (SLEEP_MSEC) {
                // Use usleep() for the remaining milliseconds (usleep()'s
                // domain is [0, 1000000]).
                usleep(SLEEP_MSEC * 1000);
            }
        }
    }

    if (!isRunning) {
        goto end;
    }

    logInfo("Waiting for callbacks...");
    pthread_mutex_lock(&mtx);
    while (isRunning && !hasRecvAll) {
        pthread_cond_wait(&condVar, &mtx);
    }
    pthread_mutex_unlock(&mtx);

end:
    pthread_cond_destroy(&condVar);
    pthread_mutex_destroy(&mtx);

    return !hasFailed && ret;
}

void jobCallback(void* data, larodError* error) {
    assert(data);
    CallbackData* callbackData = (CallbackData*) data;

    struct timespec jobEndTime;
    clock_gettime(CLOCK_MONOTONIC, &jobEndTime);

    // Compute the time taken for this job.
    unsigned long long jobTimeNs =
        (unsigned long long) (jobEndTime.tv_sec - nextJobStartTime.tv_sec) *
        (unsigned long long) NSEC_PER_SEC;
    jobTimeNs +=
        (unsigned long long) (jobEndTime.tv_nsec - nextJobStartTime.tv_nsec);

    pthread_mutex_lock(&mtx);
    totalJobTimeNs += jobTimeNs;
    // Update nextJobStartTime for the next executing callback function.
    memcpy(&nextJobStartTime, &jobEndTime, sizeof(struct timespec));

    if (callbackData->csvFile) {
        logTimeToCSV(callbackData->csvFile, jobTimeNs / 1000);
    }

    ++nbrCallbacksDone;
    pthread_mutex_unlock(&mtx);

    if (error) {
        pthread_mutex_lock(&mtx);
        *callbackData->hasFailed = true;
        pthread_mutex_unlock(&mtx);

        logError("Callback %llu received: errCode = %d, errMsg = \"%s\" "
                 "(total %llu recieved)",
                 callbackData->reqNbr, error->code, error->msg,
                 nbrCallbacksDone);
    } else {
        logDebug(debug, "Callback %llu received: (total %llu received)",
                 callbackData->reqNbr, nbrCallbacksDone);
    }

    if (isRunning && nbrCallbacksDone >= callbackData->rounds) {
        pthread_mutex_lock(&mtx);
        *callbackData->hasRecvAll = true;
        pthread_mutex_unlock(&mtx);

        pthread_cond_broadcast(&condVar);
    }

    free(callbackData);
}

bool logTimeToCSV(FILE* csvFile, unsigned long long infTimeUs) {
    assert(csvFile);

    char csvLine[64];
    int lineLen = snprintf(csvLine, sizeof(csvLine) - 1, "%llu\n", infTimeUs);
    if (lineLen >= 0 && (size_t) lineLen < sizeof(csvLine)) {
        size_t writeLen = fwrite(csvLine, (size_t) lineLen, 1, csvFile);
        if (writeLen < 1) {
            logError("Could not write CSV line: %s", strerror(errno));
            return false;
        }
    } else {
        logError("Could not format CSV line");
        return false;
    }

    return true;
}
