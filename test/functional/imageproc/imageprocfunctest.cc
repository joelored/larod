/**
 * Copyright 2020 Axis Communications
 * SPDX-License-Identifier: Apache-2.0
 */

#include "imageprocfunctest.hh"

#include <algorithm>
#include <fcntl.h>
#include <fstream>
#include <gtest/gtest.h>
#include <sys/mman.h>

#include "testutils.hh"

using namespace std;
using namespace larod::test;

/**
 * @brief Creates a temp file inside RAM and writes a buffer of bytes into it.
 *
 * @param buffer The buffer to write into the file.
 * @param bufferLen The number of bytes to write.
 * @return File descriptor of the created file.
 */
static int writeBufferToTempFile(uint8_t* buffer, size_t bufferLen);

/**
 * @brief Creates a temp file inside RAM and sets a pointer to where it starts.
 *
 * This function will use @c mmap to map the start of the file to @c buffer.
 * The buffer is read-only; writing to the file can be done by using the file
 * descriptor returned from this function.
 *
 * @param buffer Pointer that will be set to where the file starts.
 * @param fileLen The desired length in bytes of the file.
 * @return File descriptor of the created file.
 */
static int newTempFileBuffer(uint8_t** buffer, size_t fileLen);

/**
 * @brief Retrieves image meta data from larodMap.
 *
 * Returns image format, width, height, pitch and buffer size from a larodMap
 * and throws an exception if an error occurs.
 *
 * @param larodMap A map containing static parameters.
 * @param name Set to "input" or "output".
 * @param error A larodError that will be cleared in case of errors.
 * @return The meta data <"format", "width", "height", "pitch", "buffer size">.
 */
static tuple<string, size_t, size_t, size_t, size_t>
    getImgMetaData(larodMap* map, const string& name, larodError** error);

ImageProcFuncTest::ImageProcFuncTest() {
}

ImageProcFuncTest::~ImageProcFuncTest() {
    larodDestroyModel(&model);
    larodDestroyTensors(&inputTensors, numInputs);
    larodDestroyTensors(&outputTensors, numOutputs);
    larodDestroyJobRequest(&jobReq);
    larodClearError(&error);

    if (!larodDisconnect(&conn, nullptr)) {
        gtestDebugPrint("Failed closing larod connection");
    }

    if (inFd >= 0) {
        close(inFd);
    }
    if (outFd >= 0) {
        close(outFd);
    }

    // Unmap outputBuffer since it is not used anymore.
    if (outputBuffer) {
        int iRet = munmap(outputBuffer, outputBufferSize);
        if (iRet) {
            gtestDebugPrint("munmap() failed");
        }
    }

    // Call base GoogleTest Test::TearDown() for framework cleanup.
    Test::TearDown();
}

void ImageProcFuncTest::setupLarod() {
    if (!larodConnect(&conn, &error) || !conn) {
        throw runtime_error("Could not open larod connection: " +
                            getErrMsgAndClear(&error));
    }

    model = larodLoadModel(conn, -1, chip, LAROD_ACCESS_PRIVATE,
                           "preprocessing-model", modelParams, &error);
    if (!model) {
        throw runtime_error("Could not load model: " +
                            getErrMsgAndClear(&error));
    }

    inputTensors = larodCreateModelInputs(model, &numInputs, &error);
    gtestDebugPrint("number of inputs: " + to_string(numInputs));
    if (!inputTensors) {
        throw runtime_error("Could not create input tensors: " +
                            getErrMsgAndClear(&error));
    }

    outputTensors = larodCreateModelOutputs(model, &numOutputs, &error);
    gtestDebugPrint("number of outputs: " + to_string(numOutputs));
    if (!outputTensors) {
        throw runtime_error("Could not create output tensors: " +
                            getErrMsgAndClear(&error));
    }
}

static tuple<string, size_t, size_t, size_t, size_t>
    getImgMetaData(larodMap* map, const string& name, larodError** error) {
    // Get width, height, format & pitch from static larodMap.
    const int64_t* dims =
        larodMapGetIntArr2(map, ("image." + name + ".size").c_str(), error);
    if (!dims) {
        throw runtime_error("Could not get " + name +
                            " size from larodMap: " + getErrMsgAndClear(error));
    }
    size_t width = castToSizeT(dims[0], name + " width");

    size_t height = castToSizeT(dims[1], name + " height");
    const char* format =
        larodMapGetStr(map, ("image." + name + ".format").c_str(), error);
    if (!format) {
        throw runtime_error(
            "Could not get " + name +
            " format from larodMap: " + getErrMsgAndClear(error));
    }

    size_t pitch = 0;
    int64_t pitchInt64 = 0;
    if (larodMapGetInt(map, ("image." + name + ".row-pitch").c_str(),
                       &pitchInt64, error)) {
        pitch = castToSizeT(pitchInt64, name + " pitch");
    } else {
        pitch = calcPitch(width, format);
    }

    size_t bufferSize = calcBufferSize(format, height, pitch);

    return tuple<string, size_t, size_t, size_t, size_t>{format, width, height,
                                                         pitch, bufferSize};
}

void ImageProcFuncTest::setupInputOutputMetaData() {
    tie(inputFormat, inputWidth, inputHeight, inputPitch, inputBufferSize) =
        getImgMetaData(modelParams, "input", &error);
    tie(outputFormat, outputWidth, outputHeight, outputPitch,
        outputBufferSize) = getImgMetaData(modelParams, "output", &error);
}

void ImageProcFuncTest::setupTensors() {
    // Set inFd from a temporary file containing a generated pattern or from
    // the given input file.
    if (!ImageProcFuncTest::inputFilePath) {
        patternBuffer = generateYcbcrTestPattern(inputWidth, inputPitch,
                                                 inputHeight, randomSeed);
        inFd = writeBufferToTempFile(patternBuffer.data(), inputBufferSize);
    } else {
        inFd = open(inputFilePath, O_RDONLY);
        if (inFd < 0) {
            throw runtime_error("Could not open input file: " +
                                string(strerror(errno)));
        }
    }

    if (!larodSetTensorFd(inputTensors[0], inFd, &error)) {
        throw runtime_error("Could not set input tensor fd: " +
                            getErrMsgAndClear(&error));
    }

    outFd = newTempFileBuffer(&outputBuffer, outputBufferSize);
    if (!larodSetTensorFd(outputTensors[0], outFd, &error)) {
        throw runtime_error("Could not set output tensor fd: " +
                            getErrMsgAndClear(&error));
    }
}

void ImageProcFuncTest::createJobRequestAndRun() {
    jobReq =
        larodCreateJobRequest(model, inputTensors, numInputs, outputTensors,
                              numOutputs, jobParams, &error);
    if (!jobReq) {
        throw runtime_error("Could not create job request: " +
                            getErrMsgAndClear(&error));
    }

    if (!larodRunJob(conn, jobReq, &error)) {
        throw runtime_error("Could not run job request: " +
                            getErrMsgAndClear(&error));
    }
}

vector<uint8_t> ImageProcFuncTest::getVerificationBuffer() {
    if (verificationFilePath) {
        ifstream instream(verificationFilePath, ios::in | ios::binary);
        if (!instream.good()) {
            throw runtime_error("Could not open verification file");
        }
        return vector<uint8_t>(istreambuf_iterator<char>(instream),
                               istreambuf_iterator<char>());
    } else {
        if (inputFormat != "nv12") {
            throw runtime_error("Generated input is not supported for " +
                                string(inputFormat) + " format");
        }
        if (inputWidth != outputWidth || inputHeight != outputHeight) {
            throw runtime_error(
                "Generated input is only supported for converting format");
        }
        return convertYcbcrToRgb(patternBuffer, inputWidth, inputPitch,
                                 inputHeight,
                                 outputFormat == "rgb-interleaved");
    }
}

void ImageProcFuncTest::verifyOutput() {
    vector<uint8_t> verificationBuffer = getVerificationBuffer();
    if (verificationBuffer.size() != outputBufferSize) {
        throw runtime_error(
            "Verification file size does not match output buffer size (" +
            to_string(verificationBuffer.size()) +
            " != " + to_string(outputBufferSize) + ")");
    }

    auto [meanDiff, maxDiff] =
        compareBuffers(outputWidth, outputHeight, outputPitch, outputBuffer,
                       verificationBuffer.data(), outputFormat);

    EXPECT_LE(meanDiff, meanAbsDiffThreshold);
    EXPECT_LE(maxDiff, maxAbsDiffThreshold);

    if (debugPrints) {
        char s[50];
        sprintf(s, "diff mean: %.3f max: %i", meanDiff, maxDiff);
        gtestDebugPrint(s);
    }
}

void ImageProcFuncTest::gtestDebugPrint(string msg) {
    if (debugPrints) {
        gtestPrint(msg);
    }
}

static int writeBufferToTempFile(uint8_t* buffer, size_t bufferLen) {
    // Create a file.
    string tempFile = string("temp-") + string(__func__);
    int fd = memfd_create(tempFile.c_str(), MFD_CLOEXEC);
    if (fd == -1) {
        throw runtime_error("Could not create temp file in memory");
    }

    // Truncate the file to the buffer's length.
    if (ftruncate(fd, ((__off64_t) bufferLen))) {
        throw runtime_error("Error when calling ftruncate");
    }

    // Read from the buffer and write into the file.
    if (write(fd, buffer, bufferLen) < (ssize_t) bufferLen) {
        throw runtime_error("Could not write pattern to a temporary file");
    }

    // Return file offset to start of file.
    if (lseek(fd, 0, SEEK_SET) == -1) {
        close(fd);
        throw runtime_error("Failed lseek to beginning of file");
    }

    return fd;
}

static int newTempFileBuffer(uint8_t** buffer, size_t fileLen) {
    // Create a file.
    string tempFile = string("temp-") + string(__func__);
    int fd = memfd_create(tempFile.c_str(), MFD_CLOEXEC);
    if (fd == -1) {
        throw runtime_error("Could not create temp file in memory");
    }

    // Truncate the file to the buffer's length.
    if (ftruncate(fd, ((__off64_t) fileLen))) {
        throw runtime_error("Error when calling ftruncate");
    }

    // Memory map the file to the start of the buffer.
    uint8_t* buf =
        (uint8_t*) mmap(nullptr, fileLen, PROT_READ, MAP_PRIVATE, fd, 0);
    if (buf == MAP_FAILED) {
        throw runtime_error("Memory mapping temp file failed");
    }
    *buffer = buf;

    return fd;
}
