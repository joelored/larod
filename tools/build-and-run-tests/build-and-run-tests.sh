#!/bin/bash

# Run a few basic functional tests against debug chip.
#
# Requirements:
#
# - dbus
#
# Example usage:
#
#   dbus-run-session -- ./tools/build-and-run-tests/build-and-run-tests.sh

# Abort script if any command fails.
set -e

# Print commands.
set -x

# Build larod.
if [ "$1" ]; then
    git checkout "$1"
fi
meson -Duse_session_bus=true -Db_sanitize=address,undefined build-test1
ninja -C build-test1

# Let tests use this liblarod.so.
export LD_LIBRARY_PATH=$(realpath build-test1/lib)

# Start larod.
export ASAN_OPTIONS=fast_unwind_on_malloc=0
export DBUS_STARTER_ADDRESS=$DBUS_SESSION_BUS_ADDRESS
./build-test1/src/service/larod &

# If a second version is specified, build that version.
if [ "$2" ]; then
    git checkout "$2"
fi
meson -Duse_session_bus=true -Db_sanitize=address,undefined build-test2
ninja -C build-test2

# Run tests.
./build-test2/test/functional/connection/larod-conn-functional-test
./build-test2/test/functional/job/larod-job-functional-test
./build-test2/test/functional/chip/larod-chip-functional-test
./build-test2/test/functional/model/larod-model-functional-test
./build-test2/test/functional/tensor/larod-tensor-functional-test
./build-test2/test/functional/version/larod-version-functional-test
./build-test2/test/unit/larod-unit-test

# Stop larod.
kill %%
