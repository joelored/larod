# Data formats in larod
This file describes data format of model files in larod.

## Flatbuffer-based models
Some backends can accept binary data packaged with
[Flatbuffers](https://google.github.io/flatbuffers/) to define a model.
The [larod-converter](../tools/model-converter/README.md) tool can be used to
package a binary to be read by a larod backend.

## Parameters-only models
Some models do not need any binary data; instead a set of parameters is enough
for these models. This is the case for some of the preprocessing models, where
no file descriptor is passed in the model load call; instead the model is
constructed from a given `larodMap`. The parameters associated with
preprocessing backends can be found in the [preprocessing](doc/preprocessing.md)
document.
