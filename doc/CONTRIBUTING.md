Contributing
============

## Code style

The code style is based on the LLVM coding standard with some minor
modifications (e.g. indent width 4, regrouping of includes and
more) and with the following rules:

1. Variable names and functions are in camelCase, i.e. starting with lower case.
   See rule 10 for an exception to this.
2. Classes are in CamelCase, i.e. starting with upper case.
3. Constants are in upper cases.
4. Namespaces are in lower cases.
5. Inline comments starts with `//`. Comments regarding documentation of
   classes, functions and such (see rule 6) have the format:
   ```c
    /**
     * ...
     * ...
     */
   ```
6. Code documentation is written in [DoxyGen][2] format.
7. In a class declaration, public access specifier is declared first, then comes
   the protected and finally the private section. There are however some
   exceptions for nested classes and such; please see existing code for
   convention.
8. Within an access specifier in a class declaration, member functions are
   declared first then the member variables. The only exception is the private
   access section where variables come before private member
   functions. Moreover, static variables are declared first among the variables.
9. Braces should always be used for if-statements, for-loops and while-loops
   (even if it is only a single line in the body).
10. As implied by rule 1, function names must not contain underscores. However
   to distinguish different versions of the same function (needed for symbol
   versioning) function names should be suffixed by _vX, where X is a positive
   integer dictating what version of the function it is.
11. The __asm__ directives required for symbol versioning should be placed at
   the top of the file in which their corresponding functions are defined, as
   typically one would do with static function declarations.
12. [Experimental](#experimental) features should be marked clearly as such in
   a way similar to the following:
   ```c
   /// WARNING: This is an experimental function which is subject to change.
   void someExperimentalFunction();
   ```
   Note that it should be in Doxygen formatting when applicable.

Please see the code for some examples of the coding standard.

### Style checker

The tool [clang-format][1] is used as style checker and for full details please
see the file `.clang-format` in the root directory. If you want to
check the code style on your latest commit, run the following command in the
root directory (where the folder `.git` and file `.clang-format` resides):

```
git diff -U0 --no-color HEAD^ | clang-format-diff -p 1 -iregex ".*\.(c|cc|h|hh)"
```

Note that you may have to specify the installed version, for example
`clang-format-diff-7.0`.

#### Editor plugins

There are various editor plugins for clang-format to help you reformat your code
correctly according to the code style. For Vim and Emacs, please see this
[guide][1].

## Testing

All test are found in the `test` directory.

### Policy

The intention is to have both unit tests and functional tests for
functionality pertaining to both liblarod and the larod service, but without
making mocks that are hard to maintain, and to have as little redundancy in our
testing as possible, that is, not have tests overlapping in scope.

#### Library

A function in `lib` should be unit tested if the following is true:

* it's not declared in larod.h,
* it's not making any calls to sd-bus.

Otherwise it should be covered by functional tests.

#### Service

A class in `src/service` should be unit tested if it does not depend on external
libraries (such as sdbusplus or Tensorflow Lite). Otherwise it should be covered
by functional tests.

### Running unit tests

Simply run:

```
    ninja test
```

in your build catalog, or call the built binaries directly from where they
reside in the build tree (path: `build/test/unit`).

## Experimental

See `BUILD.md` for a description of what experimental entails.

### Implementing experimental features

Doing this requires one to clearly mark the feature as experimental. See point
12. of [Code style](#code-style).

### Using experimental features

As a developer working on larod one needs to take careful note about whether
an interface one is using is experimental seeing as it is as such subject to
change.

[1]: https://clang.llvm.org/docs/ClangFormat.html
[2]: http://www.doxygen.nl/
