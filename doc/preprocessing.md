Preprocessing
=============

- [What is preprocessing?](#what-is-preprocessing)
- [Preprocessing job configuration](#preprocessing-job-configuration)
  - [Common operations](#common-operations)
  - [Common configuration parameters](#common-configuration-parameters)
- [Supported backends](#supported-backends)
  - [libyuv backend](#libyuv-backend)
  - [vproc backend](#vproc-backend)
  - [Supported operations](#supported-operations)
- [Supported formats](#supported-formats)
  - [Operations requiring a color format conversion](#operations-requiring-a-color-format-conversion)
  - [Operations *not* requiring a color format conversion](#operations-not-requiring-a-color-format-conversion)

## What is preprocessing?

Preprocessing in larod can be used to process input data so that it has the
format, size and shape a neural network expects. For optimal performance the
processing operations can be offloaded to specialized preprocessing hardware.
Each supported preprocessing hardware accelerator is exposed to applications
through a backend (`larodChip`) in larod.

Currently only image processing operations are supported.

## Preprocessing job configuration

Preprocessing jobs are configured with key-value parameters in a `larodMap`.
The configuration describes the data that you have and how you want the data to
be. The selected backend will crop, scale and convert according to what the
description requires.

Below is an example job configuration. It describes a job that takes a 1280x720
input image in NV12 format, crops out 200x200 from the center of the image (X
offset 540 and Y offset 260), converts it to the RGB interleaved format and
scales it down to 48x48. In this case the [libyuv backend](#libyuv-backend) will
be used to perform these operations. In the interest of brevity error handling
has been omitted.

```c
larodMap* modelParams = larodCreateMap(NULL);
larodMapSetStr(modelParams, "image.input.format", "nv12", NULL);
larodMapSetIntArr2(modelParams, "image.input.size", 1280, 720, NULL);
larodMapSetStr(modelParams, "image.output.format", "rgb-interleaved", NULL);
larodMapSetIntArr2(modelParams, "image.output.size", 48, 48, NULL);

// Our modelParams larodMap replaces the model fd as a model description.
larodModel *model = larodLoadModel(conn, -1, LAROD_CHIP_LIBYUV,
                                   LAROD_ACCESS_PRIVATE, "", modelParams, NULL);

larodMap* jobParams = larodCreateMap(NULL);
// We can change the value of "image.input.crop" in our map between running jobs
// on the same model if we wish.
larodMapSetIntArr4(jobParams, "image.input.crop", 540, 260, 200, 200, NULL);

larodJobRequest* jobReq = larodCreateJobRequest(...);
larodSetJobRequestParams(jobReq, jobParams, NULL);
larodRunJob(conn, jobReq, NULL);
```

Note that should one be interested in just scaling the original image (from
1280x720 to 48x48) without cropping it first, one could simply neglect to
provide a `larodMap` altogether in the `larodJobRequest` used.

### Common operations

Image preprocessing backends may support the following common image processing
operations. Backends are not required to support all operations and image
formats.

| Operation     | Description                                 |
| ------------- | ------------------------------------------- |
| Image crop    | Crop out a part of an image.                |
| Image scale   | Scale up or down an image.                  |
| Image convert | Convert an image between two color formats. |

### Common configuration parameters

Image preprocessing backends may support the common image processing parameters
in the tables below to describe processing jobs. Backends are not required to
support all parameters and values.

#### Load model parameters

The following are parameters that can be set on a `larodMap` provided when
loading a model using e.g. `larodLoadModel`.

| Key                    | Value                                                                                                 |
| ---------------------- | ----------------------------------------------------------------------------------------------------- |
| image.input.format\*   | String, describing input image format.                                                                |
| image.input.size\*     | 2-integer-tuple, describing input image width and height.                                             |
| image.input.row-pitch  | Integer, describing input image width including padding, in bytes. Inferred if not explicilty given.  |
| image.output.format\*  | String, describing output image format.                                                               |
| image.output.size\*    | 2-integer-tuple, describing output image width and height.                                            |
| image.output.row-pitch | Integer, describing output image width including padding, in bytes. Inferred if not explicitly given. |

\*: This parameter is mandatory for all preprocessing backends outlined in this
document.

#### Job request parameters

The following are parameters that can be set on a `larodMap` of a
`larodJobRequest`. The map can be attached to a job request upon its creation
(`larodCreateJobRequest`) or later using `larodSetJobRequestParams`. The
parameters of the map will then be used in a subsequent call to e.g.
`larodRunJob` using this job request.

Since these parameters are not attached to a model it's possible to send job
requests having larodMaps with different values for these parameters to the
same model.

| Key              | Value                                                                                                                                                                |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| image.input.crop | 4-integer-tuple, describing the crop window. The elements in the tuple are: X offset in input image, Y offset in input image, crop window width, crop window height. |

## Supported backends

Currently the following image preprocessing backends are supported by larod.

### libyuv backend

The libyuv backend uses the open source library
[libyuv](https://chromium.googlesource.com/libyuv/libyuv/). It runs on most
CPUs and in particular uses the SIMD technology Neon on Arm architectures to
accelerate parallelizable computation. It supports image crop, scale and format
conversion.

This backend is represented by the `larodChip` `LAROD_CHIP_LIBYUV` in `larod.h`.

#### libyuv backend constraints

* The width, height and row pitch, for both the input and the output image, must
  be a multiple of 2.

### vproc backend

The vproc backend uses VPROC in Ambarella CV series chips. It supports image
crop, scale and format conversion.

This backend is represented by the `larodChip` `LAROD_CHIP_CVFLOW_PROC` in
`larod.h`.

#### vproc backend constraints

* The input image width must be a multiple of 2 when input format is nv12.
* The input image height must be a multiple of 2 when input format is nv12.
* The input image row pitch must be a multiple of 32.
* The output image width must be a multiple of 2 when output format is nv12.
* The output image height must be a multiple of 2 when output format is nv12.
* The output image row pitch must be a multiple of 32.
* For operations requiring both color format conversion and scaling the scale
factor must be at most 4.
* For operations *not* requiring color format conversion the scale factor must
be at most 256.

### Supported operations

The following table describes supported operations for each backend.

| Backend | crop | convert | scale |
| ------- | ---- | ------- | ----- |
| libyuv  | Yes  | Yes     | Yes   |
| vproc   | Yes  | Yes     | Yes   |

## Supported formats

### Operations requiring a color format conversion

The following table describes supported ***input*** formats for operations
requiring a color format conversion.

| Backend | nv12 | rgb-interleaved | rgb-planar |
| ------- | ---- | --------------- | ---------- |
| libyuv  | Yes  | Yes             |            |
| vproc   | Yes  |                 |            |

The following table describes supported ***output*** formats for operations
requiring a color format conversion.

| Backend | nv12 | rgb-interleaved | rgb-planar |
| ------- | ---- | --------------- | ---------- |
| libyuv  | Yes  | Yes             |            |
| vproc   |      |                 | Yes        |

### Operations *not* requiring a color format conversion

The following table describes supported image formats for operations *not*
requiring a color format conversion, i.e. the input and output formats are
identical. This could be e.g. a pure scaling operation.

| Backend | nv12 | rgb-interleaved | rgb-planar |
| ------- | ---- | --------------- | ---------- |
| libyuv  | Yes  | Yes             |            |
| vproc   | Yes  |                 | Yes        |
